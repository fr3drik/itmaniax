﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.DataAccess
{
    public interface IDbManager : IDisposable
    {
        IDbConnection Connection { get; }
        void Dispose();
    }
}
