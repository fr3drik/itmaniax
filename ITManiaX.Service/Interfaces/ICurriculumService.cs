﻿using ITManiaX.Models.Curriculum;
using ITManiaX.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Service.Interfaces
{
    public interface ICurriculumService
    {
        ICurriculumRepository CurriculumRepository { get; }
        void SaveCurriculaData(string file, int curriculumFieldPosition, char curriculumDelimiter);
        void GetCurricula(int hierarchyMinLevel, int hierarchyMaxLevel);
        void GetCurricula();
        IList<Curriculum> Curricula { get; }
    }
}
