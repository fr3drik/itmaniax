﻿using ITManiaX.Models.Applications.DTO;
using ITManiaX.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Service.Interfaces
{
    public interface IDomainObjectService
    {
        void GetSystemApplication(string systemApplicationUrl);
        ApplicationModel ApplicationModel { get; }
        IDomainObjectRepository DomainObjectRepository { get; }
    }
}
