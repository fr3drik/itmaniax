﻿using ITManiaX.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Service.Interfaces
{
    public interface ICallToActionService
    {
        ICallToActionRepository CallToActionRepository { get; }
        void FollowUpCallToAction(int applicationId, int callToActionId, string userName);
    }
}
