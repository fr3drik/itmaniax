﻿using ITManiaX.Models.Curriculum;
using ITManiaX.Repository.Interfaces;
using ITManiaX.Service.Interfaces;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Transactions;

namespace ITManiaX.Service.Concrete
{
    public class CurriculumService : ICurriculumService
    {
        private ICurriculumRepository _curriculumRepository;
        private IList<Course> _coursesToInsert;
        private IList<Curriculum> _curriculaToInsert;
        private Dictionary<string, List<string>> _data;
        private IList<Curriculum> _curricula;
        public ICurriculumRepository CurriculumRepository
        {
            get { return _curriculumRepository; }
        }
        public IList<Curriculum> Curricula 
        {
            get { return _curricula; }
        }
        public CurriculumService()
        {

        }
        public CurriculumService(ICurriculumRepository curriculumRepository)
        {
            _curriculumRepository = curriculumRepository;            
        }
        public void SaveCurriculaData(string file, int curriculumFieldPosition, char curriculumDelimiter)
        {
            _coursesToInsert = new List<Course>();
            _curriculaToInsert = new List<Curriculum>();
            _data = new Dictionary<string, List<string>>();
            Console.WriteLine("Getting courses...");
            _curriculumRepository.GetCourses();
            Console.WriteLine("Courses loaded.");
            Console.WriteLine("Getting curricula..");
            _curriculumRepository.GetCurricula();
            Console.WriteLine("Curricula loaded.");
            Console.WriteLine("Parsing curricula data...");
            ParseCurriculaData(file, curriculumFieldPosition, curriculumDelimiter);
            Console.WriteLine("Curricula data parsed.");
            Console.WriteLine("Processing curricula...");
            //ProcessCurricula();
            Console.WriteLine("Curricula processed.");
            Console.WriteLine("Saving curricula...");
            //SaveCurricula();
            Console.WriteLine("Curricula saved.");
            Console.WriteLine("Saving courses...");
            SaveCourses();
            Console.WriteLine("Courses saved.");
        }
        public void GetCurricula(int hierarchyMinLevel, int hierarchyMaxLevel)
        {
            _curricula = _curriculumRepository.DbManager.Connection.Query<Curriculum>("GetCurricula", commandType: CommandType.StoredProcedure).ToList();
        }
        public void GetCurricula()
        {
            _curricula = _curriculumRepository.DbManager.Connection.Query<Curriculum>("GetCurricula", commandType: CommandType.StoredProcedure).ToList();
        }
        private void ParseCurriculaData(string file, int curriculumFieldPosition, char curriculumDelimiter)
        {
            int courseCounter = 1;
            using (TextFieldParser parser = new TextFieldParser(ConfigurationManager.AppSettings["CurriculaFolder"] + file))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    if (courseCounter > 1)
                    {
                        Course course = new Course();
                        string[] fields = parser.ReadFields();
                        course.Asset_Title = fields[0];
                        course.Asset_ID = fields[1];
                        course.Asset_Category = fields[2];
                        course.Asset_Installation_Status = fields[3];
                        course.Custom_Content = fields[4];
                        course.Catalog_Folder = fields[5];
                        course.Catalog_Path = fields[6];
                        course.Asset_Description = fields[7];
                        var expectedDuration = 0;
                        int.TryParse(fields[8], out expectedDuration);
                        course.Expected_Duration = expectedDuration;
                        course.Target_Audience = fields[9];
                        course.Prerequisites = fields[10];
                        course.Objectives = fields[11];

                        if(_curriculumRepository.Courses.Where(c => c.Asset_ID == course.Asset_ID && c.Catalog_Path == course.Catalog_Path).Count() == 0)
                        {
                            if(_coursesToInsert.Where(c => c.Asset_ID == course.Asset_ID && c.Catalog_Path == course.Catalog_Path).Count() == 0)
                            {
                                _coursesToInsert.Add(course);
                            }
                        }
                        if (fields[curriculumFieldPosition].Contains(curriculumDelimiter))
                        {
                            var dataElement = fields[curriculumFieldPosition].TrimStart('"').TrimEnd('"').Trim();
                            var tuple = dataElement.Split(curriculumDelimiter).Where(s => s.Length > 0).ToList();

                            if (!_data.ContainsKey(dataElement))
                                _data.Add(dataElement, tuple);


                        }
                    }
                    courseCounter++;
                }
            }
        }
        private void SaveCourses()
        {
            IDbTransaction transaction = _curriculumRepository.DbManager.Connection.BeginTransaction();
            try
            {
                foreach (var c in _coursesToInsert)
                {
                    _curriculumRepository.DbManager.Connection.Execute("SaveCourse",
                        new { 
                            Asset_Category = c.Asset_Category,
                            Asset_Description = c.Asset_Description,
                            Asset_ID = c.Asset_ID,
                            Asset_Installation_Status = c.Asset_Installation_Status,
                            Asset_Title = c.Asset_Title,
                            Catalog_Folder = c.Catalog_Folder,
                            Catalog_Path = c.Catalog_Path,
                            Custom_Content = c.Custom_Content,
                            Expected_Duration = c.Expected_Duration,
                            Objectives = c.Objectives,
                            Prerequisites = c.Prerequisites,
                            Target_Audience = c.Target_Audience
                        },
                        commandType: CommandType.StoredProcedure,
                        transaction: transaction);
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                Console.WriteLine(ex.Message);
            }
            _curriculumRepository.DbManager.Connection.Close();
        }
    }
}
