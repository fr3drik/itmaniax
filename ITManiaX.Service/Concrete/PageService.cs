﻿using ITManiaX.Repository.Interfaces;
using ITManiaX.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Service.Concrete
{
    public class PageService : IPageService
    {
        private IPageRepository _pageRepository;
        public IPageRepository PageRepository { get { return _pageRepository; } }
        public PageService()
        {

        }
        public PageService(IPageRepository pageRepository)
        {
            _pageRepository = pageRepository;
        }
    }
}
