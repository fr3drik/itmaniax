﻿using ITManiaX.Models.Applications.DTO;
using ITManiaX.Repository.Interfaces;
using ITManiaX.Service.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Service.Concrete
{
    public class DomainObjectService : IDomainObjectService
    {
        private ApplicationModel applicationModel;
        private IDomainObjectRepository _domainObjectRepository;
        public IDomainObjectRepository DomainObjectRepository
        {
            get { return _domainObjectRepository; }
        }
        public DomainObjectService()
        {

        }
        public DomainObjectService(IDomainObjectRepository domainObjectRepository)
        {
            _domainObjectRepository = domainObjectRepository;
        }
        public void GetSystemApplication(string systemApplicationUrl)
        {
            applicationModel = new Models.Applications.DTO.ApplicationModel();
            applicationModel.ApplicationMenus = new List<Models.Applications.DTO.ApplicationMenu>();
            applicationModel.SkillPortCurricula = new List<Models.Applications.DTO.SkillPortCurriculum>();
            _domainObjectRepository.GetDomainObjectByDomainObjectFieldValue("System Application URL", "itmaniax.co.za");
            _domainObjectRepository.GetDomainObjectsByDomainObjectType("Catalogue Import");
            applicationModel.GoogleAnalyticsTrackingCode = _domainObjectRepository.DomainObjectFieldsDTO.Where(f => f.FieldName == "Google Analytics Tracking Code").First().FieldValue;
            applicationModel.PageTitle = _domainObjectRepository.DomainObjectFieldsDTO.Where(f => f.FieldName == "Page Title").First().FieldValue;
            applicationModel.ContactEmailAddress = _domainObjectRepository.DomainObjectFieldsDTO.Where(f => f.FieldName == "System Email Address").First().FieldValue;
            applicationModel.SystemApplicationPromotions = JsonConvert.DeserializeObject<List<SystemApplicationPromotion>>(_domainObjectRepository.DomainObjectFieldsDTO.Where(f => f.FieldName == "System Application Promotions Json").First().FieldValue);
            var qm = _domainObjectRepository.ChildDomainObjectFieldsDTO.Where(tg => tg.DomainObjectTypeName == "System Application Menu").GroupBy(ta => ta.DomainObjectId).Select(tf => new { DomainObjectId = tf.Key, DomainObjectFields = tf });
            foreach(var q in qm)
            {
                var am = new Models.Applications.DTO.ApplicationMenu();
                am.AlternateUrl = q.DomainObjectFields.Where(f => f.FieldName == "Alternate Url").First().FieldValue;
                am.MenuItemTitle = q.DomainObjectFields.Where(f => f.FieldName == "Menu Item Title").First().FieldValue;
                am.PagePosition = q.DomainObjectFields.Where(f => f.FieldName == "Page Position").First().FieldValue;
                int menuItemOrder = 0;
                int.TryParse(q.DomainObjectFields.Where(f => f.FieldName == "Menu Item Order").First().FieldValue, out menuItemOrder);
                am.MenuItemOrder = menuItemOrder;
                am.DomainObjectId = q.DomainObjectId;
                applicationModel.ApplicationMenus.Add(am);
            }
            applicationModel.SkillPortCurricula = JsonConvert.DeserializeObject<List<SkillPortCurriculum>>(_domainObjectRepository.RelatedDomainObjectFieldsDTO.OrderByDescending(a => a.DateCreated).Where(t => t.DomainObjectTypeName == "Catalogue Import" && t.FieldName == "JsonObject").First().FieldValue);
        }
        public ApplicationModel ApplicationModel
        {
            get { return applicationModel; }
        }
    }
}
