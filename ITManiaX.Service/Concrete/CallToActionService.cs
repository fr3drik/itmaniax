﻿using ITManiaX.Repository.Interfaces;
using ITManiaX.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Service.Concrete
{
    public class CallToActionService : ICallToActionService
    {
        public ICallToActionRepository CallToActionRepository { get { return _callToActionRepository; } }
        public IUserRepository UserRepository { get { return _userRepository; } }
        private IUserRepository _userRepository;
        private ICallToActionRepository _callToActionRepository;
        public CallToActionService()
        {

        }
        public CallToActionService(ICallToActionRepository callToActionRepository, IUserRepository userRepository)
        {
            _callToActionRepository = callToActionRepository;
            _userRepository = userRepository;
        }
        public void FollowUpCallToAction(int applicationId, int callToActionId, string userName)
        {
            _userRepository.GetUserByUserName(userName);
            _callToActionRepository.GetCallToAction(callToActionId);
            _callToActionRepository.CallToAction.FollowedUp = true;
            _callToActionRepository.CallToAction.FollowUpUser = _userRepository.User.Id;
            _callToActionRepository.UpdateCallToAction();
        }
    }
}
