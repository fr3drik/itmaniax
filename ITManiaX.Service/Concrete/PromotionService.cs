﻿using ITManiaX.Repository.Interfaces;
using ITManiaX.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Service.Concrete
{
    public class PromotionService : IPromotionService
    {
        private IPromotionRepository _promotionRepository;
        public PromotionService()
        {

        }
        public PromotionService(IPromotionRepository promotionRepository)
        {
            _promotionRepository = promotionRepository;
        }
        public IPromotionRepository PromotionRepository { get { return _promotionRepository; } }
    }
}
