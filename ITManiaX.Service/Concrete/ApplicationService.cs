﻿using ITManiaX.Repository.Interfaces;
using ITManiaX.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Service.Concrete
{
    public class ApplicationService : IApplicationService
    {
        public IApplicationRepository ApplicationRepository { get { return _applicationRepository; } }
        private IApplicationRepository _applicationRepository;
        public ApplicationService()
        {

        }
        public ApplicationService(IApplicationRepository applicationRepository)
        {
            _applicationRepository = applicationRepository;
        }
    }
}
