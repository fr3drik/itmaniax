﻿using ITManiaX.Models;
using ITManiaX.Models.Applications;
using ITManiaX.Models.CallsToAction;
using ITManiaX.Models.Curriculum;
using ITManiaX.Models.Promotions;
using ITManiaX.Service.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;

namespace ITManiaX.WebUI.Models
{
    public class ContentModel
    {
        ICurriculumService _curriculumService;
        IApplicationService _applicationService;
        IPromotionService _promotionService;
        ICallToActionService _callToActionService;
        IPageService _pageService;
        ApplicationUserManager _userManager;
        bool _callToActionValid;
        public ContentModel()
        {

        }
        public ContentModel(ICurriculumService curriculumService, IApplicationService applicationService, IPromotionService promotionService, ICallToActionService callToActionService, IPageService pageService)
        {
            _curriculumService = curriculumService;
            _applicationService = applicationService;
            _promotionService = promotionService;
            _callToActionService = callToActionService;
            _pageService = pageService;
            GetApplication();
        }
        public ContentModel(ICurriculumService curriculumService, IApplicationService applicationService, IPromotionService promotionService, IPageService pageService, ApplicationUserManager userManager)
        {
            _curriculumService = curriculumService;
            _applicationService = applicationService;
            _promotionService = promotionService;
            _pageService = pageService;
            _userManager = userManager;
            GetApplication();
        }
        private void GetApplication()
        {
            _applicationService.ApplicationRepository.GetApplicationByPropertyValue("System Application URL", ConfigurationManager.AppSettings["SystemApplicationUrl"]);
            application = _applicationService.ApplicationRepository.Application;
            applicationProperties = _applicationService.ApplicationRepository.ApplicationProperties;
            _pageService.PageRepository.GetPagesByApplicationId(application.ApplicationId);
            _pages = _pageService.PageRepository.Pages.Where(p => p.DisplayOrder.HasValue).ToList();
            _privacyPolicy = _pageService.PageRepository.Pages.Where(p => p.PageTitle == "Privacy Policy").FirstOrDefault();
            _termsAndConditions = _pageService.PageRepository.Pages.Where(p => p.PageTitle == "Terms and Conditions").FirstOrDefault();
        }
        public void GetApplicationViewModel()
        {
            _promotionService.PromotionRepository.GetPromotions();
            _callToActionService.CallToActionRepository.GetCallToActionTemplatesByApplication(application.ApplicationId);
            
            promotions = _promotionService.PromotionRepository.Promotions;
            callToActionTemplates = _callToActionService.CallToActionRepository.CallToActionTemplates;
            _callToActionTemplateFields = _callToActionService.CallToActionRepository.CallToActionTemplateFields;

            _curriculumService.GetCurricula();
            var curricula = _curriculumService.Curricula.Where(c => c.IsActive).ToList();
            _frontPageCurricula = curricula.Where(ck => ck.IsFrontPage).OrderBy(cg => cg.CurriculumName).ToList();
            vendors = curricula.Where(c => c.IsVendor && c.IsActive).ToList();
            foreach(var c in _frontPageCurricula)
            {
                c.Curricula = curricula.Where(cc => cc.ParentCurriculumPath == c.CurriculumPath).OrderBy(cc => cc.CurriculumName).ToList();
            }
        }
        public void GetCurricula(string curriculumPath)
        {
            _curriculumService.GetCurricula();
            curricula = _curriculumService.Curricula.Where(t => t.ParentCurriculumPath == curriculumPath).ToList();
        }
        public void GetCurriculum(int curriculumId)
        {
            _callToActionService.CallToActionRepository.GetCallToActionTemplatesByApplication(application.ApplicationId);
            callToActionTemplates = _callToActionService.CallToActionRepository.CallToActionTemplates;
            _callToActionTemplateFields = _callToActionService.CallToActionRepository.CallToActionTemplateFields;

            _curriculumService.GetCurricula();
            _curriculum = _curriculumService.Curricula.Where(c => c.CurriculumId == curriculumId).FirstOrDefault();
            _curriculum.Curricula = _curriculumService.Curricula.Where(c => c.ParentCurriculumPath == _curriculum.CurriculumPath).OrderBy(c => c.CurriculumName).ToList();
            
            if (_curriculumService.Curricula.Where(c => c.CurriculumPath == _curriculum.ParentCurriculumPath).Any())
                _curriculum.ParentCurriculumId = _curriculumService.Curricula.Where(c => c.CurriculumPath == _curriculum.ParentCurriculumPath).First().CurriculumId;

            foreach (var cc in _curriculum.Curricula)
            {
                if (_curriculumService.Curricula.Where(cf => cf.ParentCurriculumPath == cc.CurriculumPath).Count() == 0)
                {
                    _curriculumService.CurriculumRepository.GetCurriculumCourses(cc.CurriculumId);
                    cc.Courses = _curriculumService.CurriculumRepository.Courses.OrderBy(cv => cv.Asset_Category).ThenBy(cf => cf.Asset_ID).ToList();
                    cc.TotalExpectedDuration = cc.Courses.Select(hg => hg.Expected_Duration).Sum() / 60;
                }
            }
            if(_curriculum.Curricula.Count == 0)
            {
                _curriculumService.CurriculumRepository.GetCurriculumCourses(_curriculum.CurriculumId);
                _curriculum.Courses = _curriculumService.CurriculumRepository.Courses.OrderBy(cv => cv.Asset_Category).ThenBy(cf => cf.Asset_ID).ToList();
                _curriculum.TotalExpectedDuration = _curriculum.Courses.Select(hg => hg.Expected_Duration).Sum() / 60;
            }
            _curriculum.BreadCrumb = new List<Curriculum>();
            foreach(var cck in _curriculumService.Curricula)
            {
                if(_curriculum.CurriculumPath.TrimStart('[').TrimEnd(']').Contains(cck.CurriculumPath.TrimStart('[').TrimEnd(']')))
                {
                    _curriculum.BreadCrumb.Add(new Curriculum {
                        CurriculumName = cck.CurriculumName,
                        CurriculumId = cck.CurriculumId
                    });
                }
            }

        }
        public void UpdateCurriculum(Curriculum curriculum, List<Curriculum> breadCrumb)
        {
            _curriculumService.CurriculumRepository.Curriculum = curriculum;
                        
            _curriculumService.CurriculumRepository.Curriculum = curriculum;
            if(curriculum.Price.HasValue)
            {
                    curriculum.VAT = curriculum.Price.Value * 0.14m;
                    curriculum.ExclVAT = curriculum.Price.Value / 1.14m;
            }
            _curriculumService.CurriculumRepository.UpdateCurriculum();
            GetCurriculum(curriculum.CurriculumId);
        }
        public void BuildCurricula()
        {
            /*
            var curriculaFiltered = new List<Curriculum>();
            foreach(var c in parentCurricula)
            {
                c.Curricula = curricula.Where(r => r.ParentCurriculumLevel == c.CurriculumLevel && r.ParentCurriculumName == c.CurriculumName).ToList();
                foreach(var ck in c.Curricula)
                {
                    ck.Curricula = curricula.Where(r => r.ParentCurriculumLevel == ck.CurriculumLevel && r.ParentCurriculumName == ck.CurriculumName).ToList();
                }
                if(c.Curricula == null)
                {
                    c.Courses = _courses;
                }             
                else if(c.Curricula.Count == 0)
                {
                    c.Courses = _courses;
                }
                curriculaFiltered.Add(c);
            }
            curricula = curriculaFiltered;
            //foreach(var c in curricula.ToList())
            //{
            //    if (c.Curricula == null)
            //    {
            //        curricula.Remove(c);
            //    }
            //}
             */
        }
        public void ValidateCallToAction(List<CallToActionFieldDTO> callToActionFields, int? pageId, int? curriculumId)
        {
            var callToActionTemplateId = callToActionFields.First().CallToActionTemplateId;
            _callToActionService.CallToActionRepository.GetCallToActionTemplate(callToActionTemplateId);
            _callToActionTemplate = _callToActionService.CallToActionRepository.CallToActionTemplate;
            _callToActionTemplateFields = _callToActionService.CallToActionRepository.CallToActionTemplateFields;
            _callToActionTemplateMessageSequences = _callToActionService.CallToActionRepository.CallToActionTemplateMessageSequences;
            foreach(var f in callToActionFields)
            {
                f.FieldName = f.FieldName.Replace('-', ' ');
                if(_callToActionTemplateFields.Where(k => k.CallToActionFieldName == f.FieldName).Count() > 0)
                {
                    var ctf = _callToActionTemplateFields.Where(k => k.CallToActionFieldName == f.FieldName).First();
                    if(ctf.CallToActionFieldRequired && string.IsNullOrEmpty(f.FieldValue))
                    {
                        f.Valid = false;
                        f.ValidationMessage = f.FieldName + " cannot be left blank";
                    }
                    else if(!string.IsNullOrEmpty(ctf.ValidationRegex))
                    {
                        Regex r = new Regex(ctf.ValidationRegex);
                        if (!r.IsMatch(f.FieldValue))
                        {
                            f.Valid = false;
                            f.ValidationMessage = ctf.ValidationErrorMessage;
                        }
                        else
                        {
                            f.Valid = true;
                        }
                    }
                    else
                    {
                        f.Valid = true;
                    }
                }
                f.FieldName = f.FieldName.Replace(' ', '-');
            }
            if(callToActionFields.Where(fg => !fg.Valid).Count() > 0)
            {
                _callToActionValid = false;
            }
            else
            {
                _callToActionValid = true;
            }
            if(_callToActionValid)
            {
                SaveCallToAction(callToActionFields, pageId, curriculumId);
            }
        }
        public void SaveCallToAction(List<CallToActionFieldDTO> callToActionFields, int? pageId, int? curriculumId)
        {
            _callToAction = new CallToAction();
            _callToAction.DateCreated = DateTime.Now;
            _callToAction.DateUpdated = DateTime.Now;
            _callToAction.ApplicationId = application.ApplicationId;
            _callToAction.CallToActionName = _callToActionTemplate.CallToActionName;
            _callToActionService.CallToActionRepository.CallToAction = _callToAction;
            _callToActionService.CallToActionRepository.CallToAction.PageId = pageId;
            _callToActionService.CallToActionRepository.CallToAction.CurriculumId = curriculumId;
            _callToActionService.CallToActionRepository.SaveCallToAction();
            _callToActionFields = new List<CallToActionField>();
            foreach(var f in callToActionFields)
            {
                _callToActionFields.Add(new CallToActionField {
                    CallToActionId = _callToAction.Id,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    FieldName = f.FieldName.Replace('-',' '),
                    FieldValue = f.FieldValue
                });
            }
            _callToActionService.CallToActionRepository.CallToActionFields = _callToActionFields;
            _callToActionService.CallToActionRepository.SaveCallToActionFields();
            _callToActionMessageSequences = new List<CallToActionMessageSequence>();
            int messageCounter = 0;
            foreach(var message in _callToActionTemplateMessageSequences)
            {
                foreach(var f in callToActionFields)
                {
                    if(message.CallToActionMessageContent.Contains(f.FieldName))
                    {
                        message.CallToActionMessageContent = message.CallToActionMessageContent.Replace("[" + f.FieldName + "]", f.FieldValue);
                    }
                    else if(message.CallToActionMessageContent.Contains("[Url]"))
                    {
                        message.CallToActionMessageContent = message.CallToActionMessageContent.Replace("[Url]", "http://" + applicationProperties.Where(p => p.ApplicationPropertyName == "System Application URL").First().ApplicationPropertyValue + "/Home/Enquiry/" + _callToAction.Id);
                    }
                }
                _callToActionMessageSequences.Add(new CallToActionMessageSequence
                {
                    CallToActionId = _callToAction.Id,
                    CallToActionMessageContent = message.CallToActionMessageContent,
                    CallToActionMessageOrder = message.CallToActionMessageOrder,
                    CallToActionMessageTitle = message.CallToActionMessageTitle,
                    CallToActionMessageType = message.CallToActionMessageType,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    MessageTriggered = false
                });
                messageCounter++;
            }
            _callToActionService.CallToActionRepository.CallToActionMessageSequences = _callToActionMessageSequences;
            _callToActionService.CallToActionRepository.SaveCallToActionMessageSequences();
        }
        public void GetCallToAction(int callToActionId)
        {
            _callToActionService.CallToActionRepository.GetCallToAction(callToActionId);
            _callToAction = _callToActionService.CallToActionRepository.CallToAction;
            _callToActionFields = _callToActionService.CallToActionRepository.CallToActionFields;
            _callToActionMessageSequences = _callToActionService.CallToActionRepository.CallToActionMessageSequences.OrderBy(m => m.CallToActionMessageOrder).ToList();
            var nonTriggered = _callToActionMessageSequences.Where(m => !m.MessageTriggered).ToList();

            if (nonTriggered.Any())
            {
                _currentMessageSequence = nonTriggered.First();
                if (_currentMessageSequence.CallToActionMessageType == "Email")
                {
                    var email = _callToActionFields.Where(n => n.FieldName == "Email Address").First().FieldValue;
                    var recipient = _callToActionFields.Where(n => n.FieldName == "Name").First().FieldValue + " " + _callToActionFields.Where(n => n.FieldName == "Surname").First().FieldValue;
                    if (_currentMessageSequence.CallToActionMessageContent.Contains("[url]"))
                    {
                        _currentMessageSequence.CallToActionMessageContent = _currentMessageSequence.CallToActionMessageContent.Replace("[url]", "http://" + applicationProperties.Where(p => p.ApplicationPropertyName == "").First().ApplicationPropertyValue + "/Home/Enquiry/" + _callToAction.Id);
                    }
                    SendEmailMessage(email, recipient, _currentMessageSequence.CallToActionMessageTitle, _currentMessageSequence.CallToActionMessageContent);
                    _callToActionService.CallToActionRepository.UpdateCallToActionMessageSequenceTriggered(_currentMessageSequence.Id, true);
                }
                else
                {
                    _callToActionService.CallToActionRepository.UpdateCallToActionMessageSequenceTriggered(_currentMessageSequence.Id, true);
                }
                if(nonTriggered.Count > 1)
                {
                    var nextMessage = nonTriggered[1];
                    if (nextMessage.CallToActionMessageType == "Email")
                    {
                        var email = _callToActionFields.Where(n => n.FieldName == "Email Address").First().FieldValue;
                        var recipient = _callToActionFields.Where(n => n.FieldName == "Name").First().FieldValue + " " + _callToActionFields.Where(n => n.FieldName == "Surname").First().FieldValue;
                        if (nextMessage.CallToActionMessageContent.Contains("[url]"))
                        {
                            nextMessage.CallToActionMessageContent = _currentMessageSequence.CallToActionMessageContent.Replace("[url]", "http://" + applicationProperties.Where(p => p.ApplicationPropertyName == "").First().ApplicationPropertyValue + "/Home/Enquiry/" + _callToAction.Id);
                        }
                        SendEmailMessage(email, recipient, nextMessage.CallToActionMessageTitle, nextMessage.CallToActionMessageContent);
                        _callToActionService.CallToActionRepository.UpdateCallToActionMessageSequenceTriggered(nextMessage.Id, true);
                    }
                }
            }
        }
        private void SendEmailMessage(string email, string recipient, string subject, string body)
        {
            SmtpClient mySmtpClient = new SmtpClient();

            // add from,to mailaddresses
            MailAddress from = new MailAddress("enquiries@itmaniax.co.za", "ITManiaX");
            MailAddress to = new MailAddress(email, recipient);
            MailMessage objMail = new System.Net.Mail.MailMessage(from, to);

            // set subject and encoding
            objMail.Subject = subject;
            objMail.SubjectEncoding = System.Text.Encoding.UTF8;

            // set body-message and encoding
            objMail.Body = body;
            objMail.BodyEncoding = System.Text.Encoding.UTF8;
            // text or html
            objMail.IsBodyHtml = true;

            mySmtpClient.Send(objMail);
        }
        public void GetCallsToAction(int size, int page)
        {
            _callToActionService.CallToActionRepository.GetCallsToAction(application.ApplicationId, size, page);
            _callsToAction = _callToActionService.CallToActionRepository.CallsToAction;
        }
        public void FollowUpCallToAction(int size, int page, int applicationId, int callToActionId, string userName)
        {
            _callToActionService.FollowUpCallToAction(applicationId, callToActionId, userName);
            GetCallsToAction(size, page);
        }
        public void GetPages(int applicationId)
        {
            _pageService.PageRepository.GetPagesByApplicationId(applicationId);
            _callToActionService.CallToActionRepository.GetCallToActionTemplatesByApplication(applicationId);
            _pages = _pageService.PageRepository.Pages;
            callToActionTemplates = _callToActionService.CallToActionRepository.CallToActionTemplates;
        }
        public void SavePage(int applicationId, string pageTitle)
        {
            _pageService.PageRepository.Page = new Page();
            var page = _pageService.PageRepository.Page;
            page.ApplicationId = applicationId;
            page.PageTitle = pageTitle;
            page.PageTitleSlug = Helpers.HtmlHelperExtensions.Slug(pageTitle).ToLower();
            page.DateCreated = DateTime.Now;
            page.DateUpdated = DateTime.Now;
            _pageService.PageRepository.SavePage();
            _pageService.PageRepository.GetPagesByApplicationId(applicationId);
            _page = _pageService.PageRepository.Page;
            _pages = _pageService.PageRepository.Pages;
            _callToActionService.CallToActionRepository.GetCallToActionTemplatesByApplication(applicationId);
            callToActionTemplates = _callToActionService.CallToActionRepository.CallToActionTemplates;
        }
        public void UpdatePage(Page page, int applicationId)
        {
            _pageService.PageRepository.Page = page;
            page.DateUpdated = DateTime.Now;
            page.PageTitleSlug = Helpers.HtmlHelperExtensions.Slug(page.PageTitle).ToLower();
            _pageService.PageRepository.UpdatePage();
            _pageService.PageRepository.GetPagesByApplicationId(applicationId);
            _callToActionService.CallToActionRepository.GetCallToActionTemplatesByApplication(applicationId);
            _page = _pageService.PageRepository.Page;
            _pages = _pageService.PageRepository.Pages;
            callToActionTemplates = _callToActionService.CallToActionRepository.CallToActionTemplates;
        }
        public void DeletePage(Page page, int applicationId)
        {
            _pageService.PageRepository.Page = page;
            _pageService.PageRepository.DeletePage();
            _pageService.PageRepository.GetPagesByApplicationId(applicationId);
            _callToActionService.CallToActionRepository.GetCallToActionTemplatesByApplication(applicationId);
            _page = _pageService.PageRepository.Page;
            _pages = _pageService.PageRepository.Pages;
            callToActionTemplates = _callToActionService.CallToActionRepository.CallToActionTemplates;
        }
        public void GetPage(int pageId)
        {
            MarkdownSharp.Markdown m = new MarkdownSharp.Markdown();
            _callToActionService.CallToActionRepository.GetCallToActionTemplatesByApplication(application.ApplicationId);
            _pageService.PageRepository.GetPage(pageId);
            _page = _pageService.PageRepository.Page;
            _page.PageContentMarkDown = m.Transform(_page.PageContentMarkDown);
            callToActionTemplates = _callToActionService.CallToActionRepository.CallToActionTemplates;
            _callToActionTemplateFields = _callToActionService.CallToActionRepository.CallToActionTemplateFields;
        }
        public void GetCourse(int courseId)
        {
            _curriculumService.CurriculumRepository.GetCourse(courseId);
            _course = _curriculumService.CurriculumRepository.Course;
        }
        public void UpdateCourse(Course course)
        {
            _curriculumService.CurriculumRepository.Course = course;
            if (course.Price.HasValue)
            {
                course.VAT = course.Price.Value * 0.14m;
                course.ExclVAT = course.Price.Value / 1.14m;
            }
            _curriculumService.CurriculumRepository.UpdateCourse();
            _course = _curriculumService.CurriculumRepository.Course;
        }
        private IList<Curriculum> curricula;
        public IList<Curriculum> Curricula { get { return curricula; } }
        private IList<Curriculum> vendors;
        public IList<Curriculum> Vendors { get { return vendors; } }
        private IList<Curriculum> parentCurricula;
        private Curriculum _curriculum;
        public Curriculum Curriculum { get { return _curriculum; } }
        private IList<Curriculum> _breadCrumb;
        public IList<Curriculum> BreadCrumb { get { return _breadCrumb; } }
        private Application application;
        public Application Application { get { return application; } }
        private IList<ApplicationProperty> applicationProperties;
        public IList<ApplicationProperty> ApplicationProperties
        {
            get { return applicationProperties; }
        }
        public IList<Promotion> Promotions
        {
            get { return promotions; }
        }
        private IList<Promotion> promotions;
        public IList<ApplicationUser> Users
        {
            get { return _userManager.Users.ToList(); }
        }
        private IList<CallToActionTemplate> callToActionTemplates;
        public IList<CallToActionTemplate> CallToActionTemplates { get { return callToActionTemplates; } }
        private IList<CallToActionTemplateField> _callToActionTemplateFields;
        public IList<CallToActionTemplateField> CallToActionTemplateFields { get { return _callToActionTemplateFields; } }
        public bool CallToActionValid { get {return _callToActionValid; } }
        private CallToActionTemplate _callToActionTemplate;
        public CallToActionTemplate CallToActionTemplate
        {
            get { return _callToActionTemplate; }
        }
        private IList<CallToActionTemplateMessageSequence> _callToActionTemplateMessageSequences;
        public IList<CallToActionTemplateMessageSequence> CallToActionTemplateMessageSequences
        {
            get { return _callToActionTemplateMessageSequences; }
        }
        public CallToAction CallToAction
        {
            get { return _callToAction; }
        }
        private CallToAction _callToAction;
        private IList<CallToAction> _callsToAction;
        public IList<CallToAction> CallsToAction { get { return _callsToAction; } }
        private IList<CallToActionField> _callToActionFields;
        public IList<CallToActionField> CallToActionFields { get { return _callToActionFields; } }
        private IList<CallToActionMessageSequence> _callToActionMessageSequences;
        public IList<CallToActionMessageSequence> CallToActionMessageSequences
        {
            get { return _callToActionMessageSequences; }
        }
        private CallToActionMessageSequence _currentMessageSequence;
        public CallToActionMessageSequence CurrentMessageSequence { get { return _currentMessageSequence; } }
        private IList<Page> _pages;
        public IList<Page> Pages { get { return _pages; } }
        private Page _page;
        public Page Page { get { return _page; } }
        private Page _termsAndConditions;
        public Page TermsAndConditions { get { return _termsAndConditions; } }
        private Page _privacyPolicy;
        public Page PrivacyPolicy { get { return _privacyPolicy; } }
        private IList<Course> _courses;
        private Course _course;
        public Course Course { get { return _course; } }
        private IList<Curriculum> _frontPageCurricula;
        public IList<Curriculum> FrontPageCurricula { get { return _frontPageCurricula; } }
    }
    public class CallToActionFieldDTO
    {
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public int CallToActionTemplateId { get; set; }
        public string ValidationMessage { get; set; }
        public bool Valid { get; set; }
    }
}