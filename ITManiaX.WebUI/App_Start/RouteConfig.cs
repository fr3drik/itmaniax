﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ITManiaX.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Curriculum",
                url: "{controller}/Curriculum/{curriculumId}/{slug}",
                defaults: new { controller = "Home", action = "Curriculum" }
                );

            routes.MapRoute(
                name: "Page",
                url: "{controller}/Page/{pageId}/{slug}",
                defaults: new { controller = "Home", action = "Page" }
                );

            routes.MapRoute(
                name: "CallToAction",
                url: "{controller}/{enquiry}/{callToActionId}",
                defaults: new { controller = "Home", action = "Enquiry" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
