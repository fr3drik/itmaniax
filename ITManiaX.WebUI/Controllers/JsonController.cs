﻿using ITManiaX.Models;
using ITManiaX.Models.Curriculum;
using ITManiaX.Service.Interfaces;
using ITManiaX.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ITManiaX.WebUI.Controllers
{
    [Authorize]
    public class JsonController : Controller
    {
        // GET: Json
        ContentModel cm;
        public JsonController()
        {

        }
        public JsonController(ICurriculumService curriculumService, IApplicationService applicationService, IPromotionService promotionService, ICallToActionService callToActionService, IPageService pageService)
        {
            cm = new ContentModel(curriculumService, applicationService, promotionService, callToActionService, pageService);
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetCallsToAction(int size, int page)
        {
            cm.GetCallsToAction(size, page);
            return Json(new
            {
                CallsToAction = cm.CallsToAction.Select(ta => new
                    {
                        DateCreated = ta.DateCreated.ToShortDateString(),
                        CallToActionName = ta.CallToActionName,
                        CallToActionFields = ta.CallToActionFields,
                        FollowedUp = ta.FollowedUp,
                        FollowUpUser = ta.FollowUpUser,
                        Id = ta.Id,
                        SourceCurriculum = ta.SourceCurriculum,
                        SourcePage = ta.SourcePage
                    }),
                    UserName = User.Identity.Name,
                    ApplicationId = cm.CallsToAction.First().ApplicationId
            });
        }
        [HttpPost]
        public JsonResult FollowUpCallToAction(int size, int page, int applicationId, int callToActionId, string userName)
        {
            cm.FollowUpCallToAction(size, page, applicationId, callToActionId, userName);
            return Json(new
            {
                CallsToAction = cm.CallsToAction.Select(ta => new
                {
                    DateCreated = ta.DateCreated.ToShortDateString(),
                    CallToActionName = ta.CallToActionName,
                    CallToActionFields = ta.CallToActionFields,
                    FollowedUp = ta.FollowedUp,
                    FollowUpUser = ta.FollowUpUser,
                    Id = ta.Id,
                    SourceCurriculum = ta.SourceCurriculum,
                    SourcePage = ta.SourcePage
                }),
                UserName = User.Identity.Name,
                ApplicationId = cm.CallsToAction.First().ApplicationId
            });
        }
        [HttpPost]
        public JsonResult GetPages(int applicationId)
        {
            cm.GetPages(applicationId);
            return Json(new { Pages = cm.Pages, CallToActionTemplates = cm.CallToActionTemplates, Page = cm.Pages.First() });
        }
        [HttpPost]
        public JsonResult SavePage(int applicationId, string pageTitle)
        {
            cm.SavePage(applicationId, pageTitle);
            return Json(new { Pages = cm.Pages, Page = cm.Page, CallToActionTemplates = cm.CallToActionTemplates });
        }
        [HttpPost]
        public JsonResult UpdatePage(Page page,int applicationId)
        {
            cm.UpdatePage(page, applicationId);
            return Json(new { Pages = cm.Pages, Page = cm.Page, CallToActionTemplates = cm.CallToActionTemplates });
        }
        [HttpPost]
        public JsonResult DeletePage(Page page, int applicationId)
        {
            cm.DeletePage(page, applicationId);
            return Json(new { Pages = cm.Pages, Page = cm.Pages.First(), CallToActionTemplates = cm.CallToActionTemplates });
        }
        [HttpPost]
        public JsonResult GetCurricula(string curriculumPath)
        {
            cm.GetCurricula(curriculumPath);
            return Json(new { Curricula = cm.Curricula, Curriculum = new { CurriculumName = "Curricula" }, BreadCrumb = cm.BreadCrumb });
        }
        [HttpPost]
        public JsonResult GetCurriculum(int curriculumId)
        {
            cm.GetCurriculum(curriculumId);
            return Json(new
            {
                Curriculum = new
                {
                    CurriculumId = cm.Curriculum.CurriculumId,
                    CurriculumPath = cm.Curriculum.CurriculumPath,
                    CurriculumName = cm.Curriculum.CurriculumName,
                    Description = cm.Curriculum.Description,
                    ExclVAT = cm.Curriculum.ExclVAT,
                    IsActive = cm.Curriculum.IsActive,
                    IsVendor = cm.Curriculum.IsVendor,
                    IsFrontPage = cm.Curriculum.IsFrontPage,
                    ParentCurriculumPath = cm.Curriculum.ParentCurriculumPath,
                    ParentCurriculumId = cm.Curriculum.ParentCurriculumId,
                    Price = cm.Curriculum.Price,
                    VAT = cm.Curriculum.VAT,
                    Courses = cm.Curriculum.Courses,
                    BreadCrumb = cm.Curriculum.BreadCrumb,
                    TotalExpectedDuration = cm.Curriculum.TotalExpectedDuration
                },
                Curricula = cm.Curriculum.Curricula
            });
        }
        [HttpPost]
        public JsonResult UpdateCurriculum(Curriculum curriculum, List<Curriculum> breadCrumb)
        {
            cm.UpdateCurriculum(curriculum, breadCrumb);
            return Json(new
            {
                Curriculum = new
                {
                    CurriculumId = cm.Curriculum.CurriculumId,
                    CurriculumPath = cm.Curriculum.CurriculumPath,
                    CurriculumName = cm.Curriculum.CurriculumName,
                    Description = cm.Curriculum.Description,
                    ExclVAT = cm.Curriculum.ExclVAT,
                    IsActive = cm.Curriculum.IsActive,
                    IsVendor = cm.Curriculum.IsVendor,
                    IsFrontPage = cm.Curriculum.IsFrontPage,
                    ParentCurriculumPath = cm.Curriculum.ParentCurriculumPath,
                    ParentCurriculumId = cm.Curriculum.ParentCurriculumId,
                    Price = cm.Curriculum.Price,
                    VAT = cm.Curriculum.VAT,
                    Courses = cm.Curriculum.Courses,
                    BreadCrumb = cm.Curriculum.BreadCrumb,
                    TotalExpectedDuration = cm.Curriculum.TotalExpectedDuration
                },
                Curricula = cm.Curriculum.Curricula
            });
        }
        [HttpPost]
        public JsonResult GetCourse(int courseId, List<Curriculum> breadCrumb)
        {
            cm.GetCourse(courseId);
            return Json(new { Course = cm.Course, BreadCrumb = breadCrumb });
        }
        [HttpPost]
        public JsonResult UpdateCourse(Course course, List<Curriculum> breadCrumb, int curriculumId)
        {
            cm.UpdateCourse(course);
            return Json(new
            {
                Course = cm.Course
            });
        }
    }
}