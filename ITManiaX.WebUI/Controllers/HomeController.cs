﻿using ITManiaX.Service.Interfaces;
using ITManiaX.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ITManiaX.WebUI.Controllers
{
    public class HomeController : Controller
    {
        // GET: Homev2
        ContentModel cm;
        public HomeController()
        {

        }
        public HomeController(ICurriculumService curriculumService, IApplicationService applicationService, IPromotionService promotionService, ICallToActionService callToActionService, IPageService pageService)
        {
            cm = new ContentModel(curriculumService, applicationService, promotionService, callToActionService, pageService);
        }
        public ActionResult Index()
        {
            cm.GetApplicationViewModel();
            return View(cm);
        }
        [HttpPost]
        public ActionResult Enquiry(List<CallToActionFieldDTO> callToActionFields, int? pageId = 0, int? curriculumId = 0)
        {
            cm.ValidateCallToAction(callToActionFields, pageId, curriculumId);
            return Json(new { CallToActionFields = callToActionFields, CallToActionValid = cm.CallToActionValid, CallToAction = cm.CallToAction });
        }
        public ActionResult Enquiry(int callToActionId)
        {
            cm.GetCallToAction(callToActionId);
            if (cm.CurrentMessageSequence != null)
            {
                if (cm.CurrentMessageSequence.CallToActionMessageType == "Email")
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(cm);
                }
            }
            else if(cm.CurrentMessageSequence == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(cm);
            }
        }
        public ActionResult Page(int pageId, string slug)
        {
            cm.GetPage(pageId);
            return View(cm);
        }
        public ActionResult Curricula()
        {
            cm.GetApplicationViewModel();
            return View(cm);
        }
        public ActionResult Curriculum(int curriculumId, string slug)
        {
            cm.GetCurriculum(curriculumId);
            return View(cm);
        }
    }
}