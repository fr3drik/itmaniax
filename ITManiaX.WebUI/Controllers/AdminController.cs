﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ITManiaX.WebUI.Models;
using System.Threading.Tasks;
using ITManiaX.Service.Interfaces;

namespace ITManiaX.WebUI.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        // GET: Admin
        ContentModel cm;
        private ApplicationUserManager _userManager;
        private ICurriculumService _curriculumService;
        private IApplicationService _applicationService;
        private IPromotionService _promotionService;
        private IPageService _pageService;
        public AdminController()
        {

        }
        public AdminController(ICurriculumService curriculumService, IApplicationService applicationService, IPromotionService promotionService, IPageService pageService)
        {
            _curriculumService = curriculumService;
            _applicationService = applicationService;
            _promotionService = promotionService;
            _pageService = pageService;
        }
        public ActionResult Index()
        {
            cm = new ContentModel(_curriculumService, _applicationService, _promotionService, _pageService, UserManager);
            return View(cm);
        }
        public ActionResult NewUser()
        {
            return View();
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}