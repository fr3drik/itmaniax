﻿var vodacomDataService = new function () {
    var serviceBase = 'http://' + window.location.host + '/Pandora.Web.Public/';
    var type = 'POST';
    var restEndPoint = '';
    var data = '';
    saveVodacomTemplate = function (templateName, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SaveVodacomTemplate';
        data = "templateName=" + templateName;
        makeAjaxCall(callback);
    };

    makeJsonDataAjaxCall = function (callback, obj) {
        $.ajax({
            type: "POST",
            url: restEndPoint,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: data,
            success: function (data) {
                callback(data);
            }
        });
    };
    makeAjaxCall = function (callback) {
        $.ajax({
            type: "POST",
            url: restEndPoint,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            dataType: "json",
            data: data,
            success: function (data) {
                callback(data);
                $.unblockUI();
            }
        });
    };
    return {
        saveVodacomTemplate: saveVodacomTemplate
    };
} ();


$(".newtemplateUI").live("click", function () {
    $("#newtemplatearea").show();
    $("#existingtemplatearea").hide();
});

$("#saveTemplate").live("click", function () {
    var templateName = $("input[name = 'templateName']").val();
    vodacomDataService.saveVodacomTemplate(templateName, renderVodacomTemplateEditUI);
});

renderVodacomTemplateEditUI = function (data) {
    $("#newtemplatearea").empty();
    $("#vodacomTemplateEditUITmpl").tmpl(data).appendTo("#newtemplatearea");


};

