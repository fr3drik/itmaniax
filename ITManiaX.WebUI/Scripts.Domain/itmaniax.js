﻿
$("#callToActionButton").on("click", function (event) {
    $("#validationMessages").empty();
    $("#validationMessages").append("<div>processing your request...please wait.</div>");
    $(".control-label").removeClass('has-error');
    var callToActionTemplateId = 0;
    var pageId = 0;
    var curriculumId = 0;
    callToActionTemplateId = $("#callToActionTemplateId").val();
    pageId = $("#pageId").val();
    curriculumId = $("#curriculumId").val();
    var arr = {
        callToActionFields: [],
        pageId: pageId,
        curriculumId: curriculumId
    };
    var i = 0;
    var formdata = $('form#callToAction').serializeArray();
    for (i = 0; i < formdata.length; i++) {
        var callToActionField = { 'FieldName': formdata[i].name, 'FieldValue': formdata[i].value, 'CallToActionTemplateId': callToActionTemplateId };
        arr.callToActionFields.push(callToActionField);
    }
    $.ajax({
        url: '/home/Enquiry',
        type: 'post',
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: arr,
        success: function(data) {
            var callToActionValid = data.CallToActionValid;
            if (!callToActionValid) {
                var j = 0;
                for (j = 0; j < data.CallToActionFields.length; j++) {
                    if (!data.CallToActionFields[j].Valid) {
                        $('#' + data.CallToActionFields[j].FieldName).parent().addClass('has-error');
                        $("#validationMessages").append("<div>" + data.CallToActionFields[j].ValidationMessage + "</div>");
                    }
                }
            }
            else
            {
                window.location.replace("/Home/Enquiry/" + data.CallToAction.Id);
            }
        }
    });
    //event.preventDefault();
});