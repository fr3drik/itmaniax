﻿(function () {
    'use strict';
    var app = angular.module("ITManiaX", [
        'ngResource',
        'ngRoute',
        'ngAnimate',
        'ui.bootstrap',
        'ITManiaX.Services']);

    app.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
        $routeProvider
            .when('/callsToAction/', {
                templateUrl: 'Scripts.Domain/partials/callsToAction.html',
                controller: 'CallsToActionController'
            })
            .when('/callsToAction/template/:id', {
                templateUrl: 'Scripts.Domain/partials/callsToActionTemplate.html',
                controller: 'CallsToActionTemplateController'
            })
            .when('/curricula/:curriculumId', {
                templateUrl: 'Scripts.Domain/partials/curricula.html',
                controller: 'CurriculaController'
            })
            .when('/curricula/', {
                templateUrl: 'Scripts.Domain/partials/curricula.html',
                controller: 'CurriculaController'
            })
            .when('/promotions/', {
                templateUrl: 'Scripts.Domain/partials/promotions.html',
                controller: 'PromotionsController'
            })
            .when('/pages/:applicationId', {
                templateUrl: 'Scripts.Domain/partials/pages.html',
                controller: 'PagesController'
            })
            .when('/users', {
                templateUrl: 'Scripts.Domain/partials/users.html',
                controller: 'UsersController'
            })
            .otherwise({ redirectTo: '/callsToAction/' });

        //initialize get if not there
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }
        //disable IE ajax request caching
        $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    }]);
}());