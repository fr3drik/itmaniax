﻿(function () {
    var services = angular.module("ITManiaX.Services", ['ngResource']);
    services.factory('CallToAction', ['$resource', function ($resource) {
        return $resource('api/calltoaction/:callToActionId', { errorLogId: '@errorLogId' }, {
            getCallsToAction: { method: 'POST', url: 'json/getcallstoaction', params: {}, isArray: false },
            followUpCallToAction: { method: 'POST', url: 'json/followupcalltoaction', params: {}, isArray: false }
        });
    }]);
    services.factory('Curriculum', ['$resource', function ($resource) {
        return $resource('api/curriculum/:curriculumId', { errorLogId: '@errorLogId' }, {
            getCurricula: { method: 'POST', url: 'json/getcurricula', params: {}, isArray: false },
            getCurriculum: { method: 'POST', url: 'json/getcurriculum', params: {}, isArray: false },
            updateCurriculum: { method: 'POST', url: 'json/updatecurriculum', params: {}, isArray: false },
            updateCourse: { method: 'POST', url: 'json/updatecourse', params: {}, isArray: false }
        });
    }]);
    services.factory('CurriculumBreadCrumb', ['$resource', function ($resource) {
        var curriculumBreadCrumb = {
            breadCrumb: []
        };
        return curriculumBreadCrumb;
    }]);
    services.factory('Pages', ['$resource', function ($resource) {
        return $resource('api/pages/:pageId', { errorLogId: '@errorLogId' }, {
            savePage: { method: 'POST', url: 'json/savePage', params: {}, isArray: false },
            getPage: { method: 'POST', url: 'json/getPage', params: {}, isArray: false },
            updatePage: { method: 'POST', url: 'json/updatePage', params: {}, isArray: false },
            getPages: { method: 'POST', url: 'json/getpages', params: {}, isArray: false },
            deletePage: { method: 'POST', url: 'json/deletepage', params: {}, isArray: false }
        });
    }]);
}());