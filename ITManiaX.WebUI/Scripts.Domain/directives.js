﻿(function () {
    var directives = angular.module("ITManiaX")
            .directive("tree", function ($compile) {
                return {
                    restrict: "E",
                    scope: { family: '=', method: '&' },
                    transclude: true,
                    template:
                        '<a ng-click="method({curriculumView: family})">{{ family.CurriculumName }} </a>' +
                        '<ul>' +
                            '<li ng-repeat="childCurriculumView in family.Curricula">' +
                                '<tree method="method({curriculaView: childCurriculaView})" family="childCurriculaView"></tree>' +
                            '</li>' +
                        '</ul>',
                    link: function (scope, element, attrs) {
                        $compile(element.contents())(scope.$new());
                    },
                    compile: function (tElement, tAttr) {
                        var contents = tElement.contents().remove();
                        var compiledContents;
                        return function (scope, iElement, iAttr) {
                            if (!compiledContents) {
                                compiledContents = $compile(contents);
                            }
                            compiledContents(scope, function (clone, scope) {
                                iElement.append(clone);
                            });
                        };
                    }
                };
            });
})();