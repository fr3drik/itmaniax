﻿(function () {
    var CallsToActionController = function ($scope, $http, $resource, CallToAction) {
        $scope.page = 1;
        $scope.size = 50;
        $scope.callsToAction = [];
        $scope.userName = {};
        $scope.applicationId = 0;
        $scope.getCallsToAction = function (page) {
            $scope.page = page;
            CallToAction.getCallsToAction({ page: $scope.page, size: $scope.size }).$promise.then(function (data) {
                $scope.callsToAction = data.CallsToAction;
                $scope.userName = data.UserName;
                $scope.applicationId = data.ApplicationId;
            });
        };
        $scope.followUpCallToAction = function (userName, applicationId, callToActionId) {
            console.log(userName + ':' + applicationId + ':' + callToActionId);
            CallToAction.followUpCallToAction({ page: $scope.page, size: $scope.size, userName: userName, applicationId: applicationId, callToActionId: callToActionId }).$promise.then(function (data) {
                $scope.callsToAction = data.CallsToAction;
                $scope.userName = data.UserName;
                $scope.applicationId = data.ApplicationId;
            });
        }
        $scope.getCallsToAction($scope.page);
    };
    angular.module('ITManiaX').controller('CallsToActionController', ['$scope', '$http', '$resource', 'CallToAction', CallsToActionController]);
}());