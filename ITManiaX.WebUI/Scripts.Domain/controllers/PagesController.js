﻿(function () {
    var PagesController = function ($scope, $http, $resource, $routeParams, Pages) {
        $scope.pages = [];
        $scope.page = {};
        $scope.application = 0;
        $scope.callToActionTemplates = [];
        $scope.newPage = {};
        $scope.getPages = function () {
            $scope.applicationId = $routeParams.applicationId;
            Pages.getPages({ applicationId: $scope.applicationId }).$promise.then(function (data) {
                $scope.pages = data.Pages;
                $scope.callToActionTemplates = data.CallToActionTemplates;
                $scope.page = data.Page;
                $scope.newPage = {};
            });
        }
        $scope.savePage = function () {
            //$scope.applicationId = $routeParams.applicationId;
            Pages.savePage({ applicationId: $scope.applicationId, pageTitle: $scope.newPage.PageTitle }).$promise.then(function (data) {
                $scope.pages = data.Pages;
                $scope.callToActionTemplates = data.CallToActionTemplates;
                $scope.page = data.Page;
                $scope.newPage = {};
            });
        };
        $scope.getPage = function (pageId) {
            var i = 0;
            for(i = 0; i < $scope.pages.length;i++)
            {
                if ($scope.pages[i].Id == pageId) {
                    $scope.page = $scope.pages[i];
                }
            }
        };
        $scope.updatePage = function () {
            Pages.updatePage({ applicationId: $scope.applicationId, page: $scope.page }).$promise.then(function (data) {
                $scope.pages = data.Pages;
                $scope.callToActionTemplates = data.CallToActionTemplates;
                $scope.page = data.Page;
                $scope.newPage = {};
            });
        }
        $scope.deletePage = function () {
            Pages.deletePage({ applicationId: $scope.applicationId, page: $scope.page }).$promise.then(function (data) {
                $scope.pages = data.Pages;
                $scope.callToActionTemplates = data.CallToActionTemplates;
                $scope.page = data.Page;
                $scope.newPage = {};
            });
        }
        $scope.getPages();
    };
    angular.module('ITManiaX').controller('PagesController', ['$scope', '$http', '$resource', '$routeParams', 'Pages', PagesController]);
}());