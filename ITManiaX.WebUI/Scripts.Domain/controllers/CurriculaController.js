﻿(function () {
    var CurriculaController = function ($scope, $http, $resource, $routeParams, Curriculum, CurriculumBreadCrumb) {
        $scope.curricula = [];
        $scope.curriculum = {};
        $scope.showCourse = false;
        $scope.getCurricula = function (curriculumPath) {
            Curriculum.getCurricula({ curriculumPath: curriculumPath }).$promise.then(function (data) {
                $scope.curricula = data.Curricula;
                $scope.curriculum = data.Curriculum;
                CurriculumBreadCrumb.breadCrumb = data.BreadCrumb;
                $scope.breadCrumb = CurriculumBreadCrumb.breadCrumb;
            });
        };
        $scope.getCurriculum = function (curriculumId) {
            Curriculum.getCurriculum({ curriculumId: curriculumId, breadCrumb: CurriculumBreadCrumb.breadCrumb }).$promise.then(function (data) {
                $scope.curricula = data.Curricula;
                $scope.curriculum = data.Curriculum;
                CurriculumBreadCrumb.breadCrumb = data.BreadCrumb;
                $scope.breadCrumb = CurriculumBreadCrumb.breadCrumb;
                document.body.scrollTop = document.documentElement.scrollTop = 0;
            });
        };
        $scope.updateCurriculum = function () {
            Curriculum.updateCurriculum({ curriculum: $scope.curriculum, breadCrumb: CurriculumBreadCrumb }).$promise.then(function (data) {
                $scope.curricula = data.Curricula;
                $scope.curriculum = data.Curriculum;
            });
        };
        $scope.getCourse = function (course) {
            $scope.showCourse = true;
            $scope.course = course;
        };
        $scope.updateCourse = function (curriculumId) {
            Curriculum.updateCourse({ course: $scope.course, breadCrumb: CurriculumBreadCrumb, curriculumId: curriculumId }).$promise.then(function (data) {
                $scope.course = data.Course;
            });
        };
        if ($routeParams.curriculumId) {
            if ($routeParams.courseId && $routeParams.curriculumId)
            {
                console('course and curriculum');
            }
            else if ($routeParams.curriculumId > 0) {
                $scope.getCurriculum($routeParams.curriculumId);
            }
            else
            {
                $scope.getCurricula('[]');
            }
        }
        else {
            $scope.getCurricula('[]');
        }
    };
    angular.module('ITManiaX').controller('CurriculaController', ['$scope', '$http', '$resource', '$routeParams', 'Curriculum', 'CurriculumBreadCrumb', CurriculaController]);
}());