﻿var LogOnModel = {
    'UserName': '',
    'Password': '',
    'RememberMe': false,
    'ApplicationId': '',
    'ApplicationName': ''
};
var serviceURL = 'http://' + window.location.host + '/';
$(".learningAsset").live("click", function () {
    var domainObjectKey = $(this).parent().find("input[type='hidden']").val();
    var fillElement = $(this).parent().find(".learningAssetData");
    $(fillElement).empty();
    $(fillElement).append("retrieving details...");
    var restEndPoint = serviceURL + 'Home/GetDomainObject';
    var data = "domainObjectKey=" + domainObjectKey;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $(".learningAssetData").empty();
            $(fillElement).empty();
            $("#learningAssetDataTmpl").tmpl(temp).appendTo(fillElement);
        }
    });
    return false;
});
function submitCallToAction(guid, domainObjectTemplateId, domainObjectId, applicationId, applicationName, prevURL) {
    var elements = $("#" + guid).find("input");
    $("#" + guid).find("input[type='button']").attr("disabled", "disabled");
    var messageElements = $("#" + guid).find("textarea");
    var arrValues = $("#form_" + guid).serialize();
    var restEndPoint = serviceURL + 'Home/SaveCallToAction';
    var data = "applicationId=" + applicationId + "&applicationName=" + applicationName + "&domainObjectTemplateId=" + domainObjectTemplateId + "&domainObjectId=" + domainObjectId + "&fieldValues=" + encodeURIComponent(arrValues);
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            if (temp.ValidationList.ValidModel) {
                var orderProcessName = "";
                for (var key in temp.DomainObject.DomainObjectProperties) {
                    if (temp.DomainObject.DomainObjectProperties[key].DomainObjectPropertyName == "OrderProcessName") {
                        orderProcessName = temp.DomainObject.DomainObjectProperties[key].DomainObjectPropertyValue;
                    }
                }
                window.location = serviceURL + 'CallToAction/' + applicationId + '/' + applicationName + '/' + temp.DomainObject.DomainObjectId + '/' + orderProcessName;
                //window.location = prevURL;
            }
            else {
                for (var key in temp.ValidationList.ValidationItems) {
                    $("#" + guid + " input[name='" + temp.ValidationList.ValidationItems[key].FieldName + "']").css("background-color", "#FFFFD5");
                    $("#" + guid + " input[name='" + temp.ValidationList.ValidationItems[key].FieldName + "']").css("border", "1px solid red");
                    $("#" + guid).find("." + temp.ValidationList.ValidationItems[key].FieldName).show();
                    $("#" + guid).find("." + temp.ValidationList.ValidationItems[key].FieldName).append(temp.ValidationList.ValidationItems[key].ErrorMessage);
                    $("#" + guid).find("input[type='button']").removeAttr("disabled");
                }
            }
        }
    });
};
function getDomainObjectsByType(domainObjectTypeId, domainObjectType, domainObjectPage, pageSize) {
    var restEndPoint = serviceURL + 'Home/GetDomainObjectsByType';
    var data = "domainObjectTypeId=" + domainObjectTypeId + "&domainObjectType=" + domainObjectType + "&domainObjectPage=" + domainObjectPage + "&pageSize=" + pageSize;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
        }
    });
};
$(".expand").live("click", function () {
    var appendElem = $(this).parent().parent().find(".childContent");
    var collapseElem = $(this).parent().parent().find(".collapse");
    var expandElem = $(this).parent().parent().find(".expand");
    var ajaxLoader = $(this).parent().parent().find(".ajaxLoader");
    $(ajaxLoader).show();
    var domainObjectId = $(this).parent().find("input[name='DomainObjectId']").val();
    var curriculaCollection = $(this).parent().find("input[name='CurriculaCollection']").val();
    var content = "";
    if ($("#curriculaPath").val() != "")
        content = $("#curriculaPath").val();

    $("#curriculaPath").val(content + domainObjectId + "|");
    var restEndPoint = serviceURL + 'Home/GetCurricula';
    var data = "domainObjectId=" + domainObjectId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $(appendElem).show();
            $("#curriculaListTmpl").tmpl(temp).appendTo($(appendElem));
            $(collapseElem).show();
            $(expandElem).hide();
            $(ajaxLoader).hide();
        }
    });
});
$(".collapse").live("click", function () {
    var collapseElem = $(this).parent().find(".collapse");
    var expandElem = $(this).parent().find(".expand");
    $(collapseElem).hide();
    $(expandElem).show();
    var appendElem = $(this).parent().parent().find(".childContent");
    $(appendElem).empty();
});
function saveStreamingUser(applicationId) {
    $("#ajaxLoader").show();
    var firstName = $("input[name='firstname']").val();
    var lastName = $("input[name='lastname']").val();
    var email = $("input[name='email']").val();
    var title = $("select[name='title']").val();
    var restEndPoint = serviceURL + 'Home/SaveStreamingUser';
    var data = "applicationId=" + applicationId + "&firstName=" + firstName + "&lastName=" + lastName + "&emailAddress=" + email + "&title=" + title;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#ajaxLoader").hide();
            window.location = serviceURL + "EditUser/" + temp.DomainObject.DomainObjectId + "/" + temp.ApplicationUser.ProviderUserKey + "/" + temp.Application.ApplicationId + "/" + temp.Application.ApplicationName;
        }
    });
};
function updateStreamingUser(applicationId, domainObjectId) {
    $("#ajaxLoader").show();
    var inputs = $("input");
    $(inputs).each(function () {
        $(this).attr("disabled", "disabled");
    });
    $("input[value='Update User']").attr("value", "Updating user details...");
    var providerUserKey = $("#providerUserKey").val();
    var firstName = $("input[name='firstname']").val();
    var lastName = $("input[name='lastname']").val();
    var email = $("input[name='email']").val();
    var title = $("select[name='title']").val();
    var userRoles = [];
    var userGroups = [];
    var userGroupsInputs = $("input[name='applicationUserGroup']");
    $(userGroupsInputs).each(function () {
        if ($(this).attr("checked")) {
            userGroups.push($.trim($(this).val()));
        }
    });
    var userRolesInputs = $("input[name='applicationUserRole']");
    $(userRolesInputs).each(function () {
        if ($(this).attr("checked")) {
            userRoles.push($.trim($(this).val()));
        }
    });
    var restEndPoint = serviceURL + 'Home/UpdateStreamingUser';
    var data = "providerUserKey=" + providerUserKey + "&domainObjectId=" + domainObjectId + "&applicationId=" + applicationId + "&firstName=" + firstName + "&lastName=" + lastName + "&emailAddress=" + email + "&title=" + title + "&userGroups=" + userGroups + "&userRoles=" + userRoles;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#ajaxLoader").hide();
            window.location = serviceURL + "EditUser/" + temp.DomainObject.DomainObjectId + "/" + providerUserKey + "/" + temp.Application.ApplicationId + "/" + temp.Application.ApplicationName;
        }
    });
};
$("#user-activity tr").live("hover", function (e) {
    $("#user-activity tr").removeClass("main-table-text-selected");
    $(this).addClass("main-table-text-selected");
});
$("#user-activity").find("tr").live({
    mouseleave:
    function () { $("#user-activity").find("tr").removeClass("main-table-text-selected"); }
});
$(".userEdit").live("click", function () {
    $.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#4AA3CD',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: 0.9,
        color: '#fff'
    }
    });
    var providerUserKey = $(this).parent().find("input[name='providerUserKey']").val();
    var domainObjectId = $(this).parent().find("input[name='domainObjectId']").val();
    var applicationId = $(this).parent().find("input[name='applicationId']").val();
    var restEndPoint = serviceURL + 'Home/GetDomainObjectById';
    var data = "providerUserKey=" + providerUserKey + "&domainObjectId=" + domainObjectId + "&applicationId=" + applicationId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            //$("#ajaxLoader").hide();
            $.unblockUI();
            window.location = serviceURL + "EditUser/" + temp.DomainObject.DomainObjectId + "/" + providerUserKey + "/" + temp.Application.ApplicationId + "/" + temp.Application.ApplicationName;
        }
    });
});
$(".addToLibrary").live("click", function () {
    var actionText = $(this).find("a").text();
    switch (actionText) {
        case "Add to Library":
            $(this).find("a").text("Cancel");
            $("#addToUserLibrary").show();
            break;
        case "Cancel":
            $(this).find("a").text("Add to Library");
            $("#addToUserLibrary").hide();
            break;
    }
    return false;
});
$("#txtTitle").live("keydown", function (e) {
    if (e.keyCode == 13) {
        $("#rightSpanAjaxLoader").show();
        var applicationId = $("#txtApplicationId").val();
        var searchFieldName = $(this).attr("name");
        var searchText = $(this).val();
        var restEndPoint = serviceURL + 'Home/SearchDomainObjectByFieldKeyword';
        var data = "applicationId=" + applicationId + "&fieldName=" + searchFieldName + "&search=" + searchText;
        $.ajax({
            type: "POST",
            url: restEndPoint,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            dataType: "json",
            data: data,
            success: function (msg) {
                var jsonObj = JSON.stringify(msg);
                var temp = JSON.parse(jsonObj);
                $("#libraryFilterResult").empty();
                $("#libraryGridTmpl").tmpl(temp).appendTo("#libraryFilterResult");
                $("#rightSpanAjaxLoader").hide();
            }
        });
    }
});
$("#txtProductCode").live("keydown", function (e) {
    if (e.keyCode == 13) {
        $("#rightSpanAjaxLoader").show();
        var applicationId = $("#txtApplicationId").val();
        var searchFieldName = $(this).attr("name");
        var searchText = $(this).val();
        var restEndPoint = serviceURL + 'Home/SearchDomainObjectByFieldKeyword';
        var data = "applicationId=" + applicationId + "&fieldName=" + searchFieldName + "&search=" + searchText;
        $.ajax({
            type: "POST",
            url: restEndPoint,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            dataType: "json",
            data: data,
            success: function (msg) {
                var jsonObj = JSON.stringify(msg);
                var temp = JSON.parse(jsonObj);
                $("#libraryFilterResult").empty();
                $("#libraryGridTmpl").tmpl(temp).appendTo("#libraryFilterResult");
                $("#rightSpanAjaxLoader").hide();
            }
        });
    }
});
$("#txtMasterTitle").live("keydown", function (e) {
    if (e.keyCode == 13) {
        $("#rightSpanAjaxLoader").show();
        var applicationId = $("#txtApplicationId").val();
        var searchFieldName = $(this).attr("name");
        var searchText = $(this).val();
        var restEndPoint = serviceURL + 'Home/SearchDomainObjectByFieldKeyword';
        var data = "applicationId=" + applicationId + "&fieldName=" + searchFieldName + "&search=" + searchText;
        $.ajax({
            type: "POST",
            url: restEndPoint,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            dataType: "json",
            data: data,
            success: function (msg) {
                var jsonObj = JSON.stringify(msg);
                var temp = JSON.parse(jsonObj);
                $("#libraryFilterResult").empty();
                $("#libraryGridTmpl").tmpl(temp).appendTo("#searchResult");
                $("#rightSpanAjaxLoader").hide();
            }
        });
    }
});
$("#txtMasterProductCode").live("keydown", function (e) {
    if (e.keyCode == 13) {
        $("#rightSpanAjaxLoader").show();
        var applicationId = $("#txtApplicationId").val();
        var searchFieldName = $(this).attr("name");
        var searchText = $(this).val();
        var restEndPoint = serviceURL + 'Home/SearchDomainObjectByFieldKeyword';
        var data = "applicationId=" + applicationId + "&fieldName=" + searchFieldName + "&search=" + searchText;
        $.ajax({
            type: "POST",
            url: restEndPoint,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            dataType: "json",
            data: data,
            success: function (msg) {
                var jsonObj = JSON.stringify(msg);
                var temp = JSON.parse(jsonObj);
                $("#searchResult").empty();
                $("#libraryGridTmpl").tmpl(temp).appendTo("#searchResult");
                $("#rightSpanAjaxLoader").hide();
            }
        });
    }
});
function saveToLibrary(domainObjectId) {
    $("#rightSpanAjaxLoader").show();
    var items = $("input[name='addToLibraryItem']");
    var domainObjectIds = [];
    $(items).each(function () {
        if ($(this).attr("checked")) {
            domainObjectIds.push($.trim($(this).val()));
        }
    });
    var providerUserKey = $("#providerUserKey").val();
    var applicationId = $("#txtApplicationId").val();
    var restEndPoint = serviceURL + 'Home/SaveToLibrary';
    var data = "applicationId=" + applicationId + "&parentDomainObjectId=" + domainObjectId + "&childDomainObjectIds=" + domainObjectIds + "&providerUserKey=" + providerUserKey;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#libraryResult").empty();
            $("#userLibraryGridTmpl").tmpl(temp).appendTo("#libraryResult");
            $("#rightSpanAjaxLoader").hide();
        }
    });
}
function saveToMasterLibrary(domainObjectId) {
    var domainObjectIds = [];
    var items = $("input[name='addToLibraryItem']");
    $(items).each(function () {
        if ($(this).attr("checked")) {
            domainObjectIds.push($.trim($(this).val()));
        }
    });
    $("#masterStreamingLibAjaxLoader").show();
    var applicationId = $("#txtApplicationId").val();
    var providerUserKey = $("#txtProviderUserKey").val();
    var restEndPoint = serviceURL + 'Home/SaveToMasterLibrary';
    var data = "applicationId=" + applicationId + "&parentDomainObjectId=" + domainObjectId + "&childDomainObjectIds=" + domainObjectIds + "&providerUserKey=" + providerUserKey;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#masterStreamingLib").empty();
            $("#masterLibraryGridTmpl").tmpl(temp).appendTo("#masterStreamingLib");
            $("#masterStreamingLibAjaxLoader").hide();
        }
    });
};
function getMasterStreamingLibrary(domainObjectId) {
    $("#masterStreamingLibAjaxLoader").show();
    var restEndPoint = serviceURL + 'Home/GetMasterLibrary';
    var data = "parentDomainObjectId=" + domainObjectId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#masterStreamingLib").empty();
            $("#masterLibraryGridTmpl").tmpl(temp).appendTo("#masterStreamingLib");
            $("#masterStreamingLibAjaxLoader").hide();
        }
    });
};
function removeFromLibrary(domainObjectId) {
    $("#leftSpanAjaxLoader").show();
    var items = $("input[name='removeFromLibraryItem']");
    var domainObjectIds = [];
    $(items).each(function () {
        if ($(this).attr("checked")) {
            domainObjectIds.push($.trim($(this).val()));
        }
    });
    var applicationId = $("#txtApplicationId").val();
    var providerUserKey = $("#providerUserKey").val();
    var restEndPoint = serviceURL + 'Home/RemoveFromLibrary';
    var data = "applicationId=" + applicationId + "&providerUserKey=" + providerUserKey + "&parentDomainObjectId=" + domainObjectId + "&childDomainObjectIds=" + domainObjectIds;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#libraryResult").empty();
            $("#userLibraryGridTmpl").tmpl(temp).appendTo("#libraryResult");
            $("#leftSpanAjaxLoader").hide();
        }
    });
};
function removeFromMasterLibrary(domainObjectId) {
    $("#masterStreamingLibAjaxLoader").show();
    var items = $("input[name='removeFromLibraryItem']");
    var domainObjectIds = [];
    $(items).each(function () {
        if ($(this).attr("checked")) {
            domainObjectIds.push($.trim($(this).val()));
        }
    });
    var restEndPoint = serviceURL + 'Home/RemoveFromMasterLibrary';
    var data = "parentDomainObjectId=" + domainObjectId + "&childDomainObjectIds=" + domainObjectIds;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#masterStreamingLib").empty();
            $("#masterLibraryGridTmpl").tmpl(temp).appendTo("#masterStreamingLib");
            $("#masterStreamingLibAjaxLoader").hide();
        }
    });
};
$("input[name='userPassword']").live("keydown", function (e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        var applicationId = $("#txtApplicationId").val();
        var applicationName = $("#txtApplicationName").val();
        var inputs = $("input");
        $(inputs).each(function () {
            $(this).attr("disabled", "disabled");
        });
        var loginButton = $("input[type='button']");
        $(loginButton).attr("value", "Loggin In...");
        var userName = $("input[name=userName]").val();
        var password = $("input[name=userPassword]").val();
        LogOnModel.UserName = userName;
        LogOnModel.Password = password;
        LogOnModel.ApplicationId = applicationId;
        LogOnModel.ApplicationName = applicationName;

        var restEndPoint = serviceURL + "Home/ApplicationLogin";
        var data = "{'LogOnModel':" + JSON.stringify(LogOnModel) + "}";
        $.ajax({
            type: "POST",
            url: restEndPoint,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: data,
            success: function (msg) {
                //location.reload();
                var jsonObj = JSON.stringify(msg);
                var temp = JSON.parse(jsonObj);
                if (temp.UserIsValid) {

                    var userRoles = temp.ApplicationUser.ApplicationUserRoles;
                    $("#applicationUserLoginAjaxLoader").hide();
                    for (var i = 0; i < userRoles.length; i++) {
                        if (userRoles[i].ApplicationRoleName === "General User") {
                            document.location = serviceURL + "User/" + LogOnModel.ApplicationId + "/" + LogOnModel.ApplicationName + "/" + temp.ApplicationUser.ProviderUserKey;
                        }
                        else {
                            document.location = serviceURL + "Application/" + LogOnModel.ApplicationId + "/" + LogOnModel.ApplicationName + "/" + temp.ProviderUserKey;
                        }
                    }
                }
                else {
                    var inputs = $("input");
                    $(inputs).each(function () {
                        $(this).attr("disabled", "disabled");
                    });
                    var loginButton = $("input[type='button']");
                    $("#userFeedback").show();
                    $("#userFeedback").text("The user name or password provided is incorrect.");
                    $(inputs).each(function () {
                        $(this).removeAttr("disabled");
                    });
                    $(loginButton).attr("value", "Log On");
                }
            }
        });
    }
});
function logIn(applicationId, applicationName) {
    var inputs = $("input");
    $(inputs).each(function () {
        $(this).attr("disabled", "disabled");
    });
    var loginButton = $("input[type='button']");
    $(loginButton).attr("value", "Loggin In...");
    $("#applicationUserLoginAjaxLoader").show();
    var userName = $("input[name=userName]").val();
    var password = $("input[name=userPassword]").val();
    LogOnModel.UserName = userName;
    LogOnModel.Password = password;
    LogOnModel.ApplicationId = applicationId;
    LogOnModel.ApplicationName = applicationName;

    var restEndPoint = serviceURL + "Home/ApplicationLogin";
    var data = "{'LogOnModel':" + JSON.stringify(LogOnModel) + "}";
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            //location.reload();
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            if (temp.UserIsValid) {
                var userRoles = temp.ApplicationUser.ApplicationUserRoles;
                $("#applicationUserLoginAjaxLoader").hide();
                for (var i = 0; i < userRoles.length; i++) {
                    if (userRoles[i].ApplicationRoleName === "General User") {
                        window.location = serviceURL + "User/" + LogOnModel.ApplicationId + "/" + LogOnModel.ApplicationName + "/" + temp.ApplicationUser.ProviderUserKey;
                    }
                    else {
                        window.location = serviceURL + "Application/" + LogOnModel.ApplicationId + "/" + LogOnModel.ApplicationName + "/" + temp.ProviderUserKey;
                    }
                }
            }
            else {
                var inputs = $("input");
                $(inputs).each(function () {
                    $(this).attr("disabled", "disabled");
                });
                var loginButton = $("input[type='button']");
                $("#userFeedback").show();
                $("#userFeedback").text("The user name or password provided is incorrect.");
                $(inputs).each(function () {
                    $(this).removeAttr("disabled");
                });
                $(loginButton).attr("value", "Log On");
            }
        }
    });
};
function getUserViewSessions() {
    $("#userUsageAjaxLoader").show();
    $("#userUsage").empty();
    var providerUserKey = $("#providerUserKey").val();
    var restEndPoint = serviceURL + 'Home/GetUserViewSessions';
    var data = "providerUserKey=" + providerUserKey;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userUsageTmpl").tmpl(temp).appendTo("#userUsage");
            $("#userUsageAjaxLoader").hide();
            getUserLibraryVideos();
        }
    });
}
function getUserLibraryVideos() {
    $("#userLibraryAjaxLoader").show();
    var providerUserKey = $("#providerUserKey").val();
    var applicationId = $("#txtApplicationId").val();
    var restEndPoint = serviceURL + 'Home/GetUserLibraryVideos';
    var data = "applicationId=" + applicationId + "&providerUserKey=" + providerUserKey;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#libraryResult").empty();
            $("#userLibraryGridTmpl").tmpl(temp).appendTo("#libraryResult");
            $("#userLibraryAjaxLoader").hide();
            getOfferingLibraries();
        }
    });
}
function getOfferingLibraries() {
    $("#getOfferingLibrariesAjaxLoader").show();
    var providerUserKey = $("#providerUserKey").val();
    var applicationId = $("#txtApplicationId").val();
    var restEndPoint = serviceURL + 'Home/GetOfferingLibraries';
    var data = "applicationId=" + applicationId + "&providerUserKey=" + providerUserKey;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#offeringLibraryResult").empty();
            $("#offeringLibrariesTmpl").tmpl(temp).appendTo("#offeringLibraryResult");
            $("#getOfferingLibrariesAjaxLoader").hide();
        }
    });
};
$(".userViewSessionNextPage").live("click", function () {
    $("#userUsageSpanAjaxLoader").show();
    var pageNumber = parseInt($(this).find("input[type='hidden']").val()) + 1;
    var providerUserKey = $("#providerUserKey").val();
    var restEndPoint = serviceURL + 'Home/GetUserViewSessions';
    var data = "providerUserKey=" + providerUserKey + "&userDataPage=" + pageNumber;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userUsage").empty();
            $("#userUsageTmpl").tmpl(temp).appendTo("#userUsage");
            $("#userUsageSpanAjaxLoader").hide();
        }
    });
});
$(".userVideoLibrary").live("click", function () {
    $("#userUsageSpanAjaxLoader").show();
    var pageNumber = parseInt($(this).find("input[type='hidden']").val()) + 1;
    var providerUserKey = $("#providerUserKey").val();
    var restEndPoint = serviceURL + 'Home/GetUserViewSessions';
    var data = "providerUserKey=" + providerUserKey + "&userDataPage=" + pageNumber;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userUsage").empty();
            $("#userUsageTmpl").tmpl(temp).appendTo("#userUsage");
            $("#userUsageSpanAjaxLoader").hide();
        }
    });
});
$(".nextCurriculumPage").live("click", function () {
    $("#curriculaAjaxLoader").show();
    var pageNumber = parseInt($("input[name='pageNumber']").val()) + 1;
    var restEndPoint = serviceURL + 'Home/GetCurriculaCollection';
    var data = "curriculaPage=" + pageNumber;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#dataContent").empty();
            $("#curriculaSidebarList").tmpl(temp).appendTo("#dataContent");
            $("#curriculaAjaxLoader").hide();
        }
    });
});
$(".prevCurriculumPage").live("click", function () {
    $("#curriculaAjaxLoader").show();
    var pageNumber = parseInt($("input[name='pageNumber']").val()) - 1;
    var restEndPoint = serviceURL + 'Home/GetCurriculaCollection';
    var data = "curriculaPage=" + pageNumber;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#dataContent").empty();
            $("#curriculaSidebarList").tmpl(temp).appendTo("#dataContent");
            $("#curriculaAjaxLoader").hide();
        }
    });
});
$("#txtCurriculumName").live("keydown", function (e) {
    if (e.keyCode == 13) {
        if ($(this).val().length > 3) {
            $("#curriculaAjaxLoader").show();
            var pageNumber = parseInt($("input[name='pageNumber']").val()) + 1;
            var restEndPoint = serviceURL + 'Home/GetCurriculumNameSearch';
            var data = "curriculumSearchTerm=" + $(this).val();
            $.ajax({
                type: "POST",
                url: restEndPoint,
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                dataType: "json",
                data: data,
                success: function (msg) {
                    var jsonObj = JSON.stringify(msg);
                    var temp = JSON.parse(jsonObj);
                    $("#dataContent").empty();
                    $("#curriculaSidebarList").tmpl(temp).appendTo("#dataContent");
                    $("#curriculaAjaxLoader").hide();
                }
            });
        }
    }
});
function saveDomainObject(domainObjectTemplateId) {
    var domainObjectProperties = $("#newItemForm input");
    var domainObjectPropertyItems = [];
    $(domainObjectProperties).each(function () {
        domainObjectPropertyItems.push($(this).attr("name") + "|" + $(this).val());
    });
    console.log(domainObjectPropertyItems);
    var restEndPoint = serviceURL + 'Home/SaveDomainObject';
    var data = "domainObjectTemplateId=" + domainObjectTemplateId + "&domainObjectProperties=" + domainObjectPropertyItems;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
        }
    });
};
function saveUserGroup(applicationId) {
    $("#ajaxLoader").show();
    var userGroupName = $("input[name='userGroupName']").val();
    var restEndPoint = serviceURL + 'Home/SaveUserGroup';
    var data = "applicationId=" + applicationId + "&userGroupName=" + userGroupName;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#ajaxLoader").hide();
            window.location = serviceURL + "EditUserGroup/" + temp.Application.ApplicationId + "/" + temp.Application.ApplicationName + "/" + temp.ApplicationUserGroup.ApplicationUserGroupId;
        }
    });
}
function updateUserGroup(applicationId, applicationUserGroupId) {
    $("#ajaxLoader").show();
    var applicationUserGroupName = $("input[name='userGroupName']").val();
    var restEndPoint = serviceURL + 'Home/UpdateUserGroup';
    var data = "applicationId=" + applicationId + "&applicationUserGroupId=" + applicationUserGroupId + "&applicationUserGroupName=" + applicationUserGroupName;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#ajaxLoader").hide();
            window.location = serviceURL + "EditUserGroup/" + temp.Application.ApplicationId + "/" + temp.Application.ApplicationName + "/" + temp.ApplicationUserGroup.ApplicationUserGroupId;
        }
    });
}
$(".userGroupEdit").live("click", function () {
    var applicationUserGroupId = $(this).parent().find("input[name='userGroupId']").val();
    var applicationId = $(this).parent().find("input[name='applicationId']").val();
    var restEndPoint = serviceURL + 'Home/GetUserGroup';
    var data = "applicationId=" + applicationId + "&applicationUserGroupId=" + applicationUserGroupId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#ajaxLoader").hide();
            window.location = serviceURL + "EditUserGroup/" + temp.Application.ApplicationId + "/" + temp.Application.ApplicationName + "/" + temp.ApplicationUserGroup.ApplicationUserGroupId;
        }
    });
});
$("#txtUserName").live("keydown", function (e) {
    if (e.keyCode == 13) {
        if ($("#txtUserName").val().length > 3) {
            $("#rightSpanAjaxLoader").show();
            var applicationUserSearch = $("#txtUserName").val();
            var applicationId = $("#txtApplicationId").val();
            var restEndPoint = serviceURL + 'Home/GetApplicationUserSearch';
            var data = "applicationId=" + applicationId + "&applicationUserSearch=" + applicationUserSearch;
            $.ajax({
                type: "POST",
                url: restEndPoint,
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                dataType: "json",
                data: data,
                success: function (msg) {
                    var jsonObj = JSON.stringify(msg);
                    var temp = JSON.parse(jsonObj);
                    $("#rightSpanAjaxLoader").hide();
                    $("#userSearchResult").empty();
                    $("#userGroupUsersSearchGridTmpl").tmpl(temp).appendTo("#userSearchResult");
                }
            });
        }
    }
});
$("#txtFirstName").live("keydown", function (e) {
    if (e.keyCode == 13) {
        if ($("#txtFirstName").val().length > 3) {
            $("#rightSpanAjaxLoader").show();
            var applicationUserFirstNameSearch = $("#txtFirstName").val();
            var applicationId = $("#txtApplicationId").val();
            var restEndPoint = serviceURL + 'Home/GetApplicationUserFirstNameSearch';
            var data = "applicationId=" + applicationId + "&applicationUserFirstNameSearch=" + applicationUserFirstNameSearch;
            $.ajax({
                type: "POST",
                url: restEndPoint,
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                dataType: "json",
                data: data,
                success: function (msg) {
                    var jsonObj = JSON.stringify(msg);
                    var temp = JSON.parse(jsonObj);
                    $("#rightSpanAjaxLoader").hide();
                    $("#userSearchResult").empty();
                    $("#userGroupUsersSearchGridTmpl").tmpl(temp).appendTo("#userSearchResult");
                }
            });
        }
    }
});
$("#txtLastName").live("keydown", function (e) {
    if (e.keyCode == 13) {
        if ($("#txtLastName").val().length > 3) {
            $("#rightSpanAjaxLoader").show();
            var applicationUserLastNameSearch = $("#txtLastName").val();
            var applicationUserFirstNameSearch = $("#txtFirstName").val();
            var applicationId = $("#txtApplicationId").val();
            var restEndPoint = serviceURL + 'Home/GetApplicationUserLastNameSearch';
            var data = "applicationId=" + applicationId + "&applicationUserFirstNameSearch=" + applicationUserFirstNameSearch + "&applicationUserLastNameSearch=" + applicationUserLastNameSearch;
            $.ajax({
                type: "POST",
                url: restEndPoint,
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                dataType: "json",
                data: data,
                success: function (msg) {
                    var jsonObj = JSON.stringify(msg);
                    var temp = JSON.parse(jsonObj);
                    $("#rightSpanAjaxLoader").hide();
                    $("#userSearchResult").empty();
                    $("#userGroupUsersSearchGridTmpl").tmpl(temp).appendTo("#userSearchResult");
                }
            });
        }
    }
});
function saveUserToUserGroup(userGroupId) {
    $("#userGroupAssignUsersAjaxLoader").show();
    var userIds = [];
    var inputs = $("#userAssignment input[name='ApplicationUser']");
    $(inputs).each(function () {
        if ($(this).attr("checked")) {
            userIds.push($(this).val());
        }
    });
    var applicationId = $("#txtApplicationId").val();
    var restEndPoint = serviceURL + 'Home/SaveApplicationUserToUserGroup';
    var data = "applicationId=" + applicationId + "&userGroupId=" + userGroupId + "&userIds=" + userIds;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userGroupUsersResult").empty();
            $("#userGroupAssignUsersAjaxLoader").hide();
            $("#userGroupUsersGridTmpl").tmpl(temp).appendTo("#userGroupUsersResult");
        }
    });
};
function removeUserFromUserGroup(userGroupId) {
    $("#userGroupUsersAjaxLoader").show();
    var userIds = [];
    var inputs = $("#userGroupUsersResult input[name='ApplicationUser']");
    $(inputs).each(function () {
        if ($(this).attr("checked")) {
            userIds.push($(this).val());
        }
    });
    var applicationId = $("#txtApplicationId").val();
    var restEndPoint = serviceURL + 'Home/RemoveApplicationUserFromUserGroup';
    var data = "applicationId=" + applicationId + "&applicationUserGroupId=" + userGroupId + "&userIds=" + userIds;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userGroupUsersResult").empty();
            $("#userGroupUsersAjaxLoader").hide();
            $("#userGroupUsersGridTmpl").tmpl(temp).appendTo("#userGroupUsersResult");
        }
    });
};
function getApplicationUserGroupUsers() {
    $("#userGroupUsersAjaxLoader").show();
    var applicationUserGroupId = $("input[name='userGroupId']").val();
    var applicationId = $("#txtApplicationId").val();
    var restEndPoint = serviceURL + 'Home/GetApplicationUserGroupUsers';
    var data = "applicationId=" + applicationId + "&applicationUserGroupId=" + applicationUserGroupId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userGroupUsersAjaxLoader").hide();
            $("#userGroupUsersGridTmpl").tmpl(temp).appendTo("#userGroupUsersResult");
            if ($("input[name='ImportFileName']").length) {
                extractData($("input[name='ImportFileName']").val());
            }
            else {
                getApplicationUsers(applicationId);
            }
        }
    });
};
$(".deleteUserGroup").live("click", function () {
    var applicationUserGroupId = $(this).parent().find("input[name='userGroupId']").val();
    var applicationId = $(this).parent().find("input[name='applicationId']").val();
    var restEndPoint = serviceURL + 'Home/DeleteUserGroup';
    var data = "applicationId=" + applicationId + "&applicationUserGroupId=" + applicationUserGroupId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            window.location = serviceURL + "UserGroups/" + temp.Application.ApplicationId + "/" + temp.Application.ApplicationName;
        }
    });
});
function extractData(filename) {
    $("#userAssignment").empty();
    $("#userGroupAssignUsersAjaxLoader").show();
    var restEndPoint = serviceURL + 'Home/ExtractData';
    var data = "fileName=" + filename;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userAssignment").show();
            $("#excelExtractUsersSearchGridTmpl").tmpl(temp).appendTo("#userAssignment");
            $("#userGroupAssignUsersAjaxLoader").hide();
        }
    });
};
function saveUserToUserGroupFromExcel(applicationGroupId, applicationId) {
    var fileName = $("input[name='ImportFileName']").val();
    $("#userGroupAssignUsersAjaxLoader").show();
    var userNames = [];
    var inputs = $("#userAssignment input[name='ApplicationUser']");
    $(inputs).each(function () {
        if ($(this).attr("checked")) {
            userNames.push($(this).val());
        }
    });
    var restEndPoint = serviceURL + 'Home/SaveUserToUserGroupFromExcel';
    var data = "applicationId=" + applicationId + "&applicationGroupId=" + applicationGroupId + "&fileName=" + fileName + "&userNames=" + userNames;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userGroupAssignUsersAjaxLoader").hide();
            $("#userGroupUsersResult").empty();
            $("#userGroupUsersGridTmpl").tmpl(temp).appendTo("#userGroupUsersResult");
        }
    });
};
function saveUserToUserGroupFromPlainText(applicationGroupId, applicationId) {
    var fileName = $("input[name='ImportFileName']").val();
    $("#userGroupAssignUsersAjaxLoader").show();
    var userNames = [];
    var inputs = $("#userAssignment input[name='ApplicationUser']");
    $(inputs).each(function () {
        if ($(this).attr("checked")) {
            userNames.push($(this).val());
        }
    });
    var restEndPoint = serviceURL + 'Home/SaveUserToUserGroupFromPlainText';
    var data = "applicationId=" + applicationId + "&applicationGroupId=" + applicationGroupId + "&fileName=" + fileName + "&userNames=" + userNames;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userGroupAssignUsersAjaxLoader").hide();
            $("#userGroupUsersResult").empty();
            $("#userGroupUsersGridTmpl").tmpl(temp).appendTo("#userGroupUsersResult");
        }
    });
};
function saveStreamingUserToUserGroup(applicationId, applicationUserGroupId) {
    $("#userGroupAssignUsersAjaxLoader").show();
    var firstName = $("input[name='firstname']").val();
    var lastName = $("input[name='lastname']").val();
    var email = $("input[name='email']").val();
    var title = $("select[name='title']").val();
    var roles = [];
    var inputs = $("input[name='userRole']");
    $(inputs).each(function () {
        if ($(this).attr("checked")) {
            roles.push($(this).val());
        }
    });
    var restEndPoint = serviceURL + 'Home/SaveUserToUserGroup';
    var data = "applicationId=" + applicationId + "&applicationGroupId=" + applicationUserGroupId + "&firstName=" + firstName + "&lastName=" + lastName + "&email=" + email + "&title=" + title + "&roles=" + roles;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userGroupUsersResult").empty();
            $("#userGroupUsersGridTmpl").tmpl(temp).appendTo("#userGroupUsersResult");
            $("#userGroupAssignUsersAjaxLoader").hide();
            $("#userGroupAssignUsersAjaxLoader").show();
            $("#userAssignment").show();
            $("#userAssignment").empty();
            $("#userAssignment").html($("#newUserUserGroup").html());
            $("#userGroupAssignUsersAjaxLoader").hide();
        }
    });
}
$(".saveCustomCurricula").live("click", function () {
    $(".customCurriculum").hide();
    var curriculaCollectionId = $(this).parent().find("input[name='CurriculumCollectionId']").val();
    $("#customCurricula_" + curriculaCollectionId).show();
    $("#customCurricula_" + curriculaCollectionId).show();
    var restEndPoint = serviceURL + 'Home/GetCustomCurricula';
    var data = "";
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#customCurricula_" + curriculaCollectionId).empty();
            $("#customCurricula_" + curriculaCollectionId).css("height", "150px");
            $("#saveToCurriculumUI").tmpl(temp).appendTo("#customCurricula_" + curriculaCollectionId);
            $("input[name='CurriculaCollectionId']").val(curriculaCollectionId);
        }
    });
});
function saveCustomCurriculum() {
    $("#loader").show();
    var customCurriculumTitle = $("input[name='txtCustomCurriculumName']").val();
    var curriculaCollectionId = $("input[name='CurriculaCollectionId']").val();
    var restEndPoint = serviceURL + 'Home/SaveCustomCurriculum';
    var data = "customCurriculumTitle=" + customCurriculumTitle;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#customCurricula_" + curriculaCollectionId).empty();
            $("#saveToCurriculumUI").tmpl(temp).appendTo("#customCurricula_" + curriculaCollectionId);
            $("#loader").hide();
        }
    });
}
function saveCurriculumToCustomCurricula() {
    $("#loader").show();
    var customCurricula = [];
    var curriculaCollectionId = $("input[name='CurriculaCollectionId']").val();
    var inputs = $("input[name='curriculum']");
    $(inputs).each(function () {
        if ($(this).attr("checked")) {
            customCurricula.push($(this).val());
        }
    });
    var restEndPoint = serviceURL + 'Home/SaveCurriculumToCustomCurricula';
    var data = "curriculaCollectionId=" + curriculaCollectionId + "&customCurricula=" + customCurricula;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#customCurricula").empty();
            $("#customCurriculaRepeater").tmpl(temp).appendTo("#customCurricula");
            $("#loader").hide();
        }
    });
};
function getCustomCurricula() {
    var restEndPoint = serviceURL + 'Home/GetCustomCurricula';
    var data = "";
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#customCurricula").empty();
            $("#customCurriculaRepeater").tmpl(temp).appendTo("#customCurricula");
        }
    });
};
$(".addExistingUser").live("click", function () {
    $("#userGroupAssignUsersAjaxLoader").show();
    var applicationId = $(this).find("input").val();
    var restEndPoint = serviceURL + 'Home/GetApplicationUsers';
    var data = "applicationId=" + applicationId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userAssignment").show();
            $("#userAssignment").empty();
            $("#existingUsers").tmpl(temp).appendTo("#userAssignment");
            $("#userGroupAssignUsersAjaxLoader").hide();
        }
    });
});
function getApplicationUsers(applicationId) {
    $("#userGroupAssignUsersAjaxLoader").show();
    var restEndPoint = serviceURL + 'Home/GetApplicationUsers';
    var data = "applicationId=" + applicationId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userAssignment").show();
            $("#userAssignment").empty();
            $("#existingUsers").tmpl(temp).appendTo("#userAssignment");
            $("#userGroupAssignUsersAjaxLoader").hide();
            getApplicationUserGroupDomainObjects(applicationId)
        }
    });
};
function getApplicationUserGroupDomainObjects(applicationId) {
    $("#userGroupLibraryAjaxLoader").show();
    var applicationGroupId = $("input[name='userGroupId']").val();
    var restEndPoint = serviceURL + 'Home/GetApplicationUserGroupDomainObjects';
    var data = "applicationId=" + applicationId + "&applicationGroupId=" + applicationGroupId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userGroupLibraryGridTmpl").tmpl(temp).appendTo("#libraryResult");
            $("#userGroupLibraryAjaxLoader").hide();
        }
    });

};
$(".userGroupsNextPage").live("click", function () {
    $("#userGroupUsersAjaxLoader").show();
    var applicationId = $("#txtApplicationId").val();
    var applicationUserGroupId = $("input[name='userGroupId']").val();
    var pageNumber = parseInt($(this).find("input[type='hidden']").val()) + 1;
    var restEndPoint = serviceURL + 'Home/GetApplicationUserGroupUsers';
    var data = "applicationId=" + applicationId + "&applicationUserGroupId=" + applicationUserGroupId + "&page=" + pageNumber;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userGroupUsersResult").show();
            $("#userGroupUsersResult").empty();
            $("#userGroupUsersGridTmpl").tmpl(temp).appendTo("#userGroupUsersResult");
            $("#userGroupUsersAjaxLoader").hide();
        }
    });
});
$(".userGroupsPreviousPage").live("click", function () {
    $("#userGroupUsersAjaxLoader").show();
    var applicationId = $("#txtApplicationId").val();
    var applicationUserGroupId = $("input[name='userGroupId']").val();
    var pageNumber = parseInt($(this).find("input[type='hidden']").val()) - 1;
    var restEndPoint = serviceURL + 'Home/GetApplicationUserGroupUsers';
    var data = "applicationId=" + applicationId + "&applicationUserGroupId=" + applicationUserGroupId + "&page=" + pageNumber;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userGroupUsersResult").show();
            $("#userGroupUsersResult").empty();
            $("#userGroupUsersGridTmpl").tmpl(temp).appendTo("#userGroupUsersResult");
            $("#userGroupUsersAjaxLoader").hide();
        }
    });
});
$(".existingUserGroupsNextPage").live("click", function () {
    $("#userGroupAssignUsersAjaxLoader").show();
    var restEndPoint = serviceURL + 'Home/GetApplicationUsers';
    var applicationId = $("#txtApplicationId").val();
    var applicationPage = parseInt($(this).find("input[type='hidden']").val()) + 1;
    var data = "applicationId=" + applicationId + "&applicationPage=" + applicationPage;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userAssignment").show();
            $("#userAssignment").empty();
            $("#existingUsers").tmpl(temp).appendTo("#userAssignment");
            $("#userGroupAssignUsersAjaxLoader").hide();
        }
    });
});
$(".existingUserGroupsPreviousPage").live("click", function () {
    $("#userGroupAssignUsersAjaxLoader").show();
    var restEndPoint = serviceURL + 'Home/GetApplicationUsers';
    var applicationId = $("#txtApplicationId").val();
    var applicationPage = parseInt($(this).find("input[type='hidden']").val()) - 1;
    var data = "applicationId=" + applicationId + "&applicationPage=" + applicationPage;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userAssignment").show();
            $("#userAssignment").empty();
            $("#existingUsers").tmpl(temp).appendTo("#userAssignment");
            $("#userGroupAssignUsersAjaxLoader").hide();
        }
    });
});
$(".excelExtractUsersPreviousPage").live("click", function () {
    var fileName = $("input[name='ImportFileName']").val();
    var applicationPage = parseInt($(this).find("input[type='hidden']").val()) - 1;
    $("#userAssignment").empty();
    $("#userGroupAssignUsersAjaxLoader").show();
    var restEndPoint = serviceURL + 'Home/ExtractData';
    var data = "fileName=" + fileName + "&page=" + applicationPage;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userAssignment").show();
            $("#excelExtractUsersSearchGridTmpl").tmpl(temp).appendTo("#userAssignment");
            $("#userGroupAssignUsersAjaxLoader").hide();
        }
    });
});
$(".excelExtractUsersNextPage").live("click", function () {
    var fileName = $("input[name='ImportFileName']").val();
    var applicationPage = parseInt($(this).find("input[type='hidden']").val()) + 1;
    $("#userAssignment").empty();
    $("#userGroupAssignUsersAjaxLoader").show();
    var restEndPoint = serviceURL + 'Home/ExtractData';
    var data = "fileName=" + fileName + "&page=" + applicationPage;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userAssignment").show();
            $("#excelExtractUsersSearchGridTmpl").tmpl(temp).appendTo("#userAssignment");
            $("#userGroupAssignUsersAjaxLoader").hide();
        }
    });
});
$("input[name='searchExistingUsers']").live("keydown", function (e) {
    if (e.keyCode == 13) {
        $("#userGroupAssignUsersAjaxLoader").show();
        var applicationId = $("#txtApplicationId").val();
        var searchWords = $("input[name='searchExistingUsers']").val();
        var restEndPoint = serviceURL + 'Home/SearchApplicationUsers';
        var data = "applicationId=" + applicationId + "&searchWords=" + searchWords;
        $.ajax({
            type: "POST",
            url: restEndPoint,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            dataType: "json",
            data: data,
            success: function (msg) {
                var jsonObj = JSON.stringify(msg);
                var temp = JSON.parse(jsonObj);
                $("#userAssignment").show();
                $("#userAssignment").empty();
                $("#existingUsers").tmpl(temp).appendTo("#userAssignment");
                $("#userGroupAssignUsersAjaxLoader").hide();
            }
        });
    }
});
$("input[name='searchApplicationGroupUsers']").live("keydown", function (e) {
    if (e.keyCode == 13) {
        $("#userGroupUsersAjaxLoader").show();
        var applicationId = $("#txtApplicationId").val();
        var applicationUserGroupId = $("input[name='userGroupId']").val();
        var searchWords = $("input[name='searchApplicationGroupUsers']").val();
        var restEndPoint = serviceURL + 'Home/SearchApplicationUserGroupUsers';
        var data = "applicationId=" + applicationId + "&applicationUserGroupId=" + applicationUserGroupId + "&searchWords=" + searchWords;
        $.ajax({
            type: "POST",
            url: restEndPoint,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            dataType: "json",
            data: data,
            success: function (msg) {
                var jsonObj = JSON.stringify(msg);
                var temp = JSON.parse(jsonObj);
                $("#userGroupUsersResult").show();
                $("#userGroupUsersResult").empty();
                $("#userGroupUsersGridTmpl").tmpl(temp).appendTo("#userGroupUsersResult");
                $("#userGroupUsersAjaxLoader").hide();
            }
        });
    }
});
$(".newGroupUser").live("click", function () {
    $("#userGroupAssignUsersAjaxLoader").show();
    $("#userAssignment").show();
    $("#userAssignment").empty();
    $("#userAssignment").html($("#newUserUserGroup").html());
    $("#userGroupAssignUsersAjaxLoader").hide();
});
$(".newSurvey").live("click", function () {
    $("#content").html($("#newSurveyTmpl").html());
});
$(".importExcel").live("click", function () {
    $("#userGroupAssignUsersAjaxLoader").show();
    $("#userAssignment").show();
    $("#userAssignment").empty();
    $("#userAssignment").html($("#excelUsersUserGroup").html());
    $("#userGroupAssignUsersAjaxLoader").hide();
});
$(".clearApplicationUserGroupUserSearch").live("click", function () {
    $("#userGroupUsersResult").show();
    $("#userGroupUsersResult").empty();
    getApplicationUserGroupUsers();
});
$(".clearApplicationUserSearch").live("click", function () {
    $("#userAssignment").show();
    $("#userAssignment").empty();
    var applicationId = $("#txtApplicationId").val();
    getApplicationUsers(applicationId);
});
$(".assignRoles").live("click", function () {
    $("#userGroupUsersAjaxLoader").show();
    var restEndPoint = serviceURL + 'Home/GetApplicationUserRoles';
    var data = "";
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userRolesTmpl").tmpl(temp).appendTo("#rolesList");
            $("#assignRoles").show();
            $("#userGroupUsersAjaxLoader").hide();
        }
    });
});
function saveUsersToRoles(applicationId, applicationUserGroupId) {
    var userRoles = [];
    var users = [];
    var userInputs = $("#userGroupUsersResult input[name='ApplicationUser']");
    $(userInputs).each(function () {
        if ($(this).attr("checked")) {
            users.push($(this).val());
        }
    });
    var userRolesInput = $("input[name='AssignUserRole']");
    $(userRolesInput).each(function () {
        if ($(this).attr("checked")) {
            userRoles.push($(this).val());
        }
    });
    $("#userGroupUsersAjaxLoader").show();
    var restEndPoint = serviceURL + 'Home/SaveUsersToRoles';
    var data = "applicationId=" + applicationId + "&applicationUserGroupId=" + applicationUserGroupId + "&users=" + users + "&userRoles=" + userRoles;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#userGroupUsersResult").show();
            $("#userGroupUsersResult").empty();
            $("#userGroupUsersGridTmpl").tmpl(temp).appendTo("#userGroupUsersResult");
            $("#userGroupUsersAjaxLoader").hide();
            $("#assignRoles").hide();
            $("#rolesList").empty();
        }
    });
};
$(".newWorkItem").live("click", function () {
    alert('test');
});
$(".newAssessment").live("click", function () {
    $("#content").html($("#newAssessmentTmpl").html());
});
function saveAssessment() {
    $("#ajaxLoader").show();
    var assessmentTitle = $("input[name='assessmentTitle']").val();
    var restEndPoint = serviceURL + 'Home/CreateAssessment';
    var data = "assessmentTitle=" + assessmentTitle;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            $("#content").empty();
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#content").empty();
            $("#assessmentsGridTmpl").tmpl(temp).appendTo("#content");
            $("#ajaxLoader").hide();
            $("input[name='assessmentTitle']").val("");
        }
    });
};
$(".assessments").live("click", function () {
    getAssessments();
});
function getAssessments() {
    $("#ajaxLoader").show();
    var restEndPoint = serviceURL + 'Home/GetAssessments';
    var data = "";
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#content").empty();
            $("#assessmentsGridTmpl").tmpl(temp).appendTo("#content");
            $("#ajaxLoader").hide();
        }
    });
};
$(".assessmentItem").live("click", function () {
    $("#ajaxLoader").show();
    var assessmentId = 0;
    assessmentId = $(this).find("input").val();
    var restEndPoint = serviceURL + 'Home/GetAssessment';
    var data = "assessmentId=" + assessmentId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#content").empty();
            $("#editAssessmentTmpl").tmpl(temp).appendTo("#content");
            $("#ajaxLoader").hide();
            getAssessmentCompositions(temp.DomainObject.DomainObjectId);
        }
    });
});
$(".newAssessmentComposition").live("click", function () {
    $("#compositionContent").html($("#newAssessmentsComposition").html());
});
function saveAssessmentCompositionStatement() {
    $("#ajaxLoader").show();
    var assessmentId = $("input[name='assessmentId']").val();
    var assessmentCompositionStatement = $("input[name='assessmentCompositionStatement']").val();
    var restEndPoint = serviceURL + 'Home/SaveAssessmentStatement';
    var data = "assessmentId=" + assessmentId + "&assessmentCompositionStatement=" + assessmentCompositionStatement;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#compositionContent").empty();
            $("#editAssessmentsComposition").tmpl(temp).appendTo("#compositionContent");
            $("#ajaxLoader").hide();
        }
    });
};
function getAssessmentCompositions(assessmentId) {
    $("#ajaxLoader").show();
    var restEndPoint = serviceURL + 'Home/GetAssessmentCompositions';
    var data = "assessmentId=" + assessmentId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#compositionContent").empty();
            $("#assessmentCompositionsGrid").tmpl(temp).appendTo("#compositionContent");
            $("#ajaxLoader").hide();
        }
    });
};
$(".assessmentCompositionItem").live("click", function () {
    $("#ajaxLoader").show();
    var assessmentCompositionItemId = $("input[name='assessmentCompositionId']").val();
    var restEndPoint = serviceURL + 'Home/GetAssessmentComposition';
    var data = "assessmentCompositionItemId=" + assessmentCompositionItemId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#compositionContent").empty();
            $("#editAssessmentsComposition").tmpl(temp).appendTo("#compositionContent");
            $("#ajaxLoader").hide();
        }
    });
});
function updateAssessmentCompositionStatement(assessmentCompositionId, assessmentCompositionStatementId) {
    $("#ajaxLoader").show();
    var assessmentCompositionStatement = $("input[name='assessmentCompositionStatement']").val();
    var restEndPoint = serviceURL + 'Home/UpdateAssessmentCompositionStatement';
    var data = "assessmentCompositionId=" + assessmentCompositionId + "&assessmentCompositionStatementId=" + assessmentCompositionStatementId + "&assessmentCompositionStatement=" + assessmentCompositionStatement;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#compositionContent").empty();
            $("#editAssessmentsComposition").tmpl(temp).appendTo("#compositionContent");
            $("#ajaxLoader").hide();
        }
    });
};
function updateAssessmentTitle(assessmentId) {
    $("#ajaxLoader").show();
    var assessmentTitle = $("textarea[name='assessmentTitle']").val();
    var restEndPoint = serviceURL + 'Home/UpdateAssessmentTitle';
    var data = "assessmentId=" + assessmentId + "&assessmentTitle=" + assessmentTitle;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#content").empty();
            $("#editAssessmentTmpl").tmpl(temp).appendTo("#content");
            $("#ajaxLoader").hide();
            getAssessmentCompositions(temp.DomainObject.DomainObjectId);
        }
    });
};
function createAssessmentCompositionOption() {
    //$("#ajaxLoader").show();
    var assessmentCompositionId = $("#txtAssessmentCompositionId").val();
    var assessmentStatementOption = $("textarea[name='assessmentCompositionOption']").val();
    var restEndPoint = serviceURL + 'Home/CreateAssessmentCompositionOption';
    var data = "assessmentCompositionId=" + assessmentCompositionId + "&assessmentStatementOption=" + assessmentStatementOption;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#row_" + temp.DomainObject.DomainObjectUniqueKey).empty();
            $("#assessmentCompositionRowUI").tmpl(temp).appendTo($("#row_" + temp.DomainObject.DomainObjectUniqueKey));
        }
    });
};
$(".addAssessmentOption").live("click", function () {
    var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
    var assessmentCompositionId = $(this).parent().find("input[name='domainObjectId']").val();
    $(".createAssessmentOptionUI").empty();
    $(".createAssessmentOptionUI").show();
    var ui = $("#createAssessmentOptionUI_" + domainObjectUniqueKey);
    $(ui).html($("#assessmentCompositionCreateUI").html());
    $("#txtAssessmentCompositionId").val(assessmentCompositionId);
    $(ui).show();
});
$(".cancelAssessmentOption").live("click", function () {
    $(".createAssessmentOptionUI").empty();
    $(".createAssessmentOptionUI").hide();
});
$(".editAssessmentOption").live("click", function () {
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#ccc'
        }
    });
    var assessmentCompositionOptionId = $(this).find("input[name='asssessmentCompositionOptionId']").val();
    var assessmentCompositionId = $("#assessmentCompositionId_" + assessmentCompositionOptionId).val();
    var domainObjectUniqueKey = $("#assessmentCompositionDomainObjectUniqueKey_" + assessmentCompositionOptionId).val();
    var restEndPoint = serviceURL + 'Home/GetAssessmentCompositionOption';
    var data = "assessmentCompositionId=" + assessmentCompositionId + "&assessmentCompositionOptionId=" + assessmentCompositionOptionId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#editAssessmentOptionUI_" + assessmentCompositionOptionId).empty();
            $("#assessmentCompositionOptionEditUI").tmpl(temp).appendTo("#editAssessmentOptionUI_" + assessmentCompositionOptionId);
            $("#editAssessmentOptionUI_" + assessmentCompositionOptionId).show();
            $.unblockUI();
        }
    });
});
function updateAssessmentCompositionOption(assessmentCompositionOptionId) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#ccc'
        }
    });
    var assessmentCompositionId = $("#assessmentCompositionId_" + assessmentCompositionOptionId).val();
    var assessmentCompositionOptionStatement = $("textarea[name='assessmentCompositionOption']").val();
    var restEndPoint = serviceURL + 'Home/UpdateAssessmentCompositionOption';
    var data = "assessmentCompositionId=" + assessmentCompositionId + "&assessmentCompositionOptionId=" + assessmentCompositionOptionId + "&assessmentCompositionOptionStatement=" + assessmentCompositionOptionStatement;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#col_" + assessmentCompositionOptionId).empty();
            $("#assessmentCompositionOptionRowUI").tmpl(temp).appendTo("#col_" + assessmentCompositionOptionId);
            $.unblockUI();
        }
    });
};
$(".deleteAssessmentOption").live("click", function () {
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#ccc'
        }
    });
    var assessmentCompositionOptionId = $(this).find("input[name='asssessmentCompositionOptionId']").val();
    var assessmentCompositionId = $("#assessmentCompositionId_" + assessmentCompositionOptionId).val();
    var assessmentId = $("#assessmentId_" + assessmentCompositionId).val();
    var domainObjectUniqueKey = $("#assessmentCompositionDomainObjectUniqueKey_" + assessmentCompositionOptionId).val();
    var restEndPoint = serviceURL + 'Home/DeleteAssessmentCompositionOption';
    var data = "assessmentId="+ assessmentId +"&assessmentCompositionId=" + assessmentCompositionId + "&assessmentCompositionOptionId=" + assessmentCompositionOptionId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#compositionContent").empty();
            $("#assessmentCompositionsGrid").tmpl(temp).appendTo("#compositionContent");
            $("#ajaxLoader").hide();
            $.unblockUI();
        }
    });
});
$(".editAssessmentCompositionStatement").live("click", function () {
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#ccc'
        }
    });
    var assessmentCompositionStatementId = $(this).find("input[name='asssessmentCompositionStatementId']").val();
    var assessmentCompositionId = $("#assessmentCompositionId_" + assessmentCompositionStatementId).val();
    var domainObjectUniqueKey = $(this).find("#assessmentCompositionDomainObjectUniqueKey_" + assessmentCompositionStatementId).val();
    var restEndPoint = serviceURL + 'Home/GetAssessmentCompositionStatement';
    var data = "assessmentCompositionId=" + assessmentCompositionId + "&assessmentCompositionStatementId=" + assessmentCompositionStatementId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $(".editAssessmentStatementUI").empty();
            $(".editAssessmentStatementUI").hide();
            $("#editAssessmentStatementUI_" + assessmentCompositionStatementId).empty();
            $("#assessmentCompositionStatementEditUI").tmpl(temp).appendTo("#editAssessmentStatementUI_" + assessmentCompositionStatementId);
            $("#editAssessmentStatementUI_" + assessmentCompositionStatementId).show();
            $.unblockUI();
        }
    });
});
function updateAssessmentCompositionStatement(assessmentCompositionStatementId) {
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#ccc'
        }
    });
    var assessmentCompositionId = $("#assessmentCompositionId_" + assessmentCompositionStatementId).val();
    var assessmentCompositionStatement = $("textarea[name='assessmentCompositionStatement']").val();
    var restEndPoint = serviceURL + 'Home/UpdateAssessmentCompositionStatement';
    var data = "assessmentCompositionId=" + assessmentCompositionId + "&assessmentCompositionStatementId=" + assessmentCompositionStatementId + "&assessmentCompositionStatement=" + assessmentCompositionStatement;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#col_" + assessmentCompositionStatementId).empty();
            $("#assessmentCompositionStatementRowUI").tmpl(temp).appendTo("#col_" + assessmentCompositionStatementId);
            $.unblockUI();
        }
    });
};
function saveToUserGroup(applicationUserGroupId) {
    $("#userGroupAddToLibraryAjaxLoader").show();
    var items = $("input[name='addToLibraryItem']");
    var domainObjectIds = [];
    $(items).each(function () {
        if ($(this).attr("checked")) {
            domainObjectIds.push($.trim($(this).val()));
        }
    });
    var applicationId = $("#txtApplicationId").val();

    var restEndPoint = serviceURL + 'Home/SaveToApplicationUserGroupLibrary';
    var data = "applicationId=" + applicationId + "&applicationUserGroupId=" + applicationUserGroupId + "&childDomainObjectIds=" + domainObjectIds;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
            $("#libraryResult").empty();
            $("#userGroupAddToLibraryAjaxLoader").hide();
            $("#userGroupLibraryGridTmpl").tmpl(temp).appendTo("#libraryResult");
        }
    });
};
$("input[name='chkAssessmentCompositionAnswer']").live("click", function () {
    var assessmentCompositionId = $(this).parents().find("input[name='asssessmentCompositionId']").val();
    var domainObjectUniqueKey = $(this).parents().find("input[name='domainObjectUniqueKey']").val();
    var assessmentCompositionOptionId = $(this).val();
    var restEndPoint = serviceURL + 'Home/SaveAssessmentCompositionAnswer';
    var data = "assessmentCompositionId=" + assessmentCompositionId + "&assessmentCompositionOptionId=" + assessmentCompositionOptionId;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (msg) {
            var jsonObj = JSON.stringify(msg);
            var temp = JSON.parse(jsonObj);
        }
    });
});
$(".assetManagement").live("click", function () {
    $("#content").html($("#assetManagementTmpl").html());
    return false;
});
$(".editAssessmentTitle").live("click", function () {
    var domainObjectUniqueKey = $(this).find("input").val();
    $("#assessmentTitleEdit_" + domainObjectUniqueKey).show();
});
$(".cancelAssessmentStatementUpdate").live("click", function () {
    var assessmentStatementId = $(this).find("input").val();
    $("#editAssessmentStatementUI_" + assessmentStatementId).empty();
    $("#editAssessmentStatementUI_" + assessmentStatementId).hide();
});