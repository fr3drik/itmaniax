﻿var chartColors = [];
var chartSettings = {
    title: '',
    width: 0, height: 0,
    hAxis: { title: '', textStyle: { color: '' }, titleTextStyle: { color: ''} },
    vAxis: { textStyle: { color: '' }, minValue: 0, maxValue: 100 },
    backgroundColor: '',
    titleTextStyle: { color: '' },
    legend: { textStyle: { color: '' }, position: '', alignment: '' },
    colors: '',
    chartArea: { left: 0, top:0 },
    pointSize: 10,
    is3D: false
};
var chartSelection = [];
var chartType = '';
var isFirstLoad = true;
var timer;
var count = 0;
var individualScoreFilter = new function () {
    var DateList = [];
    var ChannelList = [];
    var RegionList = [];
    var StoreList = [];
    var PageNumber = 1;
    var FilterType = ''
    return {
        DateList: DateList,
        ChannelList: ChannelList,
        RegionList: RegionList,
        StoreList: StoreList,
        PageNumber: PageNumber,
        FilterType: FilterType
    }};
var domainObjectBreadCrumbKeys = {
    'ParentItems' : [],
    'SelectedItem' : '',
    'Page': '',
    'PageSize': '',
    'ShowDropdown':true,
    'EntityType': ''
};
var domainObjectBreadCrumbLabel = {
    'DomainObjectUniqueKey': '',
    'DomainObjectTitle': ''
};
var DomainObjectCheckItem = {
    'DomainObjectUniqueKey': '',
    'IsChecked': false
};
var renderBlockUI = {
    renderBlockUI: function () {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#fff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#ccc'
            }
        });
    },
    renderPromptBlockUI: function () {
        $.blockUI({ message: $('#question'), css: { width: '275px'} });
    },
    blockElement: function (element, elementMessage) {
        $(element).block({
            message: elementMessage, 
            css: { 
                border: 'none',
                padding: '15px',
                backgroundColor: '#008aca',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: 1.5,
                color: '#fff'
            }
        });
    }
};
var dataService = new function () {
    var serviceBase = 'http://' + window.location.host + '/Pandora.Web.Public/';
    var type = 'POST';
    var restEndPoint = '';
    var data = '';
    getApplicationUserGroups = function (applicationId, page, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationUserGroups';
        data = "applicationId=" + applicationId + "&page=" + page;
        makeAjaxCall(callback);
    },
    saveApplicationUserGroup = function (applicationId, applicationUserGroupName, page, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SaveApplicationUserGroup';
        data = 'applicationId=' + applicationId + '&applicationUserGroupName=' + applicationUserGroupName;
        makeAjaxCall(callback);
    },
    getApplicationUserGroup = function (applicationId, applicationUserGroupId, page, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationUserGroup';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&usersPage=' + page;
        makeAjaxCall(callback);
    },
    updateApplicationUserGroupName = function (applicationUserGroupId, applicationUserGroupName, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/UpdateApplicationUserGroupName';
        data = 'applicationUserGroupId=' + applicationUserGroupId + '&applicationUserGroupName=' + applicationUserGroupName;
        makeAjaxCall(callback);
    },
    getApplicationExistingUsers = function (applicationId, applicationUserGroupId, usersSelected, page, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationExistingUsers';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + "&usersSelected=" + usersSelected + "&page=" + page;
        makeAjaxCall(callback);
    },
    searchApplicationExistingUsers = function (applicationId, applicationUserGroupId, searchText, page, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SearchApplicationExistingUsers';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + "&searchText=" + searchText + "&page=" + page;
        makeAjaxCall(callback);
    },
    searchApplicationUserGroupUsers = function (applicationId, applicationUserGroupId, searchText, page, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SearchApplicationUserGroupUsers';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&searchText=' + searchText + '&page=' + page;
        makeAjaxCall(callback);
    },
    saveStreamingUser = function (applicationId, applicationUserGroupId, firstName, lastName, emailAddress, title, roles, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SaveStreamingUser';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&firstName=' + firstName + '&lastName=' + lastName + '&emailAddress=' + emailAddress + '&title=' + title + '&roles=' + roles;
        makeAjaxCall(callback);
    },
    updateStreamingUser = function (applicationId, applicationUserGroupId, applicationUserId, firstName, lastName, emailAddress, title, roles, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/UpdateStreamingUser';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&applicationUserId=' + applicationUserId + '&firstName=' + firstName + '&lastName=' + lastName + '&emailAddress=' + emailAddress + '&title=' + title + '&emailAddress=' + emailAddress + '&roles=' + roles;
        makeAjaxCall(callback);
    },
    removeApplicationUserFromGroup = function (applicationId, applicationUserGroupId, userIds, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/RemoveApplicationUserFromUserGroup';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&userIds=' + userIds;
        makeAjaxCall(callback);
    },
    getApplicationUser = function (applicationId, applicationUserId, applicationUserGroupId, pageSize, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationUser';
        data = 'applicationId=' + applicationId + '&userId=' + applicationUserId + '&applicationUserGroupId=' + applicationUserGroupId + '&itemsPageSize=' + pageSize;
        makeAjaxCall(callback);
    },
    getApplicationUserMenus = function (applicationId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationUserMenus';
        data = 'applicationId=' + applicationId;
        makeAjaxCall(callback);
    },
    getNewUserData = function (applicationId, applicationUserGroupId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetNewUserData';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId;
        makeAjaxCall(callback);
    },
    saveExistingUsersToGroup = function (applicationId, applicationUserGroupId, selectedUsers, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SaveExistingUsersToUserGroup';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&selectedUsers=' + selectedUsers;
        makeAjaxCall(callback);
    },
    extractExcelData = function (applicationId, applicationUserGroupId, selectedUsers, excelFileName, page, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/ExtractData';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&selectedUsers=' + selectedUsers + '&fileName=' + excelFileName + '&page=' + page;
        makeAjaxCall(callback);
    },
    saveImportUsers = function (applicationId, applicationUserGroupId, selectedUsers, excelFileName, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SaveImportUsersToUserGroup';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&selectedUsers=' + selectedUsers + '&fileName=' + excelFileName;
        makeAjaxCall(callback);
    },
    saveAllImportUsersToGroup = function (applicationId, applicationUserGroupId, excelFileName, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SaveAllImportUsersToUserGroup';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&fileName=' + excelFileName;
        makeAjaxCall(callback);
    },
    getDomainObjectTypes = function (callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetDomainObjectTypes';
        data = '';
        makeAjaxCall(callback);
    },
    getDomainObjectsByDomainObjectTypeId = function (applicationId, domainObjectTypeId, itemSearchText, selectedItems, page, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetDomainObjectsByDomainObjectType';
        data = 'applicationId=' + applicationId + '&domainObjectTypeId=' + domainObjectTypeId + '&selectedItems=' + selectedItems + '&itemSearchText=' + itemSearchText + '&page=' + page;
        makeAjaxCall(callback);
    },
    getApplicationLibrary = function (applicationId, applicationUserGroupId, applicationUserId, page, selectedItems, itemsPageSize, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationLibrary';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&applicationUserId=' + applicationUserId + '&page=' + page + '&selectedItems=' + selectedItems + '&itemsPageSize=' + itemsPageSize;
        makeAjaxCall(callback);
    },
    saveExistingLibraryItems = function (applicationId, selectedItems, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SaveExistingLibraryItems';
        data = 'applicationId=' + applicationId + '&selectedItems=' + selectedItems;
        makeAjaxCall(callback);
    },
    getApplicationUserGroupLibrary = function (applicationId, applicationUserGroupId, applicationUserId, selectedItems, searchPhrase, page, pageSize, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationUserGroupLibrary';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&applicationUserId=' + applicationUserId + '&selectedItems=' + selectedItems + '&searchPhrase=' + searchPhrase + '&page=' + page + '&pageSize=' + pageSize;
        makeAjaxCall(callback);
    },
    getAvailableApplicationUserGroupLibraryItems = function (applicationId, applicationUserGroupId, selectedItems, searchText, page, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home//GetAvailableApplicationUserGroupLibraryItems';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&selectedItems=' + selectedItems + '&searchText=' + searchText + '&page=' + page;
        makeAjaxCall(callback);
    },
    saveApplicationLibraryItemsToApplicationUserGroup = function (applicationId, applicationUserGroupId, selectedItems, page, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SaveApplicationLibraryItemsToApplicationUserGroup';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&selectedItems=' + selectedItems + '&page=' + page;
        makeAjaxCall(callback);
    },
    getDomainObject = function (applicationId, domainObjectId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetDomainObjectById';
        data = 'applicationId=' + applicationId + '&domainObjectId=' + domainObjectId;
        makeAjaxCall(callback);
    },
    getDomainObjectByKey = function (domainObjectKey, callback) {
        restEndPoint = serviceBase + 'Home/GetDomainObject';
        data = 'domainObjectKey=' + domainObjectKey;
        makeAjaxCall(callback);
    },
    saveDomainObjectsToApplicationUser = function (applicationId, applicationUserGroupId, applicationUserProviderUserKey, domainObjectIds, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SaveDomainObjectsToApplicationUser';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&applicationUserProviderUserKey=' + applicationUserProviderUserKey + '&domainObjectIds=' + domainObjectIds;
        makeAjaxCall(callback);
    },
    removeDomainObjectFromApplicationUser = function (applicationId, applicationUserGroupId, applicationUserProviderUserKey, domainObjectId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/RemoveDomainObjectFromApplicationUser';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&applicationUserProviderUserKey=' + applicationUserProviderUserKey + '&domainObjectId=' + domainObjectId;
        makeAjaxCall(callback);
    },
    getApplicationUserLibrary = function (applicationId, applicationUserId, currentSelectedDomainObjectId, callback, page) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationUserLibrary';
        data = 'applicationId=' + applicationId + '&applicationUserId=' + applicationUserId + '&currentSelectedDomainObjectId=' + currentSelectedDomainObjectId + '&page=' + page;
        makeAjaxCall(callback);
    },
    getApplicationUserGroupReport = function (applicationId, applicationUserGroupId, reportPage, pageSize, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationUserGroupReport';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&reportPage=' + reportPage + '&pageSize=' + pageSize;
        makeAjaxCall(callback);
    },
    generateViewingSessionItem = function (applicationId, providerUserKey, actualDuration, itemTitle, itemProductCode) {
        restEndPoint = serviceBase + 'Home/GenerateViewSession';
        data = 'applicationId=' + applicationId + '&providerUserKey=' + providerUserKey + '&actualDuration=' + actualDuration + '&itemTitle=' + itemTitle + '&itemProductCode=' + itemProductCode;
        makeAjaxCall(callback);
    },
    removeDomainObjectApplicationUserGroup = function (applicationId, applicationUserGroupId, domainObjectId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/RemoveDomainObjectApplicationUserGroup';
        data = 'applicationId=' + applicationId + '&applicationUserGroupId=' + applicationUserGroupId + '&domainObjectId=' + domainObjectId;
        makeAjaxCall(callback);
    },
    getParentDomainObjectsByTypeTitle = function (domainObjectName, domainObjectTitle, secondaryFilters, page, pageSize, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetParentDomainObjectsByTypeTitle';
        data = 'domainObjectName=' + domainObjectName + '&domainObjectTitle=' + domainObjectTitle + '&secondaryFilters=' + secondaryFilters + '&page=' + page + '&pageSize=' + pageSize;
        makeAjaxCall(callback);
    },
    getCurriculaCollectionRoot = function (containerLevel, itemIndex, nextItemIndex, containerElem, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetCurriculaCollectionItems';
        data = 'containerLevel=' + containerLevel + '&itemIndex=' + itemIndex + '&nextItemIndex=' + nextItemIndex + '&containerElem=' + containerElem;
        makeAjaxCall(callback);
    },
    getCurriculaCollectionContainer = function (applicationId, baseCurricula, curriculumContainerId, breadCrumbItems, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetCurriculumContainer';
        data = 'applicationId=' + applicationId + '&baseContainer=' + baseCurricula + '&curriculumContainerId=' + curriculumContainerId + '&breadCrumbItems=' + breadCrumbItems;
        makeAjaxCall(callback);
    },
    getExcelSchema = function (excelFile, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetExcelSchema';
        data = 'excelFileName=' + excelFile;
        makeAjaxCall(callback);
    },
    getExcelSchemaData = function (fileName, uniqueKey, itemValues, page, pageSize, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetExcelSchemaData';
        data = 'fileName=' + fileName + '&uniqueKey=' + uniqueKey + '&itemValues=' + itemValues + '&page=' + page + '&pageSize=' + pageSize;
        makeAjaxCall(callback);
    },
    importExcelData = function (fileName, uniqueKey, itemValues, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/ImportExcelData';
        data = 'fileName=' + fileName + '&uniqueKey=' + uniqueKey + '&itemValues=' + itemValues;
        makeAjaxCall(callback);
    },
    getChannelData = function (applicationId, userName, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetChannelData';
        data = 'applicationId=' + applicationId + '&userName=' + userName;
        makeAjaxCall(callback);
    },
    getRegionData = function (channelId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetRegionData';
        data = 'channelId=' + channelId;
        makeAjaxCall(callback);
    },
    getStrategicClusterContainer = function (channelId, regionId, storeId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetStrategicClusterContainer';
        data = 'channelId=' + channelId + '&regionId=' + regionId + '&storeId=' + storeId;
        makeAjaxCall(callback);
    },
    searchCurriculaCollection = function (baseContainer, searchText, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SearchCurriculaCollection';
        data = 'baseContainer=' + baseContainer + '&searchText=' + searchText;
        makeAjaxCall(callback);
    },
    getApplicationList = function (applicationId, domainObjectId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationList';
        data = 'applicationId=' + applicationId + '&domainObjectId=' + domainObjectId;
        makeAjaxCall(callback);
    },
    getCurriculumContainerById = function (curriculumContainerId, applicationId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetCurriculumContainerById';
        data = 'applicationId=' + applicationId + '&curriculumContainerId=' + curriculumContainerId;
        makeAjaxCall(callback);
    },
    curriculumToLib = function (applicationId, baseCurricula, curriculumContainerId, curriculumKey, breadCrumb, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/CurriculumToLib';
        data = 'applicationId=' + applicationId + '&baseContainer=' + baseCurricula + '&curriculumContainerId=' + curriculumContainerId + '&curriculumKey=' + curriculumKey + '&breadCrumb=' + breadCrumb;
        makeAjaxCall(callback);
    },
    curriculumFromLib = function (applicationId, baseCurricula, curriculumContainerId, curriculumKey, breadCrumb, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/CurriculumFromLib';
        data = 'applicationId=' + applicationId + '&baseContainer=' + baseCurricula + '&curriculumContainerId=' + curriculumContainerId + '&curriculumKey=' + curriculumKey + '&breadCrumb=' + breadCrumb;
        makeAjaxCall(callback);
    },
    curriculumBreadCrumbProxy = function (applicationId, baseContainer, breadCrumbs, itemType, itemId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/CurriculumBreadCrumbProxy';
        data = 'applicationId=' + applicationId + '&baseContainer=' + baseContainer + '&breadCrumbs=' + breadCrumbs + '&itemType=' + itemType + '&itemId=' + itemId;
        makeAjaxCall(callback);
    },
    getApplicationLibraryCollection = function (applicationId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationLibraryCollection';
        data = 'applicationId=' + applicationId;
        makeAjaxCall(callback);
    },
    deleteDomainObjectFromAppLib = function (applicationId, domainObjectId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/DeleteDomainObjectFromAppLib';
        data = 'applicationId=' + applicationId + '&domainObjectId=' + domainObjectId;
        makeAjaxCall(callback);
    },
    getApplicationItem = function (applicationId, domainObjectId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetApplicationItem';
        data = 'applicationId=' + applicationId + '&domainObjectId=' + domainObjectId;
        makeAjaxCall(callback);
    },
    getExcelSheetData = function (fileName, sheetName, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetExcelSheetData';
        data = 'fileName=' + fileName + '&sheetName=' + sheetName;
        makeAjaxCall(callback);
    },
    saveDataImportParameter = function (dataImportId, parameterName, parameterType, parameterValue, cellLookup, dataSetLookup, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/SaveDataImportParameter';
        data = 'dataImportId=' + dataImportId + '&parameterName=' + parameterName + '&parameterType=' + parameterType + '&parameterValue=' + parameterValue + '&cellLookup=' + cellLookup + '&dataSetLookup=' + dataSetLookup;
        makeAjaxCall(callback);
    },
    deleteDataImportParameter = function (dataImportId, dataImportParameterId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/DeleteDataImportParameter';
        data = 'dataImportId=' + dataImportId + '&dataImportParameterId=' + dataImportParameterId;
        makeAjaxCall(callback);
    },
    runDataImport = function (dataImportId, importName, dataFile, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/RunDataImport';
        data = 'dataImportId=' + dataImportId + '&importName=' + importName + '&dataFile=' + dataFile;
        makeAjaxCall(callback);
    },
    updateDataImport = function (dataImportId, organisationName, dataSetName, dataSetStartRange, dataSetEndRange, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/UpdateDataImport';
        data = 'dataImportId=' + dataImportId + '&organisationName=' + organisationName + '&dataSetName=' + dataSetName + '&dataSetStartRange=' + dataSetStartRange + '&dataSetEndRange=' + dataSetEndRange;
        makeAjaxCall(callback);
    },
    getDataImportParameter = function (dataImportId, dataImportParameterId, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetDataImportParameter';
        data = 'dataImportId=' + dataImportId + '&dataImportParameterId=' + dataImportParameterId;
        makeAjaxCall(callback);
    },
    updateDataImportParameter = function (dataImportId, dataImportParameterId, parameterName, parameterType, parameterValue, cellLookup, dataSetLookup, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/UpdateDataImportParameter';
        data = 'dataImportId=' + dataImportId + '&dataImportParameterId=' + dataImportParameterId + '&parameterName=' + parameterName + '&parameterType=' + parameterType + '&parameterValue=' + parameterValue + '&cellLookup=' + cellLookup + '&dataSetLookup=' + dataSetLookup;
        makeAjaxCall(callback);
    },
    getIndividualContainer = function (entityId, entityType, pageNumber, pageSize, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetIndividualContainer';
        data = 'entityId=' + entityId + '&entityType=' + entityType + '&pageNumber=' + pageNumber + '&pageSize=' + pageSize;
        makeAjaxCall(callback);
    },
    getIndividualScores = function (individualScoreFilter, callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetIndividualScores';
        data = "{'individualScoreFilter':" + JSON.stringify(individualScoreFilter) + "}";
        makeJsonDataAjaxCall(callback);
    },
    getDataImports = function (pageNumber, callback) {
        //renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetDataImports';
        data = 'pageNumber=' + pageNumber;
        makeAjaxCall(callback);
    },
    getChannelStoreComparison = function (callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetChannelStoreComparison';
        data = '';
        makeAjaxCall(callback);
    },
    getFilterData = function (callback) {
        renderBlockUI.renderBlockUI();
        restEndPoint = serviceBase + 'Home/GetFilterData';
        data = '';
        makeAjaxCall(callback);
    },
    filterIndividualScoreData = function (individualScoreFilter, callback) {
        $(".ajaxLoader").show();
        var chkItems = $(".blockScoresContainer").find("input[type='checkbox']");
        $(chkItems).each(function () {
            $(this).attr("disabled", "disabled");
        });
        restEndPoint = serviceBase + 'Home/FilterIndividualScoreData';
        data = "{'individualScoreFilter':" + JSON.stringify(individualScoreFilter) + "}";
        makeJsonDataAjaxCall(callback, individualScoreFilter);
    };
    getRegionListContainer = function (regionListPageNumber, callback) {
        restEndPoint = serviceBase + 'Home/GetRegionListContainer';
        data = "regionListPageNumber=" + regionListPageNumber;
        makeAjaxCall(callback);
    };
    getStoreListContainer = function (storeListPageNumber, callback) {
        restEndPoint = serviceBase + 'Home/GetStoreListContainer';
        data = "storeListPageNumber=" + storeListPageNumber;
        makeJsonDataAjaxCall(callback);
    };
    getChannelListContainer = function (individualScoreFilter, callback) {
        restEndPoint = serviceBase + 'Home/GetChannelListContainer';
        data = "{'individualScoreFilter':" + JSON.stringify(individualScoreFilter) + "}";
        makeJsonDataAjaxCall(callback);
    };
    getRegionListContainer = function (individualScoreFilter, callback) {
        restEndPoint = serviceBase + 'Home/GetRegionListContainer';
        data = "{'individualScoreFilter':" + JSON.stringify(individualScoreFilter) + "}";
        makeJsonDataAjaxCall(callback);
    };
    getStoreListContainer = function (individualScoreFilter, callback) {
        restEndPoint = serviceBase + 'Home/GetStoreListContainer';
        data = "{'individualScoreFilter':" + JSON.stringify(individualScoreFilter) + "}";
        makeJsonDataAjaxCall(callback);
    };
    getDateListContainer = function (individualScoreFilter, callback) {
        restEndPoint = serviceBase + 'Home/GetDateListContainer';
        data = "{'individualScoreFilter':" + JSON.stringify(individualScoreFilter) + "}";
        makeJsonDataAjaxCall(callback);
    };
    saveConnectionType = function (connectionTypeTitle, callback) {
        restEndPoint = serviceBase + 'Home/SaveConnectionType';
        data = 'connectionTypeTitle=' + connectionTypeTitle;
        makeAjaxCall(callback);
    };
    getConnectionTypes = function (callback) {
        restEndPoint = serviceBase + 'Home/GetConnectionTypes';
        data = '';
        makeAjaxCall(callback);
    };
    saveConnection = function (channelId, regionId, storeId, connectionDate, connectionTypeId, connectionFigure, callback) {
        restEndPoint = serviceBase + 'Home/SaveConnection';
        data = 'channelId=' + channelId + '&regionId=' + regionId + '&storeId=' + storeId + '&connectionDate=' + connectionDate + '&connectionTypeId=' + connectionTypeId + '&connectionFigure=' + connectionFigure;
        makeAjaxCall(callback);
    };
    getConnections = function (pageNumber, callback) {
        restEndPoint = serviceBase + 'Home/GetConnections';
        data = 'pageNumber=' + pageNumber;
        makeAjaxCall(callback);
    };
    getChannels = function (callback) {
        restEndPoint = serviceBase + 'Home/GetChannels';
        data = '';
        makeAjaxCall(callback);
    };
    getEntityValueContainer = function (individualScoreFilter, callback) {
        restEndPoint = serviceBase + 'Home/GetEntityValueContainer';
        data = "{'individualScoreFilter':" + JSON.stringify(individualScoreFilter) + "}";
        makeJsonDataAjaxCall(callback);
    };
    saveEntityValue = function (entityFigure, channelId, regionId, storeId, entityContainerName, entityLabelName, figureDate, callback) {
        restEndPoint = serviceBase + 'Home/SaveEntityValue';
        data = 'entityFigure=' + entityFigure + '&channelId=' + channelId + '&regionId=' + regionId + '&storeId=' + storeId + '&entityContainerName=' + entityContainerName + '&entityLabelName=' + entityLabelName + '&figureDate=' + figureDate;
        makeAjaxCall(callback);
    };
    getChannelEntityContainerValues = function (entityContainerTitle, callback) {
        restEndPoint = serviceBase + 'Home/GetChannelEntityContainerValues';
        data = 'entityContainerTitle=' + entityContainerTitle;
        makeAjaxCall(callback);
    };
    getCurriculaItems = function (callback) {
        restEndPoint = serviceBase + 'Home/GetCurriculaItems';
        data = '';
        makeAjaxCall(callback);
    };
    getCurriculumTraining = function (curriculumId, callback) {
        restEndPoint = serviceBase + 'Home/GetCurriculumTraining';
        data = 'curriculumId=' + curriculumId;
        makeAjaxCall(callback);
    };
    getIndividuals = function (trainingId, callback) {
        restEndPoint = serviceBase + 'Home/GetIndividuals';
        data = 'trainingId=' + trainingId;
        makeAjaxCall(callback);
    };
    calculateConnectionsAsPercentage = function (entityContainerTitles, callback) {
        restEndPoint = serviceBase + 'Home/CalculateConnectionsAsPercentage';
        data = 'entityContainerTitles=' + entityContainerTitles;
        makeAjaxCall(callback);
    };
    getChannelCompetence = function (callback) {
        restEndPoint = serviceBase + 'Home/GetChannelCompetence';
        data = '';
        makeAjaxCall(callback);
    };
    getClusterSummary = function (clusterKey, callback) {
        restEndPoint = serviceBase + 'Home/GetClusterSummary';
        data = 'clusterKey=' + clusterKey;
        makeAjaxCall(callback);
    };
    getActivityScoreContainers = function (activityScoreContainerKey, callback) {
        restEndPoint = serviceBase + 'Home/GetActivityScoreContainers';
        data = 'activityScoreContainerKey=' + activityScoreContainerKey;
        makeAjaxCall(callback);
    };
    getActivityScores = function (activityScoreContainerKeys, page, pageSize, callback) {
        restEndPoint = serviceBase + 'Home/GetActivityScores';
        data = 'activityScoreContainerKeys=' + activityScoreContainerKeys + '&page=' + page + '&pageSize=' + pageSize;
        makeAjaxCall(callback);
    };
    getActivityContainerPath = function (domainObjectId, containerPath, callback) {
        restEndPoint = serviceBase + 'Home/GetActivityContainerPath';
        data = 'domainObjectId=' + domainObjectId + '&containerPath=' + containerPath;
        makeAjaxCall(callback);
    };
    getEmployeeActivityTypePath = function (domainObjectId, callback) {
        restEndPoint = serviceBase + 'Home/GetEmployeeActivityTypePath';
        data = 'domainObjectId=' + domainObjectId;
        makeAjaxCall(callback);
    };
    getCelebrateData = function (callback) {
        restEndPoint = serviceBase + 'Home/GetCelebrateData';
        data = '';
        makeAjaxCall(callback);
    };
    extractCelebrateData = function (fileName, pageSize, pageNumber, callback) {
        restEndPoint = serviceBase + 'Home/ExtractCelebrateData';
        data = 'fileName=' + fileName + '&pageSize=' + pageSize + '&pageNumber=' + pageNumber;
        makeAjaxCall(callback);
    };
    saveCelebrateData = function (fileName, monthName, callback) {
        restEndPoint = serviceBase + 'Home/SaveCelebrateData';
        data = 'fileName=' + fileName + '&monthName=' + monthName;
        makeAjaxCall(callback);
    };
    getDomainObjectsByType = function (domainObjectTypeId, callback) {
        restEndPoint = serviceBase + 'Home/GetDomainObjectsByType';
        data = 'domainObjectTypeId=' + domainObjectTypeId;
        makeAjaxCall(callback);
    };
    getActivityScoreContainer = function (parentActivityScoreContainer, childActivityScoreContainer, callback) {
        restEndPoint = serviceBase + 'Home/GetActivityScoreContainer';
        data = 'parentActivityScoreContainer=' + parentActivityScoreContainer + '&childActivityScoreContainer=' + childActivityScoreContainer;
        makeAjaxCall(callback);
    };
    getActivityScoreContainerPath = function (activityScoreContainerPath, callback) {
        restEndPoint = serviceBase + 'Home/GetActivityScoreContainerByPath';
        data = 'activityScoreContainerPath=' + activityScoreContainerPath;
        makeAjaxCall(callback);
    };
    getActivityScoreContainerSummary = function (activityScoreContainerSummaryKey, callback) {
        restEndPoint = serviceBase + 'Home/GetActivityScoreContainerSummary';
        data = 'activityScoreContainerSummaryKey=' + activityScoreContainerSummaryKey;
        makeAjaxCall(callback);
    };
    getEntityDashboard = function (entityDashboardKey, callback) {
        restEndPoint = serviceBase + 'Home/GetEntityDashboard';
        data = 'entityDashboardKey=' + entityDashboardKey;
        makeAjaxCall(callback);
    };
    getConnectionsChart = function (entityDashboardKey, connectionTypeName, callback) {
        restEndPoint = serviceBase + 'Home/GetConnectionsChart';
        data = 'entityDashboardKey=' + entityDashboardKey + '&connectionTypeName=' + connectionTypeName;
        makeAjaxCall(callback);
    };
    getCompetenceChart = function (entityDashboardKey, monthRange, callback) {
        restEndPoint = serviceBase + 'Home/GetCompetenceChart';
        data = 'entityDashboardKey=' + entityDashboardKey + '&monthRange=' + monthRange;
        makeAjaxCall(callback);
    };
    getCurriculumCompetence = function (entityDashboardKey, curriculum, monthRange, callback) {
        restEndPoint = serviceBase + 'Home/GetCurriculumCompetence';
        data = 'entityDashboardKey=' + entityDashboardKey + '&curriculum=' + curriculum + '&monthRange=' + monthRange;
        makeAjaxCall(callback);
    };
    getModuleCompetence = function (entityDashboardKey, module, monthRange, callback) {
        restEndPoint = serviceBase + 'Home/GetModuleCompetence';
        data = 'entityDashboardKey=' + entityDashboardKey + '&module=' + module + "&monthRange=" + monthRange;
        makeAjaxCall(callback);
    };
    getActivityCompetence = function (entityDashboardKey, activityName, monthRange, callback) {
        restEndPoint = serviceBase + 'Home/GetActivityCompetence';
        data = 'entityDashboardKey=' + entityDashboardKey + '&activityName=' + activityName + "&monthRange=" + monthRange;
        makeAjaxCall(callback);
    };
    getActivityType = function (entityDashboardKey, activityType, activityItem, activityTypeParent, monthRange, callback) {
        restEndPoint = serviceBase + 'Home/GetActivityType';
        data = 'entityDashboardKey=' + entityDashboardKey + '&activityType=' + activityType + '&activityItem=' + activityItem + '&activityTypeParent=' + activityTypeParent + '&monthRange=' + monthRange;
        makeAjaxCall(callback);
    };
    getProductCSI = function (entityDashboardKey, productName, callback) {
        restEndPoint = serviceBase + 'Home/GetProductCSI';
        data = 'entityDashboardKey=' + entityDashboardKey + '&productName=' + encodeURIComponent(productName);
        makeAjaxCall(callback);
    };
    getLearnerDataItems = function (page, pageSize, dashboardEntityType, dashboardEntityName, callback) {
        restEndPoint = serviceBase + 'Home/GetLearnerDataItems';
        data = 'page=' + page + '&pageSize=' + pageSize + '&dashboardEntityType=' + dashboardEntityType + '&dashboardEntityName=' + dashboardEntityName;
        makeAjaxCall(callback);
    };
    get8taEntity = function (domainObjectUniqueKey, callback) {
        restEndPoint = serviceBase + 'Home/Get8taEntity';
        data = 'domainObjectUniqueKey=' + domainObjectUniqueKey;
        makeAjaxCall(callback);
    };
    getDashboardByTypeByName = function (dashboardEntityType, dashboardEntityName, callback) {
        restEndPoint = serviceBase + 'Home/GetDashboardByTypeByName';
        data = 'dashboardEntityType=' + dashboardEntityType + '&dashboardEntityName=' + dashboardEntityName;
        makeAjaxCall(callback);
    };
    getDashboard = function (domainObjectBreadCrumbKeys, callback) {
        restEndPoint = serviceBase + 'Home/GetDashboard';
        data = "{'domainObjectBreadCrumbKeys':" + JSON.stringify(domainObjectBreadCrumbKeys) + "}";
        makeJsonDataAjaxCall(callback);
    };
    getEmployee = function (domainObjectBreadCrumbKeys, callback) {
        restEndPoint = serviceBase + 'Home/GetEmployee';
        data = "{'domainObjectBreadCrumbKeys':" + JSON.stringify(domainObjectBreadCrumbKeys) + "}";
        makeJsonDataAjaxCall(callback);
    };
    getConnectionDashboardByName = function (connectionDashboardName, callback) {
        restEndPoint = serviceBase + 'Home/GetConnectionDashboardByName';
        data = 'connectionDashboardName=' + connectionDashboardName;
        makeAjaxCall(callback);
    };
    getConnectionDashboards = function (callback) {
        restEndPoint = serviceBase + 'Home/GetConnectionDashboards';
        data = '';
        makeAjaxCall(callback);
    };
    getConnectionContainers = function (callback) {
        restEndPoint = serviceBase + 'Home/GetConnectionContainers';
        data = '';
        makeAjaxCall(callback);
    };
    getConnectionDatasetType = function (domainObjectBreadCrumbKeys, domainObjectUniqueKey, selectedDatasetName, datasetType, callback) {
        restEndPoint = serviceBase + 'Home/GetConnectionDatasetType';
        data = "{'domainObjectBreadCrumbKeys':" + JSON.stringify(domainObjectBreadCrumbKeys) + ",'domainObjectUniqueKey':" + JSON.stringify(domainObjectUniqueKey) + ",'selectedDatasetName':" + JSON.stringify(selectedDatasetName) + ",'datasetType':" + JSON.stringify(datasetType) + "}";
        makeJsonDataAjaxCall(callback);
    };
    getConnectionContainer = function (domainObjectBreadCrumbKeys, callback) {
        restEndPoint = serviceBase + 'Home/GetConnectionContainer';
        data = "{'domainObjectBreadCrumbKeys':" + JSON.stringify(domainObjectBreadCrumbKeys) + "}";
        makeJsonDataAjaxCall(callback);
    };
    getTelkomMobileCurriculumActivities = function (callback) {
        restEndPoint = serviceBase + 'Home/GetTelkomMobileCurriculumActivities';
        data = '';
        makeAjaxCall(callback);
    };
    toggleActivityInCurriculum = function (domainObjectUniqueKey, isInCurriculum, callback) {
        restEndPoint = serviceBase + 'Home/ToggleActivityInCurriculum';
        data = 'domainObjectUniqueKey=' + domainObjectUniqueKey + '&isInCurriculum=' + isInCurriculum;
        makeAjaxCall(callback);
    };
    updateActivityInCurriculum = function (checkItems, callback) {
        restEndPoint = serviceBase + 'Home/UpdateActivityInCurriculum';
        data = "{'checkItems':" + JSON.stringify(checkItems) + "}";
        makeJsonDataAjaxCall(callback);
    };
    saveTelkomActivity = function (activityName, callback) {
        restEndPoint = serviceBase + 'Home/SaveTelkomActivity';
        data = 'activityName=' + activityName;
        makeAjaxCall(callback);
    };
    saveSurvey = function (surveyName, callback) {
        restEndPoint = serviceBase + 'Home/SaveSurvey';
        data = 'surveyName=' + surveyName;
        makeAjaxCall(callback);
    };
    getSurveys = function (callback) {
        restEndPoint = serviceBase + 'Home/GetSurveys';
        data = '';
        makeAjaxCall(callback);
    };
    getDashboardContainer = function (entityName, callback) {
        restEndPoint = serviceBase + 'Home/GetDashboardContainer';
        data = 'entityName=' + entityName;
        makeAjaxCall(callback);
    };
    getDashboardContainerObject = function (domainObjectBreadCrumbKeys, callback) {
        restEndPoint = serviceBase + 'Home/GetDashboardContainerObject';
        data = "{'domainObjectBreadCrumbKeys':" + JSON.stringify(domainObjectBreadCrumbKeys) + "}";
        makeJsonDataAjaxCall(callback);
    };
    getDomainObjectsByTypeGroupByField = function (domainObjectTypeName, fieldName, callback) {
        restEndPoint = serviceBase + 'Home/GetDomainObjectsByTypeGroupByField';
        data = 'domainObjectTypeName=' + domainObjectTypeName + '&fieldName=' + fieldName;
        makeAjaxCall(callback);
    };
    getDomainObjectsByTypeName = function (domainObjectTypeName, callback) {
        restEndPoint = serviceBase + 'Home/GetDomainObjectsByTypeName';
        data = 'domainObjectTypeName=' + domainObjectTypeName;
        makeAjaxCall(callback);
    };
    getSkillPortUserGroupMetrics = function (callback) {
        restEndPoint = serviceBase + 'Home/GetSkillPortUserGroupMetrics';
        data = '';
        makeAjaxCall(callback);
    };
    getSkillPortUserGroupEvaluationMetrics = function (callback) {
        restEndPoint = serviceBase + 'Home/GetSkillPortUserGroupEvaluationMetrics';
        data = '';
        makeAjaxCall(callback);
    };
    getSkillPortUserGroupMetric = function (domainObjectUniqueKey, chartType, callback) {
        restEndPoint = serviceBase + 'Home/GetSkillPortUserGroupMetric';
        data = 'domainObjectUniqueKey=' + domainObjectUniqueKey + '&chartType=' + chartType;
        makeAjaxCall(callback);
    };
    getSkillPortUserGroupEvaluationMetric = function (domainObjectUniqueKey, callback) {
        restEndPoint = serviceBase + 'Home/GetSkillPortUserGroupEvaluationMetric';
        data = 'domainObjectUniqueKey=' + domainObjectUniqueKey + '&chartType=' + chartType;
        makeAjaxCall(callback);
    };
    getSkillPortUserGroupEvaluationMetricByOrgCode = function (skillPortUserGroupOrgCode, callback) {
        restEndPoint = serviceBase + 'Home/GetSkillPortUserGroupEvaluationMetricByOrgCode';
        data = 'skillPortUserGroupOrgCode=' + skillPortUserGroupOrgCode + '&chartType=' + chartType;
        makeAjaxCall(callback);
    };
    getSkillPortUserGroupMetricByOrgCode = function (skillPortUserGroupOrgCode, callback) {
        restEndPoint = serviceBase + 'Home/GetSkillPortUserGroupMetricByOrgCode';
        data = 'skillPortUserGroupOrgCode=' + skillPortUserGroupOrgCode + '&chartType=' + chartType;
        makeAjaxCall(callback);
    };
    updateSkillPortLPPrice = function (parentDomainObjectUniqueKey, domainObjectUniqueKey, facilitatedPrice, eLearningPrice, callback) {
        restEndPoint = serviceBase + 'Home/UpdateSkillPortLPPrice';
        data = 'parentDomainObjectUniqueKey=' + parentDomainObjectUniqueKey + '&domainObjectUniqueKey=' + domainObjectUniqueKey + '&facilitatedPrice=' + facilitatedPrice + '&eLearningPrice=' + eLearningPrice;
        makeAjaxCall(callback);
    };
    getSkillPortLearningProgramMetric = function (groupOrgCode, groupName, learningProgramId, domainObjectUniqueKey, chartType, callback) {
        restEndPoint = serviceBase + 'Home/GetSkillPortLearningProgramMetric';
        data = 'groupOrgCode=' + groupOrgCode + '&groupName=' + groupName + '&learningProgramId=' + learningProgramId + '&domainObjectUniqueKey=' + domainObjectUniqueKey + '&chartType=' + chartType;
        makeAjaxCall(callback);
    };
    getSkillPortLearningProgramEvaluationMetric = function (domainObjectUniqueKey, groupName, callback) {
        restEndPoint = serviceBase + 'Home/GetSkillPortLearningProgramEvaluationMetric';
        data = 'domainObjectUniqueKey=' + domainObjectUniqueKey + '&chartType=' + chartType + '&groupName=' + groupName;
        makeAjaxCall(callback);
    };
    saveTelkomMobileImport = function (importType, importFileName, dataSheetName, importSchema, callback) {
        restEndPoint = serviceBase + 'Home/SaveTelkomMobileImport';
        data = 'importType=' + importType + '&importFileName=' + importFileName + '&dataSheetName=' + dataSheetName + '&importSchema=' + importSchema;
        makeAjaxCall(callback);
    };
    updateTelkomMobileImport = function (domainObjectUniqueKey, importFileName, dataSheetName, importSchema, callback) {
        restEndPoint = serviceBase + 'Home/UpdateTelkomMobileImport';
        data = 'domainObjectUniqueKey=' + domainObjectUniqueKey + '&importFileName=' + importFileName + '&dataSheetName=' + dataSheetName + '&importSchema=' + importSchema;
        makeAjaxCall(callback);
    };
    getLearningContent = function (callback) {
        restEndPoint = serviceBase + 'Home/GetLearningContent';
        data = '';
        makeAjaxCall(callback);
    };
    applicationLogin = function (logOnModel, callback) {
        restEndPoint = serviceBase + 'Home/ApplicationLogin';
        data = "{'logOnModel':" + JSON.stringify(logOnModel) + "}";
        makeJsonDataAjaxCall(callback);
    };
    getSkillPortCurricula = function (callback) {
        restEndPoint = serviceBase + 'Home/GetSkillPortCurricula';
        data = '';
        makeAjaxCall(callback);
    };
    saveSkillPortCurriculumStart = function (domainObjectUniqueKey, callback) {
        restEndPoint = serviceBase + 'Home/SaveSkillPortCurriculumStart';
        data = 'domainObjectUniqueKey=' + domainObjectUniqueKey;
        makeAjaxCall(callback);
    };
    saveMenuItem = function (menuItemTitle, applicationName, callback) {
        restEndPoint = serviceBase + 'Home/SaveMenuItem';
        data = 'menuItemTitle=' + menuItemTitle + '&applicationName=' + applicationName;
        makeAjaxCall(callback);
    };
    getTelkommobileOrganisationCSIPeriod = function (organisationName, callback) {
        restEndPoint = serviceBase + 'Home/GetTelkommobileOrganisationCSIPeriod';
        data = 'organisationName=' + organisationName;
        makeAjaxCall(callback);
    };
    getTelkommobileClusterCSIPeriod = function (organisationName, clusterName, callback) {
        restEndPoint = serviceBase + 'Home/GetTelkommobileClusterCSIPeriod';
        data = 'organisationName=' + organisationName + '&clusterName=' + clusterName;
        makeAjaxCall(callback);
    };
    saveDomainObjectField = function (domainObjectUniqueKey, fieldName, fieldValue, callback) {
        restEndPoint = serviceBase + 'Home/SaveDomainObjectField';
        data = 'domainObjectUniqueKey=' + domainObjectUniqueKey + '&fieldName=' + fieldName + '&fieldValue=' + fieldValue;
        makeAjaxCall(callback);
    };
    deleteDomainObject = function (domainObjectUniqueKey, domainObjectTypeName, callback) {
        restEndPoint = serviceBase + 'Home/DeleteDomainObject';
        data = 'domainObjectUniqueKey=' + domainObjectUniqueKey + '&domainObjectTypeName=' + domainObjectTypeName;
        makeAjaxCall(callback);
    };
    getMenuItems = function (applicationName, callback) {
        restEndPoint = serviceBase + 'Home/GetMenuItems';
        data = 'applicationName=' + applicationName;
        makeAjaxCall(callback);
    };
    updateMenuitemOrder = function (domainObjectUniqueKey, order, applicationName, callback) {
        restEndPoint = serviceBase + 'Home/UpdateMenuitemOrder';
        data = "domainObjectUniqueKey=" + domainObjectUniqueKey + "&order=" + order + "&applicationName=" + applicationName;
        makeAjaxCall(callback);
    };
    deleteMenuItem = function (domainObjectUniqueKey, applicationName, callback) {
        restEndPoint = serviceBase + 'Home/DeleteMenuItem';
        data = "domainObjectUniqueKey=" + domainObjectUniqueKey + "&applicationName=" + applicationName;
        makeAjaxCall(callback);
    };
    getTelkomMobileConnectionContainer = function (financialYear, container, productName, month, callback) {
        restEndPoint = serviceBase + 'Home/GetTelkomMobileConnectionContainer';
        data = "financialYear=" + financialYear + "&container=" + container + "&productName=" + productName + "&month=" + month;
        makeAjaxCall(callback);
    };
    getTelkomMobileChannelReadinessContainer = function (domainObjectUniqueKey, entityName, isRootItem, curriculumIndex, employeePageSize, employeePage, callback) {
        restEndPoint = serviceBase + 'Home/GetTelkomMobileChannelReadinessContainer';
        data = "domainObjectUniqueKey=" + domainObjectUniqueKey + "&entityName=" + entityName + "&curriculumIndex=" + curriculumIndex + "&isRootItem=" + isRootItem + "&employeePageSize=" + employeePageSize + "&employeePage=" + employeePage;
        makeAjaxCall(callback);
    };
    makeJsonDataAjaxCall = function (callback, obj) {
        $.ajax({
            type: "POST",
            url: restEndPoint,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: data,
            success: function (data) {
                callback(data);
            }
        });
    };
    makeAjaxCall = function (callback) {
        $.ajax({
            type: "POST",
            url: restEndPoint,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            dataType: "json",
            data: data,
            success: function (data) {
                callback(data);
                $.unblockUI();
            }
        });
    };
    return {
        getApplicationUserGroups: getApplicationUserGroups,
        saveApplicationUserGroup: saveApplicationUserGroup,
        getApplicationUserGroup: getApplicationUserGroup,
        updateApplicationUserGroupName: updateApplicationUserGroupName,
        getApplicationExistingUsers: getApplicationExistingUsers,
        searchApplicationExistingUsers: searchApplicationExistingUsers,
        saveStreamingUser: saveStreamingUser,
        removeApplicationUserFromGroup: removeApplicationUserFromGroup,
        getApplicationUser: getApplicationUser,
        getApplicationUserMenus: getApplicationUserMenus,
        getNewUserData: getNewUserData,
        updateStreamingUser: updateStreamingUser,
        saveExistingUsersToGroup: saveExistingUsersToGroup,
        searchApplicationUserGroupUsers: searchApplicationUserGroupUsers,
        extractExcelData: extractExcelData,
        saveImportUsers: saveImportUsers,
        saveAllImportUsersToGroup: saveAllImportUsersToGroup,
        getDomainObjectTypes: getDomainObjectTypes,
        getDomainObjectsByDomainObjectTypeId: getDomainObjectsByDomainObjectTypeId,
        getApplicationLibrary: getApplicationLibrary,
        saveExistingLibraryItems: saveExistingLibraryItems,
        getApplicationUserGroupLibrary: getApplicationUserGroupLibrary,
        getAvailableApplicationUserGroupLibraryItems: getAvailableApplicationUserGroupLibraryItems,
        saveApplicationLibraryItemsToApplicationUserGroup: saveApplicationLibraryItemsToApplicationUserGroup,
        getDomainObject: getDomainObject,
        saveDomainObjectsToApplicationUser: saveDomainObjectsToApplicationUser,
        getApplicationUserLibrary: getApplicationUserLibrary,
        removeDomainObjectFromApplicationUser: removeDomainObjectFromApplicationUser,
        getApplicationUserGroupReport: getApplicationUserGroupReport,
        generateViewingSessionItem: generateViewingSessionItem,
        removeDomainObjectApplicationUserGroup: removeDomainObjectApplicationUserGroup,
        getParentDomainObjectsByTypeTitle: getParentDomainObjectsByTypeTitle,
        getCurriculaCollectionRoot: getCurriculaCollectionRoot,
        getCurriculaCollectionContainer: getCurriculaCollectionContainer,
        getExcelSchema: getExcelSchema,
        getExcelSchemaData: getExcelSchemaData,
        importExcelData: importExcelData,
        getChannelData: getChannelData,
        getRegionData: getRegionData,
        getStrategicClusterContainer: getStrategicClusterContainer,
        searchCurriculaCollection: searchCurriculaCollection,
        getApplicationList: getApplicationList,
        getCurriculumContainerById: getCurriculumContainerById,
        curriculumToLib: curriculumToLib,
        curriculumFromLib: curriculumFromLib,
        curriculumBreadCrumbProxy: curriculumBreadCrumbProxy,
        getApplicationLibraryCollection: getApplicationLibraryCollection,
        deleteDomainObjectFromAppLib: deleteDomainObjectFromAppLib,
        getApplicationItem: getApplicationItem,
        getExcelSheetData: getExcelSheetData,
        saveDataImportParameter: saveDataImportParameter,
        deleteDataImportParameter: deleteDataImportParameter,
        runDataImport: runDataImport,
        updateDataImport: updateDataImport,
        getDataImportParameter: getDataImportParameter,
        updateDataImportParameter: updateDataImportParameter,
        getIndividualContainer: getIndividualContainer,
        getIndividualScores: getIndividualScores,
        getDataImports: getDataImports,
        getChannelStoreComparison: getChannelStoreComparison,
        getFilterData: getFilterData,
        filterIndividualScoreData: filterIndividualScoreData,
        getRegionListContainer: getRegionListContainer,
        getStoreListContainer: getStoreListContainer,
        getChannelListContainer: getChannelListContainer,
        getRegionListContainer: getRegionListContainer,
        getStoreListContainer: getStoreListContainer,
        getDateListContainer: getDateListContainer,
        saveConnectionType: saveConnectionType,
        getConnectionTypes: getConnectionTypes,
        saveConnection: saveConnection,
        getConnections: getConnections,
        getChannels: getChannels,
        getEntityValueContainer: getEntityValueContainer,
        saveEntityValue: saveEntityValue,
        getChannelEntityContainerValues: getChannelEntityContainerValues,
        getCurriculaItems: getCurriculaItems,
        getCurriculumTraining: getCurriculumTraining,
        getIndividuals: getIndividuals,
        calculateConnectionsAsPercentage: calculateConnectionsAsPercentage,
        getChannelCompetence: getChannelCompetence,
        getClusterSummary: getClusterSummary,
        getActivityScoreContainers: getActivityScoreContainers,
        getActivityScores: getActivityScores,
        getDomainObjectByKey: getDomainObjectByKey,
        getActivityContainerPath: getActivityContainerPath,
        getEmployeeActivityTypePath: getEmployeeActivityTypePath,
        getCelebrateData: getCelebrateData,
        extractCelebrateData: extractCelebrateData,
        saveCelebrateData: saveCelebrateData,
        getDomainObjectsByType: getDomainObjectsByType,
        getActivityScoreContainer: getActivityScoreContainer,
        getActivityScoreContainerPath: getActivityScoreContainerPath,
        getActivityScoreContainerSummary: getActivityScoreContainerSummary,
        getEntityDashboard: getEntityDashboard,
        getConnectionsChart: getConnectionsChart,
        getCompetenceChart: getCompetenceChart,
        getCurriculumCompetence: getCurriculumCompetence,
        getModuleCompetence: getModuleCompetence,
        getActivityCompetence: getActivityCompetence,
        getActivityType: getActivityType,
        getProductCSI: getProductCSI,
        getLearnerDataItems: getLearnerDataItems,
        get8taEntity: get8taEntity,
        getDashboardByTypeByName: getDashboardByTypeByName,
        getDashboard: getDashboard,
        getEmployee: getEmployee,
        getConnectionDashboardByName: getConnectionDashboardByName,
        getConnectionDashboards: getConnectionDashboards,
        getConnectionContainers: getConnectionContainers,
        getConnectionDatasetType: getConnectionDatasetType,
        getConnectionContainer: getConnectionContainer,
        getTelkomMobileCurriculumActivities: getTelkomMobileCurriculumActivities,
        toggleActivityInCurriculum: toggleActivityInCurriculum,
        saveTelkomActivity: saveTelkomActivity,
        saveSurvey: saveSurvey,
        getSurveys: getSurveys,
        getDashboardContainer: getDashboardContainer,
        getDashboardContainerObject: getDashboardContainerObject,
        updateActivityInCurriculum: updateActivityInCurriculum,
        getDomainObjectsByTypeGroupByField: getDomainObjectsByTypeGroupByField,
        getDomainObjectsByTypeName: getDomainObjectsByTypeName,
        getSkillPortUserGroupEvaluationMetrics: getSkillPortUserGroupEvaluationMetrics,
        getSkillPortUserGroupEvaluationMetric: getSkillPortUserGroupEvaluationMetric,
        getSkillPortLearningProgramEvaluationMetric: getSkillPortLearningProgramEvaluationMetric,
        getSkillPortUserGroupEvaluationMetricByOrgCode: getSkillPortUserGroupEvaluationMetricByOrgCode,
        getSkillPortLearningProgramEvaluationMetric: getSkillPortLearningProgramEvaluationMetric,
        getSkillPortUserGroupMetrics: getSkillPortUserGroupMetrics,
        getSkillPortUserGroupMetric: getSkillPortUserGroupMetric,
        updateSkillPortLPPrice: updateSkillPortLPPrice,
        getSkillPortLearningProgramMetric: getSkillPortLearningProgramMetric,
        getSkillPortUserGroupMetricByOrgCode: getSkillPortUserGroupMetricByOrgCode,
        saveTelkomMobileImport: saveTelkomMobileImport,
        getLearningContent: getLearningContent,
        applicationLogin: applicationLogin,
        getSkillPortCurricula: getSkillPortCurricula,
        saveSkillPortCurriculumStart: saveSkillPortCurriculumStart,
        updateTelkomMobileImport: updateTelkomMobileImport,
        saveMenuItem: saveMenuItem,
        getTelkommobileOrganisationCSIPeriod: getTelkommobileOrganisationCSIPeriod,
        getTelkommobileClusterCSIPeriod: getTelkommobileClusterCSIPeriod,
        saveDomainObjectField: saveDomainObjectField,
        deleteDomainObject: deleteDomainObject,
        getMenuItems: getMenuItems,
        updateMenuitemOrder: updateMenuitemOrder,
        deleteMenuItem: deleteMenuItem,
        getTelkomMobileConnectionContainer: getTelkomMobileConnectionContainer,
        getTelkomMobileChannelReadinessContainer: getTelkomMobileChannelReadinessContainer
    };
} ();
renderApplicationUserGroupsGrid = function (data) {
    $("#content").empty();
    $("#applicationUserGroupsGridTmpl").tmpl(data).appendTo("#content");
};
renderApplicationUserGroupEdit=function(data){
    $("#content").empty();
    $("#applicationUserGroupEditTmpl").tmpl(data).appendTo("#content");
};
renderApplicationUserGroupTitleEdit = function (data) {
    $(".editApplicationUserGroupTitle").hide();
    $(".editApplicationUserGroupTitle").empty();
    $(".updateApplicationUserGroupTitle").show();
    $(".updateApplicationUserGroupTitle").empty();
    $("#applicationUserGroupTitleEditTmpl").tmpl(data).appendTo(".updateApplicationUserGroupTitle");
};
renderApplicationUserMenus = function (data) {
    $("#navigation").empty();
    $("#applicationUserMenu").tmpl(data).appendTo("#navigation");
};
renderApplicationExistingUsers = function (data) {
    $("#content").empty();
    $("#applicationUserGroupExistingUsersTmpl").tmpl(data).appendTo("#content");
};
renderApplicationImportUsers = function (data) {
    $("#content").empty();
    $("#applicationUserGroupImportUsersTmpl").tmpl(data).appendTo("#content");
};
saveApplicationUserGroup = function (applicationId) {
    var applicationUserGroupName = $("input[name='txtApplicationUserGroupName']").val();
    dataService.saveApplicationUserGroup(applicationId, applicationUserGroupName, 1, renderApplicationUserGroupsGrid);
};
updateApplicationUserGroupName = function (applicationUserGroupId) {
    var applicationUserGroupName = $("input[name='applicationUserGroupName']").val();
    dataService.updateApplicationUserGroupName(applicationUserGroupId, applicationUserGroupName, renderApplicationUserGroupEdit);
};
getExistingUsers = function (applicationId, applicationUserGroupId) {
    dataService.getApplicationExistingUsers(applicationId, applicationUserGroupId, '', 1, renderApplicationExistingUsers);
};
cancelSaveExistingUsers = function (applicationId, applicationUserGroupId) {
    dataService.getApplicationUserGroup(applicationId, applicationUserGroupId, 1, renderApplicationUserGroupEdit);
};
saveExistingUsersToGroup = function (applicationId, applicationUserGroupId) {
    var usersSelected = $.trim($("#variables").text());
    dataService.saveExistingUsersToGroup(applicationId, applicationUserGroupId, usersSelected, renderApplicationUserGroupEdit);
};
selectAllEntities = function (obj, chkName) {
    var isChecked = false;
    if ($(obj).attr('checked')) {
        isChecked = $(obj).attr('checked');
    }
    else {
        isChecked = false;
    }
    $('input[name=' + chkName + ']').each(function () {
        $(this).attr("checked", isChecked);
    });
};
$(".gridNextPage").live("click", function () {
    var applicationId = $(this).find("input[name='applicationId']").val();
    var pageNumber = $(this).find("input[name='page']").val(); ;
    dataService.getApplicationUserGroups(applicationId, pageNumber, renderApplicationUserGroupsGrid);
});
$(".gridPrevPage").live("click", function () {
    var applicationId = $(this).find("input[name='applicationId']").val();
    var pageNumber = $(this).find("input[name='page']").val(); ;
    dataService.getApplicationUserGroups(applicationId, pageNumber, renderApplicationUserGroupsGrid);
});

$(".gridUsersNextPage").live("click", function () {
    var searchText = $("input[name='userGroupUsersSearch']").val();
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    if (searchText.length > 0) {
        dataService.searchApplicationExistingUsers(applicationId, applicationUserGroupId, searchText, pageNumber, renderApplicationUserGroupEdit);
    }
    else {
        dataService.getApplicationUserGroup(applicationId, applicationUserGroupId, pageNumber, renderApplicationUserGroupEdit);
    }
});
$(".gridUsersPrevPage").live("click", function () {
    var searchText = $("input[name='userGroupUsersSearch']").val();
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    if (searchText.length > 0) {
        dataService.searchApplicationExistingUsers(applicationId, applicationUserGroupId, searchText, pageNumber, renderApplicationUserGroupEdit);
    }
    else {
        dataService.getApplicationUserGroup(applicationId, applicationUserGroupId, pageNumber, renderApplicationUserGroupEdit);
    }
});

$(".gridExistingUsersPrevPage").live("click", function () {
    var searchText = $("input[name='existingUsersSearch']").val();
    var providerUserKeys = $.trim($("#variables").html());
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    if (searchText.length > 0) {
        dataService.searchApplicationExistingUsers(applicationId, applicationUserGroupId, searchText, pageNumber, renderApplicationExistingUsers);
    }
    else {
        dataService.getApplicationExistingUsers(applicationId, applicationUserGroupId, providerUserKeys, pageNumber, renderApplicationExistingUsers);
    }
});
$(".gridExistingUsersNextPage").live("click", function () {
    var searchText = $("input[name='existingUsersSearch']").val();
    var providerUserKeys = $.trim($("#variables").html());
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    if (searchText.length > 0) {
        dataService.searchApplicationExistingUsers(applicationId, applicationUserGroupId, searchText, pageNumber, renderApplicationExistingUsers);
    }
    else {
        dataService.getApplicationExistingUsers(applicationId, applicationUserGroupId, providerUserKeys, pageNumber, renderApplicationExistingUsers);
    }
});
$(".gridImportUsersPrevPage").live("click", function () {
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    var applicationFile = $("input[name='applicationFileName']").val();
    var selectedUsers = $.trim($("#variables").html());
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.extractExcelData(applicationId, applicationUserGroupId, selectedUsers, applicationFile, pageNumber, renderApplicationImportUsers);
});
$(".gridImportUsersNextPage").live("click", function () {
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    var applicationFile = $("input[name='applicationFileName']").val();
    var selectedUsers = $.trim($("#variables").html());
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.extractExcelData(applicationId, applicationUserGroupId, selectedUsers, applicationFile, pageNumber, renderApplicationImportUsers);
});
$(".gridImportUsersSkipToPage").live("click", function () {
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    var applicationFile = $("input[name='applicationFileName']").val();
    var selectedUsers = $.trim($("#variables").html());
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.extractExcelData(applicationId, applicationUserGroupId, selectedUsers, applicationFile, pageNumber, renderApplicationImportUsers);
});
$(".gridExistingUsersSkipToPage").live("click", function () {
    var searchText = $("input[name='existingUsersSearch']").val();
    var providerUserKeys = $.trim($("#variables").html());
    var applicationId = $(this).find("input[name='applicationId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    if (searchText.length > 0) {
        dataService.searchApplicationExistingUsers(applicationId, applicationUserGroupId, searchText, pageNumber, renderApplicationExistingUsers);
    }
    else {
        dataService.getApplicationExistingUsers(applicationId, applicationUserGroupId, providerUserKeys, pageNumber, renderApplicationExistingUsers);
    }
});
$(".gridUsersSkipToPage").live("click", function () {
    var searchText = $("input[name='userGroupUsersSearch']").val();
    var applicationId = $(this).find("input[name='applicationId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    if (searchText.length > 0) {
        dataService.searchApplicationUserGroupUsers(applicationId, applicationUserGroupId, searchText, pageNumber, renderApplicationUserGroupEdit);
    }
    else {
        dataService.getApplicationUserGroup(applicationId, applicationUserGroupId, pageNumber, renderApplicationUserGroupEdit);
    }
});
$(".gridSkipToPage").live("click", function () {
    var applicationId = $(this).find("input[name='applicationId']").val();
    var pageNumber = $(this).find("input[name='page']").val(); ;
    dataService.getApplicationUserGroups(applicationId, pageNumber, renderApplicationUserGroupsGrid);
});
$(".editApplicationUserGroup").live("click", function () {
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    dataService.getApplicationUserGroup(applicationId, applicationUserGroupId, 1, renderApplicationUserGroupEdit);
});  
$(".editApplicationUserGroupTitle").live("click", function(){
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    dataService.getApplicationUserGroup(applicationUserGroupId, renderApplicationUserGroupTitleEdit);
});
$("input[name='selectAllUsers']").live("click", function () {
    selectAllEntities($(this), 'ApplicationUser');
    var inputs = $("input[name='ApplicationUser']");
    $(inputs).each(function () {
        processCheckBox(this);
    });
    countSelectedUsers();
});
$("input[name='ApplicationUser']").live("click", function () {
    processCheckBox(this);
    countSelectedUsers();
});
$("input[name='existingUsersSearch']").live("keydown", function (e) {
    if (e.keyCode == 13) {
        var applicationId = $(this).parent().find("input[name='applicationId']").val();
        var applicationUserGroupId = $(this).parent().find("input[name='applicationUserGroupId']").val(); ;
        var searchText = $(this).val();
        dataService.searchApplicationExistingUsers(applicationId, applicationUserGroupId, searchText, 1, renderApplicationExistingUsers);
    }
});
$("input[name='userGroupUsersSearch']").live("keydown", function (e) {
    if (e.keyCode == 13) {
        var applicationId = $(this).parent().find("input[name='applicationId']").val();
        var applicationUserGroupId = $(this).parent().find("input[name='applicationUserGroupId']").val(); ;
        var searchText = $(this).val();
        dataService.searchApplicationUserGroupUsers(applicationId, applicationUserGroupId, searchText, 1, renderApplicationUserGroupEdit);
    }
});
existingUserSearch = function (applicationId, applicationUserGroupId) {
    var searchText = $("input[name='existingUsersSearch']").val();
    dataService.searchApplicationExistingUsers(applicationId, applicationUserGroupId, searchText, 1, renderApplicationExistingUsers);
};
existingItemSearch = function (applicationId, domainObjectTypeId) {
    var searchText = $("input[name='existingItemsSearch']").val();
    dataService.getDomainObjectsByDomainObjectTypeId(applicationId, domainObjectTypeId, searchText, '', 1, renderDomainObjectTypeGrid);
};
processCheckBox = function (obj) {
    if ($(obj).attr('checked')) {
        var items = $.trim($("#variables").html().toString());
        var itemIndex = items.indexOf($(obj).val() + ',');
        if (itemIndex === -1)
            $("#variables").append($(obj).val() + ',');
    }
    else {
        var userList = $.trim($("#variables").html());
        userList = userList.replace($(obj).val() + ',', '');
        $("#variables").empty();
        $("#variables").html(userList);
    }
};
countSelectedUsers = function (users) {
    var items = $.trim($("#variables").html());
    var count = 0;
    if (items.indexOf(',') !== -1) {
        var arr = items.split(',');
        count = arr.length - 1;
    }
    else {
        count = 1;
    }
    $("#userSelectedCount").empty();
    $("#userSelectedCount").append("Users Selected: " + count);
    return count;
};
countSelectedItems = function (items) {
    var items = $.trim($("#variables").html());
    var count = 0;
    if (items.indexOf(',') !== -1) {
        var arr = items.split(',');
        count = arr.length - 1;
    }
    else {
        count = 1;
    }
    $("#itemsSelectedCount").empty();
    $("#itemsSelectedCount").append("Items Selected: " + count);
    return count;
}
getNewUserUI = function (applicationId, applicationUserGroupId) {
    dataService.getNewUserData(applicationId, applicationUserGroupId, renderNewUserUI)
};
renderNewUserUI = function (data) {
    $("#content").empty();
    $("#applicationUserNewTmpl").tmpl(data).appendTo("#content");
};
$(".userGroupsBtn").live("click", function () {
    var applicationId = 0;
    applicationId = $(this).parent().find("input[name='applicationId']").val();
    dataService.getApplicationUserGroups(applicationId, 1, renderApplicationUserGroupsGrid);
});
$(".userGroupUsersBtn").live("click", function () {
    var applicationId = 0;
    var applicationUserGroupId = 0;
    applicationId = $(this).parent().find("input[name='applicationId']").val();
    applicationUserGroupId = $(this).parent().find("input[name='applicationUserGroupId']").val();
    dataService.getApplicationUserGroup(applicationId, applicationUserGroupId, 1, renderApplicationUserGroupEdit);
});

removeApplicationUsersFromGroup = function (applicationId, applicationUserGroupId) {
    var userIds = [];
    var userOptions = $("input[name='ApplicationUser']");
    $(userOptions).each(function () {
        if ($(this).attr('checked')) {
            userIds.push($(this).val());
        }
    });
    dataService.removeApplicationUserFromGroup(applicationId, applicationUserGroupId, userIds, renderApplicationUserGroupEdit);
};
$(".gridInfo").live("hover", function (e) {
    $(".gridInfo").removeClass("main-table-text-selected");
    $(this).addClass("main-table-text-selected");
});
$(".gridInfo").live({
    mouseleave:
    function () { $(".gridInfo").removeClass("main-table-text-selected"); }
});
$(".gridInfo").live("click", function () {
    var userKey = $(this).find("input[name='UserKey']").val();
    var applicationId = $(this).find("input[name='ApplicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='ApplicationUserGroupId']").val();
    dataService.getApplicationUser(applicationId, userKey, applicationUserGroupId, 5, renderApplicationUserEditUI);
});
$(".editUser").live("click", function () {
    var userKey = $(this).parent().find("input[name='UserKey']").val();
    var applicationId = $(this).parent().find("input[name='ApplicationId']").val();
    var applicationUserGroupId = $(this).parent().find("input[name='ApplicationUserGroupId']").val();
    dataService.getApplicationUser(applicationId, userKey, applicationUserGroupId, 5, renderApplicationUserEditUI);
});
$(".removeUser").live("click", function () {
    var applicationId = $(this).parent().find("input[name='ApplicationId']").val();
    var applicationUserGroupId = $(this).parent().find("input[name='ApplicationUserGroupId']").val();
    var applicationUserGroupName = $(this).parent().find("input[name='ApplicationUserGroupName']").val();
    var applicationUserName = $(this).parent().find("input[name='ApplicationUserFullName']").val();
    var userKey = $(this).parent().find("input[name='UserKey']").val();
    var dlgUI = $.trim($("#dialog-confirm").html());
    dlgUI = dlgUI.replace("#userName#", applicationUserName);
    dlgUI = dlgUI.replace("#groupName#", applicationUserGroupName);
    $("#dialog-confirm").html(dlgUI);

    $("#dialog:ui-dialog").dialog("destroy");

    $("#dialog-confirm").dialog({
        resizable: false,
        height: 170,
        modal: true,
        buttons: {
            "Remove User": function () {
                dataService.removeApplicationUserFromGroup(applicationId, applicationUserGroupId, userKey, renderApplicationUserGroupEdit);
                $(this).dialog("close");
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });

});
$(".userGroups").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    dataService.getApplicationUserGroups(applicationId, 1, renderApplicationUserGroupsGrid);
});
cancelNewUser = function (applicationId, applicationUserGroupId) {
    dataService.getApplicationUserGroup(applicationUserGroupId, 1, renderApplicationUserGroupEdit);
};
saveUser = function (applicationId, applicationUserGroupId) {
    var firstName = $("input[name='firstname']").val();
    var lastName = $("input[name='lastname']").val();
    var emailAddress = $("input[name='email']").val();
    var title = $("select[name='title']").val();
    var roles = [];
    var roleOptions = $("input[name='applicationUserRole']");
    $(roleOptions).each(function () {
        if ($(this).attr('checked')) {
            roles.push($(this).val());
        }
    });
    dataService.saveStreamingUser(applicationId, applicationUserGroupId, firstName, lastName, emailAddress, title, roles, renderApplicationUserEditUI);
};
updateStreamingUser = function (applicationId, applicationUserGroupId, applicationUserId) {
    var firstName = $("input[name='firstname']").val();
    var lastName = $("input[name='lastname']").val();
    var emailAddress = $("input[name='email']").val();
    var title = $("select[name='title']").val();
    var roles = [];
    var roleOptions = $("input[name='applicationUserRole']");
    $(roleOptions).each(function () {
        if ($(this).attr('checked')) {
            roles.push($(this).val());
        }
    });
    dataService.updateStreamingUser(applicationId, applicationUserGroupId, applicationUserId, firstName, lastName, emailAddress, title, roles, renderApplicationUserEditUI);
};
renderApplicationUserEditUI = function (data) {
    $("#content").empty();
    $("#applicationUserEditTmpl").tmpl(data).appendTo("#content");
};
userGroupUserSearch = function (applicationId, applicationUserGroupId) {
    var searchText = $("input[name='userGroupUsersSearch']").val();
    dataService.searchApplicationUserGroupUsers(applicationId, applicationUserGroupId, searchText, 1, renderApplicationUserGroupEdit);
};
getApplicationUserGroup = function (applicationId, applicationUserGroupId) {
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    dataService.getApplicationUserGroup(applicationId, applicationUserGroupId, 1, renderApplicationUserGroupEdit);
};
getImportUI = function () {
    $("#dialog:ui-dialog").dialog("destroy");

    $("#dialog-ImportUsers").dialog({
        resizable: false,
        height: 270,
        width:400,
        modal: true,
        buttons: {
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });
};
uploadFile = function () {
    $("#uploadProgress").show();
    $("#ExcelForm").submit();
    $("#dialog:ui-dialog").dialog("destroy");
};
$("input[name='ImportUser']").live("click", function () {
    processCheckBox(this);
    countSelectedUsers();
});
cancelSaveImportUsers = function (applicationId, applicationUserGroupId) {
    dataService.getApplicationUserGroup(applicationId, applicationUserGroupId, 1, renderApplicationUserGroupEdit);
};
saveImportUsersToGroup = function (applicationId, applicationUserGroupId) {
    var applicationFile = $("input[name='applicationFileName']").val();
    var selectedUsers = $.trim($("#variables").html());
    dataService.saveImportUsers(applicationId, applicationUserGroupId, selectedUsers, applicationFile, renderApplicationUserGroupEdit);
};
saveAllImportUsersToGroup = function (applicationId, applicationUserGroupId) {
    var applicationFile = $("input[name='applicationFileName']").val();
    dataService.saveAllImportUsersToGroup(applicationId, applicationUserGroupId, applicationFile, renderApplicationUserGroupEdit);
};
$(".mainLibrary").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    dataService.getApplicationLibrary(applicationId, 0, '', 1, '', 10, renderApplicationLibrary);
});
renderApplicationLibrary = function (data) {
    $("#content").empty();
    $("#applicationLibraryTmpl").tmpl(data).appendTo("#content");
};
createNewLibraryItem = function () {
    $("#txtHiddenOperationType").val("new");
    dataService.getDomainObjectTypes(renderDomainObjectTypeModal);
};
addExistingLibraryItem = function () {
    dataService.getDomainObjectTypes(renderDomainObjectTypeModal);
    $("#txtHiddenOperationType").val("existing");
};
renderDomainObjectTypeModal = function (data, dialogUI, title) {
    var dialogTitle = "";
    if ($("#txtHiddenOperationType")) {
        if ($("#txtHiddenOperationType").val() == "new") {
            title = "Select the item type to create";
        }
        else if ($("#txtHiddenOperationType").val() == "existing") {
            title = "Select the item type to add";
        }
    }
    $("#itemTypeDrp").empty();
    $("#domainObjectTypeDrpDownTmpl").tmpl(data).appendTo("#itemTypeDrp");
    $("#dialog-ExistingLibraryItem").dialog({
        resizable: false,
        height: 270,
        width: 400,
        modal: true,
        title: title,
        buttons: {
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });
};
renderDomainObjectTypeGrid = function (data) {
    $("#content").empty();
    var domainObjectTypeName = data.DomainObjectType.DomainObjectTypeName;
    $("#" + domainObjectTypeName + "DomainObjectTypeGridTmpl").tmpl(data).appendTo("#content");
};
$("select[name='itemType']").live("change", function () {
    var domainObjectTypeId = $(this).val();
    var applicationId = $("input[name='applicationId']").val();
    $("#dialog-ExistingLibraryItem").dialog("close");
    dataService.getDomainObjectsByDomainObjectTypeId(applicationId, domainObjectTypeId, '', '', 1, renderDomainObjectTypeGrid);
});
$("input[name='applicationLibraryItem']").live("click", function () {
    processCheckBox(this);
    countSelectedItems();
});
$("input[name='applicationLibraryItems']").live("click", function () {
    selectAllEntities($(this), 'applicationLibraryItem');
    var inputs = $("input[name='applicationLibraryItem']");
    $(inputs).each(function () {
        processCheckBox(this);
    });
    countSelectedItems();
});
clearItemsSearch = function (applicationId, domainObjectTypeId) {
    dataService.getDomainObjectsByDomainObjectTypeId(applicationId, domainObjectTypeId, '', 1, renderDomainObjectTypeGrid);
};
$("input[name='existingItemsSearch']").live("keydown", function (e) {
    if (e.keyCode == 13) {
        var applicationId = $("input[name='applicationId']").val();
        var domainObjectTypeId = $("input[name='domainObjectTypeId']").val();
        var searchText = $(this).val();
        dataService.getDomainObjectsByDomainObjectTypeId(applicationId, domainObjectTypeId, searchText, '', 1, renderDomainObjectTypeGrid);
    }
});
cancelSaveExistingLibraryItem = function (applicationId) {
    dataService.getApplicationLibrary(applicationId, 0, 0, '', 10, renderApplicationLibrary);
};
$(".gridSaveExistingItemPrevPage").live("click", function () {
    var selectedItems = $.trim($("#variables").html());
    var applicationId = $("input[name='applicationId']").val();
    var domainObjectTypeId = $("input[name='domainObjectTypeId']").val();
    var searchText = $("input[name='existingItemsSearch']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getDomainObjectsByDomainObjectTypeId(applicationId, domainObjectTypeId, searchText, selectedItems, pageNumber, renderDomainObjectTypeGrid);
});
$(".gridSaveExistingItemNextPage").live("click", function () {
    var selectedItems = $.trim($("#variables").html());
    var applicationId = $("input[name='applicationId']").val();
    var domainObjectTypeId = $("input[name='domainObjectTypeId']").val();
    var searchText = $("input[name='existingItemsSearch']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getDomainObjectsByDomainObjectTypeId(applicationId, domainObjectTypeId, searchText, selectedItems, pageNumber, renderDomainObjectTypeGrid);
});
$(".gridSaveExistingItemSkipToPage").live("click", function () {
    var selectedItems = $.trim($("#variables").html());
    var applicationId = $("input[name='applicationId']").val();
    var domainObjectTypeId = $("input[name='domainObjectTypeId']").val();
    var searchText = $("input[name='existingItemsSearch']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getDomainObjectsByDomainObjectTypeId(applicationId, domainObjectTypeId, searchText, selectedItems, pageNumber, renderDomainObjectTypeGrid);
});
saveExistingLibraryItems = function (applicationId) {
    var selectedItems = $.trim($("#variables").html());
    dataService.saveExistingLibraryItems(applicationId, selectedItems, renderApplicationLibrary);
};
$(".gridApplicationLibraryNextPage").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getApplicationLibrary(applicationId, 0, '', pageNumber, '', 10, renderApplicationLibrary);
});
$(".gridApplicationLibraryPrevPage").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getApplicationLibrary(applicationId, 0, '', pageNumber, '', 10, renderApplicationLibrary);
});
$(".gridApplicationLibrarySkipToPage").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getApplicationLibrary(applicationId, 0, '', pageNumber,'', 10, renderApplicationLibrary);
});
getApplicationUserGroupLibrary = function (applicationId, applicationUserGroupId) {
    //alert(applicationId + " " + applicationUserGroupId);
    dataService.getApplicationUserGroupLibrary(applicationId, applicationUserGroupId, 0, '', '', 1, 10, renderApplicationUserGroupLibrary);
};
renderApplicationUserGroupLibrary = function (data) {
    $("#content").empty()
    $("#applicationUserGroupLibrary").tmpl(data).appendTo("#content");
};
renderAvailableApplicationUserGroupLibraryItems = function (data) {
    $("#content").empty()
    $("#applicationUserGroupLibraryAddItemsTmpl").tmpl(data).appendTo("#content");
};
addApplicationUserGroupLibraryItem = function (applicationId, applicationUserGroupId) {
    $("#variables").empty();
    dataService.getAvailableApplicationUserGroupLibraryItems(applicationId, applicationUserGroupId, '', '', 1, renderAvailableApplicationUserGroupLibraryItems);
};
applicationLibrarySearch = function (applicationId, applicationUserGroupId) {
    var selectedItems = $.trim($("#variables").html());
    var searchText = $("input[name='applicationLibrarySearch']").val();
    dataService.getAvailableApplicationUserGroupLibraryItems(applicationId, applicationUserGroupId, selectedItems, searchText, 1, renderAvailableApplicationUserGroupLibraryItems);
};
clearApplicationLibrarySearch = function (applicationId, applicationUserGroupId) {
    var selectedItems = $.trim($("#variables").html());
    dataService.getAvailableApplicationUserGroupLibraryItems(applicationId, applicationUserGroupId, selectedItems, '', 1, renderAvailableApplicationUserGroupLibraryItems);
};
$("input[name='applicationLibrarySearch']").live("keydown", function (e) {
    if (e.keyCode == 13) {
        var selectedItems = $.trim($("#variables").html());
        var searchText = $("input[name='applicationLibrarySearch']").val();
        var applicationId = $("input[name='applicationId']").val();
        var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
        dataService.getAvailableApplicationUserGroupLibraryItems(applicationId, applicationUserGroupId, selectedItems, searchText, 1, renderAvailableApplicationUserGroupLibraryItems);
    }
});
$("input[name='applicationUserGroupLibraryItem']").live("click", function () {
    processCheckBox(this);
    countSelectedItems();
});
$("input[name='applicationUserGroupLibraryItems']").live("click", function () {
    selectAllEntities($(this), 'applicationUserGroupLibraryItem');
    var inputs = $("input[name='applicationUserGroupLibraryItem']");
    $(inputs).each(function () {
        processCheckBox(this);
    });
    countSelectedItems();
});
$(".gridAvailableApplicationUserGroupLibraryNextPage").live("click", function () {
    var selectedItems = $.trim($("#variables").html());
    var searchText = $("input[name='applicationLibrarySearch']").val();
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getAvailableApplicationUserGroupLibraryItems(applicationId, applicationUserGroupId, selectedItems, searchText, pageNumber, renderAvailableApplicationUserGroupLibraryItems);
});
$(".gridAvailableApplicationUserGroupLibraryPrevPage").live("click", function () {
    var selectedItems = $.trim($("#variables").html());
    var searchText = $("input[name='applicationLibrarySearch']").val();
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getAvailableApplicationUserGroupLibraryItems(applicationId, applicationUserGroupId, selectedItems, searchText, pageNumber, renderAvailableApplicationUserGroupLibraryItems);
});
$(".gridAvailableApplicationUserGroupLibrarySkipToPage").live("click", function () {
    var selectedItems = $.trim($("#variables").html());
    var searchText = $("input[name='applicationLibrarySearch']").val();
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getAvailableApplicationUserGroupLibraryItems(applicationId, applicationUserGroupId, selectedItems, searchText, pageNumber, renderAvailableApplicationUserGroupLibraryItems);
});
saveApplicationLibraryItemsToApplicationUserGroup = function (applicationId, applicationUserGroupId) {
    var selectedItems = $.trim($("#variables").html());
    dataService.saveApplicationLibraryItemsToApplicationUserGroup(applicationId, applicationUserGroupId, selectedItems, 1, renderApplicationUserGroupLibrary);
};
$(".gridApplicationUserGroupLibraryNextPage").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getApplicationUserGroupLibrary(applicationId, applicationUserGroupId, 0, '', '', pageNumber, 10, renderApplicationUserGroupLibrary);
});
$(".gridApplicationUserGroupLibraryPrevPage").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getApplicationUserGroupLibrary(applicationId, applicationUserGroupId, 0, '', '', pageNumber, 10, renderApplicationUserGroupLibrary);
});
$(".gridApplicationUserGroupLibrarySkipPage").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getApplicationUserGroupLibrary(applicationId, applicationUserGroupId, 0, '', '', pageNumber, 10, renderApplicationUserGroupLibrary);
});
$(".editItem").live("click", function () {
    var domainObjectId = $(this).parent().find("input[name='domainObjectId']").val();
    var applicationId = $("input[name='applicationId']").val();
    dataService.getDomainObject(applicationId, domainObjectId, renderDomainObjectView);
});
renderDomainObjectView = function (data) {
    $("#content").empty();
    $("#" + data.DomainObject.DomainObjectNameSlug + "EditTmpl").tmpl(data).appendTo("#content");
};
addItemToUserLibrary = function (applicationId, applicationUserGroupId, applicationUserId) {
    dataService.getApplicationUserGroupLibrary(applicationId, applicationUserGroupId, applicationUserId, '', '', 1, 5, renderApplicationLibraryUserInsert);
};
renderApplicationLibraryUserInsert = function (data) {
    $("#userLibraryGrid").empty();
    $("#addItemToUserLibraryGridTmpl").tmpl(data).appendTo("#userLibraryGrid");
};
saveItemsToUserLibrary = function () {
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var applicationUserProviderUserKey = $("input[name='applicationUserProviderUserKey']").val();
    var selectedItems = $.trim($("#variables").html());
    dataService.saveDomainObjectsToApplicationUser(applicationId, applicationUserGroupId, applicationUserProviderUserKey, selectedItems, renderApplicationUserEditUI);
};
$("input[name='applicationUserLibraryItem']").live("click", function () {
    processCheckBox($(this));
    countSelectedItems();
});
getApplicationUserLibrary = function (applicationId, applicationUserId) {
    dataService.getApplicationUserLibrary(applicationId, applicationUserId, 0, renderApplicationUserLibrary, 1);
};
renderApplicationUserLibrary = function (data) {
    $("#content").empty();
    $("#applicationUserLibraryTmpl").tmpl(data).appendTo("#content");
};
$(".videoListItem").live("click", function () {
    $("#videoContainer").empty();
    var domainObjectId = $(this).find("input[name='domainObjectId']").val();
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserId = $("input[name='applicationUserId']").val();
    dataService.getApplicationUserLibrary(applicationId, applicationUserId, domainObjectId, renderApplicationUserLibrary, 1);
});
$(".removeApplicationUserLibraryItem").live("click", function () {
    var userKey = $("input[name='applicationUserKey']").val();
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var domainObjectId = $("input[name='domainObjectId']").val();
    dataService.removeDomainObjectFromApplicationUser(applicationId, applicationUserGroupId, userKey, domainObjectId, renderApplicationUserEditUI);
});
cancelSaveItemsToUserLibrary = function (applicationId) {
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var userKey = $("input[name='applicationUserKey']").val();
    dataService.getApplicationUser(applicationId, userKey, applicationUserGroupId, 5, renderApplicationUserEditUI);
};
$(".gridSaveToUserLibraryNextPage").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var applicationUserId = $("input[name='applicationUserId']").val();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getApplicationUserGroupLibrary(applicationId, applicationUserGroupId, applicationUserId, '', '', pageNumber, 5, renderApplicationLibraryUserInsert);
});
$(".gridSaveToUserLibraryPrevPage").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var applicationUserId = $("input[name='applicationUserId']").val();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getApplicationUserGroupLibrary(applicationId, applicationUserGroupId, applicationUserId, '', '', pageNumber, 5, renderApplicationLibraryUserInsert);
});
$(".gridSaveToUserLibrarySkipPage").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var applicationUserId = $("input[name='applicationUserId']").val();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getApplicationUserGroupLibrary(applicationId, applicationUserGroupId, applicationUserId, '', '', pageNumber, 5, renderApplicationLibraryUserInsert);
});
getApplicationUserGroupReport = function (applicationId, applicationUserGroupId) {
    dataService.getApplicationUserGroupReport(applicationId, applicationUserGroupId, 1, 10, renderApplicationUserGroupReport);
};
renderApplicationUserGroupReport = function (data) {
    $("#content").empty();
    $("#applicationUserGroupReportTmpl").tmpl(data).appendTo("#content");
};
$(".gridUserUsageNextPage").live("click", function () {
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getApplicationUserGroupReport(applicationId, applicationUserGroupId, pageNumber, 10, renderApplicationUserGroupReport);
});
$(".gridUserUsagePrevPage").live("click", function () {
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getApplicationUserGroupReport(applicationId, applicationUserGroupId, pageNumber, 10, renderApplicationUserGroupReport);
});
$(".gridUserUsageSkipPage").live("click", function () {
    var applicationId = $(this).find("input[name='applicationId']").val();
    var applicationUserGroupId = $(this).find("input[name='applicationUserGroupId']").val();
    var pageNumber = $(this).find("input[name='page']").val();
    dataService.getApplicationUserGroupReport(applicationId, applicationUserGroupId, pageNumber, 10, renderApplicationUserGroupReport);
});
generateViewingSession = function () {
    var serviceBase = 'http://' + window.location.host + '/Pandora.Web.Public/';
    var providerUserKey = $("#txtProviderUserKey").val();
    var viewingSession = $("#txtViewingSession").val();
    var applicationId = $("#txtApplicationId").val();
    var actualDuration = $("#txtActualDuration").val();
    var itemTitle = $("#txtItemTitle").val();
    var itemProductCode = $("#txtItemProductCode").val();
    var restEndPoint = serviceBase + 'Home/GenerateViewSession';
    var data = 'applicationId=' + applicationId + '&providerUserKey=' + providerUserKey + '&actualDuration=' + actualDuration + '&itemTitle=' + encodeURIComponent(itemTitle) + '&itemProductCode=' + itemProductCode;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (data) {
            $("#txtViewingSession").val(data.DomainObject.DomainObjectUniqueKey);
        }
    });
};
saveViewItem = function () {
    var serviceBase = 'http://' + window.location.host + '/Pandora.Web.Public/';
    var viewSession = $("#txtViewingSession").val();
    var viewItem = $("#txtSegmentKey").val();
    var startTime = $("#txtSegmentStartTime").val();
    var endTime = $("#txtSegmentEndTime").val();
    var restEndPoint = serviceBase + 'Home/SaveViewItem';
    //console.log(viewSession + ' ' + viewItem + ' ' + startTime + ' ' + endTime);
    var data = 'viewSession=' + viewSession + '&viewItemKey=' + viewItem + '&startTime=' + startTime + '&endTime=' + endTime;
    $.ajax({
        type: "POST",
        url: restEndPoint,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType: "json",
        data: data,
        success: function (data) {

        }
    });
};
$(".removeUserGroupLibraryItem").live("click", function () {
    var applicationId = $("input[name='applicationId']").val();
    var applicationUserGroupId = $("input[name='applicationUserGroupId']").val();
    var domainObjectId = $(this).parent().find("input[name='domainObjectLibraryItem']").val();
    dataService.removeDomainObjectApplicationUserGroup(applicationId, applicationUserGroupId, domainObjectId, renderApplicationUserGroupLibrary);
});
getParentDomainObjectsByTypeTitle = function (domainObjectName, domainObjectTitle, secondaryFilters, page, pageSize) {
    dataService.getParentDomainObjectsByTypeTitle(domainObjectName, domainObjectTitle, secondaryFilters, page, pageSize, renderParentDomainObjects);
};
renderParentDomainObjects = function (data) {
    $("#contentArea").empty();
    $("#curriculumCollectionView").tmpl(data).appendTo("#contentArea");
};
$(".gridCurriculaCollectionNextPage").live("click", function () {
    var pageNumber = $(this).find("input[name='page']").val();
    var secondaryFilters = $("input[name='secondaryFilters']").val();
    var curriculumName = $("input[name='initCurriculum']").val();
    getParentDomainObjectsByTypeTitle('Curricula Collection', curriculumName, secondaryFilters, pageNumber, 4);
});
$(".gridCurriculaCollectionPrevPage").live("click", function () {
    var pageNumber = $(this).find("input[name='page']").val();
    var secondaryFilters = $("input[name='secondaryFilters']").val();
    var curriculumName = $("input[name='initCurriculum']").val();
    getParentDomainObjectsByTypeTitle('Curricula Collection', curriculumName, secondaryFilters, pageNumber, 4);
});
$(".gridCurriculaCollectionSkipPage").live("click", function () {
    var pageNumber = $(this).find("input[name='page']").val();
    var secondaryFilters = $("input[name='secondaryFilters']").val();
    var curriculumName = $("input[name='initCurriculum']").val();
    getParentDomainObjectsByTypeTitle('Curricula Collection', curriculumName, secondaryFilters, pageNumber, 4);
});
$(".initCurriculum").live("click", function () {
    var curriculumName = $.trim($(this).text());
    $("input[name='secondaryFilters']").val('');
    $("input[name='initCurriculum']").val(curriculumName);
    getParentDomainObjectsByTypeTitle('Curricula Collection', curriculumName, '', 1, 4);
});
$(".currFilter").live("click", function () {
    var curriculumName = $(this).find("input[type='hidden']").val();
    $("input[name='secondaryFilters']").val($("input[name='secondaryFilters']").val() + ',' + curriculumName);
    getParentDomainObjectsByTypeTitle('Curricula Collection', $("input[name='initCurriculum']").val(), $("input[name='secondaryFilters']").val(), 1, 4);
});
$(".vendorSelected").live("click", function () {
    var curriculumName = $(this).find("input[type='hidden']").val();
    var secondaryFilters = $("input[name='secondaryFilters']").val();
    secondaryFilters = secondaryFilters.replace(curriculumName, '');
    $("input[name='secondaryFilters']").val(secondaryFilters);
    getParentDomainObjectsByTypeTitle('Curricula Collection', $("input[name='initCurriculum']").val(), $("input[name='secondaryFilters']").val(), 1, 4);
});
$(".childCurriculumItem").live("click", function () {
    var curriculumContainerId = $(this).find("input[name='domainObjectKey']").val();
    var applicationId = $(this).find("input[name='applicationId']").val();
    var baseCurricula = $(this).find("input[name='baseCurricula']").val();
    dataService.getCurriculaCollectionContainer(applicationId, baseCurricula, curriculumContainerId, renderCurriculumChildren);
    return false;
});
renderCurriculumChildren = function (data) {
    var curriculumParent = data.CurriculumParent;
    $("#" + curriculumParent).empty();
    $("#curriculumChildren").tmpl(data).appendTo("#" + curriculumParent);
};
renderExcelSchemaData = function (data) {
    $("#importContainer").empty();
    $("#excelSheetImportListTmpl").tmpl(data).appendTo("#importContainer");
};
getExcelSchemaData = function (uniqueKey, fileName) {
    //dataService.getExcelSchemaData(uniqueKey, alert('completed'));
    var inputs = $("#row_" + uniqueKey).find("input[type='text']");
    var itemValues = [];
    $(inputs).each(function () {
        var itemName = $(this).attr("name");
        var itemValue = $(this).val();
        itemValues.push(itemName + '|' + itemValue);
    });
    dataService.getExcelSchemaData(fileName, uniqueKey, itemValues, 1, 20, renderExcelSchemaDataItem);
};
renderExcelSchemaDataItem = function (data) {
    $("#individualsResultTmpl").tmpl(data).appendTo("#individualsResult");
};
importExcelData = function (uniqueKey, fileName) {
    var inputs = $("#row_" + uniqueKey).find("input[type='text']");
    var itemValues = [];
    $(inputs).each(function () {
        var itemName = $(this).attr("name");
        var itemValue = $(this).val();
        itemValues.push(itemName + '|' + itemValue);
    });
    dataService.importExcelData(fileName, uniqueKey, itemValues, renderImportedDataGrid);
};
renderImportedDataGrid = function (data) {
    alert("finished");
};
getChannelData = function () {
    var userName = $("input[name='userName']").val();
    var applicationId = $("input[name='applicationId']").val();
    dataService.getChannelData(applicationId, userName, renderChannelDataUI);
};
renderChannelDataUI = function (data) {
    //window.scrollTo(0, 0);
    $("#dataContainer").empty();
    $("#channelResultTmpl").tmpl(data).appendTo("#dataContainer");
//    if (typeof data.GraphOptions !== "undefined") {
//        if (data.GraphOptions !== null) {
//            options.series = data.GraphOptions.series;
//            options.xAxis.categories = data.GraphOptions.categories;
//            var chart = new Highcharts.Chart(options);
//        }
//    }
};
$(".clusterContainer").live("click", function () {
    var channelId = 0;
    var regionId = 0;
    var storeId = 0;

    if ($(this).parent().find("input[name='channelId']").length)
        channelId = $(this).parent().find("input[name='channelId']").val();
    else
        channelId = 0;

    if ($(this).parent().find("input[name='regionId']").length)
        regionId = $(this).parent().find("input[name='regionId']").val();
    else
        regionId = 0;

    if ($(this).parent().find("input[name='storeId']").length)
        storeId = $(this).parent().find("input[name='storeId']").val();
    else
        storeId = 0;

    dataService.getStrategicClusterContainer(channelId, regionId, storeId, renderChannelDataUI);
    return false;
});
searchCurriculaCollection = function (baseContainer) {
    var searchText = $("input[name='curriculumSearch']").val();
    dataService.searchCurriculaCollection(baseContainer, searchText, renderCurriculaSearch);
};
$("input[name='curriculumSearch']").live("keydown", function (e) {
    if (e.keyCode == 13) {
        var searchText = $("input[name='curriculumSearch']").val();
        var baseContainer = $(this).parent().find("input[type='hidden']").val();
        dataService.searchCurriculaCollection(baseContainer, searchText, renderCurriculaSearch);
    }
});
renderCurriculaSearch = function (data) {
    $("#curriculumListContainer").empty();
    $("#curriculumChildren").tmpl(data).appendTo("#curriculumListContainer");
};
$(".curriculumToLib").live("click", function () {
    var breadCrumb = $("input[name='breadCrumbItems']").val();
    var curriculumKey = $(this).parent().find("input[name='curriculumKey']").val();
    var domainObjectUniqueKey = $(this).parent().find("input[name='domainObjectUniqueKey']").val();

    var applicationId = $("#tr_" + domainObjectUniqueKey).find("input[name='applicationId']").val();
    var domainObjectId = $("#tr_" + domainObjectUniqueKey).find("input[name='parentCurriculum']").val();
    var baseCurricula = $("#tr_" + domainObjectUniqueKey).find("input[name='baseCurricula']").val();  
    dataService.curriculumToLib(applicationId, baseCurricula, domainObjectId, curriculumKey, breadCrumb, renderCurriculumGridChildren);
    return false;
});
$(".curriculumFromLib").live("click", function () {
    var breadCrumb = $("input[name='breadCrumbItems']").val();
    var curriculumKey = $(this).parent().find("input[name='curriculumKey']").val();
    var domainObjectUniqueKey = $(this).parent().find("input[name='domainObjectUniqueKey']").val();

    var applicationId = $("#tr_" + domainObjectUniqueKey).find("input[name='applicationId']").val();
    var domainObjectId = $("#tr_" + domainObjectUniqueKey).find("input[name='parentCurriculum']").val();
    var baseCurricula = $("#tr_" + domainObjectUniqueKey).find("input[name='baseCurricula']").val();
    dataService.curriculumFromLib(applicationId, baseCurricula, domainObjectId, curriculumKey, breadCrumb, renderCurriculumGridChildren);
    return false;
});
renderCurriculumToLib = function (data) {
    $("#applicationLibrary" + data.DomainObject.DomainObjectId).empty();
    $("#curriculumOutputTmpl").tmpl(data).appendTo("#outputArea");
};
getApplicationCurriculaCollection = function (applicationId) {
    $("#curriculumOutputTmpl").tmpl(data).appendTo("#outputArea");
};
$(".gridChildItem").live("click", function () {
    var breadCrumb = $("input[name='breadCrumbItems']").val();
    var curriculumContainerId = $(this).parent().find("input[name='domainObjectKey']").val();
    var applicationId = $(this).parent().find("input[name='applicationId']").val();
    var baseCurricula = $(this).parent().find("input[name='baseCurricula']").val();
    dataService.getCurriculaCollectionContainer(applicationId, baseCurricula, curriculumContainerId, breadCrumb, renderCurriculumGridChildren);
});
renderCurriculumGridChildren = function (data) {
    $("#curriculumGrid").empty();
    $("#curriculumGridTmpl").tmpl(data).appendTo("#curriculumGrid");
};
$(".breadCrumbContainer").live("click", function () {
    if ($(this).parent()[0].tagName == "LI") {
        $(".nav-list > li").removeClass("active");
        $($(this).parent()[0]).addClass("active");
    }
    var entityType = $(this).parent().find("input[name='entityType']").val();
    var entityId = $(this).parent().find("input[name='entityId']").val();
    var applicationId = $(this).parent().find("input[name='applicationId']").val();
    var baseCurricula = $(this).parent().find("input[name='baseCurricula']").val();
    var breadCrumb = $("input[name='breadCrumbItems']").val();
    dataService.curriculumBreadCrumbProxy(applicationId, baseCurricula, breadCrumb, entityType, entityId, renderCurriculumGridChildren);
});
$(".remDomainObjectFromAppLibrary").live("click", function () {
    var applicationId = $(this).parent().find("input[name='applicationId']").val();
    var domainObjectId = $(this).parent().find("input[name='domainObjectId']").val();
    dataService.deleteDomainObjectFromAppLib(applicationId, domainObjectId, renderApplicationLibraryGridUI);
});
getApplicationLibraryCollection = function (applicationId) {
    dataService.getApplicationLibraryCollection(applicationId, renderApplicationLibraryGridUI);
};
renderApplicationLibraryGridUI = function (data) {
    $("#curriculumGrid").empty();
    $("#applicationLibraryGridTmpl").tmpl(data).appendTo("#curriculumGrid");
};
$(".applicationGridChildItem").live("click", function () {
    var applicationId = $(this).parent().find("input[name='applicationId']").val();
    var domainObjectTypeName = $(this).parent().find("input[name='domainObjectType']").val();
    var domainObjectId = $(this).parent().find("input[name='domainObjectId']").val();
    dataService.getApplicationItem(applicationId, domainObjectId, renderCurriculumGridChildren);
});
$(".importChannel").live("click", function () {
    var fileName = $(this).find("input[name='fileName']").val();
    var sheetName = $(this).find("input[name='sheetName']").val();
    getExcelSheetData(fileName, sheetName, renderSheetUI);
});
renderSheetUI = function (data) {
    $("#importContainer").empty();
    $("#excelSheetChannelTmpl").tmpl(data).appendTo("#importContainer");
};
$(".importConnections").live("click", function () {
    var fileName = $(this).find("input[name='fileName']").val();
    var sheetName = $(this).find("input[name='sheetName']").val();
    getExcelSheetData(fileName, sheetName, renderSheetUI);
});
newParameter = function (dataImportId) {
    $("#dialog:ui-dialog").dialog("destroy");

    $("#dialog-newParameter").dialog({
        resizable: false,
        height: 370,
        width: 400,
        modal: true,
        buttons: {
            Cancel: function () {
                $(this).dialog("close");
            },
            Save: function () {
                var parameterName = $("input[name='parameterName']").val();
                var parameterType = $("input[name='parameterType']").val();
                var parameterValue = $("input[name='parameterValue']").val();
                var cellLookup = $("input[name='cellLookup']").prop("checked");
                var dataSetLookup = $("input[name='dataSetLookup']").prop("checked");
                dataService.saveDataImportParameter(dataImportId, parameterName, parameterType, parameterValue, cellLookup, dataSetLookup, renderSheetUI);
                $(this).dialog("close");
            }
        }
    });
};
saveDataImportParameter = function (dataImportId) {
    var parameterName = $("input[name='parameterName']").val();
    var parameterType = $("input[name='parameterType']").val();
    var parameterValue = $("input[name='parameterValue']").val();
    var cellLookup = $("input[name='cellLookup']").prop("checked");
    var dataSetLookup = $("input[name='dataSetLookup']").prop("checked");
    dataService.saveDataImportParameter(dataImportId, parameterName, parameterType, parameterValue, cellLookup, dataSetLookup, renderSheetUI);
};
deleteParameter = function (dataImportId, dataImportParameterId) {
    dataService.deleteDataImportParameter(dataImportId, dataImportParameterId, renderSheetUI);
};
editParameter = function (dataImportId, dataImportParameterId) {
    dataService.getDataImportParameter(dataImportId, dataImportParameterId, renderDataImportParameterUI);
};
runDataImport = function (dataImportId, importName, dataFile) {
    dataService.runDataImport(dataImportId, importName, dataFile, renderSheetUI);
};
renderDataImportUI = function (data) {
    alert('done');
};
updateDataImport = function (dataImportId) {
    var dataSetStartRange = $("input[name='dataSetStartRange']").val();
    var dataSetEndRange = $("input[name='dataSetEndRange']").val();
    var dataSetName = $("input[name='dataSetName']").val();
    var organisationName = $("input[name='organisationName']").val();
    dataService.updateDataImport(dataImportId, organisationName, dataSetName, dataSetStartRange, dataSetEndRange, renderSheetUI);
};
renderDataImportParameterUI = function (data) {
    $("#parameterEditUI").empty();
    $("#dataImportParameterEditTmpl").tmpl(data).appendTo("#parameterEditUI");
    $("#dialog:ui-dialog").dialog("destroy");

    $("#dialog-editParameter").dialog({
        resizable: false,
        height: 370,
        width: 400,
        modal: true,
        buttons: {
            Cancel: function () {
                $(this).dialog("close");
            },
            Update: function () {
                $("#dialog:ui-dialog").dialog("destroy");
                var dataImportId = data.DataImport.DataImportId;
                var dataImportParameterId = data.DataImportParameter.DataImportParameterId;
                var parameterName = $("#parameterEditUI input[name='parameterName']").val();
                var parameterType = $("#parameterEditUI input[name='parameterType']").val();
                var parameterValue = $("#parameterEditUI input[name='parameterValue']").val();
                var cellLookup = $("#parameterEditUI input[name='cellLookup']").prop("checked");
                var dataSetLookup = $("#parameterEditUI input[name='dataSetLookup']").prop("checked");
                dataService.updateDataImportParameter(dataImportId, dataImportParameterId, parameterName, parameterType, parameterValue, cellLookup, dataSetLookup, renderSheetUI);
                $(this).dialog("close");
            }
        }
    });
};
getIndividualContainer = function (entityId, entityType) {
    dataService.getIndividualContainer(entityId, entityType, 1, 10, renderIndividualContainer);
};
renderIndividualContainer = function (data) {
    $("#dataContainer").empty();
    $("#individualContainerTmpl").tmpl(data).appendTo("#dataContainer");
};
renderIndividualScoresContainer = function (data) {
    $("#dataContainer").empty();
    $("#individualScoresContainerTmpl").tmpl(data).appendTo("#dataContainer");
    getDataImports();
};
getDataImports = function () {
    dataService.getDataImports(1, renderDataImportsGridUI);
    $("#dataImportAjaxLoader").show();
};
renderDataImportsGridUI = function (data) {
    $("#dataImportAjaxLoader").hide();
    $("#importContainer").empty();
    $("#dataImportTmpl").tmpl(data).appendTo("#importContainer");
};
renderIndividualDataGrid = function (data) {
    $("#individualScoresContainer").empty();
    $("#individualScoresTmpl").tmpl(data).appendTo("#individualScoresContainer");
};
getIndividualScores = function () {
    dataService.getIndividualScores(individualScoreFilter, renderIndividualScoresContainer);
};
$(".individualScoresPage").live("click", function (e) {
    e.preventDefault();
    var pageNumber = $(this).find("input[name='page']").val();
    individualScoreFilter.PageNumber = pageNumber;
    dataService.getIndividualScores(individualScoreFilter, renderIndividualDataGrid);
});
$(".individualScoresNextPage").live("click", function (e) {
    var pageNumber = $(this).find("input[name='page']").val();
    e.preventDefault();
    individualScoreFilter.PageNumber = pageNumber;
    dataService.getIndividualScores(individualScoreFilter, renderIndividualDataGrid);
});
$(".individualScoresPrevPage").live("click", function (e) {
    e.preventDefault();
    var pageNumber = $(this).find("input[name='page']").val();
    individualScoreFilter.PageNumber = pageNumber;
    dataService.getIndividualScores(individualScoreFilter, renderIndividualDataGrid);
});
$(".individualScores").live("click", function (e) {
    individualScoreFilter.ChannelList = [];
    individualScoreFilter.RegionList = [];
    individualScoreFilter.StoreList = [];
    individualScoreFilter.PageNumber = 1;
    individualScoreFilter.FilterType = '';
    $("input[name='individualScoreFilter']").val("");
    $("#dashBoardGraph").empty();
    e.preventDefault();
    getIndividualScores();
    $(".nav8ta > li").removeClass("active8ta");
    $(this).addClass("active8ta");
});
$(".imports").live("click", function (e) {
    e.preventDefault();
    dataService.getDataImports(1, renderDataImportsUI);
});
renderDataImportsUI = function (data) {
    $("#dataContainer").empty();
    $("#dataImportTmpl").tmpl(data).appendTo("#dataContainer");
};
$(".getExcelSchema").live("click", function () {
    var importId = $(this).find("input").val();
    dataService.getExcelSchema(importId, renderExcelSchemaData);
});
$(".channelStoreComparison").live("click", function () {
    getChannelStoreComparison();
});
getChannelStoreComparison = function () {
    dataService.getChannelStoreComparison(renderChannelStoreComparison);
};
renderChannelStoreComparison = function (data) {
    $("#dataContainer").empty();
    $("#channelStoreComparisonTmpl").tmpl(data).appendTo("#dataContainer");
};
$(".searchDate").live("click", function () {
    dataService.getFilterData(renderFilterDataUI);
});
renderFilterDataUI = function (data) {
    $("#dataFilterUI").empty();
    $("#filterDataTmpl").tmpl(data).appendTo("#dataFilterUI");
    $("#dialog:ui-dialog").dialog("destroy");

    $("#dialog-filterData").dialog({
        resizable: false,
        height: 370,
        width: 500,
        modal: true,
        buttons: {
            Cancel: function () {
                $(this).dialog("close");
            },
            Filter: function () {
                getFilterValues();
            }
        }
    });
};
getFilterValues = function (pageNumber, callback) {
    var dateFilter = $("input[name='dateListItem']")
    var channelFilter = $("input[name='channelListItem']");
    var regionFilter = $("input[name='regionListItem']");
    $(dateFilter).each(function () {
        if ($(this).attr('checked')) {
            individualScoreFilter.dateList.push($(this).val());
        }
    });
    $(channelFilter).each(function () {
        if ($(this).attr('checked')) {
            individualScoreFilter.channelList.push($(this).val());
        }
    });
    $(regionFilter).each(function () {
        if ($(this).attr('checked')) {
            individualScoreFilter.regionList.push($(this).val());
        }
    });
    dataService.filterIndividualScoreData(individualScoreFilter, callback);
};
renderFilterSheetUI = function (data) {
    $("#dataContainer").empty();
    $("#individualScoresContainerTmpl").tmpl(data).appendTo("#dataContainer");
};

$(".individualContainers").live("click", function () {
    $("#dataContainer").empty();
    $("#dataContainer").html($("#individualContainerTmpl").html());
    $(".nav8ta > li").removeClass("active8ta");
    $(this).addClass("active8ta");
});
$("input[name='dateListToggle']").live("click", function () {
    toggle(this, "dateListItem");
});
$("input[name='channelListToggle']").live("click", function () {
    toggle(this, "channelListItem");
    showChannelRegions();
    $(this).attr("disabled", "disabled");
});
$("input[name='regionListToggle']").live("click", function () {
    toggle(this, "regionListItem");
});
$("input[name='storesListToggle']").live("click", function () {
    toggle(this, "storeListItem");
});
toggle = function (source, inputName) {
    checkboxes = $("input[name='" + inputName + "']");
    for (var i in checkboxes)
        checkboxes[i].checked = source.checked;
};
$(".regionListPrevPage").live("click", function (e) {
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getRegionListContainer(pageNumber, renderRegionGridUI);
});
$(".regionListNextPage").live("click", function (e) {
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getRegionListContainer(pageNumber, renderRegionGridUI);
});
$(".regionListPage").live("click", function (e) {
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getRegionListContainer(pageNumber, renderRegionGridUI);
});
renderRegionGridUI = function (data) {
    $("#regionListContainer").empty();
    $("#regionListContainerTmpl").tmpl(data).appendTo("#regionListContainer");
};
renderStoreGridUI = function (data) {
    $("#storeListContainer").empty();
    $("#storeListContainerTmpl").tmpl(data).appendTo("#storeListContainer");
};
$(".storeListPrevPage").live("click", function (e) {
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getStoreListContainer(pageNumber, renderStoreGridUI);
});
$(".storeListNextPage").live("click", function (e) {
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getStoreListContainer(pageNumber, renderStoreGridUI);
});
$(".storeListPage").live("click", function (e) {
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getStoreListContainer(pageNumber, renderStoreGridUI);
});
$("input[name='channelListItem']").live("click", function () {
    showChannelRegions();
});
showChannelRegions = function () {
    $("#storeList").hide();
    $("#dateList").hide();
    if ($("input[name='individualScoreFilter']").val().length > 0) {
        individualScoreFilter = JSON.parse($.trim($("input[name='individualScoreFilter']").val()));
    }
    $("#channelAjaxLoader").show();
    var channelFilter = $("input[name='channelListItem']");
    $(channelFilter).each(function () {
        $(this).attr("disabled", "disabled");
        if ($(this).attr('checked')) {
            if ($.inArray(parseInt($(this).val()), individualScoreFilter.ChannelList) === -1) {
                individualScoreFilter.ChannelList.push(parseInt($(this).val()));
            }
        }
        else {
            if ($.inArray(parseInt($(this).val()), individualScoreFilter.ChannelList) > -1) {
                individualScoreFilter.ChannelList.pop(parseInt($(this).val()));
            }
        }
    });
    individualScoreFilter.PageNumber = 1;
    individualScoreFilter.FilterType = 'Channel';
    dataService.getChannelListContainer(individualScoreFilter, renderRegionListUI);
};
renderRegionListUI = function (data) {
    if (data.IndividualScoreSummary.IndividualScoreFilter.ChannelList === null) {
        $("#regionListContainer").empty();
        $("#regionList").hide();
    }
    else {
        $("#regionListContainer").empty();
        $("#regionListContainerTmpl").tmpl(data).appendTo("#regionListContainer");
        $("#regionList").show();
    }
    $("#individualScoresContainer").empty();
    $("#individualScoresTmpl").tmpl(data).appendTo("#individualScoresContainer");
    $("#aggregatesContainer").empty();
    $("#aggregatesTmpl").tmpl(data).appendTo("#aggregatesContainer");
    $("#channelAjaxLoader").hide();
    var channelFilter = $("input[name='channelListItem']");
    $(channelFilter).each(function () {
        $(this).removeAttr("disabled");
    });
    $("input[name='individualScoreFilter']").val(JSON.stringify(data.IndividualScoreSummary.IndividualScoreFilter));
    $("input[name='channelListToggle']").removeAttr("disabled");
};
$("input[name='regionListItem']").live("click", function () {
    showRegionStores();
});
showRegionStores = function () {
    $("#dateList").hide();
    if ($("input[name='individualScoreFilter']").val().length > 0) {
        individualScoreFilter = JSON.parse($.trim($("input[name='individualScoreFilter']").val()));
    }

    if (individualScoreFilter.RegionList === null)
        individualScoreFilter.RegionList = [];

    $("#regionAjaxLoader").show();
    var regionFilter = $("input[name='regionListItem']");
    $(regionFilter).each(function () {
        $(this).attr("disabled", "disabled");
        if ($(this).attr('checked')) {
            if ($.inArray(parseInt($(this).val()), individualScoreFilter.RegionList) === -1) {
                individualScoreFilter.RegionList.push(parseInt($(this).val()));
            }
        }
        else {
            if ($.inArray(parseInt($(this).val()), individualScoreFilter.RegionList) > -1) {
                individualScoreFilter.RegionList.pop(parseInt($(this).val()));
            }
        }
    });
    individualScoreFilter.PageNumber = 1;
    individualScoreFilter.FilterType = 'Region';
    dataService.getRegionListContainer(individualScoreFilter, renderStoreListUI);
};
$("input[name='storeListItem']").live("click", function () {
    showStoreIndividuals();
});
showStoreIndividuals = function () {
    if ($("input[name='individualScoreFilter']").val().length > 0) {
        individualScoreFilter = JSON.parse($.trim($("input[name='individualScoreFilter']").val()));
    }

    if (individualScoreFilter.StoreList === null)
        individualScoreFilter.StoreList = [];

    $("#storeAjaxLoader").show();
    var storeFilter = $("input[name='storeListItem']");
    $(storeFilter).each(function () {
        $(this).attr("disabled", "disabled");
        if ($(this).attr('checked')) {
            if ($.inArray(parseInt($(this).val()), individualScoreFilter.StoreList) === -1) {
                individualScoreFilter.StoreList.push(parseInt($(this).val()));
            }
        }
        else {
            if ($.inArray(parseInt($(this).val()), individualScoreFilter.StoreList) > -1) {
                individualScoreFilter.StoreList.pop(parseInt($(this).val()));
            }
        }
    });
    individualScoreFilter.PageNumber = 1;
    individualScoreFilter.FilterType = 'Store';
    dataService.getStoreListContainer(individualScoreFilter, renderStoreIndividualGrid);
};
$("input[name='dateListItem']").live("click", function () {
    showDateIndividuals();
});
showDateIndividuals = function () {
    if ($("input[name='individualScoreFilter']").val().length > 0) {
        individualScoreFilter = JSON.parse($.trim($("input[name='individualScoreFilter']").val()));
    }

    if (individualScoreFilter.DateList === null)
        individualScoreFilter.DateList = [];

    $("#dateAjaxLoader").show();
    var dateFilter = $("input[name='dateListItem']");
    $(dateFilter).each(function () {
        $(this).attr("disabled", "disabled");
        if ($(this).attr('checked')) {
            if ($.inArray($(this).val(), individualScoreFilter.StoreList) === -1) {
                individualScoreFilter.DateList.push($(this).val());
            }
        }
        else {
            if ($.inArray($(this).val(), individualScoreFilter.StoreList) > -1) {
                individualScoreFilter.DateList.pop($(this).val());
            }
        }
    });
    individualScoreFilter.PageNumber = 1;
    individualScoreFilter.FilterType = 'Date';
    dataService.getDateListContainer(individualScoreFilter, renderDateIndividualGrid);
};
$("input[name='regionListToggle']").live("click", function () {
    showRegionStores();
});
$("input[name='storesListToggle']").live("click", function () {
    showStoreIndividuals();
});
renderStoreIndividualGrid = function (data) {
    if (data.IndividualScoreSummary.IndividualScoreFilter.StoreList === null) {
        $("#dateListContainer").empty();
        $("#dateList").hide();
    }
    else {
        $("#dateListContainer").empty();
        $("#dateListContainerTmpl").tmpl(data).appendTo("#dateListContainer");
        $("#dateList").show();
    }
    $("#individualScoresContainer").empty();
    $("#individualScoresTmpl").tmpl(data).appendTo("#individualScoresContainer");
    $("#aggregatesContainer").empty();
    $("#aggregatesTmpl").tmpl(data).appendTo("#aggregatesContainer");
    $("#storeAjaxLoader").hide();
    var storeFilter = $("input[name='storeListItem']");
    $(storeFilter).each(function () {
        $(this).removeAttr("disabled");
    });
    $("input[name='individualScoreFilter']").val(JSON.stringify(data.IndividualScoreSummary.IndividualScoreFilter));
    $("input[name='storeListToggle']").removeAttr("disabled");
};
renderDateIndividualGrid = function (data) {
    $("#individualScoresContainer").empty();
    $("#individualScoresTmpl").tmpl(data).appendTo("#individualScoresContainer");
    $("#dateAjaxLoader").hide();
    var dateFilter = $("input[name='dateListItem']");
    $(dateFilter).each(function () {
        $(this).removeAttr("disabled");
    });
    $("input[name='individualScoreFilter']").val(JSON.stringify(data.IndividualScoreSummary.IndividualScoreFilter));
    $("input[name='dateListToggle']").removeAttr("disabled");
};

renderStoreListUI = function (data) {
    if (data.IndividualScoreSummary.IndividualScoreFilter.RegionList === null) {
        $("#storeListContainer").empty();
        $("#storeList").hide();
    }
    else {
        $("#storeListContainer").empty();
        $("#storeListContainerTmpl").tmpl(data).appendTo("#storeListContainer");
        $("#storeList").show();
    }
    $("#individualScoresContainer").empty();
    $("#individualScoresTmpl").tmpl(data).appendTo("#individualScoresContainer");
    $("#aggregatesContainer").empty();
    $("#aggregatesTmpl").tmpl(data).appendTo("#aggregatesContainer");
    $("#regionAjaxLoader").hide();
    var regionFilter = $("input[name='regionListItem']");
    $(regionFilter).each(function () {
        $(this).removeAttr("disabled");
    });
    $("input[name='individualScoreFilter']").val(JSON.stringify(data.IndividualScoreSummary.IndividualScoreFilter));
    $("input[name='regionListToggle']").removeAttr("disabled");
};
$(".dataImportPage").live("click", function (e) {
    $("#dataImportAjaxLoader").show();
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getDataImports(pageNumber, renderDataImportsGridUI);
});
$(".dataImportNextPage").live("click", function (e) {
    $("#dataImportAjaxLoader").show();
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getDataImports(pageNumber, renderDataImportsGridUI);
});
$(".dataImportPrevPage").live("click", function (e) {
    $("#dataImportAjaxLoader").show();
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getDataImports(pageNumber, renderDataImportsGridUI);
});
$(".connections").live("click", function (e) {
    e.preventDefault();
    $("#dataContainer").empty();
    $("#dataContainer").html($("#connectionContainerTmpl").html());
    $(".nav8ta > li").removeClass("active8ta");
    $(this).addClass("active8ta");
    dataService.getIndividualScores(individualScoreFilter, renderChannelList);
    dataService.getConnections(1, renderConnectionGrid);
    $("select[name='connectionChannel']").empty();
    $("select[name='connectionChannel']").attr("disabled", "disabled");
    $("select[name='connectionChannel']").append("<option>Getting channels</option>");
});
renderDateList = function (data) {
    $("select[name='connectionDate']").removeAttr("disabled");
    $("select[name='connectionDate']").empty();
    $("#connectionDateListTmpl").tmpl(data).appendTo("select[name='connectionDate']");
};
renderChannelList = function (data) {
    $("select[name='connectionChannel']").removeAttr("disabled");
    $("select[name='connectionChannel']").empty();
    $("#connectionChannelListTmpl").tmpl(data).appendTo("select[name='connectionChannel']");
};
$("select[name='connectionChannel']").live("change", function () {
    individualScoreFilter.ChannelList.push(parseInt($(this).val()));
    $("select[name='connectionRegion']").empty();
    $("select[name='connectionRegion']").append("<option>Getting regions</option>");
    dataService.getChannelListContainer(individualScoreFilter, renderRegionList);
});
$("select[name='connectionRegion']").live("change", function () {
    var channelId = $("select[name='connectionChannel']").val();
    var regionId = $("select[name='connectionRegion']").val();
    individualScoreFilter.ChannelList.push(parseInt(channelId));
    individualScoreFilter.RegionList.push(parseInt(regionId));
    $("select[name='connectionStore']").empty();
    $("select[name='connectionStore']").append("<option>Getting stores</option>");
    dataService.getRegionListContainer(individualScoreFilter, renderStoreList);
});
$("select[name='connectionStore']").live("change", function () {
    var channelId = $("select[name='connectionChannel']").val();
    var regionId = $("select[name='connectionRegion']").val();
    var storeId = $("select[name='connectionStore']").val();
    individualScoreFilter.ChannelList.push(parseInt(channelId));
    individualScoreFilter.RegionList.push(parseInt(regionId));
    individualScoreFilter.StoreList.push(parseInt(storeId));
    $("select[name='connectionDate']").empty();
    $("select[name='connectionDate']").append("<option>Getting dates</option>");
    dataService.getStoreListContainer(individualScoreFilter, renderDateList);
});
$("select[name='connectionDate']").live("change", function () {
    $("select[name='connectionType']").empty();
    $("select[name='connectionType']").append("<option>Getting connection types</option>");
    dataService.getConnectionTypes(renderConnectionTypeList);
});
renderRegionList = function (data) {
    $("select[name='connectionRegion']").removeAttr("disabled");
    $("select[name='connectionRegion']").empty();
    $("#connectionRegionListTmpl").tmpl(data).appendTo("select[name='connectionRegion']");
};
renderStoreList = function (data) {
    $("select[name='connectionStore']").removeAttr("disabled");
    $("select[name='connectionStore']").empty();
    $("#connectionStoreListTmpl").tmpl(data).appendTo("select[name='connectionStore']");
};
renderDateList = function (data) {
    $("select[name='connectionDate']").removeAttr("disabled");
    $("select[name='connectionDate']").empty();
    $("#connectionDateListTmpl").tmpl(data).appendTo("select[name='connectionDate']");
};
renderConnectionTypeList = function (data) {
    $("select[name='connectionType']").removeAttr("disabled");
    $("select[name='connectionType']").empty();
    $("#connectionTypeDiv").empty();
    $("#selectConnectionType").tmpl(data).appendTo("#connectionTypeDiv");
};
$(".revenue").live("click", function (e) {
    e.preventDefault();
    $("#dataContainer").empty();
    $("#dataContainer").html($("#revenueContainerTmpl").html());
    $(".nav8ta > li").removeClass("active8ta");
    $(this).addClass("active8ta");
});
$(".addConnectionType").live("click", function () {
    $("#connectionTypeDiv").empty();
    $("#connectionTypeDiv").html($("#addConnectionType").html());
});
$(".saveConnectionType").live("click", function () {
    var connectionTypeName = $("input[name='connectionTypeName']").val();
    dataService.saveConnectionType(connectionTypeName, renderConnectionTypeList);
});
$("input[name='connectionFigure']").live("blur", function () {
    $("input[name='connectionSaveBtn']").removeAttr("disabled");
});
$("select[name='connectionType']").live("click", function () {
    $("input[name='connectionFigure']").removeAttr("disabled");
});
saveConnection = function () {
    var channelId = $("select[name='connectionChannel']").val();
    var regionId = $("select[name='connectionRegion']").val();
    var storeId = $("select[name='connectionStore']").val();
    var connectionDate = $("select[name='connectionDate']").val(); ;
    var connectionType = $("select[name='connectionType']").val(); ;
    var connectionFigure = $("input[name='connectionFigure']").val(); ;
    dataService.saveConnection(channelId, regionId, storeId, connectionDate, connectionType, connectionFigure, renderConnectionGrid);
};
renderConnectionGrid = function (data) {
    $("#connectionsGrid").empty();
    $("#connectionsGridTmpl").tmpl(data).appendTo("#connectionsGrid");
};
$(".connectionsNextPage").live("click", function (e) {
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getConnections(pageNumber, renderConnectionGrid);
});
$(".connectionsPrevPage").live("click", function (e) {
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getConnections(pageNumber, renderConnectionGrid);
});
$(".connectionsPage").live("click", function (e) {
    e.preventDefault();
    var pageNumber = parseInt($(this).find("input[name='page']").val());
    dataService.getConnections(pageNumber, renderConnectionGrid);
});
loadDashboard = function () {
    $(".nav8ta > li").removeClass("active8ta");
    $(".dashboard").addClass("active8ta");
    $("#dashBoardGraph").empty();
    $("#dashBoardGraph").html($("#dashboardHeaderTmpl").html());
    $("#dataContainer").html($("#dashBoardContainerTmpl").html());
    dataService.getChannels(renderDashboardUI);
    dataService.getChannelEntityContainerValues("Revenue", renderRevenueUI);
};
renderRevenueUI = function (data) {
    $(".dashBoardAjaxLoader").hide();
    $("#revenueGridContainer").empty();
    $("#revenueGridTmpl").tmpl(data).appendTo("#revenueGridContainer");
    $("#connectionContainer").show();
    $(".connectionAjaxLoader").show();
    drawVisualization("revenueGraphContainer", data.IndividualScoreSummary.ChartColumns, data.IndividualScoreSummary.ChartRows, "Revenue by Channel", "Channels", chartColors);
    dataService.getChannelEntityContainerValues("Connections", renderConnectionUI);
};
renderConnectionUI = function (data) {
    $(".connectionAjaxLoader").hide();
    $("#connectionGridContainer").empty();
    $("#connectionGridTmpl").tmpl(data).appendTo("#connectionGridContainer");
    drawVisualization("connectionGraphContainer", data.IndividualScoreSummary.ChartColumns, data.IndividualScoreSummary.ChartRows, "Connections by Channel", "Channels", ["#ffffff", "#b4b4b4", "#e64097"]);
    $("#csiContainer").show();
    $(".csiBoardAjaxLoader").show();
    dataService.calculateConnectionsAsPercentage("CSI", renderCSIGraph);
};
renderCSIGraph = function (data) {
    $(".csiBoardAjaxLoader").hide();
    drawPieChartVisualisation("csiGraphContainer", data.IndividualScoreSummary.ChartColumns, data.IndividualScoreSummary.ChartRows, "CSI", "CSI", ["#ffffff", "#b4b4b4", "#e64097"]);
    $(".competenceAjaxLoader").show();
    $("#competenceContainer").show();
    dataService.getChannelCompetence(renderCompetenceGraph);
};
renderCompetenceGraph = function (data) {
    $("#competenceContainer").show();
    $(".individualCompetenceAjaxLoader").show();
    $("#individualCompetenceContainer").show();
    $(".competenceAjaxLoader").hide();
    drawVisualization("competenceGraphContainer", data.IndividualScoreSummary.ChartColumns, data.IndividualScoreSummary.ChartRows, "Channel", "Channels", ["#ffffff", "#b4b4b4", "#e64097"]);
    dataService.getCurriculaItems(renderIndividualCompetence);
};
renderIndividualCompetence = function (data) {
    $("#individualCompetenceContainer").show();
    $(".individualCompetenceAjaxLoader").hide();
    $("#individualCompetenceGridContainer").empty();
    $("#individualCompetenceGridTmpl").tmpl(data).appendTo("#individualCompetenceGridContainer");
    $(".individualCompetenceRemoveFilter").hide();
    drawVisualization("individualCompetenceGraphContainer", data.IndividualScoreSummary.ChartColumns, data.IndividualScoreSummary.ChartRows, "Coverage/Competence", "Curricula", ["#ffffff", "#b4b4b4", "#e64097"]);
};
$(".dashboard").live("click", function (e) {
    e.preventDefault();
    loadDashboard();
});
renderDashboardUI = function (data) {
    $("#dashBoardGraph").empty();
    $("#dashBoardHeaderGridTmpl").tmpl(data).appendTo("#dashBoardGraph");
};
$(".dataMap").live("click", function (e) {
    e.preventDefault();
    $("#dashBoardGraph").empty();
    $("#dataContainer").empty();
    $("#dataContainer").html($("#dataMapContainerTmpl").html());
    $(".nav8ta > li").removeClass("active8ta");
    $(".dataMap").addClass("active8ta");
    $(".dataMapAjaxLoader").show();
    dataService.getChannels(renderChannelTreeNavUI);
});
renderChannelTreeNavUI = function (data) {
    $(".dataMapAjaxLoader").hide();
    $("#holder").empty();
    $("#dataMapChannelTreeView").tmpl(data).appendTo("#holder");
    $("#entityValueContainerGridTmpl").tmpl(data).appendTo("#entityValueContainersDiv");
};
$(".channelTreeItemClosed").live("click", function (e) {
    e.preventDefault();
    var channelId = $(this).find("input[type='hidden']").val();
    $(this).removeClass("channelTreeItemClosed");
    $(this).addClass("channelTreeItemOpen");
    renderChannelTreeView(channelId);
});
$(".channelTreeItemOpen").live("click", function (e) {
    e.preventDefault();
    var channelId = $(this).find("input[type='hidden']").val();
    $(".dataMapAjaxLoader").show();
    $("#channelContainer" + channelId + " .icon-chevron-right").show();
    $("#channelContainer" + channelId + " .icon-chevron-down").hide();
    $("#channel" + channelId).empty();
    $(this).removeClass("channelTreeItemClosed");
    $(this).addClass("channelTreeItemOpen");
    dataService.getChannels(renderChannelTreeNavUI);
});
renderChannelTreeView = function (channelId) {
    individualScoreFilter.ChannelList = [];
    individualScoreFilter.ChannelList.push(channelId);
    $("#channelContainer" + channelId + " .treeViewAjaxLoader").show();
    $("#channelContainer" + channelId + " .icon-chevron-right").hide();
    $("#channelContainer" + channelId + " .icon-chevron-down").show();
    $(".valuesAjaxLoader").show();
    dataService.getChannelListContainer(individualScoreFilter, renderRegionTreeNavUI);
};
renderRegionTreeNavUI = function (data) {
    var channelId = data.IndividualScoreSummary.IndividualScoreFilter.ChannelList[0];
    individualScoreFilter = data.IndividualScoreSummary.IndividualScoreFilter;
    $("#channelContainer" + channelId + " .treeViewAjaxLoader").hide();
    $("#channel" + channelId).empty();
    $("#dataMapRegionTreeView").tmpl(data).appendTo("#channel" + channelId);
    $("#valuesGrid").empty();
    $("#valuesGridTmpl").tmpl(data).appendTo("#valuesGrid");
    $(".valuesAjaxLoader").hide();
    $("#entityValueContainersDiv").empty();
    $("#entityValueContainerGridTmpl").tmpl(data).appendTo("#entityValueContainersDiv");
};
$(".regionTreeItemClosed").live("click", function (e) {    
    e.preventDefault();
    var regionId = $(this).find("input[type='hidden']").val();
    individualScoreFilter.RegionList = [];
    individualScoreFilter.RegionList.push(regionId);
    $(this).find(".treeViewAjaxLoader").show();
    $(this).find(".icon-chevron-right").hide();
    $(this).find(".icon-chevron-down").show();
    $(this).removeClass("regionTreeItemClosed");
    $(this).addClass("regionTreeItemOpen");
    $(".valuesAjaxLoader").show();
    dataService.getRegionListContainer(individualScoreFilter, renderStoreTreeNavUI);
});
$(".regionTreeItemOpen").live("click", function (e) {
    e.preventDefault();
    var channelId = individualScoreFilter.ChannelList[0];
    $("#channelContainer" + channelId + " .treeViewAjaxLoader").show();
    $(this).removeClass("regionTreeItemClosed");
    $(this).addClass("regionTreeItemOpen");
    dataService.getChannelListContainer(individualScoreFilter, renderRegionTreeNavUI);
});
renderStoreTreeNavUI = function (data) {
    var regionId = data.IndividualScoreSummary.IndividualScoreFilter.RegionList[0];
    $("#regionContainer" + regionId + " .treeViewAjaxLoader").hide();
    $("#region" + regionId).empty();
    $("#dataMapStoreTreeView").tmpl(data).appendTo("#region" + regionId);
    $("#valuesGrid").empty();
    $("#valuesGridTmpl").tmpl(data).appendTo("#valuesGrid");
    $(".valuesAjaxLoader").hide();
    $("#entityValueContainersDiv").empty();
    $("#entityValueContainerGridTmpl").tmpl(data).appendTo("#entityValueContainersDiv");
};
$(".storeTreeItemClosed").live("click", function () {
    $(".valuesAjaxLoader").show();
    var storeId = $(this).find("input[type='hidden']").val();
    individualScoreFilter.StoreList = [];
    individualScoreFilter.StoreList.push(storeId);
    $("#storeContainer" + storeId + " .treeViewAjaxLoader").show();
    dataService.getStoreListContainer(individualScoreFilter, renderStoreDetailUI);
});
renderStoreDetailUI = function (data) {
    individualScoreFilter = data.IndividualScoreSummary.IndividualScoreFilter;
    var storeId = individualScoreFilter.StoreList[0];
    $("#valuesGrid").empty();
    $("#valuesGridTmpl").tmpl(data).appendTo("#valuesGrid");
    $(".valuesAjaxLoader").hide();
    $("#storeContainer" + storeId + " .treeViewAjaxLoader").hide();
    $("#storeContainer" + storeId + " .icon-arrow-right").hide();
    $("#storeContainer" + storeId + " .icon-ok").show();
    $("#entityValueContainersDiv").empty();
    $("#entityValueContainerGridTmpl").tmpl(data).appendTo("#entityValueContainersDiv");
    $("#newEntityValueContainer").show();
};
$(".entityRevenueValues").live("click", function () {
    $("#revenueContainer").html($("#entityRevenueValuesGridTmpl").html());
});
$("#newEntityValue").live("click", function () {
    $(".entityValueAjaxLoader").show();
    dataService.getEntityValueContainer(individualScoreFilter, renderEntityValueContainerUI);
});
renderEntityValueContainerUI = function (data) {
    individualScoreFilter = data.IndividualScoreSummary.IndividualScoreFilter;
    console.log(individualScoreFilter);
    $("#newEntityValueCapture").empty();
    $("#newEntityValueTmpl").tmpl(data).appendTo("#newEntityValueCapture");
    $(".entityValueAjaxLoader").hide();
};
$(".addNewEntityContainer").live("click", function () {    
    $("#entityContainerNameContainer").html($("#newEntityValueContainerInputTmpl").html());
});
$(".addNewEntityLabel").live("click", function () {
    $("#entityContainerLabelContainer").html($("#newEntityValueLabelInputTmpl").html());
});
saveEntityValue = function () {
    var entityContainerName = '';
    var entityContainerLabel = '';
    if ($("select[name='entityValueContainerName']").length)
        entityContainerName = $("select[name='entityValueContainerName']").val();
    else if ($("input[name='entityValueContainerName']").length)
        entityContainerName = $("input[name='entityValueContainerName']").val();

    if ($("select[name='entityValueLabelName']").length)
        entityContainerLabel = $("select[name='entityValueLabelName']").val();
    else if ($("input[name='entityValueLabelName']").length)
        entityContainerLabel = $("input[name='entityValueLabelName']").val();

    var entityValueFigure = $("input[name='entityValueFigure']").val();
    var channelId = individualScoreFilter.ChannelList[0];
    var regionId = individualScoreFilter.RegionList[0];
    var storeId = individualScoreFilter.StoreList[0];

    var figureDate = $("select[name='figureDate']").val();
    $(".entityValueAjaxLoader").show();
    dataService.saveEntityValue(entityValueFigure, channelId, regionId, storeId, entityContainerName, entityContainerLabel, figureDate, renderEntityValueUI);
};
renderEntityValueUI = function (data) {
    $(".entityValueAjaxLoader").hide();
    $("#entityValueContainersDiv").empty();
    $("#entityValueContainerGridTmpl").tmpl(data).appendTo("#entityValueContainersDiv");
};
drawVisualization = function (container, chartColumns, chartRows, chartTitle, hAxisTitle, hAxisTextStyleColor, hAxisTitleTextStyleColor, vAxisTitleTextStyleColor, backgroundColor, titleTextStyleColor, legendTextStyleColor, chartColors, gridWidth, gridHeight, position, alignment) {
    var data = new google.visualization.DataTable();
    for (var i = 0; i < chartColumns.length; i++) {
        data.addColumn(chartColumns[i].ColumnDataType.toString(), chartColumns[i].ColumnName.toString());
    }
    for (var i = 0; i < chartRows.length; i++) {
        data.addRow(chartRows[i]);
    }

    // Create and draw the visualization.
    new google.visualization.ColumnChart(document.getElementById(container)).
            draw(data,
                 { title: chartTitle,
                     width: gridWidth, height: gridHeight,
                     hAxis: { title: hAxisTitle, textStyle: { color: hAxisTextStyleColor }, titleTextStyle: { color: hAxisTitleTextStyleColor } },
                     vAxis: { textStyle: { color: vAxisTitleTextStyleColor}, minValue:0, maxValue:100 },
                     backgroundColor: backgroundColor,
                     titleTextStyle: { color: titleTextStyleColor },
                     legend: { textStyle: { color: legendTextStyleColor }, position: position, alignment: alignment },
                     colors: chartColors,
                     chartArea: { left: 100 }
                 }
            );
};
drawBarChartVisualization = function (container, chartColumns, chartRows, chartSettingsObj) {
    var data = new google.visualization.DataTable();
    for (var i = 0; i < chartColumns.length; i++) {
        data.addColumn(chartColumns[i].ColumnDataType.toString(), chartColumns[i].ColumnName.toString());
    }
    for (var i = 0; i < chartRows.length; i++) {
        data.addRow(chartRows[i]);
    }

    // Create and draw the visualization.
    new google.visualization.BarChart(document.getElementById(container)).
            draw(data, chartSettingsObj);

};
drawColumnChartVisualization = function (container, chartColumns, chartRows, chartSettingsObj) {
    var data = new google.visualization.DataTable();
    for (var i = 0; i < chartColumns.length; i++) {
        data.addColumn(chartColumns[i].ColumnDataType.toString(), chartColumns[i].ColumnName.toString());
    }
    for (var i = 0; i < chartRows.length; i++) {
        data.addRow(chartRows[i]);
    }
    var formatter_long = new google.visualization.DateFormat({ pattern: 'MMM yyyy' });
    // Create and draw the visualization.
    new google.visualization.ColumnChart(document.getElementById(container)).
            draw(data, chartSettingsObj);

};
drawLineChartVisualization = function (container, chartColumns, chartRows, chartSettingsObj, chartSelectionObj) {
    var data = new google.visualization.DataTable();
    for (var i = 0; i < chartColumns.length; i++) {
        data.addColumn(chartColumns[i].ColumnDataType.toString(), chartColumns[i].ColumnName.toString());
    }
    for (var i = 0; i < chartRows.length; i++) {
        data.addRow(chartRows[i]);
    }

    // Create and draw the visualization.
    var table = new google.visualization.LineChart(document.getElementById(container));
    table.draw(data, chartSettingsObj);
    if (chartSelectionObj != null) {
        table.setSelection(chartSelectionObj);
    }
};
drawPieChartVisualisation = function (container, chartColumns, chartRows, chartSettingsObj, chartSelectionObj) {
    var data = new google.visualization.DataTable();
    for (var i = 0; i < chartColumns.length; i++) {
        data.addColumn(chartColumns[i].ColumnDataType.toString(), chartColumns[i].ColumnName.toString());
    }
    for (var i = 0; i < chartRows.length; i++) {
        data.addRow(chartRows[i]);
    }
    var chart = new google.visualization.PieChart(document.getElementById(container));
    chart.draw(data, chartSettingsObj);

};
drawAreaChartVisualization = function (container, chartColumns, chartRows, chartSettingsObj, chartSelectionObj) {
    var data = new google.visualization.DataTable();
    for (var i = 0; i < chartColumns.length; i++) {
        data.addColumn(chartColumns[i].ColumnDataType.toString(), chartColumns[i].ColumnName.toString());
    }
    for (var i = 0; i < chartRows.length; i++) {
        data.addRow(chartRows[i]);
    }

    // Create and draw the visualization.
    var table = new google.visualization.AreaChart(document.getElementById(container));
    table.draw(data, chartSettingsObj);
    if (chartSelectionObj != null) {
        table.setSelection(chartSelectionObj);
    }
};
$(".curriculumChildren").live("click", function () {
    var curriculumId = $(this).find("input[type='hidden']").val();
    $(".individualCompetenceAjaxLoader").show();
    dataService.getCurriculumTraining(curriculumId, renderCurriculumTrainingGrid);
});
renderCurriculumTrainingGrid = function (data) {
    $(".individualCompetenceRemoveFilter").show();
    $(".individualCompetenceAjaxLoader").hide();
    $("#individualCompetenceGridContainer").empty();
    $("#individualCompetenceTrainingGridTmpl").tmpl(data).appendTo("#individualCompetenceGridContainer");
    drawVisualization("individualCompetenceGraphContainer", data.IndividualScoreSummary.ChartColumns, data.IndividualScoreSummary.ChartRows, "Coverage/Competence", "Training", ["#ffffff", "#b4b4b4", "#e64097"]);
};
$(".trainingChildren").live("click", function () {
    $(".individualCompetenceAjaxLoader").show();
    var trainingId = $(this).find("input[type='hidden']").val();
    dataService.getIndividuals(trainingId, renderIndividualGrid);
});
renderIndividualGrid = function (data) {
    $(".individualCompetenceRemoveFilter").show();
    $(".individualCompetenceAjaxLoader").hide();
    $("#individualCompetenceGridContainer").empty();
    $("#individualCompetenceIndividualGridTmpl").tmpl(data).appendTo("#individualCompetenceGridContainer");
    drawVisualization("individualCompetenceGraphContainer", data.IndividualScoreSummary.ChartColumns, data.IndividualScoreSummary.ChartRows, "Competence", "Individuals", ["#ffffff", "#b4b4b4", "#e64097"]);
};
$(".individualCompetenceRemoveFilter").live("click", function () {
    $(".individualCompetenceAjaxLoader").show();
    dataService.getCurriculaItems(renderIndividualCompetence);
});

$(".serviceAdd").live("click", function () {
    alert("Service Added");
});
$(".clusterItem").live("click", function (e) {
    var clusterKey = $(this).find("input[name='ClusterId']").val();
    dataService.getClusterSummary(clusterKey, renderClusterSummaryUI);
});
renderClusterSummaryUI = function (data) {
    $("#output").empty();
    $("#outPutTmpl").tmpl(data).appendTo("#output");
};
getActivityScoreContainers = function () {
    var containerKeys = $("input[name='ContainerKey']");
    var containerArr = [];
    $(containerKeys).each(function () {
        containerArr.push($(this).val());
    });
    dataService.getActivityScoreContainers(containerArr, renderActivityScoreContainerTreeNav);
};
getActivityScores = function () {
    $("#content").empty();
    $("#content").html($("#loader").html());
    var domainObjectUniqueKeys = $("input[name='domainObjectUniqueKey']");
    var domainObjectUniqueKeyArr = [];
    $(domainObjectUniqueKeys).each(function () {
        domainObjectUniqueKeyArr.push($(this).val());
    });
    dataService.getActivityScores(domainObjectUniqueKeyArr, 1, 10, renderActivityScoreGridUI);
};
$(".activityScoreNextPage").live("click", function (e) {
    e.preventDefault();
    $("#pagerLoader").empty();
    $("#pagerLoader").html($("#loader").html());
    var domainObjectUniqueKeys = $("input[name='domainObjectUniqueKey']");
    var domainObjectUniqueKeyArr = [];
    $(domainObjectUniqueKeys).each(function () {
        domainObjectUniqueKeyArr.push($(this).val());
    });
    var page = parseInt($(this).find("input[name='page']").val());
    dataService.getActivityScores(domainObjectUniqueKeyArr, page, 10, renderActivityScoreGridUI);
});
$(".activityScorePrevPage").live("click", function (e) {
    e.preventDefault();
    $("#pagerLoader").empty();
    $("#pagerLoader").html($("#loader").html());
    var domainObjectUniqueKeys = $("input[name='domainObjectUniqueKey']");
    var domainObjectUniqueKeyArr = [];
    $(domainObjectUniqueKeys).each(function () {
        domainObjectUniqueKeyArr.push($(this).val());
    });
    var page = parseInt($(this).find("input[name='page']").val());
    dataService.getActivityScores(domainObjectUniqueKeyArr, page, 10, renderActivityScoreGridUI);
});
$(".activityScorePage").live("click", function (e) {
    e.preventDefault();
    $("#pagerLoader").empty();
    $("#pagerLoader").html($("#loader").html());
    var domainObjectUniqueKeys = $("input[name='domainObjectUniqueKey']");
    var domainObjectUniqueKeyArr = [];
    $(domainObjectUniqueKeys).each(function () {
        domainObjectUniqueKeyArr.push($(this).val());
    });
    var page = parseInt($(this).find("input[name='page']").val());
    dataService.getActivityScores(domainObjectUniqueKeyArr, page, 10, renderActivityScoreGridUI);
});
renderActivityScoreContainerTreeNav = function (data) {
    alert('done');
};
renderActivityScoreGridUI = function (data) {
    $("#content").empty();
    $("#activityScoresTmpl").tmpl(data).appendTo("#content");
};
$(".containerItem").live("click", function () {
    //    $(".containerItem").removeClass("currentContainerItem");
    //    $(this).find(".treeViewAjaxLoader").show();
    //    $(this).addClass("currentContainerItem");
    //    var nextPath = $(this).find("input[name='NextPath']").val();
    //    var domainObjectId = $(this).find("input[name='DomainObjectId']").val();
    //    var containerId = $(this).find("input[name='ContainerId']").val();
    //    $("input[name='currentContainerId']").val(containerId);
    //    dataService.getActivityContainerPath(domainObjectId, nextPath, renderActivityContainerPathUI);
    var activityContainerPath = $(this).find("input[name='activityContainerPath']").val();
    dataService.getActivityScoreContainerPath(activityContainerPath, renderActivityScoreContainerUI);
});
renderActivityContainerPathUI = function (data) {
    $(".treeViewAjaxLoader").hide();
    var containerId = $("input[name='currentContainerId']").val();
    var domainObjectUniqueKey = data.DomainObject.DomainObjectUniqueKey;
    $("#containerChildItems" + containerId).empty();
    $("#containerItemTmpl").tmpl(data).appendTo("#containerChildItems" + containerId);
    $("#metricsOutput").empty();
    $("#metricsOutputTmpl").tmpl(data).appendTo("#metricsOutput");
    var activityContainerPath = data.ActivityContainerPath;
    $("input[name='containerPath']").val(activityContainerPath);
    drawVisualization("competenceGraph", data.ChartColumns, data.ChartRows, "Competence/Coverage/Readiness", "Training", ["#ffffff", "#b4b4b4", "#e64097"], 870, 430);
};
renderContainerItem = function (data) {
    var domainObjectUniqueKey = data.DomainObject.DomainObjectUniqueKey;
    $("#containerChildItems" + domainObjectUniqueKey).empty();
    $("#containerItemTmpl").tmpl(data).appendTo("#containerChildItems" + domainObjectUniqueKey);
};
renderTestOutputUI = function (data) {
    $("#testOutput").empty();
    $("#testOutputTmpl").tmpl(data).appendTo("#testOutput");
};
$(".employeeItem").live("click", function (e) {
    $(".containerItem").removeClass("currentContainerItem");
    $(this).find(".treeViewAjaxLoader").show();
    $(this).addClass("currentContainerItem");
    var domainObjectId = $(this).find("input[name='DomainObjectId']").val();
    dataService.getEmployeeActivityTypePath(domainObjectId, renderEmployeeItem);
});
renderEmployeeItem = function (data) {
    $(".treeViewAjaxLoader").hide();
    $("#metricsOutput").empty();
    $("#metricsOutputTmpl").tmpl(data).appendTo("#metricsOutput");
};
getCelebrateData = function () {
    dataService.getCelebrateData(renderCelebrateUI);
};
renderCelebrateUI = function (data) {
    drawVisualization("graphOutput", data.ChartColumns, data.ChartRows, "Celebrates by BU", "Celebrates Made / Received", ["#d2232a", "#0095d4", "#7fb6e3"], 1170, 700);
};
$(".uploadData").live("click", function () {
    $(".nav li").removeClass("active");
    $(this).addClass("active");
    $("#dialog-ImportCelebrates").show();
    $("#graphOutput").hide();
});
uploadCelebrates = function () {
    $("#graphOutput").html($("#loader").html());
    $("#ExcelForm").submit();
};
renderCelebrateDataUI = function (data) {
    $("#graphOutput").empty();
    $("#celebrateDataExtractTmpl").tmpl(data).appendTo("#graphOutput");
};
$(".celebrateDataExtractPrevPage").live("click", function (e) {
    e.preventDefault();
    var page = parseInt($(this).find("input[name='page']").val());
    var applicationFileName = $("input[name='ApplicationFileName']").val();
    dataService.extractCelebrateData(applicationFileName, 25, page, renderCelebrateDataUI);
});
$(".celebrateDataExtractNextPage").live("click", function (e) {
    e.preventDefault();
    var page = parseInt($(this).find("input[name='page']").val());
    var applicationFileName = $("input[name='ApplicationFileName']").val();
    dataService.extractCelebrateData(applicationFileName, 25, page, renderCelebrateDataUI);
});
$(".celebrateDataExtractPage").live("click", function (e) {
    e.preventDefault();
    var page = parseInt($(this).find("input[name='page']").val());
    var applicationFileName = $("input[name='ApplicationFileName']").val();
    dataService.extractCelebrateData(applicationFileName, 25, page, renderCelebrateDataUI);
});
$(".importCelebrateData").live("click", function (e) {
    e.preventDefault();
    var monthName = $("select[name='celebrateDataMonth']").val();
    var applicationFileName = $("input[name='ApplicationFileName']").val();
    dataService.saveCelebrateData(applicationFileName, monthName, renderCelebrateDataUI);
});
getActivityScoreContainer = function (parentActivityScoreContainer, childActivityScoreContainer) {
    dataService.getActivityScoreContainer(parentActivityScoreContainer, childActivityScoreContainer, renderActivityScoreContainerUI);
};
renderActivityScoreContainerUI = function (data) {
    $("#containerItemTmpl").tmpl(data).appendTo("#sideMenu");
};
getActivityScoreContainerSummary = function (activityScoreContainerKey) {
    $("#loaderArea").html($("#loader").html());
    dataService.getActivityScoreContainerSummary(activityScoreContainerKey, renderActivityScoreContainerSummaryUI);
};
renderActivityScoreContainerSummaryUI = function (data) {
    $("#sideBar").empty();
    $("#sideBarTmpl").tmpl(data).appendTo("#sideBar");
    $("#mainDashBoard").empty();
    $("#mainDashboardTmpl").tmpl(data).appendTo("#mainDashBoard");
    drawVisualization("connectionsChartArea", data.ChartColumns, data.ChartRows, " ", " ", ["#ffffff", "#b4b4b4", "#e64097", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4", "#ed634"]);
    drawVisualization("competenceChartArea", data.CompetenceChartColumns, data.CompetenceChartRows, " ", " ", ["#ffffff", "#b4b4b4", "#e64097", "#9000ff", "#0084ff", "#00ff7", "#d2ff00", "#eec400", "#eeb00", "#f02d00"]);
};
$(".activityScoreContainerItem").live("click", function (e) {
    e.preventDefault();
    var activityScoreContainerSummaryKey = $(this).find("input[name='activityScoreContainerSummaryKey']").val();
    $("#loaderArea").html($("#loader").html());
    dataService.getActivityScoreContainerSummary(activityScoreContainerSummaryKey, renderActivityScoreContainerSummaryUI);
});
renderEntityDashboard = function (data) {
    $("#mainDashBoard").empty();
    $("#mainDashboardTmpl").tmpl(data).appendTo("#mainDashBoard");
    $("#sideBar").empty();
    $("#sideBarTmpl").tmpl(data).appendTo("#sideBar");
    $(".breadCrumbLoader").hide();
    if (data.ConnectionsCharts !== null) {
        drawVisualization("connectionsChartArea", data.ConnectionsCharts.ChartColumns, data.ConnectionsCharts.ChartRows, " ", " ", ["#e64097", "#ffffff", "#b4b4b4", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4e", "#ed6343"], 850, 210, 'none', 'center');
    }
    if (data.CompetenceChart !== null) {
        drawVisualization("competenceChartArea", data.CompetenceChart.ChartColumns, data.CompetenceChart.ChartRows, " ", " ", ["#e64097", "#ffffff", "#b4b4b4", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4e", "#ed6343"], 850, 210, 'top', 'center');
    }
    if (data.CompetenceChart == null && data.ConnectionsCharts == null) {
        $("#mainFigures").hide();
        $("#capabilityData").hide();
        $("#connectionsData").hide();
    }
    else {
        $("#mainFigures").show();
        $("#capabilityData").show();
        $("#connectionsData").show();
    }
    if (data.CSISentimentChart.ChartRows.length) {
        drawPieChartVisualisation("csiChartArea", data.CSISentimentChart.ChartColumns, data.CSISentimentChart.ChartRows, "", "", ["#e64097", "#ffffff", "#b4b4b4", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4e", "#ed6343"], 850, 210);
        $("#csiArea").show();
    }
    if (data.CSISentimentSourceChart.ChartRows.length) {
        drawPieChartVisualisation("csiSourceChartArea", data.CSISentimentSourceChart.ChartColumns, data.CSISentimentSourceChart.ChartRows, "", "", ["#e64097", "#ffffff", "#b4b4b4", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4e", "#ed6343"], 850, 210);
        $("#csiArea").show();
    }
};
$("select[name='connectionType']").live("change", function () {
    $(".connectionsLoader").show();
    var domainObjectUniqueKey = $("#mainDashBoard").find("input[name='DomainObjectUniqueKey']").val();
    var connectionType = $(this).val();
    dataService.getConnectionsChart(domainObjectUniqueKey, connectionType, renderConnectionTypeVisualisation);
});
renderConnectionTypeVisualisation = function (data) {
    $(".connectionsLoader").hide();
    drawVisualization("connectionsChartArea", data.ConnectionsCharts.ChartColumns, data.ConnectionsCharts.ChartRows, " ", " ", ["#e64097", "#ffffff", "#b4b4b4", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4", "#ed634"], 850, 210, 'none', 'center');
};
$("select[name='competenceMonthRange']").live("change", function () {
    $("select[name='moduleCompetence']").empty();
    $("select[name='moduleCompetence']").prop("disabled", true);
    $("select[name='moduleCompetence']").append("<option value=\"none\">modules</option>");
    $("select[name='activityCompetence']").empty();
    $("select[name='activityCompetence']").prop("disabled", true);
    $("select[name='activityCompetence']").append("<option value=\"none\">activities</option>");
    $(".competenceLoader").show();
    var domainObjectUniqueKey = $("#mainDashBoard").find("input[name='DomainObjectUniqueKey']").val();
    var monthRange = $(this).val();
    dataService.getActivityType(domainObjectUniqueKey, '', '', '', monthRange, renderActivityTypeUI);
});
$("select[name='curriculumCompetence']").live("change", function () {
    $("select[name='activityCompetence']").empty();
    $("select[name='activityCompetence']").prop("disabled", true);
    $("select[name='activityCompetence']").append("<option value=\"none\">activities</option>");
    $(".competenceLoader").show();
    var domainObjectUniqueKey = $("#mainDashBoard").find("input[name='DomainObjectUniqueKey']").val();
    var monthRange = $("select[name='competenceMonthRange']").val();
    dataService.getActivityType(domainObjectUniqueKey, 'Curriculum', $(this).val(), '', monthRange, renderCurriculumActivityTypeUI);
});
$("select[name='moduleCompetence']").live("change", function () {
    $(".competenceLoader").show();
    var domainObjectUniqueKey = $("#mainDashBoard").find("input[name='DomainObjectUniqueKey']").val();
    var monthRange = $("select[name='competenceMonthRange']").val();
    var activityParent = $("select[name='curriculumCompetence']").val();
    dataService.getActivityType(domainObjectUniqueKey, 'Module', $(this).val(), activityParent, monthRange, renderModuleActivityTypeUI);
});
$("select[name='activityCompetence']").live("change", function () {
    $(".competenceLoader").show();
    var domainObjectUniqueKey = $("#mainDashBoard").find("input[name='DomainObjectUniqueKey']").val();
    var monthRange = $("select[name='competenceMonthRange']").val();
    dataService.getActivityType(domainObjectUniqueKey, 'Activity', $(this).val(), '', monthRange, renderActivityActivityTypeUI);
});
renderActivityTypeUI = function (data) {
    $(".competenceLoader").hide();
    $("select[name='curriculumCompetence']").empty();
    $("#activityListTmpl").tmpl(data).appendTo("select[name='curriculumCompetence']");
    drawVisualization("competenceChartArea", data.CompetenceChart.ChartColumns, data.CompetenceChart.ChartRows, " ", " ", ["#e64097", "#ffffff", "#b4b4b4", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4", "#ed634"], 850, 210, 'top', 'center');
};
renderCurriculumActivityTypeUI = function (data) {
    $(".competenceLoader").hide();
    $("select[name='moduleCompetence']").empty();
    $("select[name='moduleCompetence']").prop("disabled", false);
    $("#activityListTmpl").tmpl(data).appendTo("select[name='moduleCompetence']");
    drawVisualization("competenceChartArea", data.CompetenceChart.ChartColumns, data.CompetenceChart.ChartRows, " ", " ", ["#e64097", "#ffffff", "#b4b4b4", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4", "#ed634"], 850, 210, 'top', 'center');
};
renderModuleActivityTypeUI = function (data) {
    $(".competenceLoader").hide();
    $("select[name='activityCompetence']").empty();
    $("select[name='activityCompetence']").prop("disabled", false);
    $("#activityListTmpl").tmpl(data).appendTo("select[name='activityCompetence']");
    drawVisualization("competenceChartArea", data.CompetenceChart.ChartColumns, data.CompetenceChart.ChartRows, " ", " ", ["#e64097", "#ffffff", "#b4b4b4", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4", "#ed634"], 850, 210, 'top', 'center');
};
renderActivityActivityTypeUI = function (data) {
    $(".competenceLoader").hide();
    drawVisualization("competenceChartArea", data.CompetenceChart.ChartColumns, data.CompetenceChart.ChartRows, " ", " ", ["#e64097", "#ffffff", "#b4b4b4", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4", "#ed634"], 850, 210, 'top', 'center');
};
$(".entityDashboard").live("click", function (e) {
    e.preventDefault();
    var domainObjectUniqueKey = $(this).find("input[name='DomainObjectUniqueKey']").val();
    $(".breadCrumbLoader").show();
    dataService.getEntityDashboard(domainObjectUniqueKey, renderEntityDashboard);
});
$(".employeeDashboard").live("click", function (e) {
    e.preventDefault();
    var domainObjectUniqueKey = $(this).find("input[name='DomainObjectUniqueKey']").val();
    $(".breadCrumbLoader").show();
    dataService.getEntityDashboard(domainObjectUniqueKey, renderEmployeeDashboard);
});
renderEmployeeDashboard = function (data) {
    $("#mainDashBoard").empty();
    $("#employeeDashboardTmpl").tmpl(data).appendTo("#mainDashBoard");
    $(".breadCrumbLoader").hide();
    drawVisualization("competenceChartArea", data.CompetenceChart.ChartColumns, data.CompetenceChart.ChartRows, " ", " ", ["#e64097", "#ffffff", "#b4b4b4", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4", "#ed634"], 850, 210, 'top', 'center');
};
$("select[name='csiProduct']").live("change", function () {
    $(".csiLoader").show();
    var productName = $(this).val();
    var domainObjectUniqueKey = $("#csiArea").find("input[name='DomainObjectUniqueKey']").val();
    dataService.getProductCSI(domainObjectUniqueKey, productName, renderProductCSIChart);
});
renderProductCSIChart = function (data) {
    if (data.CSISentimentChart.ChartRows.length) {
        drawPieChartVisualisation("csiChartArea", data.CSISentimentChart.ChartColumns, data.CSISentimentChart.ChartRows, "", "", ["#e64097", "#ffffff", "#b4b4b4", "#bf6cff", "#6fb9ff", "#4dffa5", "#e3ff5f", "#e7ce57", "#e69d4", "#ed634"], 850, 210);
        $("#csiArea").show();
        $(".csiLoader").hide();
    }
};
$(".btnSentimentReset").live("click", function (e) {
    e.preventDefault();
    $(".csiLoader").show();
    var domainObjectUniqueKey = $("#csiArea").find("input[name='DomainObjectUniqueKey']").val();
    dataService.getEntityDashboard(domainObjectUniqueKey, renderProductCSIChart);
    $("select[name='csiProduct']").val("reset");
});

$(".resellerItem").live("mouseenter", function () {
    $(this).children(".resellerImage").fadeOut();
});
$(".resellerItem").live("mouseleave", function () {
    $(this).children(".resellerImage").fadeIn();
});
getLearnerDataItems = function (dashboardEntityType, dashboardEntityName) {
    $("#content").empty();
    $("#content").html($("#littleAjaxLoader").html());
    dataService.getLearnerDataItems(1, 15, dashboardEntityType, dashboardEntityName, renderLearnerDataGridUI);
};
getDashboardByTypeByName = function (dashboardEntityType, dashboardEntityName) {
    renderBlockUI.renderBlockUI();
    dataService.getDashboardByTypeByName(dashboardEntityType, dashboardEntityName, renderLearnerDataGridUI);
};
$(".learnerDataPage").live("click", function (e) {
    e.preventDefault();
    $(".pagination").find(".pageLoader").show();
    var page = $(this).find("input[name='page']").val();
    dataService.getLearnerDataItems(page, 15, renderLearnerDataGridUI);
});
$(".learnerDataNextPage").live("click", function (e) {
    e.preventDefault();
    $(".pagination").find(".pageLoader").show();
    var page = $(this).find("input[name='page']").val();
    dataService.getLearnerDataItems(page, 15, renderLearnerDataGridUI);
});
$(".learnerDataPrevPage").live("click", function (e) {
    e.preventDefault();
    $(".pagination").find(".pageLoader").show();
    var page = $(this).find("input[name='page']").val();
    dataService.getLearnerDataItems(page, 15, renderLearnerDataGridUI);
});
renderLearnerDataGridUI = function (data) {
    chartSettings.height = 370;
    chartSettings.width = 1140;
    chartSettings.chartArea.left = 100;
    chartSettings.chartArea.top = 50;
    $("#content").empty();
    $("#dashboardTmpl").tmpl(data).appendTo("#content");
    domainObjectBreadCrumbKeys = data.DomainObjectBreadCrumbKeys;
    if (data.StatSummary != null) {
        $("#dashBoardGraph").empty();
        $("#statsIndicatorTmpl").tmpl(data).appendTo("#dashBoardGraph");
    }

    drawBarChartVisualization("graphArea", data.ChartColumns, data.ChartRows, chartSettings);
};
$(".entityFilter").live("click", function () {
    var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
    dataService.get8taEntity(domainObjectUniqueKey, render8taEntityUI);
});
render8taEntityUI = function (data) {
    alert('done');
};
$(".dashboardItem").live("click", function (e) {
    e.preventDefault();
    $(".nav").find(".littleAjaxLoader").show();
    $('.dropdown.open .dropdown-toggle').dropdown('toggle');
    var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
    var domainObjectTitle = $(this).find("input[name='domainObjectTitle']").val();
    domainObjectBreadCrumbKeys.SelectedItem = domainObjectUniqueKey;
    domainObjectBreadCrumbKeys.Page = 1;
    domainObjectBreadCrumbLabel.DomainObjectUniqueKey = domainObjectUniqueKey;
    domainObjectBreadCrumbLabel.DomainObjectTitle = domainObjectTitle;
    domainObjectBreadCrumbKeys.ParentItems.push(domainObjectBreadCrumbLabel);
    domainObjectBreadCrumbKeys.ShowDropdown = true;
    dataService.getDashboardContainerObject(domainObjectBreadCrumbKeys, renderDashboardContainerUI);
});
$(".dashboardNextPage").live("click", function (e) {
    $(".pagination").find(".pageLoader").show();
    e.preventDefault();
    var page = $(this).find("input[name='page']").val();
    domainObjectBreadCrumbKeys.Page = page;
    dataService.getDashboard(domainObjectBreadCrumbKeys, renderLearnerDataGridUI);
});
$(".dashboardPrevPage").live("click", function (e) {
    $(".pagination").find(".pageLoader").show();
    e.preventDefault();
    var page = $(this).find("input[name='page']").val();
    domainObjectBreadCrumbKeys.Page = page;
    dataService.getDashboard(domainObjectBreadCrumbKeys, renderLearnerDataGridUI);
});
$(".dashboardPage").live("click", function (e) {
    $(".pagination").find(".pageLoader").show();
    e.preventDefault();
    var page = $(this).find("input[name='page']").val();
    domainObjectBreadCrumbKeys.Page = page;
    dataService.getDashboard(domainObjectBreadCrumbKeys, renderLearnerDataGridUI);
});
$(".monthDashboardItem").live("click", function (e) {
    $(this).parent().find(".littleAjaxLoader").show();
    e.preventDefault();
    var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
    var domainObjectTitle = $(this).find("input[name='domainObjectTitle']").val();
    domainObjectBreadCrumbKeys.SelectedItem = domainObjectUniqueKey;
    domainObjectBreadCrumbLabel.DomainObjectUniqueKey = domainObjectUniqueKey;
    domainObjectBreadCrumbLabel.DomainObjectTitle = domainObjectTitle;
    domainObjectBreadCrumbKeys.ShowDropdown = false;
    domainObjectBreadCrumbKeys.Page = 1;
    domainObjectBreadCrumbKeys.ParentItems.push(domainObjectBreadCrumbLabel);
    dataService.getDashboard(domainObjectBreadCrumbKeys, renderLearnerDataGridUI);
});
$(".activityDashboardItem").live("click", function (e) {
    $(this).parent().find(".littleAjaxLoader").show();
    e.preventDefault();
    var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
    var domainObjectTitle = $(this).find("input[name='domainObjectTitle']").val();
    domainObjectBreadCrumbKeys.SelectedItem = domainObjectUniqueKey;
    domainObjectBreadCrumbKeys.Page = 1;
    domainObjectBreadCrumbLabel.DomainObjectUniqueKey = domainObjectUniqueKey;
    domainObjectBreadCrumbLabel.DomainObjectTitle = domainObjectTitle;
    domainObjectBreadCrumbKeys.ShowDropdown = false;
    domainObjectBreadCrumbKeys.ParentItems.push(domainObjectBreadCrumbLabel);
    dataService.getEmployee(domainObjectBreadCrumbKeys, renderEmployeeGridUI);
});
renderEmployeeGridUI = function (data) {
    $("#content").empty();
    $("#employeeActivityDashboardTmpl").tmpl(data).appendTo("#content");
    domainObjectBreadCrumbKeys = data.DomainObjectBreadCrumbKeys;
    $(".nav").find(".littleAjaxLoader").hide();
    //drawVisualization("graphArea", data.ChartColumns, data.ChartRows, "Learner Data", "Channels", chartColors);
    drawBarChartVisualization("graphArea", data.ChartColumns, data.ChartRows, " ", " ", ["#ffffff", "#b4b4b4", "#e64097"], 1100, 250);
};
$(".employeeDashboardPage").live("click", function (e) {
    $(".pagination").find(".pageLoader").show();
    e.preventDefault();
    var page = $(this).find("input[name='page']").val();
    domainObjectBreadCrumbKeys.Page = page;
    dataService.getEmployee(domainObjectBreadCrumbKeys, renderEmployeeGridUI);
});
$(".employeeDashboardPrevPage").live("click", function (e) {
    $(".pagination").find(".pageLoader").show();
    e.preventDefault();
    var page = $(this).find("input[name='page']").val();
    domainObjectBreadCrumbKeys.Page = page;
    dataService.getEmployee(domainObjectBreadCrumbKeys, renderEmployeeGridUI);
});
$(".employeeDashboardNextPage").live("click", function (e) {
    $(".pagination").find(".pageLoader").show();
    e.preventDefault();
    var page = $(this).find("input[name='page']").val();
    domainObjectBreadCrumbKeys.Page = page;
    dataService.getEmployee(domainObjectBreadCrumbKeys, renderEmployeeGridUI);
});
$("#connections").live("click", function (e) {
    e.preventDefault();
    $(".nav > li").removeClass("active");
    $(this).parent().addClass("active");
    renderBlockUI.renderBlockUI();
    dataService.getConnectionContainers(renderConnectionsDashboardUI);
});
$("#capability").live("click", function (e) {
    chartSettings.height = 270;
    chartSettings.width = 1100;
    chartSettings.chartArea.left = 100;
    e.preventDefault();
    $(".nav > li").removeClass("active");
    $(this).parent().addClass("active");
    renderBlockUI.renderBlockUI();
    dataService.getDashboardContainer("Telkom Mobile", renderDashboardContainerUI);
});
renderDashboardContainerUI = function (data) {
    domainObjectBreadCrumbKeys = data.DomainObjectBreadCrumbKeys;
    $.unblockUI();
    $("#content").empty();
    $("#dashboardTmpl").tmpl(data).appendTo("#content");
    $("#dashBoardGraph").empty();
    if (data.ChartRows !== null) {
        chartSettings.height = 370;
        chartSettings.width = 1140;
        chartSettings.chartArea.left = 100;
        chartSettings.chartArea.top = 50;
        drawLineChartVisualization("graphArea", data.ChartColumns, data.ChartRows, chartSettings, data.ChartSelection);
    }
    if (data.StatSummary != null) {
        $("#dashBoardGraph").empty();
        $("#statsIndicatorTmpl").tmpl(data).appendTo("#dashBoardGraph");
    }
};
$("#onlineSummary").live("click", function (e) {
    e.preventDefault();
    $(".nav > li").removeClass("active");
    $(this).parent().addClass("active");
});
 renderConnectionsDashboardUI = function (data) {
     $.unblockUI();
     domainObjectBreadCrumbKeys = data.DomainObjectBreadCrumbKeys;
     $("#content").empty();
     $("#connectionDashboardTmpl").tmpl(data).appendTo("#content");
     if (data.ChartRows !== null) {
         chartSettings.height = 370;
         chartSettings.width = 1140;
         chartSettings.chartArea.left = 100;
         chartSettings.chartArea.top = 50;
         drawLineChartVisualization("graphArea", data.ChartColumns, data.ChartRows, chartSettings, data.ChartSelection);
     }
     if (data.PieChart != null) {
         chartSettings.width = 700;
         chartSettings.height = 450;
         chartSettings.is3D = true;
         chartSettings.chartArea.top = 0;
         chartSettings.legend.alignment = 'center';
         chartSettings.chartArea.left = 100;
         drawPieChartVisualisation("pieChartArea", data.PieChart.ChartColumns, data.PieChart.ChartRows, chartSettings, data.ChartSelection);
     }
     $("#dashBoardGraph").empty();
     $("#statsIndicatorTmpl").tmpl(data).appendTo("#dashBoardGraph");
 };
 $(".connectionDatasetType").live("click", function (e) {
     renderBlockUI.renderBlockUI();
     var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
     var datasetType = $(this).find("input[name='datasetType']").val();
     var selectedDatasetName = $("select[name='datasetName']").val();
     dataService.getConnectionDatasetType(domainObjectBreadCrumbKeys, domainObjectUniqueKey, selectedDatasetName, datasetType, renderConnectionsDashboardUI);
 });
 $("select[name='datasetName']").live("change", function () {
     renderBlockUI.renderBlockUI();
    var domainObjectUniqueKey = $(this).parent().find("input[name='domainObjectUniqueKey']").val();
    var datasetType = 'Month Total';
    var selectedDatasetName = $("select[name='datasetName']").val();
    dataService.getConnectionDatasetType(domainObjectBreadCrumbKeys, domainObjectUniqueKey, selectedDatasetName, datasetType, renderConnectionsDashboardUI);
});
$(".connectionDashboardItem").live("click", function (e) {
    $(".nav-pills").find(".littleAjaxLoader").show();
    e.preventDefault();
    var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
    var domainObjectTitle = $(this).find("input[name='domainObjectTitle']").val();
    domainObjectBreadCrumbKeys.SelectedItem = domainObjectUniqueKey;
    domainObjectBreadCrumbKeys.Page = 1;
    domainObjectBreadCrumbLabel.DomainObjectUniqueKey = domainObjectUniqueKey;
    domainObjectBreadCrumbLabel.DomainObjectTitle = domainObjectTitle;
    domainObjectBreadCrumbKeys.ShowDropdown = false;
    domainObjectBreadCrumbKeys.ParentItems.push(domainObjectBreadCrumbLabel);
    dataService.getConnectionContainer(domainObjectBreadCrumbKeys, renderConnectionsDashboardUI);
});
$(".mainConnectionDashboardItem").live("click", function (e) {
    e.preventDefault();
    dataService.getConnectionContainers(renderConnectionsDashboardUI);
});
$("#activitiesCurriculum").live("click", function (e) {  
    e.preventDefault();
    $(".nav > li").removeClass("active");
    $(this).parent().addClass("active");
    renderBlockUI.renderBlockUI();
    dataService.getTelkomMobileCurriculumActivities(renderCurriculumActivitiesUI);
});
renderCurriculumActivitiesUI = function (data) {
    $.unblockUI();
    $("#content").empty();
    $("#curriculumActivitiesTmpl").tmpl(data).appendTo("#content");
    $("#dashBoardGraph").empty();
    $("#statsIndicatorTmpl").tmpl(data).appendTo("#dashBoardGraph");
};
$(".btnToggleCurriculum").live("click", function () {
    renderBlockUI.renderBlockUI();
    var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
    var isInCurriculum = $(this).find("input[name='isInCurriculum']").val();
    dataService.toggleActivityInCurriculum(domainObjectUniqueKey, isInCurriculum, renderCurriculumActivitiesUI);
});
$(".saveActivity").live("click", function (e) {
    var activityName = $("input[name='activityName']").val();
    renderBlockUI.renderBlockUI();
    dataService.saveTelkomActivity(activityName, renderCurriculumActivitiesUI);
});
$(".saveSurvey").live("click", function (e) {
    var surveyName = $(this).val();
    dataService.saveSurvey(surveyName, renderSurveySaveUI);
});
getSurveys = function () {
    dataService.getSurveys(renderSurveyGridUI);
};
renderSurveyGridUI = function (data) {
    $("#contentAea").empty();
    $("#surveyGridTmpl").tmpl(data).appendTo("#contentAea");
};
$(".newSurvey").live("click", function (e) {
    e.preventDefault();
    $("#contentAea").hide();
    $("#newSurveyAea").show();
});
renderSurveySaveUI = function (data) {
    $("#newSurveyAea").empty();
    $("#editSurveyTmpl").tmpl(data).appendTo("#newSurveyAea");
};
$(".btnUpdateIncludeStatus").live("click", function () {
    renderBlockUI.renderBlockUI();
    var radionBtns = $("input[name='activityInclude']");
    var checkItems = [];
    $(radionBtns).each(function () {
        DomainObjectCheckItem = new Object();
        DomainObjectCheckItem.IsChecked = $(this).is(':checked');
        DomainObjectCheckItem.DomainObjectUniqueKey = $(this).val();
        checkItems.push(DomainObjectCheckItem);
    });
    dataService.updateActivityInCurriculum(checkItems, renderCurriculumActivitiesUI);
});
$("#lpCostSavings").live("click", function (e) {
    e.preventDefault();
    dataService.getSkillPortUserGroupMetrics(renderSkillPortUserGroupMetricUI);
});
$(".skillPortUserGroupMetricItem").live("click", function (e) {
   // alert('.skillPortUserGroupMetricItem');
    e.preventDefault();
    renderBlockUI.renderBlockUI();
    var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
    if (chartType === "LearningActivity")
        dataService.getSkillPortUserGroupMetric(domainObjectUniqueKey, chartType, renderSkillPortLearningProgramsGridUI);
    else if (chartType === "LearningGrowth")
        dataService.getSkillPortUserGroupMetric(domainObjectUniqueKey, chartType, renderSkillPortLearningProgramsGridUI);
    else if (chartType === "LearningEvaluation")
        dataService.getSkillPortUserGroupEvaluationMetric(domainObjectUniqueKey, renderSkillPortLearningProgramsGridUI);
      
});
$(".editSkillPortLearningProgramMetric").live("click", function (e) {
    e.preventDefault();
    var domainObjectUniqueKey = $(this).parent().find("input[name='domainObjectUniqueKey']").val();
    $("#trEdit_" + domainObjectUniqueKey).show();
    $("#trView_" + domainObjectUniqueKey).hide();
});
$(".updateLP").live("click", function (e) {
    renderBlockUI.renderBlockUI();
    e.preventDefault();
    var parentDomainObjectUniqueKey = $(this).parent().find("input[name='parentDomainObjectUniqueKey']").val();
    var domainObjectUniqueKey = $(this).parent().find("input[name='domainObjectUniqueKey']").val();
    var facilitatedPrice = $("#trEdit_" + domainObjectUniqueKey).find("input[name='facilitatedPrice']").val();
    var eLearningPrice = $("#trEdit_" + domainObjectUniqueKey).find("input[name='eLearningPrice']").val();
    dataService.updateSkillPortLPPrice(parentDomainObjectUniqueKey, domainObjectUniqueKey, facilitatedPrice, eLearningPrice, renderSkillPortLearningProgramsGridUI);
});
$(".cancelUpdateLP").live("click", function (e) {
    e.preventDefault();
    var parentDomainObjectUniqueKey = $(this).parent().find("input[name='parentDomainObjectUniqueKey']").val();
    dataService.getSkillPortUserGroupMetric(parentDomainObjectUniqueKey, renderSkillPortLearningProgramsGridUI);
});
$(".lpGridItem a").live("click", function (e) {
    // alert('lpGridItem a click');
    renderBlockUI.renderBlockUI();
   
    e.preventDefault();
    var domainObjectUniqueKey = $(this).parent().find("input[name='domainObjectUniqueKey']").val();
    var groupOrgCode = $(this).parent().find("input[name='groupOrgCode']").val();
    var groupName = $(this).parent().find("input[name='groupName']").val();
    var learningProgramId = $(this).parent().find("input[name='learningProgramId']").val();
    if (chartType === "LearningActivity")
        dataService.getSkillPortLearningProgramMetric(groupOrgCode, groupName, learningProgramId, domainObjectUniqueKey, chartType, renderLearningProgramItemUI);
    else if (chartType === "LearningGrowth")
        dataService.getSkillPortLearningProgramMetric(groupOrgCode, groupName, learningProgramId, domainObjectUniqueKey, chartType, renderLearningProgramItemUI);
    else if (chartType === "LearningEvaluation")
        dataService.getSkillPortLearningProgramEvaluationMetric(domainObjectUniqueKey, groupName, renderLearningProgramItemUI);
    
});
$("#learningActivity").live("click", function (e) {
   
    e.preventDefault();
    $(".nav > li").removeClass("active");
    $(this).parent().addClass("active");
    renderBlockUI.renderBlockUI();
    chartType = 'LearningActivity';
    dataService.getSkillPortUserGroupMetrics(renderSkillPortUserGroupMetricUI);
});
$("#learningGrowth").live("click", function (e) {
    e.preventDefault();
    $(".nav > li").removeClass("active");
    $(this).parent().addClass("active");
    renderBlockUI.renderBlockUI();
    chartType = 'LearningGrowth';
    dataService.getSkillPortUserGroupMetrics(renderSkillPortUserGroupMetricUI);

});
$("#learningEvaluation").live("click", function (e) {
    e.preventDefault();
    $(".nav > li").removeClass("active");
    $(this).parent().addClass("active");
    renderBlockUI.renderBlockUI();
    chartType = 'LearningEvaluation';
    dataService.getSkillPortUserGroupEvaluationMetrics(renderSkillPortUserGroupEvaluationMetricUI);

});
renderSkillPortUserGroupEvaluationMetricUI = function (data) {
    window.scrollTo(0, 0);
    //$.unblockUI();
    $("#content").empty();
    $("#lpCostSavingsGridTmpl").tmpl(data).appendTo("#content");
    dataService.getSkillPortUserGroupEvaluationMetric(data.DomainObjects[0].DomainObjectUniqueKey, renderSkillPortLearningProgramsGridUI);
    //dataService.getSkillPortUserGroupEvaluationMetric(data.DomainObjects.DomainObjectUniqueKey, chartType, renderSkillPortLearningProgramsGridUI);

};
renderSkillPortUserGroupMetricUI = function (data) {
    window.scrollTo(0, 0);
    //$.unblockUI();
    $("#content").empty();
    $("#lpCostSavingsGridTmpl").tmpl(data).appendTo("#content");
    dataService.getSkillPortUserGroupMetric(data.DomainObjects[0].DomainObjectUniqueKey, chartType, renderSkillPortLearningProgramsGridUI);
};
renderSkillPortUserGroupByOrgCodeMetricUI = function (data) {
    window.scrollTo(0, 0);
    //$.unblockUI();
    $("#content").empty();
    $("#lpCostSavingsGridTmpl").tmpl(data).appendTo("#content");
    dataService.getSkillPortUserGroupMetric(data.DomainObject.DomainObjectUniqueKey, chartType, renderSkillPortLearningProgramsGridUI);
};
renderSkillPortUserGroupByOrgCodeEvaluationMetricUI = function (data) {
    window.scrollTo(0, 0);

    $("#content").empty();
    $("#lpCostSavingsGridTmpl").tmpl(data).appendTo("#content");

    //dataService.getSkillPortLearningProgramEvaluationMetric(data.DomainObject.DomainObjectUniqueKey, renderSkillPortLearningProgramsGridUI);

    if (data.Warning !== null) {
        $.unblockUI();
      //  alert('Warning');
        $("#dashBoardGraph").empty();
      //  $("#learningProgramMenu").empty();
        $("#dashBoardGraph").empty();
        $("#skillPortLearningProgramMetricArea").empty();
        $("#warningTmpl").tmpl(data).appendTo("#skillPortLearningProgramMetricArea");
    }
    else {
        dataService.getSkillPortUserGroupEvaluationMetric(data.DomainObject.DomainObjectUniqueKey, renderSkillPortLearningProgramsGridUI);
    }
};
renderSkillPortLearningProgramsGridUI = function (data) {
    window.scrollTo(0, 0);
    $.unblockUI();
    $("#skillPortLearningProgramMetricArea").empty();
    if (chartType === "LearningActivity")
        $("#skillPortLearningProgramMetricTmpl").tmpl(data).appendTo("#skillPortLearningProgramMetricArea");
    else if (chartType === "LearningGrowth")
        $("#skillPortLearningGrowthTmpl").tmpl(data).appendTo("#skillPortLearningProgramMetricArea");
    else if (chartType === "LearningEvaluation")
        $("#skillPortLearningEvaluationTmpl").tmpl(data).appendTo("#skillPortLearningProgramMetricArea");

    $("#dashBoardGraph").empty();
    $("#statsIndicatorTmpl").tmpl(data).appendTo("#dashBoardGraph");
    $("#learningProgramMenu").empty();
    $("#skillPortLearningProgramMenuTmpl").tmpl(data).appendTo("#learningProgramMenu");
    $(".groupNameLabel").html(data.Temp + "<b class=\"caret\"></b>");
    if (data.ChartRows !== null) {
        chartSettings.height = 370;
        chartSettings.width = 1140;
        chartSettings.chartArea.left = 100;
        chartSettings.chartArea.top = 50;
        if (chartType === 'LearningActivity') {
            chartSettings.height = 370;
            chartSettings.width = 1140;
            chartSettings.chartArea.left = 100;
            chartSettings.chartArea.top = 50;
            drawLineChartVisualization("graphArea", data.ChartColumns, data.ChartRows, chartSettings, data.ChartSelection);
        }
        else if (chartType === 'LearningGrowth') {
            chartSettings.height = 370;
            chartSettings.width = 1140;
            chartSettings.chartArea.left = 100;
            chartSettings.chartArea.top = 50;
            drawColumnChartVisualization("graphArea", data.ChartColumns, data.ChartRows, chartSettings, data.ChartSelection);
        }
        else if (chartType === 'LearningEvaluation') {
            chartSettings.height = 370;
            chartSettings.width = 1000;
            chartSettings.chartArea.left = 300;
            chartSettings.chartArea.top = 50;
            drawPieChartVisualisation("graphArea", data.ChartColumns, data.ChartRows, chartSettings, data.ChartSelection);
        }
    }
};
renderLearningProgramItemUI = function (data) {
    window.scrollTo(0, 0);
    $.unblockUI();
    $("#dashBoardGraph").empty();
    $("#statsIndicatorTmpl").tmpl(data).appendTo("#dashBoardGraph");
    $("#skillPortLearningProgramMetricArea").empty();
    if (chartType === "LearningActivity")
        $("#skillPortLearningProgramMetricDetailTmpl").tmpl(data).appendTo("#skillPortLearningProgramMetricArea");
    else if (chartType === "LearningGrowth")
        $("#skillPortLearningProgramMetricGrowthDetailTmpl").tmpl(data).appendTo("#skillPortLearningProgramMetricArea");
    else if (chartType === "LearningEvaluation")
        $("#skillPortLearningProgramEvaluationMetricDetailTmpl").tmpl(data).appendTo("#skillPortLearningProgramMetricArea");
    $("#breadCrumb").empty();
    $("#learningProgramBreadCrumb").tmpl(data).appendTo("#breadCrumb");
    if (data.ChartRows !== null) {
        chartSettings.height = 370;
        chartSettings.width = 1140;
        chartSettings.chartArea.left = 100;
        chartSettings.chartArea.top = 50;
        if (chartType === 'LearningActivity')
         drawLineChartVisualization("graphArea", data.ChartColumns, data.ChartRows, chartSettings, data.ChartSelection);
        else if (chartType === 'LearningGrowth')
            drawColumnChartVisualization("graphArea", data.ChartColumns, data.ChartRows, chartSettings, data.ChartSelection);
    }
    if (data.DoCharts !== null && chartType === 'LearningEvaluation') {
        chartSettings.height = 370;
        chartSettings.width = 580;
        chartSettings.chartArea.left = 115;
        chartSettings.chartArea.top = 50;
        for (var i = 0; i < data.DoCharts.length; i++) {
            var ck = data.DoCharts[i];
            var uk = '1';
            for (var j = 0; j < ck.length; j++) {
               // console.log(ck[j].Key + '  ' + ck[j].Value);
                if (ck[j].Key === 'uniquekey') {
                    uk = ck[j].Value;
                    //console.log('uk ' + uk);
                }
                if (ck[j].Key === 'ChartRows') {
                    drawPieChartVisualisation("graphArea" + uk, ck[j + 1].Value, ck[j].Value, chartSettings, data.ChartSelection);
                }
            }
          }
    }
};
$("#mustekLearningGrowth").live("click", function (e) {
    e.preventDefault();
    $(".nav > li").removeClass("active");
    $(this).parent().addClass("active");
    renderBlockUI.renderBlockUI();
    chartType = 'LearningGrowth';
    //console.log(' mustekLearningGrowth.live click');
    dataService.getSkillPortUserGroupMetricByOrgCode('org_10', renderSkillPortUserGroupByOrgCodeMetricUI);
});
 $("#mustekLearningActivity").live("click", function (e) {
     e.preventDefault();
     $(".nav > li").removeClass("active");
     $(this).parent().addClass("active");
     renderBlockUI.renderBlockUI();
     chartType = 'LearningActivity';
     //console.log(' mustekLearningActivity.live click');
     dataService.getSkillPortUserGroupMetricByOrgCode('org_10', renderSkillPortUserGroupByOrgCodeMetricUI);
 });
 $("#mustekLearningEvaluation").live("click", function (e) {
     e.preventDefault();
     $(".nav > li").removeClass("active");
     $(this).parent().addClass("active");
     renderBlockUI.renderBlockUI();
     chartType = 'LearningEvaluation';
     //console.log(' mustekLearningEvaluation.live click');
     dataService.getSkillPortUserGroupEvaluationMetricByOrgCode('org_10', renderSkillPortUserGroupByOrgCodeEvaluationMetricUI);
 });

$("#directAxisLearningGrowth").live("click", function (e) {
    e.preventDefault();
    $(".nav > li").removeClass("active");
    $(this).parent().addClass("active");
    renderBlockUI.renderBlockUI();
    chartType = 'LearningGrowth';
    dataService.getSkillPortUserGroupMetricByOrgCode('org_160', renderSkillPortUserGroupByOrgCodeMetricUI);
});
$("#directAxisLearningActivity").live("click", function (e) {
    e.preventDefault();
    $(".nav > li").removeClass("active");
    $(this).parent().addClass("active");
    renderBlockUI.renderBlockUI();
    chartType = 'LearningActivity';
    dataService.getSkillPortUserGroupMetricByOrgCode('org_160', renderSkillPortUserGroupByOrgCodeMetricUI);
 });
$("#directAxisLearningEvaluation").live("click", function (e) {
    e.preventDefault();
    $(".nav > li").removeClass("active");
    $(this).parent().addClass("active");
    renderBlockUI.renderBlockUI();
    chartType = 'LearningEvaluation';
    dataService.getSkillPortUserGroupEvaluationMetricByOrgCode('org_160', renderSkillPortUserGroupByOrgCodeEvaluationMetricUI);
 });
 $(".lpGroup").live("click", function (e) {
     renderBlockUI.renderBlockUI();
     e.preventDefault();
     var groupOrgCode = $(this).parent().find("input[name='groupCode']").val();
     //alert(chartType);
     if (chartType === 'LearningActivity') {

         dataService.getSkillPortUserGroupMetricByOrgCode(groupOrgCode, renderSkillPortUserGroupByOrgCodeMetricUI);
     }
     else if (chartType === 'LearningGrowth') {

         dataService.getSkillPortUserGroupMetricByOrgCode(groupOrgCode, renderSkillPortUserGroupByOrgCodeMetricUI);

     }
     else if (chartType === 'LearningEvaluation') {
         dataService.getSkillPortUserGroupEvaluationMetricByOrgCode(groupOrgCode, renderSkillPortUserGroupByOrgCodeEvaluationMetricUI);

     }

 });
 $("#imports").live("click", function (e) {
     e.preventDefault();
 });
 $("input[name='signIn']").live("click", function () {
     renderBlockUI.renderBlockUI();
     var userName = $("input[name='username']").val();
     var password = $("input[name='password']").val();
     var applicationId = $("input[name='applicationId']").val();
     var applicationName = $("input[name='applicationName']").val();
     LogOnModel.UserName = userName;
     LogOnModel.Password = password;
     LogOnModel.ApplicationId = applicationId;
     LogOnModel.ApplicationName = applicationName;
     dataService.applicationLogin(LogOnModel, renderApplicationLogin);
 });
 renderApplicationLogin = function (data) {
     location.reload(true);
     //$.unblockUI();
 };
 $("input[name='password']").live("keydown", function (e) {
     if (e.keyCode == 13) {
         renderBlockUI.renderBlockUI();
         var userName = $("input[name='username']").val();
         var password = $("input[name='password']").val();
         var applicationId = $("input[name='applicationId']").val();
         var applicationName = $("input[name='applicationName']").val();
         LogOnModel.UserName = userName;
         LogOnModel.Password = password;
         LogOnModel.ApplicationId = applicationId;
         LogOnModel.ApplicationName = applicationName;
         dataService.applicationLogin(LogOnModel, renderApplicationLogin);
     }
 });
 $("#catalogue").live("click", function (e) {
     e.preventDefault();
     $(".mainNavigation ul > li > a").removeClass("menuActive");
     $(this).parent().parent().parent().find(".dropdown-toggle").addClass("menuActive");     
     dataService.getSkillPortCurricula(renderSkillPortCurriculumUI);
 });
 renderSkillPortCurriculumUI = function (data) {
     $("#mainContentArea").empty();
     $("#skillPortCurriculaTmpl").tmpl(data).appendTo("#mainContentArea");
 };
 $(".makeCurriculumStartPoint").live("click", function () {
     var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
     dataService.saveSkillPortCurriculumStart(domainObjectUniqueKey, renderSkillPortCurriculumUI);
 });
 $("#imports").live("click", function (e) {
     e.preventDefault();
     $(".nav > li").removeClass("active");
     $(this).parent().addClass("active");
     renderBlockUI.renderBlockUI();
     dataService.getDomainObjectsByTypeName("Telkom Mobile Import", renderTelkomMobileImportGridUI);
 });
 renderTelkomMobileImportGridUI = function (data) {
     $("#dashBoardGraph").empty();
     $("#content").empty();
     $("#dataImportGridTmpl").tmpl(data).appendTo("#content");
 };
 $("input[name='btnSaveImport']").live("click", function () {
     var importFileName = $("input[name='importFileName']").val();
     var dataSheetName = encodeURIComponent($("input[name='dataSheetName']").val());
     var importType = encodeURIComponent($("select[name='importType']").val());
     var importSchema = encodeURIComponent($("input[name='importSchema']").val());
     dataService.saveTelkomMobileImport(importType, importFileName, dataSheetName, importSchema, renderTelkomMobileImportGridUI);
 });
 $(".editImport").live("click", function () {
     var domainObjectUniqueKey = $(this).parent().find("input[name='domainObjectUniqueKey']").val();
     $("#viewRow" + domainObjectUniqueKey).hide();
     $("#editRow" + domainObjectUniqueKey).show();
 });
 $(".updateImport").live("click", function () {
     var domainObjectUniqueKey = $(this).parent().find("input[name='domainObjectUniqueKey']").val();
     var importFileName = encodeURIComponent($("#editRow" + domainObjectUniqueKey).find("input[name='importFileName']").val());
     var dataSheetName = encodeURIComponent($("#editRow" + domainObjectUniqueKey).find("input[name='dataSheetName']").val());
     var importSchema = encodeURIComponent($("#editRow" + domainObjectUniqueKey).find("input[name='importSchema']").val());
     dataService.updateTelkomMobileImport(domainObjectUniqueKey, importFileName, dataSheetName, importSchema, renderTelkomMobileImportGridUI);
 });
 $(".skillPortCurriculum").live("click", function () {
     var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
     dataService.getDomainObjectByKey(domainObjectUniqueKey, renderSkillPortCurriculumChildrenUI);
 });
 renderSkillPortCurriculumChildrenUI = function (data) {
     var domainObjectUniqueKey = data.DomainObject.DomainObjectUniqueKey;
     $("#" + domainObjectUniqueKey + "children").empty();
     $("#skillPortCurriculumItemTmpl").tmpl(data).appendTo("#" + domainObjectUniqueKey + "children");
 };
 $("#siteManagement").live("click", function (e) {
     renderBlockUI.renderBlockUI();
     e.preventDefault();
     $(".partnerLogos").hide();
     $(".footer").hide();
     var applicationId = $("input[name='applicationId']").val();
     var applicationName = $("input[name='applicationName']").val();
     dataService.getMenuItems(applicationName, renderSiteManagementUI);
 });
 renderSiteManagementUI = function (data) {
     $.unblockUI();
     $("#mainContentArea").empty();
     $("#siteManagementTmpl").tmpl(data).appendTo("#mainContentArea");
 };
 $("input[name='saveMenuItemTitle']").live("click", function () {
     renderBlockUI.renderBlockUI();
     var menuItemTitle = $("input[name='menuItemTitle']").val();
     var applicationId = $("input[name='applicationId']").val();
     var applicationName = $("input[name='applicationName']").val();
     dataService.saveMenuItem(menuItemTitle, applicationName, renderSiteManagementUI);
 });
 $(".editMenu").live("click", function () {
     var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
     dataService.getDomainObjectByKey(domainObjectUniqueKey, renderPreviewMenuItemUI);
 });
 renderEditMenuItemUI = function (data) {
     $("#mainContentArea").empty();
     $("#menuItemTmpl").tmpl(data).appendTo("#mainContentArea");
     $('#editMenuItemTab').trigger('click');
 };
 renderPreviewMenuItemUI = function (data) {
     $("#mainContentArea").empty();
     $("#menuItemTmpl").tmpl(data).appendTo("#mainContentArea");
     $('#previewMenuItemTab').trigger('click');
 };
 $("#csiData").live("click", function (e) {
     e.preventDefault();
     $(".nav > li").removeClass("active");
     $(this).parent().addClass("active");
     renderBlockUI.renderBlockUI();
     dataService.getTelkommobileOrganisationCSIPeriod("Telkom Mobile", renderTelkomMobileCSIUI);
 });

 renderTelkomMobileCSIUI = function (data) {
     $.unblockUI();
     $("#dashBoardGraph").empty();
     $("#content").empty();
     $("#organisationCsiDatePeriodsTmpl").tmpl(data).appendTo("#content");
     if (data.Charts[0].ChartRows !== null) {
         chartSettings.height = 370;
         chartSettings.width = 1140;
         chartSettings.chartArea.left = 100;
         chartSettings.chartArea.top = 50;
         drawColumnChartVisualization("graphArea", data.Charts[0].ChartColumns, data.Charts[0].ChartRows, chartSettings, data.ChartSelection);
     }
     if (data.Charts[1].ChartRows !== null) {
         chartSettings.height = 370;
         chartSettings.width = 1140;
         chartSettings.chartArea.left = 100;
         chartSettings.chartArea.top = 50;
         drawColumnChartVisualization("graphArea2", data.Charts[1].ChartColumns, data.Charts[1].ChartRows, chartSettings, data.ChartSelection);
     }
     $("#statsIndicatorTmpl").tmpl(data).appendTo("#dashBoardGraph");
 };
 $(".csiOrganisation").live("click", function (e) {
     renderBlockUI.renderBlockUI();
     e.preventDefault();
     var organisationName = $(this).find("input[name='organisationName']").val();
     getTelkommobileOrganisationCSIPeriod(organisationName, renderTelkomMobileCSIUI);
 });
 $(".csiCluster").live("click", function (e) {
     renderBlockUI.renderBlockUI();
     e.preventDefault();
     var organisationName = $(this).find("input[name='organisationName']").val();
     var clusterName = $(this).find("input[name='csiClusterName']").val();
     getTelkommobileClusterCSIPeriod(organisationName, clusterName, renderTelkomMobileClusterCSIUI);
 });
 renderTelkomMobileClusterCSIUI = function (data) {
     $.unblockUI();
     $("#dashBoardGraph").empty();
     $("#content").empty();
     $("#clusterCsiDatePeriodsTmpl").tmpl(data).appendTo("#content");
     if (data.Charts[0].ChartRows !== null) {
         chartSettings.height = 370;
         chartSettings.width = 1140;
         chartSettings.chartArea.left = 100;
         chartSettings.chartArea.top = 50;
         drawColumnChartVisualization("graphArea", data.Charts[0].ChartColumns, data.Charts[0].ChartRows, chartSettings, data.ChartSelection);
     }
     if (data.Charts[1].ChartRows !== null) {
         chartSettings.height = 370;
         chartSettings.width = 1140;
         chartSettings.chartArea.left = 100;
         chartSettings.chartArea.top = 50;
         drawColumnChartVisualization("graphArea2", data.Charts[1].ChartColumns, data.Charts[1].ChartRows, chartSettings, data.ChartSelection);
     }
     $("#statsIndicatorTmpl").tmpl(data).appendTo("#dashBoardGraph");
 };
 $("#previewMenuItemTab").live("click", function (e) {
     e.preventDefault();
     $("#menuItemTab > li").removeClass("active");
     $(this).parent().addClass("active");
     $("#editMenuItemContent").hide();
     $("#previewMenuItemContent").show();
 });
 $("#editMenuItemTab").live("click", function (e) {
     e.preventDefault();
     $("#menuItemTab > li").removeClass("active");
     $(this).parent().addClass("active");
     $("#editMenuItemContent").show();
     $("#previewMenuItemContent").hide();
 });
 $(".saveMenuItemContent").live("click", function () {
     var domainObjectUniqueKey = $("input[name='domainObjectUniqueKey']").val();
     var menuItemContent = $("#menuItemContent").val();
     $("#menuItemContent").prop("disabled", "disabled");
     $(this).prop("disabled", "disabled");
     $(this).text("saving...please wait");
     dataService.saveDomainObjectField(domainObjectUniqueKey, "MenuItemContent", encodeURIComponent(menuItemContent), renderEditMenuItemUI);
 });
 $(".deleteMenu").live("click", function () {
     renderBlockUI.renderBlockUI();
     var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
     var applicationId = $("input[name='applicationId']").val();
     var applicationName = $("input[name='applicationName']").val();
     dataService.deleteMenuItem(domainObjectUniqueKey, applicationName, renderSiteManagementUI);
 });
 $(".decreaseMenuItemOrder").live("click", function (e) {
     renderBlockUI.renderBlockUI();
     e.preventDefault();
     var domainObjectUniqueKey = $(this).parent().find("input[name='domainObjectUniqueKey']").val();
     var order = parseInt($(this).parent().find("input[name='order']").val()) - 1;
     var applicationId = $("input[name='applicationId']").val();
     var applicationName = $("input[name='applicationName']").val();
     dataService.updateMenuitemOrder(domainObjectUniqueKey, order, applicationName, renderSiteManagementUI);
 });
 $(".increaseMenuItemOrder").live("click", function (e) {
     renderBlockUI.renderBlockUI();
     e.preventDefault();
     var domainObjectUniqueKey = $(this).parent().find("input[name='domainObjectUniqueKey']").val();
     var order = parseInt($(this).parent().find("input[name='order']").val()) + 1;
     var applicationId = $("input[name='applicationId']").val();
     var applicationName = $("input[name='applicationName']").val();
     dataService.updateMenuitemOrder(domainObjectUniqueKey, order, applicationName, renderSiteManagementUI);
 });
 $("#connectionsAlt").live("click", function (e) {
     e.preventDefault();
     renderBlockUI.renderBlockUI();
     $(".nav > li").removeClass("active");
     $(this).parent().addClass("active");
     dataService.getTelkomMobileConnectionContainer('', '', '', '', renderTelkomMobileConnectionContainerUI);
 });
 renderTelkomMobileConnectionContainerUI = function (data) {
     $.unblockUI();
     $("#dashBoardGraph").empty();
     $("#statsIndicatorTmpl").tmpl(data).appendTo("#dashBoardGraph");
     $("#content").empty();
     $("#connectionContainerTmpl").tmpl(data).appendTo("#content");
     if (data.ChartRows !== null) {
         chartSettings.height = 370;
         chartSettings.width = 1140;
         chartSettings.chartArea.left = 100;
         chartSettings.chartArea.top = 50;
         drawLineChartVisualization("graphArea", data.ChartColumns, data.ChartRows, chartSettings, data.ChartSelection);
     }
     if (data.PieChart != null) {
         chartSettings.width = 700;
         chartSettings.height = 450;
         chartSettings.is3D = true;
         chartSettings.chartArea.top = 0;
         chartSettings.legend.alignment = 'center';
         chartSettings.chartArea.left = 100;
         drawPieChartVisualisation("pieChartArea", data.PieChart.ChartColumns, data.PieChart.ChartRows, chartSettings, data.ChartSelection);
     }
 };
 $(".financialYear").live("click", function (e) {
     e.preventDefault();
     var financialYear = $(this).find("input[name='financialYear']").val();
     renderBlockUI.renderBlockUI();
     dataService.getTelkomMobileConnectionContainer(financialYear, '', '', '', renderTelkomMobileConnectionContainerUI);
 });
 $(".connectionContainerItem").live("click", function (e) {
     e.preventDefault();
     renderBlockUI.renderBlockUI();
     var financialYear = $(this).find("input[name='financialYear']").val();
     var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
     dataService.getTelkomMobileConnectionContainer(financialYear, domainObjectUniqueKey, '', '', renderTelkomMobileConnectionContainerUI);
 });
 $(".productTab").live("click", function (e) {
     e.preventDefault();
     var domainObjectUniqueKey = $(this).find("input[name='domainObjectUniqueKey']").val();
     var financialYear = $(this).find("input[name='financialYear']").val();
     var productName = $(this).find("input[name='productName']").val();
     var month = $("select[name='connectionMonth']").val();
     renderBlockUI.renderBlockUI();
     dataService.getTelkomMobileConnectionContainer(financialYear, domainObjectUniqueKey, productName, month, renderTelkomMobileConnectionContainerUI);
 });
 $("select[name='connectionMonth']").live("change", function () {
     renderBlockUI.renderBlockUI();
     var productName = $("#datasetTabs > li.active > a").find("input[name='productName']").val();
     var domainObjectUniqueKey = $("#datasetTabs > li.active > a").find("input[name='domainObjectUniqueKey']").val();
     var financialYear = $("#datasetTabs > li.active > a").find("input[name='financialYear']").val();
     var month = $("select[name='connectionMonth']").val();
     dataService.getTelkomMobileConnectionContainer(financialYear, domainObjectUniqueKey, productName, month, renderTelkomMobileConnectionContainerUI);
 });
 $("#channelReadiness").live("click", function (e) {
     e.preventDefault();
     e.preventDefault();
     renderBlockUI.renderBlockUI();
     $(".nav > li").removeClass("active");
     $(this).parent().addClass("active");
     dataService.getTelkomMobileChannelReadinessContainer('', '', true, 0, 50, 1, renderChannelReadinessUI);
 });
 renderChannelReadinessUI = function (data) {
     $.unblockUI();
     $("#dashBoardGraph").empty();
     $("#statsIndicatorTmpl").tmpl(data).appendTo("#dashBoardGraph");
     $("#content").empty();
     $("#readinessContainerTmpl").tmpl(data).appendTo("#content");
     if (data.ChartRows !== null) {
         chartSettings.height = 370;
         chartSettings.width = 1140;
         chartSettings.chartArea.left = 100;
         chartSettings.chartArea.top = 50;
         drawLineChartVisualization("graphArea", data.ChartColumns, data.ChartRows, chartSettings, data.ChartSelection);
     }
     $('.percentageCompetence').easyPieChart({
         animate: 1000,
         barColor: '#d6083b',
         onStep: function (value) {
             this.$el.find('span').text(~ ~value);
         }
     });
     $('.percentageCoverage').easyPieChart({
         animate: 1000,
         barColor: '#00b259',
         onStep: function (value) {
             this.$el.find('span').text(~ ~value);
         }
     });
     $('.percentageReadiness').easyPieChart({
         animate: 1000,
         barColor: '#005ba1',
         onStep: function (value) {
             this.$el.find('span').text(~ ~value);
         }
     });
 };
 $(".readinessContainer").live("click", function (e) {
     e.preventDefault();
     renderBlockUI.renderBlockUI();
     var readinessContainerKey = $(this).find("input[name='readinessContainerKey']").val();
     var readinessContainerName = $(this).find("input[name='readinessContainerName']").val();
     dataService.getTelkomMobileChannelReadinessContainer(readinessContainerKey, readinessContainerName, false, 0, 50, 1, renderChannelReadinessUI);
 });
 $(".curriculumTab").live("click", function (e) {
     e.preventDefault();
     renderBlockUI.renderBlockUI();
     var readinessContainerKey = $(this).find("input[name='readinessContainerKey']").val();
     var readinessContainerName = $(this).find("input[name='readinessContainerName']").val();
     var curriculumIndex = $(this).find("input[name='curriculumIndex']").val();
     dataService.getTelkomMobileChannelReadinessContainer(readinessContainerKey, readinessContainerName, false, 0, 50, 1, renderChannelReadinessUI);
 });
 $(".readinessEmployeePrevPage").live("click", function (e) {
     e.preventDefault();
     renderBlockUI.renderBlockUI();
     var readinessContainerKey = $(this).find("input[name='readinessContainerKey']").val();
     var readinessContainerName = $(this).find("input[name='readinessContainerName']").val();
     var page = $(this).find("input[name='page']").val();
     dataService.getTelkomMobileChannelReadinessContainer(readinessContainerKey, readinessContainerName, false, 0, 50, page, renderChannelReadinessUI);
 });
 $(".readinessEmployeeNextPage").live("click", function (e) {
     e.preventDefault();
     renderBlockUI.renderBlockUI();
     var readinessContainerKey = $(this).find("input[name='readinessContainerKey']").val();
     var readinessContainerName = $(this).find("input[name='readinessContainerName']").val();
     var page = $(this).find("input[name='page']").val();
     dataService.getTelkomMobileChannelReadinessContainer(readinessContainerKey, readinessContainerName, false, 0, 50, page, renderChannelReadinessUI);
 });
 $(".readinessEmployeePage").live("click", function (e) {
     e.preventDefault();
     renderBlockUI.renderBlockUI();
     var readinessContainerKey = $(this).find("input[name='readinessContainerKey']").val();
     var readinessContainerName = $(this).find("input[name='readinessContainerName']").val();
     var page = $(this).find("input[name='page']").val();
     dataService.getTelkomMobileChannelReadinessContainer(readinessContainerKey, readinessContainerName, false, 0, 50, page, renderChannelReadinessUI);
 });