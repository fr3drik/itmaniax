/*

        THIS JAVASCRIPT SOURCE CODE FILE CONTAINS CODE COMMONLY USED ON ALL OF THE PAGES
        DO NOT ADD PAGE SPECIFIC JAVASCRIPT TO THIS PAGE.
        Contents:
            BrowserCheck()
 */

// BrowserCheck Object
// provides most commonly needed browser checking variables
// 19990326
// Copyright (C) 1999 Dan Steinman
// Distributed under the terms of the GNU Library General Public License
// Available at http://www.dansteinman.com/dynapi/
function BrowserCheck() {
	var b = navigator.appName
	var agt = navigator.userAgent.toLowerCase();

	if (b == "Microsoft Internet Explorer") {
		this.b = "ie";
	} else {
		this.b = b;
	}

	this.v = parseInt(navigator.appVersion);
	this.minor = parseFloat(navigator.appVersion);
	this.ie = this.b == "ie" && this.v >= 4;
	this.ie6 = navigator.userAgent.indexOf('MSIE 6') > 0;
	this.ie7 = navigator.userAgent.indexOf('MSIE 7') > 0;
	this.ie8 = navigator.userAgent.indexOf('MSIE 8') > 0;
	var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	if (re.exec(navigator.userAgent) != null) {
		this.v = parseFloat(RegExp.$1);
	}
	this.ff = agt.indexOf('gecko') != -1
			&& (agt.indexOf('firefox') != -1 || agt.indexOf('mozilla') != -1)
			|| agt.indexOf('chrome') != -1;

	this.min = this.ff || this.ie
	var APPLE_WEB_KIT_LENGTH = 12;
	var VERSION_LENGTH = 8;
	var appleWebKitIndex = -1;
	var emptyIndex = -1;
	var appleWebKitIndex = agt.indexOf("applewebkit");
	var versionIndex = agt.indexOf("version");
	var macIndex = agt.indexOf("mac");
	// alert(appleWebKitIndex);
	// alert(versionIndex);
	// alert(macIndex);

	// As of safari 3.0, the safari user agent has the version information.
	if (macIndex >= 0 && appleWebKitIndex >= 0 && versionIndex >= 0) {
		emptyIndex = agt.indexOf(" ", appleWebKitIndex + APPLE_WEB_KIT_LENGTH);
		var appleWebKitBuild = parseInt(agt.substring(appleWebKitIndex
				+ APPLE_WEB_KIT_LENGTH, emptyIndex));
		// alert(appleWebKitBuild);
		emptyIndex = -1;
		emptyIndex = agt.indexOf(" ", versionIndex + VERSION_LENGTH);
		if (emptyIndex == -1) {
			emptyIndex = agt.length;
		}
		var safariBuild = parseInt(agt.substring(versionIndex + VERSION_LENGTH,
				emptyIndex));
		// alert(safariBuild);
		this.b = "sf";
		this.sf = this.b == "sf" && this.v >= 5 && safariBuild >= 3;
		this.min = this.min || this.sf;
	}
	// alert(this.sf);
	// ---------------------------------------------------------------
	// End Macintosh fixed PhongLe 02-09-2006
}

// automatically create the "is" object
is = new BrowserCheck()
// end Dan Steinman code
