﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using ITManiaX.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using ITManiaX.Repository.Interfaces;
using ITManiaX.Repository.Repositories;
using ITManiaX.WebUI.App_Start;
using System.Web.Optimization;
using ITManiaX.Service.Concrete;
using ITManiaX.Service.Interfaces;

namespace ITManiaX.WebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var builder = new ContainerBuilder();

            var config = GlobalConfiguration.Configuration;

            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<DomainObjectRepository>().As<IDomainObjectRepository>();
            builder.RegisterType<DomainObjectService>().As<IDomainObjectService>();
            builder.RegisterType<CurriculumRepository>().As<ICurriculumRepository>();
            builder.RegisterType<CurriculumService>().As<ICurriculumService>();
            builder.RegisterType<ApplicationService>().As<IApplicationService>();
            builder.RegisterType<ApplicationRepository>().As<IApplicationRepository>();
            builder.RegisterType<PromotionRepository>().As<IPromotionRepository>();
            builder.RegisterType<PromotionService>().As<IPromotionService>();
            builder.RegisterType<CallToActionRepository>().As<ICallToActionRepository>();
            builder.RegisterType<CallToActionService>().As<ICallToActionService>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<PageRepository>().As<IPageRepository>();
            builder.RegisterType<PageService>().As<IPageService>();
            builder.RegisterType<DbManager>().As<IDbManager>()
                .WithParameter("connectionStringName", "ITManiaX");


            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
