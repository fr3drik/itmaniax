﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ITManiaX.WebUI.Helpers
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString OutputText(this HtmlHelper helper, string text)
        {
            return MvcHtmlString.Create(text);
        }
        public static MvcHtmlString Slug(this HtmlHelper html, string title)
        {
            if (string.IsNullOrEmpty(title))
                return MvcHtmlString.Create("");
            title = title.Trim();
            StringBuilder stringBuilder = new StringBuilder(title.Length);
            bool flag = false;
            for (int index = 0; index < title.Length; ++index)
            {
                char ch = title[index];
                int num;
                switch (ch)
                {
                    case ' ':
                    case ',':
                    case '.':
                    case '/':
                    case '\\':
                        num = 0;
                        break;
                    default:
                        num = (int)ch != 45 ? 1 : 0;
                        break;
                }
                if (num == 0)
                {
                    if (!flag)
                    {
                        stringBuilder.Append('-');
                        flag = true;
                    }
                }
                else if ((int)ch >= 97 && (int)ch <= 122 || (int)ch >= 48 && (int)ch <= 57 || (int)ch >= 65 && (int)ch <= 90)
                {
                    stringBuilder.Append(ch);
                    flag = false;
                }
                if (index == 80)
                    break;
            }
            title = ((object)stringBuilder).ToString();
            if (title.EndsWith("-"))
                title = title.Substring(0, title.Length - 1);
            return MvcHtmlString.Create(title);
        }
        public static string Slug(string title)
        {
            if (string.IsNullOrEmpty(title))
                return string.Empty;
            title = title.Trim();
            StringBuilder stringBuilder = new StringBuilder(title.Length);
            bool flag = false;
            for (int index = 0; index < title.Length; ++index)
            {
                char ch = title[index];
                int num;
                switch (ch)
                {
                    case ' ':
                    case ',':
                    case '.':
                    case '/':
                    case '\\':
                        num = 0;
                        break;
                    default:
                        num = (int)ch != 45 ? 1 : 0;
                        break;
                }
                if (num == 0)
                {
                    if (!flag)
                    {
                        stringBuilder.Append('-');
                        flag = true;
                    }
                }
                else if ((int)ch >= 97 && (int)ch <= 122 || (int)ch >= 48 && (int)ch <= 57 || (int)ch >= 65 && (int)ch <= 90)
                {
                    stringBuilder.Append(ch);
                    flag = false;
                }
                if (index == 80)
                    break;
            }
            title = ((object)stringBuilder).ToString();
            if (title.EndsWith("-"))
                title = title.Substring(0, title.Length - 1);
            return title;
        }
    }
}