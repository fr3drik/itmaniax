﻿CREATE PROCEDURE [dbo].[ArchiveAssets]
AS
  TRUNCATE TABLE CurriculumCourseArchive
  TRUNCATE TABLE CurriculumArchive
  TRUNCATE TABLE CourseArchive

  INSERT INTO CurriculumCourseArchive
  SELECT CurriculumId, CourseId FROM CurriculumCourse

  INSERT INTO CurriculumArchive
  SELECT ParentCurriculumId, CurriculumName, CurriculumPath, ParentCurriculumPath, IsVendor, IsActive, [Description], Price, VAT, ExclVAT, IsFrontPage, DateCreated, DateUpdated FROM Curriculum

  INSERT INTO CourseArchive
  SELECT Asset_Title, Asset_ID, Asset_Category,Asset_Type,Asset_Installation_Status,Custom_Content,Catalog_Folder,Catalog_Path,Library_Path,Library_Topic,Asset_Description,Expected_Duration,Target_Audience,Prerequisites,Objectives,Price,VAT,ExclVAT,IsActive,DateCreated,DateUpdated FROM Course

  TRUNCATE TABLE CurriculumCourse
  TRUNCATE TABLE Curriculum
  TRUNCATE TABLE Course
RETURN 0
