﻿CREATE PROCEDURE [dbo].[SaveCallToAction]
	@ApplicationId INT,
	@CallToActionName NVARCHAR(255),
	@PageId INT = 0,
	@CurriculumId INT = 0,
	@DateCreated DATETIME,
	@DateUpdated DATETIME
AS
	DECLARE @CallToActionId INT
	INSERT INTO CallToAction (CallToActionName, ApplicationId, DateCreated, DateUpdated,PageId,CurriculumId)VALUES(@CallToActionName,@ApplicationId,@DateCreated,@DateUpdated,@PageId,@CurriculumId)
	SET @CallToActionId = SCOPE_IDENTITY()
	EXECUTE GetCallToAction @CallToActionId
RETURN 0
