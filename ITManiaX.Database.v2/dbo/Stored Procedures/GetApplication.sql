﻿CREATE PROCEDURE [dbo].[GetApplication]
	@ApplicationId INT
AS
	SELECT
		A.ApplicationId,
		A.ApplicationName,
		A.DateCreated,
		A.DateUpdated
	FROM
		[Application] A
	WHERE
		A.ApplicationId = @ApplicationId

	SELECT
		AP.ApplicationId,
		AP.ApplicationPropertyName,
		AP.ApplicationPropertyValue,
		AP.DateCreated,
		AP.DateUpdated,
		AP.Id
	FROM
		ApplicationProperty AP
	WHERE
		AP.ApplicationId = @ApplicationId
RETURN 0
