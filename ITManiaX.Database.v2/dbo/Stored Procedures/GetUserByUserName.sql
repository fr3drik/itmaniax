﻿CREATE PROCEDURE [dbo].[GetUserByUserName]
	@UserName NVARCHAR(255)
AS
	SELECT
		A.AccessFailedCount,
		A.Email,
		A.EmailConfirmed,
		A.Id,
		A.LockoutEnabled,
		A.LockoutEndDateUtc,
		A.PasswordHash,
		A.PhoneNumber,
		A.PhoneNumberConfirmed,
		A.SecurityStamp,
		A.TwoFactorEnabled,
		A.UserName
	FROM
		AspNetUsers A
	WHERE
		A.UserName = @UserName
RETURN 0
