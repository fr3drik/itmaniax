﻿CREATE PROCEDURE [dbo].[UpdatePage]
	@PageId INT,
	@DateUpdated DATETIME,
	@ApplicationId INT,
	@PageContentMarkDown NVARCHAR(MAX),
	@PageTitle NVARCHAR(255),
	@PageTitleSlug NVARCHAR(255),
	@CallToActionTemplateId INT = 0,
	@Displayorder INT = 0
AS
	UPDATE [Page] SET
		PageContentMarkDown = @PageContentMarkDown,
		PageTitle = @PageTitle,
		PageTitleSlug = @PageTitleSlug,
		DateUpdated = @DateUpdated,
		CallToActionTemplateId = @CallToActionTemplateId,
		DisplayOrder = @Displayorder
	WHERE
		Id = @PageId

	EXEC GetPage @PageId
RETURN 0
