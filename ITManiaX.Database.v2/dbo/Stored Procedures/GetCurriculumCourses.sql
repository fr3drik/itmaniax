﻿CREATE PROCEDURE [dbo].[GetCurriculumCourses]
	@CurriculumId INT
AS
	SELECT
		C.Asset_Category,
		C.Asset_Description,
		C.Asset_ID,
		C.Asset_Installation_Status,
		C.Asset_Title,
		C.Catalog_Folder,
		C.Catalog_Path,
		C.CourseId,
		C.Custom_Content,
		C.Expected_Duration,
		C.Objectives,
		C.Prerequisites,
		C.Target_Audience,
		C.Price,
		C.IsActive,
		C.VAT,
		C.ExclVAT
	FROM
		Course C JOIN CurriculumCourse CC ON C.CourseId = CC.CourseId
	WHERE
		CC.CurriculumId = @CurriculumId
		--C.Library_Path 
RETURN 0
