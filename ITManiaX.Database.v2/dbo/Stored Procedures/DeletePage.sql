﻿CREATE PROCEDURE [dbo].[DeletePage]
	@PageId INT
AS
	DELETE FROM [Page] WHERE Id = @PageId
RETURN 0
