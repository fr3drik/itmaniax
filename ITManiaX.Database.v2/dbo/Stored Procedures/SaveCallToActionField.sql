﻿CREATE PROCEDURE [dbo].[SaveCallToActionField]
	@CallToActionId INT,
	@DateCreated DATETIME,
	@DateUpdated DATETIME,
	@FieldName NVARCHAR(255),
	@FieldValue NVARCHAR(MAX)
AS
	INSERT INTO CallToActionField(DateCreated, DateUpdated, CallToActionId, FieldName, FieldValue)VALUES(@DateCreated,@DateUpdated,@CallToActionId,@FieldName,@FieldValue)
RETURN 0
