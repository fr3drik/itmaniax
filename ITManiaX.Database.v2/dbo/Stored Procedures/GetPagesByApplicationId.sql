﻿CREATE PROCEDURE [dbo].[GetPagesByApplicationId]
	@ApplicationId INT
AS
	SELECT
		P.ApplicationId,
		P.DateCreated,
		P.DateUpdated,
		P.Id,
		P.PageContentMarkDown,
		P.PageTitle,
		P.PageTitleSlug,
		P.CallToActionTemplateId,
		P.DisplayOrder
	FROM
		[Page] P
	WHERE 
		P.ApplicationId = @ApplicationId
	ORDER BY P.DisplayOrder
RETURN 0
