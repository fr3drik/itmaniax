﻿CREATE PROCEDURE [dbo].[SavePromotion]
	@DateCreated DATETIME,
	@DateUpdated DATETIME,
	@PromotionName NVARCHAR(255),
	@DisplayOrder INT,
	@Content NVARCHAR(MAX),
	@Active BIT
AS
	DECLARE @PromotionId INT
	INSERT INTO Promotion (DateCreated, DateUpdated, PromotionName, DisplayOrder, Content, Active)VALUES(@DateCreated,@DateUpdated,@PromotionName,@DisplayOrder,@Content,@Active)
	SET @PromotionId = SCOPE_IDENTITY()
	EXECUTE GetPromotion @PromotionId
RETURN 0
