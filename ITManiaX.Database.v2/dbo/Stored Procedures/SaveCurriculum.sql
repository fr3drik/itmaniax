﻿CREATE PROCEDURE [dbo].[SaveCurriculum]
	@CurriculumName NVARCHAR(MAX),
	@CurriculumPath NVARCHAR(MAX),
	@ParentCurriculumPath NVARCHAR(MAX)
AS
	INSERT INTO Curriculum(CurriculumName, CurriculumPath, ParentCurriculumPath) VALUES (@CurriculumName, @CurriculumPath, @ParentCurriculumPath)
RETURN 0
