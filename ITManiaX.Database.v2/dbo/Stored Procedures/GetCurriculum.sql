﻿CREATE PROCEDURE [dbo].[GetCurriculum]
	@CurriculumId INT,
	@CurriculumPath NVARCHAR(MAX)
AS
	SELECT 
		C.CurriculumId,
		C.CurriculumPath,
		C.CurriculumName,
		C.ParentCurriculumPath,
		c.ParentCurriculumId,
		C.IsVendor,
		C.[Description],
		C.ExclVAT,
		C.IsActive,
		C.Price,
		C.VAT
	FROM
		Curriculum C
	WHERE
		C.CurriculumId = @CurriculumId

	EXEC GetCurricula
	EXEC GetCoursesByCurriculumPath @CurriculumPath
RETURN 0
