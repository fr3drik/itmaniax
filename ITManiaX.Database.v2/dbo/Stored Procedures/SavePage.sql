﻿CREATE PROCEDURE [dbo].[SavePage]
	@DateCreated DATETIME,
	@DateUpdated DATETIME,
	@ApplicationId INT,
	@PageContentMarkDown NVARCHAR(MAX),
	@PageTitle NVARCHAR(255),
	@PageTitleSlug NVARCHAR(255),
	@CallToActionTemplateId INT = 0,
	@DisplayOrder INT = 0
AS
	DECLARE @PageId INT
	INSERT INTO [Page](DateCreated, DateUpdated, ApplicationId, PageContentMarkDown, PageTitle, PageTitleSlug,CallToActionTemplateId,DisplayOrder) 
	VALUES
	(@DateCreated, @DateUpdated, @ApplicationId, @PageContentMarkDown, @PageTitle, @PageTitleSlug,@CallToActionTemplateId,@DisplayOrder)
	SET @PageId = SCOPE_IDENTITY()
	EXEC GetPage @PageId
RETURN 0
