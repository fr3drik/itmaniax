﻿CREATE PROCEDURE [dbo].[GetPromotions]
AS
	SELECT
		P.Active,
		P.Content,
		P.DateCreated,
		P.DateUpdated,
		P.DisplayOrder,
		P.PromotionId,
		P.PromotionName
	FROM
		Promotion P
  WHERE
    P.Active = 1
  ORDER BY P.DisplayOrder
RETURN 0
