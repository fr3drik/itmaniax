﻿CREATE PROCEDURE [dbo].[UpdateCurriculum]
	@CurriculumId INT,
	@IsVendor BIT,
	@IsActive BIT,
	@IsFrontPage BIT,
	@Description NVARCHAR(MAX),
	@Price DECIMAL(18,2),
	@VAT DECIMAL(18,2),
	@ExclVAT DECIMAL(18,2)
AS
	UPDATE Curriculum SET IsVendor = @IsVendor, IsActive = @IsActive, [Description] = @Description, Price = @Price, VAT = @VAT, ExclVAT = @ExclVAT, IsFrontPage = @IsFrontPage
	WHERE
		CurriculumId = @CurriculumId
RETURN 0
