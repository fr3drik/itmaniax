﻿CREATE PROCEDURE [dbo].[GetPromotion]
	@PromotionId INT
AS
	SELECT
		P.Active,
		P.Content,
		P.DateCreated,
		P.DateUpdated,
		P.DisplayOrder,
		P.PromotionId,
		P.PromotionName
	FROM
		Promotion P
	WHERE
		P.PromotionId = @PromotionId
RETURN 0
