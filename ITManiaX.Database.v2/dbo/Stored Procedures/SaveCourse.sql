﻿CREATE PROCEDURE [dbo].[SaveCourse]
	@Asset_Category NVARCHAR(255),
	@Asset_Description NVARCHAR(MAX),
	@Asset_ID NVARCHAR(255),
	@Asset_Installation_Status NVARCHAR(255),
	@Asset_Title NVARCHAR(255),
	@Catalog_Folder NVARCHAR(MAX),
	@Catalog_Path NVARCHAR(MAX),
	@Custom_Content NVARCHAR(255),
	@Expected_Duration INT,
	@Objectives NVARCHAR(MAX),
	@Prerequisites NVARCHAR(MAX),
	@Target_Audience NVARCHAR(MAX)
AS
	INSERT INTO Course 
	(Asset_Category, 
	 Asset_Description, 
	 Asset_ID, 
	 Asset_Installation_Status, 
	 Asset_Title, 
	 Catalog_Folder, 
	 Catalog_Path, 
	 Custom_Content, 
	 Expected_Duration, 
	 Objectives, 
	 Prerequisites,
	 Target_Audience)
	 VALUES
	 (@Asset_Category,
	 @Asset_Description,
	 @Asset_ID,
	 @Asset_Installation_Status,
	 @Asset_Title,
	 @Catalog_Folder,
	 @Catalog_Path,
	 @Custom_Content,
	 @Expected_Duration,
	 @Objectives,
	 @Prerequisites,
	 @Target_Audience)
RETURN 0
