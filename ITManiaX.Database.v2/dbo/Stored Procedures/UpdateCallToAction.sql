﻿CREATE PROCEDURE [dbo].[UpdateCallToAction]
	@CallToActionId INT,
	@DateUpdated DATETIME,
	@FollowUpUserId NVARCHAR(128),
	@FollowedUp BIT
AS
	UPDATE CallToAction SET DateUpdated = @DateUpdated, FollowUpUserId = @FollowUpUserId, FollowedUp = @FollowedUp WHERE Id = @CallToActionId
	EXEC GetCallToAction @CallToActionId
RETURN 0
