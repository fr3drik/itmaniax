﻿CREATE PROCEDURE [dbo].[GetCurricula]
AS
	SELECT 
		C.CurriculumId,
		C.CurriculumPath,
		C.ParentCurriculumPath,
		C.ParentCurriculumId,
		C.CurriculumName,
		C.IsVendor,
		C.IsFrontPage,
		C.[Description],		
		C.ExclVAT,
		C.IsActive,
		C.Price,
		C.VAT
	FROM
		Curriculum C
RETURN 0
