﻿CREATE PROCEDURE [dbo].[UpdateCallToActionMessageSequenceTriggered]
	@CallToActionMessageSequenceId INT,
	@MessageTriggered BIT
AS
	UPDATE CallToActionMessageSequence SET MessageTriggered = @MessageTriggered WHERE Id = @CallToActionMessageSequenceId
RETURN 0
