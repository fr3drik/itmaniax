﻿CREATE PROCEDURE [dbo].[GetCallToAction]
	@CallToActionId INT
AS
	SELECT
		C.ApplicationId,
		C.CallToActionName,
		C.DateCreated,
		C.DateUpdated,
		C.Id
	FROM
		CallToAction C
	WHERE
		C.Id = @CallToActionId

	SELECT 
		C.CallToActionId,
		C.DateCreated,
		C.DateUpdated,
		C.FieldName,
		C.FieldValue,
		C.Id
	FROM
		CallToActionField C
	WHERE
		C.CallToActionId = @CallToActionId

	SELECT
		C.CallToActionId,
		C.CallToActionMessageContent,
		C.CallToActionMessageOrder,
		C.CallToActionMessageTitle,
		C.CallToActionMessageType,
		C.DateCreated,
		C.DateUpdated,
		C.Id,
		C.MessageTriggered
	FROM
		CallToActionMessageSequence C
	WHERE
		C.CallToActionId = @CallToActionId
RETURN 0
