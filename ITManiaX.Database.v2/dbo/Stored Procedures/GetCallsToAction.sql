﻿CREATE PROCEDURE [dbo].[GetCallsToAction]
	@ApplicationId INT
AS
	SELECT
		C.ApplicationId,
		C.CallToActionName,
		C.DateCreated,
		C.DateUpdated,
		C.Id,
		C.FollowedUp,
		C.FollowUpUserId,
		A.UserName As FollowUpUser,
		C.PageId,
		C.CurriculumId,
		P.PageTitle As SourcePage,
		M.CurriculumName As SourceCurriculum
	FROM
		CallToAction C LEFT JOIN AspNetUsers A ON A.Id = C.FollowUpUserId LEFT JOIN [Page] P ON P.Id = C.PageId LEFT JOIN Curriculum M ON M.CurriculumId = C.CurriculumId
	WHERE
		C.ApplicationId = @ApplicationId
	ORDER BY C.DateCreated DESC

	SELECT 
		C.CallToActionId,
		C.DateCreated,
		C.DateUpdated,
		C.FieldName,
		C.FieldValue,
		C.Id
	FROM
		CallToActionField C
	WHERE
		C.CallToActionId IN (SELECT C.Id FROM CallToAction C WHERE C.ApplicationId = @ApplicationId)
RETURN 0
