﻿CREATE PROCEDURE [dbo].[GetCallToActionTemplate]
	@CallToActionTemplateId INT
AS
	SELECT
		C.ApplicationId,
		C.CallToActionName,
		C.DateCreated,
		C.DateUpdated,
		C.Id
	FROM 
		CallToActionTemplate C
	WHERE
		C.Id = @CallToActionTemplateId

	SELECT
		C.CallToActionFieldName,
		C.CallToActionFieldOrder,
		C.CallToActionFieldRequired,
		C.CallToActionFieldType,
		C.CallToActionTemplateId,
		C.DateCreated,
		C.DateUpdated,
		C.Id,
		C.IsHtml,
		C.IsSelected,
		C.ValidationErrorMessage,
		C.ValidationRegex
	FROM
		CallToActionTemplateField C
	WHERE
		C.CallToActionTemplateId = @CallToActionTemplateId

	SELECT
		C.CallToActionMessageContent,
		C.CallToActionMessageOrder,
		C.CallToActionMessageTitle,
		C.CallToActionMessageType,
		C.CallToActionTemplateId,
		C.DateCreated,
		C.DateUpdated,
		C.Id,
		C.MessageTriggered
	FROM
		CallToActionTemplateMessageSequence C
	WHERE
		C.CallToActionTemplateId = @CallToActionTemplateId
RETURN 0
