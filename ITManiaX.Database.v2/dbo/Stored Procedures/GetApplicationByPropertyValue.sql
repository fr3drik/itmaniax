﻿CREATE PROCEDURE [dbo].[GetApplicationByPropertyValue]
	@ApplicationPropertyName NVARCHAR(255),
	@ApplicationPropertyValue NVARCHAR(MAX)
AS
	DECLARE @ApplicationId INT
	SET @ApplicationId = (SELECT A.ApplicationId FROM [Application] A JOIN ApplicationProperty AP ON A.ApplicationId = AP.ApplicationId WHERE	AP.ApplicationPropertyName = @ApplicationPropertyName AND AP.ApplicationPropertyValue = @ApplicationPropertyValue)
	EXECUTE GetApplication @ApplicationId
RETURN 0
