﻿CREATE PROCEDURE [dbo].[GetPage]
	@PageId INT
AS
	SELECT
		P.ApplicationId,
		P.DateCreated,
		P.DateUpdated,
		P.Id,
		P.PageContentMarkDown,
		P.PageTitle,
		P.PageTitleSlug,
		P.CallToActionTemplateId,
		P.DisplayOrder
	FROM
		[Page] P
	WHERE 
		P.Id = @PageId
RETURN 0
