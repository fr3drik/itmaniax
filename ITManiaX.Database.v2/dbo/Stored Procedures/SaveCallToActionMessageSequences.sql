﻿CREATE PROCEDURE [dbo].[SaveCallToActionMessageSequence]
	@DateCreated DATETIME,
	@DateUpdated DATETIME,
	@CallToActionId INT,
	@CallToActionMessageTitle NVARCHAR(255),
	@CallToActionMessageContent NVARCHAR(MAX),
	@CallToActionMessageOrder INT,
	@CallToActionMessageType NVARCHAR(255),
	@MessageTriggered BIT
AS
	INSERT INTO 
	CallToActionMessageSequence
	(DateCreated,DateUpdated,CallToActionId,CallToActionMessageTitle,CallToActionMessageContent,CallToActionMessageOrder,CallToActionMessageType,MessageTriggered)
	VALUES
	(@DateCreated,@DateUpdated,@CallToActionId,@CallToActionMessageTitle,@CallToActionMessageContent,@CallToActionMessageOrder,@CallToActionMessageType,@MessageTriggered)
RETURN 0
