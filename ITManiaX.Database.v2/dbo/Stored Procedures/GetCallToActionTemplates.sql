﻿CREATE PROCEDURE [dbo].[GetCallToActionTemplatesByApplication]
	@ApplicationId INT
AS
	SELECT
		C.ApplicationId,
		C.CallToActionName,
		C.DateCreated,
		C.DateUpdated,
		C.Id
	FROM
		CallToActionTemplate C
	WHERE
		C.ApplicationId = @ApplicationId

	SELECT
		C.CallToActionFieldName,
		C.CallToActionFieldOrder,
		C.CallToActionFieldRequired,
		C.CallToActionFieldType,
		C.CallToActionTemplateId,
		C.DateCreated,
		C.DateUpdated,
		C.Id,
		C.IsHtml,
		C.IsSelected,
		C.ValidationErrorMessage,
		C.ValidationRegex
	FROM
		CallToActionTemplateField C
	WHERE
		C.CallToActionTemplateId IN (SELECT	C.Id FROM CallToActionTemplate C WHERE C.ApplicationId = @ApplicationId)
RETURN 0
