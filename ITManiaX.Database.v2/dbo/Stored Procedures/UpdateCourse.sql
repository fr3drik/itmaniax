﻿CREATE PROCEDURE [dbo].[UpdateCourse]
	@CourseId INT,
	@Price DECIMAL(18,2),
	@VAT DECIMAL(18,2),
	@ExclVAT DECIMAL(18,2),
	@IsActive BIT
AS
	UPDATE Course SET Price = @Price, VAT = @VAT, ExclVAT = @ExclVAT, IsActive = @IsActive WHERE CourseId = @CourseId
	EXEC GetCourse @CourseId
RETURN 0
