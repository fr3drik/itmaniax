﻿CREATE TABLE [dbo].[CurriculumCourseArchive]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY,
	CurriculumArchiveId INT NOT NULL,
	CourseArchiveId INT NOT NULL
)
