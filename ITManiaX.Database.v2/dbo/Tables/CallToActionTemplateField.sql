﻿CREATE TABLE [dbo].[CallToActionTemplateField]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY,
	DateCreated DATETIME NOT NULL,
	DateUpdated DATETIME NOT NULL,
	CallToActionTemplateId INT NOT NULL,
	CallToActionFieldName NVARCHAR(255) NOT NULL,
	CallToActionFieldRequired BIT NOT NULL,
	CallToActionFieldType NVARCHAR(255) NOT NULL,
	CallToActionFieldOrder INT NOT NULL,
	IsHtml BIT NOT NULL,
	IsSelected BIT NOT NULL,
	ValidationErrorMessage NVARCHAR(MAX) NULL,
	ValidationRegex NVARCHAR(MAX) NULL
)
