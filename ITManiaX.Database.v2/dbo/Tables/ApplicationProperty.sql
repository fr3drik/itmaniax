﻿CREATE TABLE [dbo].[ApplicationProperty]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY,
	DateCreated DATETIME NOT NULL,
	DateUpdated DATETIME NOT NULL,
	ApplicationId INT NOT NULL,
	ApplicationPropertyName NVARCHAR(255) NOT NULL,
	ApplicationPropertyValue NVARCHAR(MAX) NOT NULL
)
