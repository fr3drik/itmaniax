﻿CREATE TABLE [dbo].[CurriculumArchive] (
    [CurriculumArchiveId]     INT            NOT NULL IDENTITY,
	[ParentCurriculumId] INT NULL,
    [CurriculumName]   NVARCHAR (MAX) NOT NULL,
	[CurriculumPath] NVARCHAR(MAX) NOT NULL,
	[ParentCurriculumPath] NVARCHAR(MAX) NULL,
	IsVendor BIT NULL,
	IsActive BIT NULL,
	[Description] NVARCHAR(MAX),
	Price DECIMAL(18,2) NULL,
	VAT DECIMAL(18,2) NULL,
	ExclVAT DECIMAL(18,2) NULL,
	IsFrontPage BIT NULL,
  DateCreated DATETIME NULL,
  DateUpdated DATETIME NULL
    CONSTRAINT [PK_CurriculumArchivw] PRIMARY KEY CLUSTERED ([CurriculumArchiveId] ASC)
);

