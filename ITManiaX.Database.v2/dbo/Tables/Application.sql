﻿CREATE TABLE [dbo].[Application]
(
	[ApplicationId] INT IDENTITY NOT NULL PRIMARY KEY,
	DateCreated DATETIME NOT NULL,
	DateUpdated DATETIME NOT NULL,
	ApplicationName NVARCHAR(255) NOT NULL
)
