﻿CREATE TABLE [dbo].[CurriculumCourse]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY,
	CurriculumId INT NOT NULL,
	CourseId INT NOT NULL,
  DateUpdated DATETIME NULL,
  DateCreated DATETIME NULL
)
