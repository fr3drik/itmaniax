﻿CREATE TABLE [dbo].[CallToActionTemplate]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY,
	DateCreated DATETIME NOT NULL,
	DateUpdated DATETIME NOT NULL,
	[CallToActionName] NVARCHAR(255) NOT NULL,
	ApplicationId INT NOT NULL
)
