﻿CREATE TABLE [dbo].[Page]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY,
	DateCreated DATETIME NOT NULL,
	DateUpdated DATETIME NOT NULL,
	ApplicationId INT NOT NULL,
	PageTitle NVARCHAR(255),
	PageTitleSlug NVARCHAR(255),
	PageContentMarkDown NVARCHAR(MAX),
	CallToActionTemplateId INT NULL,
	DisplayOrder INT NULL
)
