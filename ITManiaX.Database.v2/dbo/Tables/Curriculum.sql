﻿CREATE TABLE [dbo].[Curriculum] (
    [CurriculumId]     INT            NOT NULL IDENTITY,
	[ParentCurriculumId] INT NULL,
    [CurriculumName]   NVARCHAR (MAX) NOT NULL,
	[CurriculumPath] NVARCHAR(MAX) NOT NULL,
	[LibraryPath] NVARCHAR(MAX) NULL,
	[ParentCurriculumPath] NVARCHAR(MAX) NULL,
	IsVendor BIT NULL,
	IsActive BIT NULL,
	[Description] NVARCHAR(MAX),
	Price DECIMAL(18,2) NULL,
	VAT DECIMAL(18,2) NULL,
	ExclVAT DECIMAL(18,2) NULL,
	IsFrontPage BIT NULL,
  DateCreated DATETIME NULL,
  DateUpdated DATETIME NULL
    CONSTRAINT [PK_Curriculum] PRIMARY KEY CLUSTERED ([CurriculumId] ASC)
);

