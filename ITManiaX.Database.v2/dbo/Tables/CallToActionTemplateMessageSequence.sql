﻿CREATE TABLE [dbo].[CallToActionTemplateMessageSequence]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY,
	DateCreated DATETIME NOT NULL,
	DateUpdated DATETIME NOT NULL,
	CallToActionTemplateId INT NOT NULL,
	CallToActionMessageTitle NVARCHAR(MAX) NOT NULL,
	CallToActionMessageContent NVARCHAR(MAX) NOT NULL,
	CallToActionMessageOrder INT NOT NULL,
	CallToActionMessageType NVARCHAR(255) NOT NULL,
	MessageTriggered BIT NOT NULL
)
