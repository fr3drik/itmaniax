﻿CREATE PROCEDURE GetDomainObjectByKey
	-- Add the parameters for the stored procedure here
	@UniqueKey UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @DomainObjectId INT
	SET @DomainObjectId = (SELECT Id FROM DomainObjects WHERE UniqueKey = @UniqueKey)

	EXEC GetDomainObjectById @DomainObjectId
END