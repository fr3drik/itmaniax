﻿CREATE PROCEDURE [dbo].[GetDomainObjectsByDomainObjectTypeName]
	@DomainObjectTypeName NVARCHAR(MAX)
AS
	DECLARE @DomainObjectTypeId INT
	SET @DomainObjectTypeId = (SELECT TOP 1 Id FROM DomainObjectTypes WHERE DomainObjectTypeName = @DomainObjectTypeName)

	SELECT D.[Id]
		  ,D.[UniqueKey]
		  ,D.[DateCreated]
		  ,D.[DateUpdated]
		  ,D.[DomainObjectTypeId]
		  ,DT.DomainObjectTypeName
		  ,DF.[Id] AS DomainObjectFieldId
		  ,DF.[FieldName]
		  ,DF.[FieldValue]
		  ,DF.[DomainObjectId]
		  ,DF.[DateCreated] As DomainObjectFieldDateCreated
		  ,DF.[DateUpdated] As DomainObjectFieldDateUpdated
		  ,DF.[UniqueKey] As DomainObjectFieldUniqueKey
	FROM DomainObjects D JOIN DomainObjectFields DF ON D.Id = DF.DomainObjectId JOIN DomainObjectTypes DT ON DT.Id = D.DomainObjectTypeId
	WHERE
		D.DomainObjectTypeId = @DomainObjectTypeId
RETURN 0
