﻿CREATE PROCEDURE GetDomainObjectById 
	-- Add the parameters for the stored procedure here
	@DomainObjectId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT D.[Id]
		  ,D.[UniqueKey]
		  ,D.[DateCreated]
		  ,D.[DateUpdated]
		  ,D.[DomainObjectTypeId]
		  ,DT.DomainObjectTypeName
		  ,DF.[Id] AS DomainObjectFieldId
		  ,DF.[FieldName]
		  ,DF.[FieldValue]
		  ,DF.[DomainObjectId]
		  ,DF.[DateCreated] As DomainObjectFieldDateCreated
		  ,DF.[DateUpdated] As DomainObjectFieldDateUpdated
		  ,DF.[UniqueKey] As DomainObjectFieldUniqueKey
	FROM DomainObjects D JOIN DomainObjectFields DF ON D.Id = DF.DomainObjectId JOIN DomainObjectTypes DT ON DT.Id = D.DomainObjectTypeId
	WHERE
		D.Id = @DomainObjectId

	SELECT D.[Id]
		  ,D.[UniqueKey]
		  ,D.[DateCreated]
		  ,D.[DateUpdated]
		  ,D.[DomainObjectTypeId]
		  ,DT.DomainObjectTypeName
		  ,DF.[Id] As DomainObjectFieldId
		  ,DF.[FieldName]
		  ,DF.[FieldValue]
		  ,DF.[DomainObjectId]
		  ,DF.[DateCreated] As DomainObjectFieldDateCreated
		  ,DF.[DateUpdated] As DomainObjectFieldDateUpdated
		  ,DF.[UniqueKey] As DomainObjectFieldUniqueKey
	FROM DomainObjects D JOIN DomainObjectFields DF ON D.Id = DF.DomainObjectId JOIN DomainObjectTypes DT ON DT.Id = D.DomainObjectTypeId
	WHERE D.Id IN (SELECT DomainObjectChildId FROM DomainObjectChilds WHERE DomainObjectId = @DomainObjectId)
END