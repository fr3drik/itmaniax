﻿CREATE PROCEDURE GetDomainObjectByDomainObjectFieldValue
	-- Add the parameters for the stored procedure here
	@DomainObjectFieldName NVARCHAR(MAX),
	@DomainObjectFieldValue NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @DomainObjectId INT
	SET @DomainObjectId = (SELECT TOP 1 DomainObjectId FROM DomainObjectFields WHERE FieldName = @DomainObjectFieldName AND FieldValue = @DomainObjectFieldValue)

	EXEC GetDomainObjectById @DomainObjectId
END