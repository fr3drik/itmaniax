﻿CREATE TABLE [dbo].[DomainObjectPropertyValueTypes] (
    [Id]                                 INT              IDENTITY (1, 1) NOT NULL,
    [DateCreated]                        DATETIME         NOT NULL,
    [DateUpdated]                        DATETIME         NOT NULL,
    [DomainObjectPropertyValueTypeValue] NVARCHAR (MAX)   NOT NULL,
    [UniqueKey]                          UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_DomainObjectPropertyValueTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

