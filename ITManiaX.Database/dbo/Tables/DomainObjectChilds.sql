﻿CREATE TABLE [dbo].[DomainObjectChilds] (
    [Id]                  INT              IDENTITY (1, 1) NOT NULL,
    [DomainObjectId]      INT              NOT NULL,
    [DomainObjectChildId] INT              NOT NULL,
    [DateCreated]         DATETIME         NOT NULL,
    [DateUpdated]         DATETIME         NOT NULL,
    [UniqueKey]           UNIQUEIDENTIFIER NOT NULL,
    [History]             BIT              CONSTRAINT [DF_DomainObjectChilds_History] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_DomainObjectChilds] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DomainObjectDomainObjectChild] FOREIGN KEY ([DomainObjectId]) REFERENCES [dbo].[DomainObjects] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_DomainObjectDomainObjectChild]
    ON [dbo].[DomainObjectChilds]([DomainObjectId] ASC);

