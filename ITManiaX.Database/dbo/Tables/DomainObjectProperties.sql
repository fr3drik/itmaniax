﻿CREATE TABLE [dbo].[DomainObjectProperties] (
    [Id]                                     INT              IDENTITY (1, 1) NOT NULL,
    [DateCreated]                            DATETIME         NOT NULL,
    [DateUpdated]                            DATETIME         NOT NULL,
    [DomainObjectId]                         INT              NOT NULL,
    [DomainObjectPropertyValueTypeId]        INT              NOT NULL,
    [DomainObjectPropertyNameTypeId]         INT              NOT NULL,
    [DomainObjectPropertyDataTypeNameTypeId] INT              NOT NULL,
    [UniqueKey]                              UNIQUEIDENTIFIER NOT NULL,
    [IsEditable]                             BIT              NULL,
    CONSTRAINT [PK_DomainObjectProperties] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DomainObjectDomainObjectProperty] FOREIGN KEY ([DomainObjectId]) REFERENCES [dbo].[DomainObjects] ([Id]),
    CONSTRAINT [FK_DomainObjectPropertyDataTypeNameTypeDomainObjectProperty] FOREIGN KEY ([DomainObjectPropertyDataTypeNameTypeId]) REFERENCES [dbo].[DomainObjectPropertyDataTypeNameTypes] ([Id]),
    CONSTRAINT [FK_DomainObjectPropertyNameTypeDomainObjectProperty] FOREIGN KEY ([DomainObjectPropertyNameTypeId]) REFERENCES [dbo].[DomainObjectPropertyNameTypes] ([Id]),
    CONSTRAINT [FK_DomainObjectPropertyValueTypeDomainObjectProperty] FOREIGN KEY ([DomainObjectPropertyValueTypeId]) REFERENCES [dbo].[DomainObjectPropertyValueTypes] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_DomainObjectDomainObjectProperty]
    ON [dbo].[DomainObjectProperties]([DomainObjectId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_DomainObjectPropertyValueTypeDomainObjectProperty]
    ON [dbo].[DomainObjectProperties]([DomainObjectPropertyValueTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_DomainObjectPropertyNameTypeDomainObjectProperty]
    ON [dbo].[DomainObjectProperties]([DomainObjectPropertyNameTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_DomainObjectPropertyDataTypeNameTypeDomainObjectProperty]
    ON [dbo].[DomainObjectProperties]([DomainObjectPropertyDataTypeNameTypeId] ASC);

