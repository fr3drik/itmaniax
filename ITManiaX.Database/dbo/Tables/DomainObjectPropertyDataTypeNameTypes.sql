﻿CREATE TABLE [dbo].[DomainObjectPropertyDataTypeNameTypes] (
    [Id]                                    INT              IDENTITY (1, 1) NOT NULL,
    [DateCreated]                           DATETIME         NOT NULL,
    [DateUpdated]                           DATETIME         NOT NULL,
    [UniqueKey]                             UNIQUEIDENTIFIER NOT NULL,
    [DomainObjectPropertyDataTypeNameValue] NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_DomainObjectPropertyDataTypeNameTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

