﻿CREATE TABLE [dbo].[DomainObjectTemplates] (
    [Id]                             INT              IDENTITY (1, 1) NOT NULL,
    [DateCreated]                    DATETIME         NOT NULL,
    [DateUpdated]                    DATETIME         NOT NULL,
    [UniqueKey]                      UNIQUEIDENTIFIER NOT NULL,
    [DomainObjectTemplateNameTypeId] INT              NOT NULL,
    CONSTRAINT [PK_DomainObjectTemplates] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DomainObjectTemplateNameTypeDomainObjectTemplate] FOREIGN KEY ([DomainObjectTemplateNameTypeId]) REFERENCES [dbo].[DomainObjectTemplateNameTypes] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_DomainObjectTemplateNameTypeDomainObjectTemplate]
    ON [dbo].[DomainObjectTemplates]([DomainObjectTemplateNameTypeId] ASC);

