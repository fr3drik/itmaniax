﻿CREATE TABLE [dbo].[DomainObjectPropertyNameTypes] (
    [Id]                                INT              IDENTITY (1, 1) NOT NULL,
    [DateCreated]                       DATETIME         NOT NULL,
    [DateUpdated]                       DATETIME         NOT NULL,
    [UniqueKey]                         UNIQUEIDENTIFIER NOT NULL,
    [DomainObjectPropertyNameTypeValue] NVARCHAR (MAX)   NOT NULL,
    [IsLookup]                          BIT              NULL,
    CONSTRAINT [PK_DomainObjectPropertyNameTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

