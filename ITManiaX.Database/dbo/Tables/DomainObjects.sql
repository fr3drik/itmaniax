﻿CREATE TABLE [dbo].[DomainObjects] (
    [Id]                 INT              IDENTITY (1, 1) NOT NULL,
    [UniqueKey]          UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]        DATETIME         NOT NULL,
    [DateUpdated]        DATETIME         NOT NULL,
    [DomainObjectTypeId] INT              NOT NULL,
    CONSTRAINT [PK_DomainObjects] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DomainObjectTypeDomainObject] FOREIGN KEY ([DomainObjectTypeId]) REFERENCES [dbo].[DomainObjectTypes] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_DomainObjectTypeDomainObject]
    ON [dbo].[DomainObjects]([DomainObjectTypeId] ASC);

