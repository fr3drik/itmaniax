﻿CREATE TABLE [dbo].[DomainObjectTemplatePropertyDomainObject] (
    [DomainObjectTemplateProperties_Id] INT NOT NULL,
    [DomainObjects_Id]                  INT NOT NULL,
    CONSTRAINT [PK_DomainObjectTemplatePropertyDomainObject] PRIMARY KEY NONCLUSTERED ([DomainObjectTemplateProperties_Id] ASC, [DomainObjects_Id] ASC),
    CONSTRAINT [FK_DomainObjectTemplatePropertyDomainObject_DomainObject] FOREIGN KEY ([DomainObjects_Id]) REFERENCES [dbo].[DomainObjects] ([Id]),
    CONSTRAINT [FK_DomainObjectTemplatePropertyDomainObject_DomainObjectTemplateProperty] FOREIGN KEY ([DomainObjectTemplateProperties_Id]) REFERENCES [dbo].[DomainObjectTemplateProperties] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_DomainObjectTemplatePropertyDomainObject_DomainObject]
    ON [dbo].[DomainObjectTemplatePropertyDomainObject]([DomainObjects_Id] ASC);

