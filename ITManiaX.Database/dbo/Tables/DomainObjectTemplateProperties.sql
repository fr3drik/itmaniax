﻿CREATE TABLE [dbo].[DomainObjectTemplateProperties] (
    [Id]                                     INT              IDENTITY (1, 1) NOT NULL,
    [DateCreated]                            DATETIME         NOT NULL,
    [DateUpdated]                            DATETIME         NOT NULL,
    [DomainObjectTemplateId]                 INT              NOT NULL,
    [DomainObjectTemplatePropertyName]       NVARCHAR (MAX)   NOT NULL,
    [DomainObjectTemplatePropertyDataType]   NVARCHAR (MAX)   NOT NULL,
    [UniqueKey]                              UNIQUEIDENTIFIER NOT NULL,
    [DomainObjectTemplatePropertyStartValue] NVARCHAR (MAX)   NOT NULL,
    [Required]                               BIT              NOT NULL,
    [Editable]                               BIT              NOT NULL,
    [Order]                                  INT              NOT NULL,
    CONSTRAINT [PK_DomainObjectTemplateProperties] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DomainObjectTemplateDomainObjectTemplateProperty] FOREIGN KEY ([DomainObjectTemplateId]) REFERENCES [dbo].[DomainObjectTemplates] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_DomainObjectTemplateDomainObjectTemplateProperty]
    ON [dbo].[DomainObjectTemplateProperties]([DomainObjectTemplateId] ASC);

