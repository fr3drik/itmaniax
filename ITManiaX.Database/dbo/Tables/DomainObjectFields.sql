﻿CREATE TABLE [dbo].[DomainObjectFields] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [FieldName]      NVARCHAR (MAX)   NOT NULL,
    [FieldValue]     NVARCHAR (MAX)   NOT NULL,
    [DomainObjectId] INT              NOT NULL,
    [DateCreated]    DATETIME         NOT NULL,
    [DateUpdated]    DATETIME         NOT NULL,
    [UniqueKey]      UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_DomainObjectFields] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DomainObjectDomainObjectField] FOREIGN KEY ([DomainObjectId]) REFERENCES [dbo].[DomainObjects] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_DomainObjectDomainObjectField]
    ON [dbo].[DomainObjectFields]([DomainObjectId] ASC);

