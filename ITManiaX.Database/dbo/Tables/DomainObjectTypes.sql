﻿CREATE TABLE [dbo].[DomainObjectTypes] (
    [Id]                   INT              IDENTITY (1, 1) NOT NULL,
    [DateCreated]          DATETIME         NOT NULL,
    [DateUpdated]          DATETIME         NOT NULL,
    [DomainObjectTypeName] NVARCHAR (MAX)   NOT NULL,
    [UniqueKey]            UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_DomainObjectTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

