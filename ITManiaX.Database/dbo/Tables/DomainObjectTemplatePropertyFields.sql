﻿CREATE TABLE [dbo].[DomainObjectTemplatePropertyFields] (
    [Id]                             INT              IDENTITY (1, 1) NOT NULL,
    [DateCreated]                    DATETIME         NOT NULL,
    [DateUpdated]                    DATETIME         NOT NULL,
    [UniqueKey]                      UNIQUEIDENTIFIER NOT NULL,
    [DomainObjectTemplatePropertyId] INT              NOT NULL,
    [DomainObjectPropertyId]         INT              NOT NULL,
    CONSTRAINT [PK_DomainObjectTemplatePropertyFields] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DomainObjectTemplatePropertyDomainObjectTemplatePropertyFields] FOREIGN KEY ([DomainObjectTemplatePropertyId]) REFERENCES [dbo].[DomainObjectTemplateProperties] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_DomainObjectTemplatePropertyDomainObjectTemplatePropertyFields]
    ON [dbo].[DomainObjectTemplatePropertyFields]([DomainObjectTemplatePropertyId] ASC);

