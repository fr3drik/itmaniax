﻿CREATE TABLE [dbo].[DomainObjectTemplateNameTypes] (
    [Id]                           INT              IDENTITY (1, 1) NOT NULL,
    [DateCreated]                  DATETIME         NOT NULL,
    [DateUpdated]                  DATETIME         NOT NULL,
    [UniqueKey]                    UNIQUEIDENTIFIER NOT NULL,
    [DomainObjectTemplateTypeName] NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_DomainObjectTemplateNameTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

