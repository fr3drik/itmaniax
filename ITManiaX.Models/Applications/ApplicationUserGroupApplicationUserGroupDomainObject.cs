﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Applications
{
    public class ApplicationUserGroupApplicationUserGroupDomainObject
    {
        public int ApplicationUserGroups_Id { get; set; }
        public int ApplicationUserGroupDomainObjects_Id { get; set; }
    }
}
