﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Applications
{
    public class ApplicationSection
    {
        public int Id { get; set; }
        public int ApplicationId { get; set; }
        public string ApplicationSectionName { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public int SortOrder { get; set; }
        public int MenuIndex { get; set; }
        public Guid Uniquekey { get; set; }
        public string URL { get; set; }
        public int DomainObjectId { get; set; }
    }
}
