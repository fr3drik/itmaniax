﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Applications
{
    public class ApplicationSectionDomainObjects
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid Uniquekey { get; set; }
        public int DomainObjectId { get; set; }
        public int ChildPageSize { get; set; }
        public int ApplicationSectionId { get; set; }
    }
}
