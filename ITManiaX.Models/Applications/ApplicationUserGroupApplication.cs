﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Applications
{
    public class ApplicationUserGroupApplication
    {
        public int ApplicationUserGroups_Id { get; set; }
        public int Applications_Id { get; set; }
    }
}
