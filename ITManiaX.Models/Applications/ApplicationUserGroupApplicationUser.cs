﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Applications
{
    public class ApplicationUserGroupApplicationUser
    {
        public int ApplicationUserGroups_Id { get; set; }
        public int ApplicationUsers_Id { get; set; }
    }
}
