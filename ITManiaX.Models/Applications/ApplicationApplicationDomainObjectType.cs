﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Applications
{
    public class ApplicationApplicationDomainObjectType
    {
        public int Applications_Id { get; set; }
        public int ApplicationDomainObjectTypes_Id { get; set; }
    }
}
