﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Applications.DTO
{
    public class ApplicationModel
    {
        public string GoogleAnalyticsTrackingCode { get; set; }
        public string PageTitle { get; set; }
        public string ContactEmailAddress { get; set; }
        public IList<SystemApplicationPromotion> SystemApplicationPromotions { get; set; }
        public IList<ApplicationMenu> ApplicationMenus { get; set; }
        public IList<SkillPortCurriculum> SkillPortCurricula { get; set; }
    }
    public class SystemApplicationPromotion
    {
        public Guid SystemApplicationPromotionKey { get; set; }
        public string SystemApplicationPromotionName { get; set; }
        public int SystemApplicationPromotionOrder { get; set; }
        public string SystemApplicationPromotionContent { get; set; }
        public bool SystemApplicationPromotionActive { get; set; }
    }
    public class ApplicationMenu
    {
        public string PagePosition { get; set; }
        public string AlternateUrl { get; set; }
        public string MenuItemTitle { get; set; }
        public int DomainObjectId { get; set; }
        public int MenuItemOrder { get; set; }
    }
    public class SkillPortCurriculum
    {
        public Guid SkillPortCurriculumKey { get; set; }
        public string SkillPortCurriculumName { get; set; }
        public string SkillPortCurriculumFormattedName { get; set; }
        public List<string> SkillPortCurriculumPath { get; set; }
        public List<string> SkillPortCurriculumParentPath { get; set; }
        public List<SkillPortCurriculum> ChildCurricula { get; set; }
        public bool IsRoot { get; set; }
        public Guid Root { get; set; }
        public Decimal Duration { get; set; }
        public List<SkillPortCurriculumAsset> Assets { get; set; }
        public Guid DomainObjectUniqueKey { get; set; }
        public int DomainObjectId { get; set; }
        public bool IsVisible { get; set; }
        public bool IsVendor { get; set; }
        public bool HasPrice { get; set; }
        public bool RequiresPrice { get; set; }
        public bool HasWriteUp { get; set; }
        public bool RequiresWriteUp { get; set; }
        public bool IsNew { get; set; }
        public int CurriculumLevel { get; set; }
    }
    public class SkillPortCurriculumAsset
    {
        public string SkillPortCurriculumAssetId { get; set; }
        public string SkillPortCurriculumAssetTitle { get; set; }
        public string SkillPortCurriculumAssetType { get; set; }
        public Decimal SkillPortCurriculumAssetDuration { get; set; }
    }
}
