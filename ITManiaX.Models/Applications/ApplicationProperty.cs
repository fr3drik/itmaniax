﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Applications
{
    public class ApplicationProperty
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string ApplicationPropertyName { get; set; }
        public string ApplicationPropertyValue { get; set; }
        public int ApplicationId { get; set; }
    }
}
