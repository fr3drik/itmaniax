﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.CallsToAction
{
    public class CallToAction
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string CallToActionName { get; set; }
        public int ApplicationId { get; set; }
        public IList<CallToActionField> CallToActionFields { get; set; }
        public string FollowUpUser { get; set; }
        public bool FollowedUp { get; set; }
        public int? PageId { get; set; }
        public int? CurriculumId { get; set; }
        public string SourcePage { get; set; }
        public string SourceCurriculum { get; set; }
    }
}
