﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.CallsToAction
{
    public class CallToActionMessageSequence
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public int CallToActionId { get; set; }
        public string CallToActionMessageTitle { get; set; }
        public string CallToActionMessageContent { get; set; }
        public int CallToActionMessageOrder { get; set; }
        public string CallToActionMessageType { get; set; }
        public bool MessageTriggered { get; set; }
    }
}
