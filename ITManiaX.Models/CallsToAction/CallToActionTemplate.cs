﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.CallsToAction
{
    public class CallToActionTemplate
    {
        public int Id { get; set; }
        public string CallToActionName { get; set; }
        public int ApplicationId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
