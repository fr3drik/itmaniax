﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.CallsToAction
{
    public class CallToActionField
    {
        public int Id { get; set; }
        public int CallToActionId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
    }
}
