﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.CallsToAction
{
    public class CallToActionTemplateField
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public int CallToActionTemplateId { get; set; }
        public string CallToActionFieldName { get; set; }
        public bool CallToActionFieldRequired { get; set; }
        public string CallToActionFieldType { get; set; }
        public int CallToActionFieldOrder { get; set; }
        public bool IsHtml { get; set; }
        public bool IsSelected { get; set; }
        public string ValidationErrorMessage { get; set; }
        public string ValidationRegex { get; set; }
    }
}
