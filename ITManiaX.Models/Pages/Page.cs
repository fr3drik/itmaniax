﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models
{
    public class Page
    {
        public int Id { get; set; }
        public int ApplicationId { get; set; }
        public string PageTitle { get; set; }
        public string PageTitleSlug { get; set; }
        public string PageContentMarkDown { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public int? CallToActionTemplateId { get; set; }
        public int? DisplayOrder { get; set; }
    }
}
