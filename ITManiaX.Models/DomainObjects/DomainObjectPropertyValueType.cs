﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.DomainObjects
{
    public class DomainObjectPropertyValueType
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string DomainObjectPropertyValueTypeValue { get; set; }
        public Guid UniqueKey { get; set; }
    }
}
