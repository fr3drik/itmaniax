﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.DomainObjects
{
    public class DomainObject
    {
        public int Id { get; set; }
        public Guid UniqueKey { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public int DomainObjectTypeId { get; set; }
    }
}
