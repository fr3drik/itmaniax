﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.DomainObjects
{
    public class DomainObjectTemplateProperty
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public int DomainObjectTemplateId { get; set; }
        public string DomainObjectTemplatePropertyName { get; set; }
        public string DomainObjectTemplatePropertyDataType { get; set; }
        public Guid UniqueKey { get; set; }
        public string DomainObjectTemplatePropertyStartValue { get; set; }
        public bool Required { get; set; }
        public bool Editable { get; set; }
        public int Order { get; set; }
    }
}
