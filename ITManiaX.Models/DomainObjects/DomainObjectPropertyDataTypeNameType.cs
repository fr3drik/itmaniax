﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.DomainObjects
{
    public class DomainObjectPropertyDataTypeNameType
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid UniqueKey { get; set; }
        public string DomainObjectPropertyDataTypeNameValue { get; set; }
    }
}
