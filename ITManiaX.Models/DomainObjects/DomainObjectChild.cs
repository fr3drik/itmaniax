﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.DomainObjects
{
    public class DomainObjectChild
    {
        public int Id { get; set; }
        public int DomainObjectId { get; set; }
        public int DomainObjectChildId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid UniqueKey { get; set; }
        public bool History { get; set; }
    }
}
