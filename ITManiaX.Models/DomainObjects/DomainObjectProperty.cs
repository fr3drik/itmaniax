﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.DomainObjects
{
    public class DomainObjectProperty
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public int DomainObjectId { get; set; }
        public int DomainObjectPropertyValueTypeId { get; set; }
        public int DomainObjectPropertyNameTypeId { get; set; }
        public int DomainObjectPropertyDataTypeNameTypeId { get; set; }
        public Guid UniqueKey { get; set; }
        public bool IsEditable { get; set; }
    }
}
