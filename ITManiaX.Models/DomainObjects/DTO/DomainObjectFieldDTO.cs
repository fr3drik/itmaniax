﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.DomainObjects.DTO
{
    public class DomainObjectFieldDTO
    {
        public int Id { get; set; }
        public Guid UniqueKey { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public int DomainObjectTypeId { get; set; }
        public string DomainObjectTypeName { get; set; }
        public int DomainObjectFieldId { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public int DomainObjectId { get; set; }
        public DateTime DomainObjectFieldDateCreated { get; set; }
        public DateTime DomainObjectFieldDateUpdated { get; set; }
        public Guid DomainObjectFieldUniqueKey { get; set; }
    }
}
