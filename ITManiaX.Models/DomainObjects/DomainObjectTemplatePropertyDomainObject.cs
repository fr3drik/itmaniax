﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.DomainObjects
{
    public class DomainObjectTemplatePropertyDomainObject
    {
        public int DomainObjectTemplateProperties_Id { get; set; }
        public int DomainObjects_Id { get; set; }
    }
}
