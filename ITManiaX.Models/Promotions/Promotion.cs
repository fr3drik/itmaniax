﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Promotions
{
    public class Promotion
    {
        public int PromotionId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string PromotionName { get; set; }
        public int DisplayOrder { get; set; }
        public string Content { get; set; }
        public bool Active { get; set; }
    }
}
