﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Curriculum
{
  public class Curriculum
  {
    public int CurriculumId { get; set; }
    public int ParentCurriculumId { get; set; }
    public string CurriculumName { get; set; }
    public string CurriculumPath { get; set; }
	public string LibraryPath { get; set; }
    public string ParentCurriculumPath { get; set; }
    public bool IsVendor { get; set; }
    public bool IsActive { get; set; }
    public bool IsFrontPage { get; set; }
    public string Description { get; set; }
    public decimal? Price { get; set; }
    public decimal? VAT { get; set; }
    public decimal? ExclVAT { get; set; }
    public IList<Curriculum> Curricula { get; set; }
    public IList<Course> Courses { get; set; }
    public IList<int> CourseIds { get; set; }
    public IList<Curriculum> BreadCrumb { get; set; }
    public double TotalExpectedDuration { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime DateUpdated { get; set; }
  }
}
