﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Curriculum
{
  public class Course
  {
    public int CourseId { get; set; }
    public string Asset_Title { get; set; }
    public string Asset_ID { get; set; }
    public string Asset_Category { get; set; }
    public string Asset_Installation_Status { get; set; }
    public string Custom_Content { get; set; }
    public string Catalog_Folder { get; set; }
    public string Catalog_Path { get; set; }
    public string Asset_Description { get; set; }
    public int Expected_Duration { get; set; }
    public string Target_Audience { get; set; }
    public string Prerequisites { get; set; }
    public string Objectives { get; set; }
    public decimal? Price { get; set; }
    public decimal? VAT { get; set; }
    public decimal? ExclVAT { get; set; }
    public bool IsActive { get; set; }
    public string Library_Path { get; set; }
    public string Library_Topic { get; set; }
    public string Asset_Type { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime DateUpdated { get; set; }
  }
}
