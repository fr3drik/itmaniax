﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Models.Curriculum
{
    public class CurriculumCourse
    {
        public int Id { get; set; }
        public int CurriculumId { get; set; }
        public string Asset_ID { get; set; }
        public string Catalog_Path { get; set; }
    }
}
