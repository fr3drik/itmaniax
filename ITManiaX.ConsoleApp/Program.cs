﻿using ITManiaX.DataAccess;
using ITManiaX.Repository.Interfaces;
using ITManiaX.Repository.Repositories;
using ITManiaX.Service.Concrete;
using ITManiaX.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;
using ITManiaX.Models.Applications;
using ITManiaX.Models.CallsToAction;
using ITManiaX.Models.Curriculum;
using System.IO;
using System.Data.OleDb;
using Microsoft.VisualBasic.FileIO;
using System.Transactions;
using System.Data.SqlClient;
using System.Configuration;

namespace ITManiaX.ConsoleApp
{
	class Program
	{
		static void Main(string[] args)
		{
			//Library_Path,Library_Topic,Asset_Type,Asset_Title,Asset_ID,Asset_Description,Asset_Installation_Status,Custom_Content,Expected_Duration,Target_Audience
			ArchiveAssets();
			ProcessCourses();
			ProcessCurricula();
			RestoreCurricula();
			CurriculumDifferences();
			ActivateCurricula();
			ProcessCourseCurricula();
			Console.ReadKey();
		}
		static void ProcessCurricula()
		{
			try
			{
				IDbManager db = new DbManager("ITManiaX");
				var curriculumPaths = db.Connection.Query<string>("select library_path from course group by library_path").ToList();
				var existingCurricula = db.Connection.Query<Curriculum>(
				  @" select
             [CurriculumId]
            ,[ParentCurriculumId]
            ,[CurriculumName]
            ,[CurriculumPath]
            ,[ParentCurriculumPath]
            ,[IsVendor]
            ,[IsActive]
            ,[Description]
            ,[Price]
            ,[VAT]
            ,[ExclVAT]
            ,[IsFrontPage]
            ,[DateCreated]
            ,[DateUpdated]
         from [Curriculum]").ToList();
				IList<Curriculum> curriculaList = new List<Curriculum>();
				foreach (var c in curriculumPaths)
				{
					if (!string.IsNullOrEmpty(c))
					{
						var curricula = c;
						var path = c.Split('|').ToList();
						var curriculumCount = path.Count();
						var pathCounter = 0;

						foreach (var ck in path)
						{
							var list = new List<string>();
							list = path.GetRange(1, pathCounter);
							var parentList = new List<string>();
							if (pathCounter - 1 > 0)
								parentList = path.GetRange(1, pathCounter - 1);
							var cpath = JsonConvert.SerializeObject(list);
							var ppath = JsonConvert.SerializeObject(parentList);
							var libaryPath = "|" + string.Join("|", list);
							var cm = new Curriculum
							{
								CurriculumPath = cpath,
								ParentCurriculumPath = ppath,
								CurriculumName = ck,
								LibraryPath = libaryPath,
								DateCreated = DateTime.Now,
								DateUpdated = DateTime.Now
							};
							if (list.Count > 0)
							{
								if (curriculaList.Where(cl => cl.CurriculumPath == cpath).Count() == 0)
									curriculaList.Add(cm);

							}
							pathCounter++;
						}
					}
				}
				db = new DbManager("ITManiaX");
				using (var transaction = db.Connection.BeginTransaction())
				{
					Console.WriteLine("Inserting new curricula");
					foreach (var c in curriculaList)
					{
						db.Connection.Execute(
						@"insert into Curriculum
          (
            CurriculumName,
            CurriculumPath,
			LibraryPath,
            ParentCurriculumPath,
            DateCreated,
            DateUpdated
          )values(
            @curriculumName,
            @curriculumPath,
			@libraryPath,
            @parentCurriculumPath,
            @dateCreated,
            @dateUpdated);",
						new
						{
							curriculumName = c.CurriculumName,
							curriculumPath = c.CurriculumPath,
							libraryPath = c.LibraryPath,
							parentCurriculumPath = c.ParentCurriculumPath,
							dateCreated = c.DateCreated,
							dateUpdated = c.DateUpdated
						}, transaction);
					}
					transaction.Commit();
					Console.WriteLine("Inserting new curricula complete");
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}
		static void ArchiveAssets()
		{
			Console.WriteLine("Archiving Assets");
			try
			{
				IDbManager db = new DbManager("ITManiaX");
				db.Connection.Execute("ArchiveAssets", commandType: System.Data.CommandType.StoredProcedure);
				db.Dispose();
				Console.WriteLine("Archiving Assets done");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}
		static void ProcessCourses()
		{
			try
			{
				int recordCounter = 1;
				IList<Course> courses = new List<Course>();
				using (TextFieldParser parser = new TextFieldParser(ConfigurationManager.AppSettings["CurriculaFolder"] + "library-catalog-course catalog.csv"))
				{
					parser.TextFieldType = FieldType.Delimited;
					parser.SetDelimiters(",");
					while (!parser.EndOfData)
					{
						Course course = new Course();
						//Process row
						string[] fields = parser.ReadFields();
						course.Library_Path = fields[0];
						course.Library_Topic = fields[1];
						course.Asset_Type = fields[2];
						course.Asset_Title = fields[3];
						course.Asset_ID = fields[4];
						course.Asset_Description = fields[5];
						course.Asset_Installation_Status = fields[6];
						course.Custom_Content = fields[7];
						var expectedDuration = 0;
						int.TryParse(fields[8], out expectedDuration);
						course.Expected_Duration = expectedDuration;
						course.Target_Audience = fields[9];
						course.DateCreated = DateTime.Now;
						course.DateUpdated = DateTime.Now;
						courses.Add(course);
						recordCounter++;
					}
				}

				IDbManager db = new DbManager("ITManiaX");
				var currentCourses = db.Connection.Query<Course>(
						@"SELECT 
             [CourseId]
            ,[Asset_Title]
            ,[Asset_ID]
            ,[Asset_Category]
            ,[Asset_Installation_Status]
            ,[Custom_Content]
            ,[Catalog_Folder]
            ,[Catalog_Path]
            ,[Library_Path]
            ,[Library_Topic]
            ,[Asset_Description]
            ,[Expected_Duration]
            ,[Target_Audience]
            ,[Prerequisites]
            ,[Objectives]
            ,[Price]
            ,[VAT]
            ,[ExclVAT]
            ,[IsActive]
    FROM [Course]").ToList();
				Console.WriteLine("Asset count - From CSV Data Source: " + courses.Count);
				db.Dispose();
				db = new DbManager("ITManiaX");
				using (var transaction = db.Connection.BeginTransaction())
				{
					Console.WriteLine("Inserting new courses");
					foreach (var c in courses)
					{
						db.Connection.Execute(
						@"insert into Course
    (
      Library_Path,
      Library_Topic,
      Asset_Type,
      Asset_Title,
      Asset_ID,
      Asset_Description,
      Asset_Installation_Status,
      Custom_Content,
      Expected_Duration,
      Target_Audience,
      DateCreated,
      DateUpdated
    )values(
      @libraryPath,
      @libraryTopic,
      @assetType,
      @assetTitle,
      @assetId,
      @assetDescription,
      @assetInstallationStatus,
      @customContent,
      @expectedDuration,
      @targetAudience,
      @dateCreated,
      @dateUpdated);",
						new
						{
							libraryPath = c.Library_Path,
							libraryTopic = c.Library_Topic,
							assetType = c.Asset_Type,
							assetTitle = c.Asset_Title,
							assetId = c.Asset_ID,
							assetDescription = c.Asset_Description,
							assetInstallationStatus = c.Asset_Installation_Status,
							customContent = c.Custom_Content,
							expectedDuration = c.Expected_Duration,
							targetAudience = c.Target_Audience,
							dateCreated = c.DateCreated,
							dateUpdated = c.DateUpdated
						}, transaction);
					}
					transaction.Commit();
					Console.WriteLine("Inserting new courses complete");
				}
				db.Dispose();
				Console.WriteLine("Done");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}
		static void RestoreCurricula()
		{
			try
			{
				IDbManager db = new DbManager("ITManiaX");
				var archivedCurricula = db.Connection.Query<Curriculum>(
				  @"SELECT [CurriculumArchiveId]
      ,[ParentCurriculumId]
      ,[CurriculumName]
      ,[CurriculumPath]
      ,[ParentCurriculumPath]
      ,[IsVendor]
      ,[IsActive]
      ,[Description]
      ,[Price]
      ,[VAT]
      ,[ExclVAT]
      ,[IsFrontPage]
      ,[DateCreated]
      ,[DateUpdated]
  FROM [CurriculumArchive]"
				  ).ToList();
				using (var transaction = db.Connection.BeginTransaction())
				{
					Console.WriteLine("Updating Curriculum with previous values");
					foreach (var c in archivedCurricula)
					{
						db.Connection.Execute(
						@"update Curriculum set IsVendor=@isvendor, IsActive=@isactive,Price=@price,VAT=@vat,ExclVAT=@exclvat,IsFrontPage=@isfrontpage where CurriculumPath=@curriculumpath",
						new
						{
							isvendor = c.IsVendor,
							isactive = c.IsActive,
							price = c.Price,
							vat = c.VAT,
							exclvat = c.ExclVAT,
							isfrontpage = c.IsFrontPage,
							curriculumpath = c.CurriculumPath,
						}, transaction);
					}
					transaction.Commit();
					Console.WriteLine("Updating Curriculum with previous values done");
				}
				db.Dispose();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}
		static void CurriculumDifferences()
		{
			try
			{
				IDbManager db = new DbManager("ITManiaX");
				var archivedCurricula = db.Connection.Query<Curriculum>(
				  @"SELECT [CurriculumArchiveId]
      ,[ParentCurriculumId]
      ,[CurriculumName]
      ,[CurriculumPath]
      ,[ParentCurriculumPath]
      ,[IsVendor]
      ,[IsActive]
      ,[Description]
      ,[Price]
      ,[VAT]
      ,[ExclVAT]
      ,[IsFrontPage]
      ,[DateCreated]
      ,[DateUpdated]
  FROM [CurriculumArchive]"
				  ).ToList();
				var currentCurricula = db.Connection.Query<Curriculum>(
				  @"SELECT [CurriculumId]
      ,[ParentCurriculumId]
      ,[CurriculumName]
      ,[CurriculumPath]
      ,[ParentCurriculumPath]
      ,[IsVendor]
      ,[IsActive]
      ,[Description]
      ,[Price]
      ,[VAT]
      ,[ExclVAT]
      ,[IsFrontPage]
      ,[DateCreated]
      ,[DateUpdated]
  FROM [Curriculum]"
				  ).ToList();
				var curriculaIn = new List<Curriculum>();
				var curriculaOut = new List<Curriculum>();
				foreach (var c in currentCurricula)
				{
					if (archivedCurricula.Where(a => a.CurriculumPath == c.CurriculumPath).Count() == 0)
					{
						if (curriculaIn.Where(ci => ci.CurriculumPath == c.CurriculumPath).Count() == 0)
							curriculaIn.Add(c);
					}
				}
				foreach (var c in archivedCurricula)
				{
					if (currentCurricula.Where(a => a.CurriculumPath == c.CurriculumPath).Count() == 0)
					{
						if (curriculaOut.Where(ci => ci.CurriculumPath == c.CurriculumPath).Count() == 0)
							curriculaOut.Add(c);
					}
				}
				curriculaIn = curriculaIn.OrderBy(c => c.CurriculumName).ToList();
				curriculaOut = curriculaOut.OrderBy(c => c.CurriculumName).ToList();
				StringBuilder curriculaInBuilder = new StringBuilder();
				curriculaInBuilder.AppendLine("Curricula In:");
				curriculaInBuilder.AppendLine("========================");
				foreach (var c in curriculaIn)
				{
					curriculaInBuilder.AppendLine(c.CurriculumName + "," + c.CurriculumPath);
				}
				StringBuilder curriculaOutBuilder = new StringBuilder();
				curriculaOutBuilder.AppendLine("Curricula Out:");
				curriculaOutBuilder.AppendLine("========================");
				foreach (var c in curriculaOut)
				{
					curriculaOutBuilder.AppendLine(c.CurriculumName + "," + c.CurriculumPath);
				}
				File.WriteAllText(ConfigurationManager.AppSettings["CurriculaFolder"] + "curriculaIn.txt", curriculaInBuilder.ToString());
				File.WriteAllText(ConfigurationManager.AppSettings["CurriculaFolder"] + "curriculaOut.txt", curriculaOutBuilder.ToString());
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}
		static void ActivateCurricula()
		{
			IDbManager db = new DbManager("ITManiaX");
			var frontPageCurricula = db.Connection.Query<Curriculum>(
			  @"SELECT [CurriculumId]
      ,[ParentCurriculumId]
      ,[CurriculumName]
      ,[CurriculumPath]
      ,[ParentCurriculumPath]
      ,[IsVendor]
      ,[IsActive]
      ,[Description]
      ,[Price]
      ,[VAT]
      ,[ExclVAT]
      ,[IsFrontPage]
      ,[DateCreated]
      ,[DateUpdated]
  FROM [Curriculum] WHERE IsFrontPage = 1 "
			  ).ToList();
			var curricula = db.Connection.Query<Curriculum>(
		@"SELECT [CurriculumId]
      ,[ParentCurriculumId]
      ,[CurriculumName]
      ,[CurriculumPath]
      ,[ParentCurriculumPath]
      ,[IsVendor]
      ,[IsActive]
      ,[Description]
      ,[Price]
      ,[VAT]
      ,[ExclVAT]
      ,[IsFrontPage]
      ,[DateCreated]
      ,[DateUpdated]
  FROM [Curriculum]"
		).ToList();
			Console.WriteLine(frontPageCurricula.Count);
			foreach (var c in frontPageCurricula)
			{
				var parentList = JsonConvert.DeserializeObject<List<string>>(c.CurriculumPath);
				foreach (var k in curricula)
				{
					var childList = JsonConvert.DeserializeObject<List<string>>(k.CurriculumPath);
					if (childList.Count > parentList.Count)
					{
						var isChild = childList.GetRange(0, parentList.Count).SequenceEqual(parentList);
						if (isChild)
						{
							if (c.Curricula == null)
								c.Curricula = new List<Curriculum>();

							c.Curricula.Add(k);
						}
					}
				}
				Console.WriteLine(c.CurriculumName + ":" + c.Curricula.Count);
				foreach (var f in c.Curricula)
				{
					Console.WriteLine("\t" + f.CurriculumName);
				}
			}
			using (var transaction = db.Connection.BeginTransaction())
			{
				Console.WriteLine("Setting active to false for all curricula");
				foreach (var c in frontPageCurricula)
				{
					foreach (var ck in c.Curricula)
					{
						db.Connection.Execute("update curriculum set IsActive=0", transaction: transaction);
					}
				}
				transaction.Commit();
				Console.WriteLine("Setting active to false for all curricula done");
			}
			using (var transaction = db.Connection.BeginTransaction())
			{
				Console.WriteLine("Updating active curricula");
				foreach (var c in frontPageCurricula)
				{
					foreach (var ck in c.Curricula)
					{
						db.Connection.Execute("update curriculum set IsActive=1 where CurriculumId=@curriculumId", new { curriculumId = ck.CurriculumId }, transaction: transaction);
					}
					db.Connection.Execute("update curriculum set IsActive=1 where CurriculumId=@curriculumId", new { curriculumId = c.CurriculumId }, transaction: transaction);
				}
				transaction.Commit();
				Console.WriteLine("Updating active curricula done");
			}
		}
		static void ProcessCourseCurricula()
		{
			try
			{
				Console.WriteLine("Associating courses to curriculum started");
				IDbManager db = new DbManager("ITManiaX");
				var courses = db.Connection.Query<Course>(@"SELECT [CourseId]
															  ,[Asset_Title]
															  ,[Asset_ID]
															  ,[Asset_Category]
															  ,[Asset_Type]
															  ,[Asset_Installation_Status]
															  ,[Custom_Content]
															  ,[Catalog_Folder]
															  ,[Catalog_Path]
															  ,[Library_Path]
															  ,[Library_Topic]
															  ,[Asset_Description]
															  ,[Expected_Duration]
															  ,[Target_Audience]
															  ,[Prerequisites]
															  ,[Objectives]
															  ,[Price]
															  ,[VAT]
															  ,[ExclVAT]
															  ,[IsActive]
															  ,[DateCreated]
															  ,[DateUpdated]
														  FROM Course");
				var activeCurricula = db.Connection.Query<Curriculum>(@"SELECT [CurriculumId]
																		  ,[ParentCurriculumId]
																		  ,[CurriculumName]
																		  ,[CurriculumPath]
																		  ,[LibraryPath]
																		  ,[ParentCurriculumPath]
																		  ,[IsVendor]
																		  ,[IsActive]
																		  ,[Description]
																		  ,[Price]
																		  ,[VAT]
																		  ,[ExclVAT]
																		  ,[IsFrontPage]
																		  ,[DateCreated]
																		  ,[DateUpdated]
																	  FROM Curriculum
																	  WHERE IsActive = 1").ToList();
				foreach (var c in activeCurricula)
				{
					if (courses.Where(ck => ck.Library_Path.Contains(c.LibraryPath)).Count() > 0)
					{
						c.Courses = courses.Where(ck => ck.Library_Path.Contains(c.LibraryPath)).GroupBy(cf => cf.CourseId).Select(kf => kf.First()).ToList();
					}
					Console.WriteLine($"{c.CurriculumName}-{c.Courses.Count}");
					using (var transaction = db.Connection.BeginTransaction())
					{
						Console.WriteLine($"Starting transaction to associate courses for {c.CurriculumName}");
						foreach (var course in c.Courses)
						{
							db.Connection.Execute("insert into CurriculumCourse(CurriculumId,CourseId,DateUpdated,DateCreated)values(@curriculumId,@courseId,GetDate(),GetDate())", new { curriculumId = c.CurriculumId , courseId = course.CourseId }, transaction: transaction);
						}
						transaction.Commit();
						Console.WriteLine($"Transaction complete to associate courses for {c.CurriculumName}");
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}
		public static List<List<T>> Permutations<T>(List<T> list)
		{
			List<List<T>> result = new List<List<T>>();
			if (list.Count == 1)
			{ // If only one possible permutation
				result.Add(list); // Add it and return it
				return result;
			}
			foreach (T element in list)
			{ // For each element in that list
				var remainingList = new List<T>(list);
				remainingList.Remove(element); // Get a list containing everything except of chosen element
				foreach (List<T> permutation in Permutations<T>(remainingList))
				{ // Get all possible sub-permutations
					permutation.Add(element); // Add that element
					result.Add(permutation);
				}
			}
			return result;
		}
		public class CurriculumPath
		{
			public string PathName { get; set; }
			public IList<string> PathElements { get; set; }
			public IList<string> ParentPathElements { get; set; }
			public IList<CurriculumPath> ChildElements { get; set; }
			public IList<int> Courses { get; set; }
		}
		public class SystemApplicationPromotion
		{
			public Guid SystemApplicationPromotionKey { get; set; }

			public string SystemApplicationPromotionName { get; set; }

			public int SystemApplicationPromotionOrder { get; set; }

			public string SystemApplicationPromotionContent { get; set; }

			public bool SystemApplicationPromotionActive { get; set; }
		}
		public class CallToActionTemplateTuple
		{
			public int Id { get; set; }
			public int ApplicationId { get; set; }
			public string TemplateName { get; set; }
			public DateTime DateCreated { get; set; }
			public DateTime DateUpdated { get; set; }
			public List<CallToActionField> Fields { get; set; }
			public List<CallToActionMessageSequence> Messages { get; set; }
		}
		public class CallToActionValueTuple
		{
			public int Id { get; set; }
			public int ApplicationId { get; set; }
			public string CallToActionName { get; set; }
			public DateTime DateCreated { get; set; }
			public DateTime DateUpdated { get; set; }
			public List<CallToActionFieldValue> Values { get; set; }
			public List<CallToActionMessageSequence> Messages { get; set; }
		}
		public class CallToActionField
		{
			public Guid CallToActionFieldKey { get; set; }

			public string CallToActionFieldName { get; set; }

			public string CallToActionFieldType { get; set; }

			public bool CallToActionFieldRequired { get; set; }

			public string CallToActionRegex { get; set; }

			public DomainObjectFieldValidation FieldValidation { get; set; }

			public int CallToActionFieldOrder { get; set; }

			public bool IsHtml { get; set; }

			public bool IsVisible { get; set; }

			public bool IsSelected { get; set; }
		}
		public class CallToActionMessageSequence
		{
			public Guid CallToActionMessageSequenceKey { get; set; }

			public string CallToActionMessageTitle { get; set; }

			public string CallToActionMessageContent { get; set; }

			public int CallToActionMessageOrder { get; set; }

			public string CallToActionMessageType { get; set; }

			public bool MessageTriggered { get; set; }
		}
		public class CallToAction
		{
			public int ApplicationId { get; set; }
			public int CallToActionId { get; set; }
			public string CallToActionName { get; set; }
			public IList<CallToActionFieldValue> Tuple { get; set; }
		}
		public class CallToActionFieldValue
		{
			public int CallToActionId { get; set; }
			public int CallToActionTemplate { get; set; }
			public string FieldName { get; set; }
			public string FieldValue { get; set; }
		}
		public class DomainObjectFieldValidation
		{
			public string FieldValidationName { get; set; }

			public string FieldValidatorErrorMessage { get; set; }

			public string FieldValidatorRegex { get; set; }
		}

		public class AssetLibraryPath
		{
			public string AssetId { get; set; }
			public int CourseId { get; set; }
			public string LibraryPath { get; set; }
			public string CurriculumPath { get; set; }
			public int CurriculumId { get; set; }
		}
	}
}
