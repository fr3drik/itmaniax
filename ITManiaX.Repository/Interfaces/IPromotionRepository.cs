﻿using ITManiaX.DataAccess;
using ITManiaX.Models.Promotions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Repository.Interfaces
{
    public interface IPromotionRepository : IBaseRepository
    {
        void GetPromotions();
        void SavePromotion(Promotion promotion);
        IList<Promotion> Promotions { get; }
        Promotion Promotion { get; }
    }
}
