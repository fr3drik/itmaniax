﻿using ITManiaX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Repository.Interfaces
{
    public interface IPageRepository : IBaseRepository
    {
        IList<Page> Pages { get; }
        Page Page { get; set; }
        void GetPagesByApplicationId(int applicationId);
        void GetPage(int pageId);
        void SavePage();
        void UpdatePage();
        void DeletePage();
    }
}
