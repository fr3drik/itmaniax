﻿using ITManiaX.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Repository.Interfaces
{
    public interface IUserRepository : IBaseRepository
    {
        void GetUserByUserName(string userName);
        IdentityUser User { get; }
    }
}
