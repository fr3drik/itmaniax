﻿using ITManiaX.DataAccess;
using ITManiaX.Models.Applications;
using ITManiaX.Models.DomainObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Repository.Interfaces
{
    public interface IApplicationRepository : IBaseRepository
    {
        void GetApplicationByPropertyValue(string applicationPropertyName, string applicationPropertyValue);
        Application Application { get; }
        IList<ApplicationProperty> ApplicationProperties { get; }
    }
}
