﻿using ITManiaX.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Repository.Interfaces
{
    public interface IBaseRepository
    {
        IDbManager DbManager { get; set; }
    }
}
