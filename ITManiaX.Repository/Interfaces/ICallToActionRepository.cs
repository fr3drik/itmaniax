﻿using ITManiaX.Models.CallsToAction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Repository.Interfaces
{
    public interface ICallToActionRepository : IBaseRepository
    {
        void GetCallToActionTemplatesByApplication(int applicationId);
        void GetCallToActionTemplate(int callToActionTemplateId);
        void SaveCallToAction();
        void UpdateCallToAction();
        void SaveCallToActionFields();
        void SaveCallToActionMessageSequences();
        void GetCallToAction(int callToActionId);
        void GetCallsToAction(int applicationId, int size, int page);
        void UpdateCallToActionMessageSequenceTriggered(int callToActionMessageSequenceId, bool messageTriggered);
        IList<CallToActionTemplate> CallToActionTemplates { get; }
        IList<CallToActionTemplateField> CallToActionTemplateFields { get; }
        IList<CallToActionTemplateMessageSequence> CallToActionTemplateMessageSequences { get; }
        CallToActionTemplate CallToActionTemplate { get; }
        CallToAction CallToAction { get; set; }
        IList<CallToActionField> CallToActionFields { get; set; }
        IList<CallToActionMessageSequence> CallToActionMessageSequences { get; set; }
        IList<CallToAction> CallsToAction { get; }
    }
}
