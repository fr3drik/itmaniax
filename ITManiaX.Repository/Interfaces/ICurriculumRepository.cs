﻿using ITManiaX.DataAccess;
using ITManiaX.Models.Curriculum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Repository.Interfaces
{
    public interface ICurriculumRepository : IBaseRepository
    {
        void GetCurricula();
        void GetCourses();
        void GetCurriculum(int curriculumId, string curriculumPath);
        void UpdateCurriculum();
        void UpdateCourse();
        void GetCourse(int courseId);
        void GetCurriculumCourses(int curriculumId);
        IList<Curriculum> Curricula { get; }
        IList<Course> Courses { get; }
        Curriculum Curriculum { get; set; }
        Course Course { get; set; }
    }
}
