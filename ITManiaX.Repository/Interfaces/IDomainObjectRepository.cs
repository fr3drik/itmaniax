﻿using ITManiaX.DataAccess;
using ITManiaX.Models.DomainObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Repository.Interfaces
{
    public interface IDomainObjectRepository:IBaseRepository
    {
        void GetDomainObjectByKey(string domainObjectUniqueKey);
        void GetDomainObjectByDomainObjectFieldValue(string domainObjectFieldName, string domainObjectFieldValue);        
        void GetDomainObjectsByDomainObjectType(string domainObjectTypeName);
        DomainObject DomainObject { get; }
        IList<Models.DomainObjects.DTO.DomainObjectFieldDTO> DomainObjectFieldsDTO { get; }
        IList<Models.DomainObjects.DTO.DomainObjectFieldDTO> ChildDomainObjectFieldsDTO { get; }
        IList<Models.DomainObjects.DTO.DomainObjectFieldDTO> RelatedDomainObjectFieldsDTO { get; }        
    }
}
