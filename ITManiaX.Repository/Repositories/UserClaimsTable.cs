﻿using ITManiaX.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ITManiaX.Models.Identity;

namespace ITManiaX.Repository.Repositories
{
    public class UserClaimsTable
    {
        private IDbManager _dbManager;

        public UserClaimsTable()
        {

        }
        public UserClaimsTable(IDbManager dbManager)
        {
            _dbManager = dbManager;
        }

        public ClaimsIdentity FindByUserId(string userId)
        {
            ClaimsIdentity claims = new ClaimsIdentity();
            string commandText = "SELECT * FROM AspNetUserClaims WHERE UserId = @userId";
            claims = _dbManager.Connection.Query<ClaimsIdentity>(commandText, new { UserId = userId }).FirstOrDefault();
            return claims;
        }

        public int Delete(string userId)
        {
            string commandText = "DELETE FROM AspNetUserClaims WHERE UserId = @userId";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = userId });
        }

        public int Insert(Claim userClaim, string userId)
        {
            string commandText = "INSERT INTO AspNetUserClaims (ClaimValue, ClaimType, UserId) VALUES (@value, @type, @userId)";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new { value = userClaim.Value, type = userClaim.Type, userId = userId });
        }

        public int Delete(IdentityUser user, Claim claim)
        {
            string commandText = "DELETE FROM AspNetUserClaims WHERE UserId = @userId AND @ClaimValue = @value AND ClaimType = @type";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = user.Id, value = claim.Value, type = claim.Type });
        }
    }
}
