﻿using ITManiaX.DataAccess;
using ITManiaX.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace ITManiaX.Repository.Repositories
{
    public class UserTable<TUser>
           where TUser : IdentityUser
    {
        private IDbManager _dbManager;

        public UserTable()
        {
            _dbManager = new DbManager("ITManiaX");
        }
        /// <summary>
        /// Constructor that takes a PostgreSQL database instance. 
        /// </summary>
        /// <param name="database"></param>
        public UserTable(IDbManager dbManager)
        {
            _dbManager = dbManager;
        }

        /// <summary>
        /// Gets the user's name, provided with an ID.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserName(string userId)
        {
            string commandText = "SELECT UserName FROM AspNetUsers WHERE Id = @id";

            return _dbManager.Connection.Query<string>(commandText, new { id = userId }).First();
        }

        /// <summary>
        /// Gets the user's ID, provided with a user name.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public string GetUserId(string userName)
        {
            //Due to PostgreSQL's case sensitivity, we have another column for the user name in lowercase.
            if (userName != null)
                userName = userName.ToLower();

            string commandText = "SELECT Id FROM AspNetUsers WHERE UserName = @name";

            return _dbManager.Connection.Query<string>(commandText, new { name = userName }).First();
        }

        /// <summary>
        /// Returns an TUser given the user's id.
        /// </summary>
        /// <param name="userId">The user's id.</param>
        /// <returns></returns>
        public TUser GetUserById(string userId)
        {
            string commandText = "SELECT * FROM AspNetUsers WHERE Id = @id";

            return _dbManager.Connection.Query<TUser>(commandText, new { id = userId }).First();
        }

        /// <summary>
        /// Returns a list of TUser instances given a user name.
        /// </summary>
        /// <param name="userName">User's name.</param>
        /// <returns></returns>
        public List<TUser> GetUserByName(string userName)
        {
            //Due to PostgreSQL's case sensitivity, we have another column for the user name in lowercase.
            if (userName != null)
                userName = userName.ToLower();

            List<TUser> users = new List<TUser>();
            string commandText = "SELECT * FROM AspNetUsers WHERE UserName = @name";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { "@name", userName } };

            return _dbManager.Connection.Query<TUser>(commandText, new { name = userName }).ToList();
        }

        /// <summary>
        /// Returns a list of TUser instances given a user email.
        /// </summary>
        /// <param name="email">User's email address.</param>
        /// <returns></returns>
        public List<TUser> GetUserByEmail(string email)
        {
            //Due to PostgreSQL's case sensitivity, we have another column for the user name in lowercase.
            if (email != null)
                email = email.ToLower();

            List<TUser> users = new List<TUser>();
            string commandText = "SELECT * FROM AspNetUsers WHERE Email = @email";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { "@email", email } };

            return _dbManager.Connection.Query<TUser>(commandText, parameters).ToList();
        }

        /// <summary>
        /// Returns a list of TUser instances.
        /// </summary>
        /// <returns></returns>
        public IQueryable<TUser> GetAll()
        {
            //Due to PostgreSQL's case sensitivity, we have another column for the user name in lowercase.

            List<TUser> users = new List<TUser>();
            string commandText = "SELECT * FROM AspNetUsers";

            return _dbManager.Connection.Query<TUser>(commandText, null).AsQueryable();
        }

        /// <summary>
        /// Return the user's password hash.
        /// </summary>
        /// <param name="userId">The user's id.</param>
        /// <returns></returns>
        public string GetPasswordHash(string userId)
        {
            string commandText = "SELECT PasswordHash FROM AspNetUsers WHERE Id = @id";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@id", userId);

            return _dbManager.Connection.Query<string>(commandText, new { id = userId }).First();
        }

        /// <summary>
        /// Sets the user's password hash.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        public int SetPasswordHash(string userId, string passwordHash)
        {
            string commandText = "UPDATE AspNetUsers SET PasswordHash = @pwdHash WHERE Id = @id";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new { id = userId, pwdHash = passwordHash });
        }

        /// <summary>
        /// Returns the user's security stamp.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetSecurityStamp(string userId)
        {
            string commandText = "SELECT SecurityStamp FROM AspNetUsers WHERE Id = @id";
            return _dbManager.Connection.Query<string>(commandText, new { id = userId }).First();
        }

        /// <summary>
        /// Inserts a new user in the AspNetUsers table.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Insert(TUser user)
        {
            var lowerCaseEmail = user.Email == null ? null : user.Email.ToLower();

            string commandText = @"
            INSERT INTO AspNetUsers(Id, UserName, PasswordHash, SecurityStamp, Email, 
                                        EmailConfirmed,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEnabled,AccessFailedCount)
            VALUES (@id, @name, @pwdHash, @SecStamp, @email, @emailconfirmed,@phoneNumberConfirmed,@twoFactorEnabled,@lockoutEnabled,@accessFailedCount);";


            return _dbManager.Connection.ExecuteScalar<int>(commandText, 
                new { 
                    name = user.UserName,
                    id = user.Id,
                    pwdHash = user.PasswordHash,
                    SecStamp = user.SecurityStamp,
                    email = user.Email,
                    emailConfirmed = user.EmailConfirmed,
                    phoneNumberConfirmed = user.PhoneNumberConfirmed,
                    twoFactorEnabled = user.TwoFactorAuthEnabled,
                    lockoutEnabled = user.LockoutEnabled,
                    accessFailedCount = user.AccessFailedCount
                });
        }

        /// <summary>
        /// Deletes a user from the AspNetUsers table.
        /// </summary>
        /// <param name="userId">The user's id.</param>
        /// <returns></returns>
        private int Delete(string userId)
        {
            string commandText = "DELETE FROM AspNetUsers WHERE Id = @userId";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new { id = userId });
        }

        /// <summary>
        /// Deletes a user from the AspNetUsers table.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Delete(TUser user)
        {
            return Delete(user.Id);
        }

        /// <summary>
        /// Updates a user in the AspNetUsers table.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Update(TUser user)
        {
            var lowerCaseEmail = user.Email == null ? null : user.Email.ToLower();

            string commandText = @"
                UPDATE AspNetUsers
                   SET UserName = @userName, PasswordHash = @pswHash, SecurityStamp = @secStamp, Email = @email, 
                        EmailConfirmed = @emailconfirmed, 
                        PhoneNumberConfirmed = @phoneNumberConfirmed, 
                        TwoFactorEnabled = @twoFactorEnabled,
                        LockoutEnabled = @lockoutEnabled,
                        AccessFailedCount = @accessFailedCount
                 WHERE Id = @userId;";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new {
                userName = user.UserName,
                pswHash = user.PasswordHash,
                secStamp = user.SecurityStamp,
                userId = user.Id,
                email = user.Email,
                emailConfirmed = user.EmailConfirmed,
                phoneNumberConfirmed = user.PhoneNumberConfirmed,
                twoFactorEnabled = user.TwoFactorAuthEnabled,
                lockoutEnabled = user.LockoutEnabled,
                accessFailedCount = user.AccessFailedCount
            });
        }
    }
}
