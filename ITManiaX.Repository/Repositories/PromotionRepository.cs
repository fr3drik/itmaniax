﻿using ITManiaX.DataAccess;
using ITManiaX.Models.Promotions;
using ITManiaX.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace ITManiaX.Repository.Repositories
{
    public class PromotionRepository : IBaseRepository, IPromotionRepository
    {
        public PromotionRepository()
        {

        }
        public PromotionRepository(IDbManager dbManager)
        {
            DbManager = dbManager;
        }
        public void GetPromotions()
        {
            _promotions = DbManager.Connection.Query<Promotion>("GetPromotions", commandType: CommandType.StoredProcedure).ToList();
        }
        public void SavePromotion(Promotion promotion)
        {
            _promotion = DbManager.Connection.Query<Promotion>("SavePromotion", new { DateCreated = promotion.DateCreated, DateUpdated = promotion.DateUpdated, PromotionName = promotion.PromotionName, DisplayOrder = promotion.DisplayOrder, Content = promotion.Content, Active = promotion.Active }, commandType: CommandType.StoredProcedure).First();
        }
        public IDbManager DbManager { get; set; }
        private IList<Promotion> _promotions;
        public IList<Promotion> Promotions { get { return _promotions; } }
        private Promotion _promotion;
        public Promotion Promotion
        {
            get { return _promotion; }
        }
    }
}
