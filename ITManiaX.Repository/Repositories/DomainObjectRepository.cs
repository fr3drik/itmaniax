﻿using ITManiaX.DataAccess;
using ITManiaX.Models.DomainObjects;
using ITManiaX.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace ITManiaX.Repository.Repositories
{
    public class DomainObjectRepository : IBaseRepository, IDomainObjectRepository
    {
        private DomainObject domainObject;

        private IList<Models.DomainObjects.DTO.DomainObjectFieldDTO> domainObjectFieldsDTO;
        private IList<Models.DomainObjects.DTO.DomainObjectFieldDTO> childDomainObjectFieldsDTO;
        private IList<Models.DomainObjects.DTO.DomainObjectFieldDTO> relatedDomainObjectFieldsDTO;

        public DomainObjectRepository()
        {

        }

        public DomainObjectRepository(IDbManager dbManager)
        {
            DbManager = dbManager;
        }

        public void GetDomainObjectByKey(string domainObjectUniqueKey)
        {
            Guid uniqueKey = new Guid();
            if (Guid.TryParse(domainObjectUniqueKey, out uniqueKey))
            {
                var domainObjectFieldsResult = DbManager.Connection.QueryMultiple("GetDomainObjectByKey", new { UniqueKey = uniqueKey }, commandType: CommandType.StoredProcedure);
                ProcessDomainObjectResult(domainObjectFieldsResult);
            }
        }
        public void GetDomainObjectByDomainObjectFieldValue(string domainObjectFieldName, string domainObjectFieldValue)
        {
            var domainObjectFieldsResult = DbManager.Connection.QueryMultiple("GetDomainObjectByDomainObjectFieldValue", new { DomainObjectFieldName = domainObjectFieldName, DomainObjectFieldValue = domainObjectFieldValue }, commandType: CommandType.StoredProcedure);
            ProcessDomainObjectResult(domainObjectFieldsResult);
        }
        public void GetDomainObjectsByDomainObjectType(string domainObjectTypeName)
        {
            var domainObjectFieldsResult = DbManager.Connection.QueryMultiple("GetDomainObjectsByDomainObjectTypeName", new { DomainObjectTypeName = domainObjectTypeName }, commandType: CommandType.StoredProcedure);
            relatedDomainObjectFieldsDTO = domainObjectFieldsResult.Read<Models.DomainObjects.DTO.DomainObjectFieldDTO>().ToList();
        }
        private void ProcessDomainObjectResult(Dapper.SqlMapper.GridReader domainObjectFieldsResult)
        {
            domainObjectFieldsDTO = domainObjectFieldsResult.Read<Models.DomainObjects.DTO.DomainObjectFieldDTO>().ToList();
            childDomainObjectFieldsDTO = domainObjectFieldsResult.Read<Models.DomainObjects.DTO.DomainObjectFieldDTO>().ToList();
        }

        public DomainObject DomainObject
        {
            get { return domainObject; }
        }

        public IList<Models.DomainObjects.DTO.DomainObjectFieldDTO> DomainObjectFieldsDTO { get { return domainObjectFieldsDTO; } }
        public IList<Models.DomainObjects.DTO.DomainObjectFieldDTO> ChildDomainObjectFieldsDTO { get { return childDomainObjectFieldsDTO; } }
        public IList<Models.DomainObjects.DTO.DomainObjectFieldDTO> RelatedDomainObjectFieldsDTO { get { return relatedDomainObjectFieldsDTO; } }
        public IDbManager DbManager { get; set; }
    }
}
