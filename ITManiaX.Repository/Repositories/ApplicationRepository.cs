﻿using ITManiaX.DataAccess;
using ITManiaX.Models.Applications;
using ITManiaX.Models.DomainObjects;
using ITManiaX.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace ITManiaX.Repository.Repositories
{
    public class ApplicationRepository : IBaseRepository, IApplicationRepository
    {
        public ApplicationRepository()
        {

        }
        public ApplicationRepository(IDbManager dbManager)
        {
            DbManager = dbManager;
        }
        public IDbManager DbManager { get; set; }
        public Application Application { get { return _application; } }
        private Application _application;
        public IList<ApplicationProperty> ApplicationProperties
        {
            get { return _applicationProperties; }
        }
        private IList<ApplicationProperty> _applicationProperties;
        public void GetApplicationByPropertyValue(string applicationPropertyName, string applicationPropertyValue)
        {
            var applicationResult = DbManager.Connection.QueryMultiple("GetApplicationByPropertyValue", new { ApplicationPropertyName = applicationPropertyName, ApplicationPropertyValue = applicationPropertyValue }, commandType: CommandType.StoredProcedure);
            _application = applicationResult.Read<Application>().First();
            _applicationProperties = applicationResult.Read<ApplicationProperty>().ToList();
        }
    }
}
