﻿using ITManiaX.DataAccess;
using ITManiaX.Models.Curriculum;
using ITManiaX.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace ITManiaX.Repository.Repositories
{
    public class CurriculumRepository : IBaseRepository, ICurriculumRepository
    {
        private IList<Curriculum> curricula;
        private IList<Course> courses;
        public CurriculumRepository()
        {

        }
        public CurriculumRepository(IDbManager dbManager)
        {
            DbManager = dbManager;
        }
        public void GetCurricula()
        {
            curricula = DbManager.Connection.Query<Curriculum>("GetCurricula", commandType: CommandType.StoredProcedure).ToList();
        }
        public void GetCourses()
        {
            courses = DbManager.Connection.Query<Course>("GetCurricula", commandType: CommandType.StoredProcedure).ToList();
        }
        public void GetCurriculum(int curriculumId, string curriculumPath)
        {
            var curriculumResult = DbManager.Connection.QueryMultiple("GetCurriculum", new { CurriculumId = curriculumId, CurriculumPath = curriculumPath }, commandType: CommandType.StoredProcedure);
            _curriculum = curriculumResult.Read<Curriculum>().First();
            curricula = curriculumResult.Read<Curriculum>().ToList();
            courses = curriculumResult.Read<Course>().ToList();
        }
        public void UpdateCurriculum()
        {
            var curriculumResult = DbManager.Connection.QueryMultiple("UpdateCurriculum", new { 
                CurriculumId = _curriculum.CurriculumId,
                IsVendor = _curriculum.IsVendor,
                IsActive = _curriculum.IsActive,
                IsFrontPage = _curriculum.IsFrontPage,
                Description = _curriculum.Description,
                Price = _curriculum.Price,
                VAT = _curriculum.VAT,
                ExclVAT = _curriculum.ExclVAT
            }, commandType: CommandType.StoredProcedure);
            DbManager.Connection.Close();
        }
        public void UpdateCourse()
        {
            _course = DbManager.Connection.Query<Course>("UpdateCourse", new { CourseId = _course.CourseId, Price = _course.Price, VAT = _course.VAT, ExclVAT = _course.ExclVAT, IsActive = _course.IsActive }, commandType: CommandType.StoredProcedure).First();
        }
        public void GetCourse(int courseId)
        {
            _course = DbManager.Connection.Query<Course>("GetCourse", new { CourseId = _course.CourseId }, commandType: CommandType.StoredProcedure).First();
        }
        public void GetCurriculumCourses(int curriculumId)
        {
            courses = DbManager.Connection.Query<Course>("GetCurriculumCourses", new { CurriculumId = curriculumId }, commandType: CommandType.StoredProcedure).ToList();
        }
        private Course _course;
        public Course Course { get { return _course; } set { _course = value; } }
        public Curriculum Curriculum { get { return _curriculum; } set { _curriculum = value; } }
        private Curriculum _curriculum;
        public IDbManager DbManager { get; set; }
        public IList<Curriculum> Curricula
        {
            get { return curricula; }
        }
        public IList<Course> Courses
        {
            get { return courses; }
        }
    }
}
