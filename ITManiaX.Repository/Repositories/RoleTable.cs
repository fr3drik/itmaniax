﻿using ITManiaX.DataAccess;
using ITManiaX.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace ITManiaX.Repository.Repositories
{
    public class RoleTable<TRole>
        where TRole : IdentityRole
    {
        private IDbManager _dbManager;

        public RoleTable()
        {

        }
        public RoleTable(IDbManager dbManager)
        {
            _dbManager = dbManager;
        }

        public int Delete(string roleId)
        {
            string commandText = "DELETE FROM AspNetRoles WHERE Id = @id";

            return _dbManager.Connection.Execute(commandText, new { id = roleId });
        }

        public int Insert(IdentityRole role)
        {
            string commandText = "INSERT INTO AspNetRoles (Id, Name) VALUES (@id, @name)";

            return _dbManager.Connection.Execute(commandText, new { name = role.Name, id = role.Id });
        }

        public string GetRoleName(string roleId)
        {
            string commandText = "SELECT Name FROM AspNetRoles WHERE Id = @id";

            return _dbManager.Connection.Query<string>(commandText, new { id = roleId }).First();
        }

        public string GetRoleId(string roleName)
        {
            string roleId = null;
            string commandText = "SELECT Id FROM AspNetRoles WHERE Name = @name";

            var result = _dbManager.Connection.Query<string>(commandText, new { name = roleName }).First();
            if (result != null)
            {
                return Convert.ToString(result);
            }

            return roleId;
        }

        public IdentityRole GetRoleById(string roleId)
        {
            var roleName = GetRoleName(roleId);
            IdentityRole role = null;

            if (roleName != null)
            {
                role = new IdentityRole(roleName, roleId);
            }

            return role;

        }

        public IdentityRole GetRoleByName(string roleName)
        {
            var roleId = GetRoleId(roleName);
            IdentityRole role = null;

            if (roleId != null)
            {
                role = new IdentityRole(roleName, roleId);
            }

            return role;
        }

        public int Update(IdentityRole role)
        {
            string commandText = "UPDATE AspNetRoles SET Name = @name WHERE Id = @id";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new { id = role.Id });
        }
        public IQueryable<TRole> GetAll()
        {
            List<TRole> roles = new List<TRole>();
            string commandText = "SELECT Id, Name FROM AspNetRoles";

            var rows = _dbManager.Connection.Query<TRole>(commandText, null);

            return roles.AsQueryable<TRole>();
        }
    }
}
