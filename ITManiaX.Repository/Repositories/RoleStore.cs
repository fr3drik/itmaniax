﻿using ITManiaX.DataAccess;
using ITManiaX.Models.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITManiaX.Repository.Repositories
{
    public class RoleStore<TRole> : IQueryableRoleStore<TRole>
           where TRole : IdentityRole
    {
        private RoleTable<TRole> roleTable;
        public IDbManager DbManager { get; set; }
        public IQueryable<TRole> Roles
        {
            get
            {
                return roleTable.GetAll();
            }
        }
        public RoleStore()
        {
            //new RoleStore<TRole>(new DbManager("ITManiaX"));
        }
        public RoleStore(IDbManager dbManager)
        {
            this.DbManager = dbManager;
            this.roleTable = new RoleTable<TRole>(dbManager);
        }
        public Task CreateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            roleTable.Insert(role);

            return Task.FromResult<object>(null);
        }
        public Task DeleteAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("user");
            }

            roleTable.Delete(role.Id);

            return Task.FromResult<Object>(null);
        }
        public Task<TRole> FindByIdAsync(string roleId)
        {
            TRole result = roleTable.GetRoleById(roleId) as TRole;

            return Task.FromResult<TRole>(result);
        }
        public Task<TRole> FindByNameAsync(string roleName)
        {
            TRole result = roleTable.GetRoleByName(roleName) as TRole;

            return Task.FromResult<TRole>(result);
        }
        public Task UpdateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("user");
            }

            roleTable.Update(role);

            return Task.FromResult<Object>(null);
        }
        public void Dispose()
        {
            if (DbManager.Connection != null)
            {
                DbManager.Connection.Dispose();
                DbManager = null;
            }
        }
    }

}
