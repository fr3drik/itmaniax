﻿using ITManiaX.DataAccess;
using ITManiaX.Models.Identity;
using ITManiaX.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace ITManiaX.Repository.Repositories
{
    public class UserRepository : IUserRepository
    {
        private IDbManager _dbManager;
        public UserRepository()
        {

        }
        public UserRepository(IDbManager dbManager)
        {
            _dbManager = dbManager;
        }
        public void GetUserByUserName(string userName)
        {
            user = _dbManager.Connection.Query<IdentityUser>("GetUserByUserName", new { UserName = userName }, commandType: CommandType.StoredProcedure).First();
        }
        private IdentityUser user;
        public IdentityUser User { get { return user; } }
        public IDbManager DbManager { get { return _dbManager; } set { _dbManager = value; } }
    }
}
