﻿using ITManiaX.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ITManiaX.Models.Identity;

namespace ITManiaX.Repository.Repositories
{
    public class UserRolesTable
    {
        private IDbManager _dbManager;

        public UserRolesTable()
        {

        }
        public UserRolesTable(IDbManager dbManager)
        {
            _dbManager = dbManager;
        }

        public List<string> FindByUserId(string userId)
        {
            List<string> roles = new List<string>();
            //TODO: This probably does not work, and may need testing.
            string commandText = "SELECT R.Name FROM AspNetUsers U JOIN AspNetUserRoles AR ON U.Id=AR.UserId JOIN ASPNETRoles R ON AR.RoleId = R.Id";
            commandText += " WHERE U.Id = @userId";

            return _dbManager.Connection.Query<string>(commandText, new { userId = userId }).ToList();
        }

        public int Delete(string userId)
        {
            string commandText = "DELETE FROM AspNetRoles WHERE UserId = @userId";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = userId });
        }

        public int Insert(IdentityUser user, string roleId)
        {
            string commandText = "INSERT INTO AspNetUserRoles (UserId, RoleId) VALUES (@userId, @roleId)";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = user.Id, roleId = roleId });
        }
    }
}
