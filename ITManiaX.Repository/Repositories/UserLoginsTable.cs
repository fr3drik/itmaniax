﻿using ITManiaX.DataAccess;
using ITManiaX.Models.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace ITManiaX.Repository.Repositories
{
    public class UserLoginsTable
    {
        private IDbManager _dbManager;

        public UserLoginsTable()
        {

        }
        public UserLoginsTable(IDbManager dbManager)
        {
            _dbManager = dbManager;
        }

        public int Delete(IdentityUser user, UserLoginInfo login)
        {
            string commandText = "DELETE FROM AspNetUserLogins WHERE UserId = @userId AND LoginProvider = @loginProvider AND ProviderKey = @providerKey";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = user.Id, loginProvider = login.LoginProvider, providerKey = login.ProviderKey });
        }

        public int Delete(string userId)
        {
            string commandText = "DELETE FROM AspNetUserLogins WHERE UserId = @userId";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = userId });
        }

        public int Insert(IdentityUser user, UserLoginInfo login)
        {
            string commandText = "INSERT INTO AspNetUserLogins (LoginProvider, ProviderKey, UserId) VALUES (@loginProvider, @providerKey, @userId)";

            return _dbManager.Connection.ExecuteScalar<int>(commandText, new {
                loginProvider = login.LoginProvider,
                providerKey = login.ProviderKey,
                userId = user.Id
            });
        }

        public string FindUserIdByLogin(UserLoginInfo userLogin)
        {
            string commandText = "SELECT UserId FROM AspNetUserLogins WHERE LoginProvider = @loginProvider AND ProviderKey = @providerKey";

            return _dbManager.Connection.Query<string>(commandText, new {
                loginProvider = userLogin.LoginProvider,
                providerKey = userLogin.ProviderKey
            }).First();
        }

        public List<UserLoginInfo> FindByUserId(string userId)
        {
            List<UserLoginInfo> logins = new List<UserLoginInfo>();
            string commandText = "SELECT * FROM AspNetUserLogins WHERE UserId = @userId";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { "@userId", userId } };

            return _dbManager.Connection.Query<UserLoginInfo>(commandText, new { userId = userId }).ToList();
        }
    }
}
