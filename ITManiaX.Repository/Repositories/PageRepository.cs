﻿using ITManiaX.DataAccess;
using ITManiaX.Models;
using ITManiaX.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace ITManiaX.Repository.Repositories
{
    public class PageRepository : IPageRepository
    {
        private IDbManager _dbManager;
        public IDbManager DbManager { get { return _dbManager; } set { _dbManager = value; } }
        public PageRepository()
        {

        }
        public PageRepository(IDbManager dbManager)
        {
            _dbManager = dbManager;
        }
        private Page _page;
        public Page Page { get { return _page; } set { _page = value; } }

        private IList<Page> _pages;
        public IList<Page> Pages { get { return _pages; } }
        public void SavePage()
        {
            _page = _dbManager.Connection.Query<Page>("SavePage", new { ApplicationId = _page.ApplicationId, DateCreated = _page.DateCreated, DateUpdated = _page.DateUpdated, PageTitle = _page.PageTitle, PageTitleSlug = _page.PageTitleSlug, PageContentMarkDown = _page.PageContentMarkDown, CallToActionTemplateId = _page.CallToActionTemplateId, DisplayOrder = _page.DisplayOrder }, commandType: CommandType.StoredProcedure).First();
        }
        public void UpdatePage()
        {
            var page = _dbManager.Connection.Query<Page>("UpdatePage", new { PageId = _page.Id, DateUpdated = _page.DateUpdated, ApplicationId = _page.ApplicationId, PageTitle = _page.PageTitle, PageTitleSlug = _page.PageTitleSlug, PageContentMarkDown = _page.PageContentMarkDown, CallToActionTemplateId = _page.CallToActionTemplateId, DisplayOrder = _page.DisplayOrder }, commandType: CommandType.StoredProcedure).First();
            _page = page;
        }
        public void GetPagesByApplicationId(int applicationId)
        {
            _pages = _dbManager.Connection.Query<Page>("GetPagesByApplicationId", new { ApplicationId = applicationId }, commandType: CommandType.StoredProcedure).ToList();
        }
        public void GetPage(int pageId)
        {
            _page = _dbManager.Connection.Query<Page>("GetPage", new { PageId = pageId }, commandType: CommandType.StoredProcedure).First();
        }
        public void DeletePage()
        {
            _dbManager.Connection.Execute("DeletePage", new { PageId = _page.Id }, commandType: CommandType.StoredProcedure);
        }
    }
}
