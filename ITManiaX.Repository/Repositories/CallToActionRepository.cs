﻿using ITManiaX.DataAccess;
using ITManiaX.Models.CallsToAction;
using ITManiaX.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace ITManiaX.Repository.Repositories
{
    public class CallToActionRepository : ICallToActionRepository
    {
        private IList<CallToActionTemplate> _callToActionTemplates;
        private IList<CallToActionTemplateField> _callToActionTemplateFields;
        private CallToActionTemplate _callToActionTemplate;
        private IList<CallToActionTemplateMessageSequence> _callToActionTemplateMessageSequences;
        private CallToAction _callToAction;
        public CallToActionRepository()
        {

        }
        public CallToActionRepository(IDbManager dbManager)
        {
            DbManager = dbManager;
        }
        public void GetCallToActionTemplatesByApplication(int applicationId)
        {
            var callToActionTemplateResult = DbManager.Connection.QueryMultiple("GetCallToActionTemplatesByApplication", new { ApplicationId = applicationId }, commandType: CommandType.StoredProcedure);
            _callToActionTemplates = callToActionTemplateResult.Read<CallToActionTemplate>().ToList();
            _callToActionTemplateFields = callToActionTemplateResult.Read<CallToActionTemplateField>().ToList();
        }
        public void GetCallToActionTemplate(int callToActionTemplateId)
        {
            var callToActionTemplateResult = DbManager.Connection.QueryMultiple("GetCallToActionTemplate", new { CallToActionTemplateId = callToActionTemplateId }, commandType: CommandType.StoredProcedure);
            _callToActionTemplate = callToActionTemplateResult.Read<CallToActionTemplate>().First();
            _callToActionTemplateFields = callToActionTemplateResult.Read<CallToActionTemplateField>().ToList();
            _callToActionTemplateMessageSequences = callToActionTemplateResult.Read<CallToActionTemplateMessageSequence>().ToList();
        }
        public void SaveCallToAction()
        {
            var callToAction = DbManager.Connection.Query<CallToAction>("SaveCallToAction", new { ApplicationId = _callToAction.ApplicationId, CallToActionName = _callToAction.CallToActionName, DateCreated = _callToAction.DateCreated, DateUpdated = _callToAction.DateUpdated, PageId = _callToAction.PageId, CurriculumId = _callToAction.CurriculumId }, commandType: CommandType.StoredProcedure).First();
            _callToAction.Id = callToAction.Id;
        }
        public void SaveCallToActionFields()
        {
            foreach(var f in _callToActionFields)
            {
                DbManager.Connection.Execute("SaveCallToActionField", new { CallToActionId = f.CallToActionId, DateCreated = f.DateCreated, DateUpdated = f.DateUpdated, FieldName = f.FieldName, FieldValue = f.FieldValue }, commandType: CommandType.StoredProcedure);
            }
        }
        public void SaveCallToActionMessageSequences()
        {
            foreach(var m in _callToActionMessageSequences)
            {
                DbManager.Connection.Execute("SaveCallToActionMessageSequence", new { 
                    DateCreated = m.DateCreated,
                    DateUpdated = m.DateUpdated,
                    CallToActionId = m.CallToActionId,
                    CallToActionMessageTitle = m.CallToActionMessageTitle,
                    CallToActionMessageContent = m.CallToActionMessageContent,
                    CallToActionMessageOrder = m.CallToActionMessageOrder,
                    CallToActionMessageType = m.CallToActionMessageType,
                    MessageTriggered = m.MessageTriggered
                }, commandType: CommandType.StoredProcedure);
            }
        }
        public void GetCallToAction(int callToActionId)
        {
            var callToActionResult = DbManager.Connection.QueryMultiple("GetCallToAction", new { CallToActionId = callToActionId }, commandType: CommandType.StoredProcedure);
            _callToAction = callToActionResult.Read<CallToAction>().First();
            _callToActionFields = callToActionResult.Read<CallToActionField>().ToList();
            _callToActionMessageSequences = callToActionResult.Read<CallToActionMessageSequence>().ToList();
        }
        public void GetCallsToAction(int applicationId, int size, int page)
        {
            var callToActionResult = DbManager.Connection.QueryMultiple("GetCallsToAction", new { ApplicationId = applicationId }, commandType: CommandType.StoredProcedure);
            _callsToAction = callToActionResult.Read<CallToAction>().ToList();
            _callToActionFields = callToActionResult.Read<CallToActionField>().ToList();
            if (page == 1)
                page = 0;
            _callsToAction = _callsToAction.Skip(size * page).Take(size).ToList();
            var callsToActionIds = _callsToAction.Select(a => a.Id).Distinct();
            _callToActionFields = _callToActionFields.Where(a => callsToActionIds.Contains(a.CallToActionId)).ToList();
            foreach(var c in _callsToAction)
            {
                c.CallToActionFields = _callToActionFields.Where(t => t.CallToActionId == c.Id).ToList();
            }
        }
        public void UpdateCallToActionMessageSequenceTriggered(int callToActionMessageSequenceId, bool messageTriggered)
        {
            DbManager.Connection.Execute("[UpdateCallToActionMessageSequenceTriggered]", new { CallToActionMessageSequenceId = callToActionMessageSequenceId, MessageTriggered = messageTriggered }, commandType: CommandType.StoredProcedure);
        }
        public void UpdateCallToAction()
        {
            var callToAction = DbManager.Connection.Query<CallToAction>("UpdateCallToAction", new { CallToActionId = _callToAction.Id, DateUpdated = _callToAction.DateUpdated, FollowUpUserId = _callToAction.FollowUpUser, FollowedUp = _callToAction.FollowedUp }, commandType: CommandType.StoredProcedure).First();
        }
        public IDbManager DbManager { get; set; }
        public IList<CallToActionTemplate> CallToActionTemplates { get { return _callToActionTemplates; } }
        public IList<CallToActionTemplateField> CallToActionTemplateFields { get { return _callToActionTemplateFields; } }
        public CallToActionTemplate CallToActionTemplate { get { return _callToActionTemplate; } }
        public IList<CallToActionTemplateMessageSequence> CallToActionTemplateMessageSequences { get { return _callToActionTemplateMessageSequences; } }
        public CallToAction CallToAction
        {
            get { return _callToAction; }
            set { _callToAction = value; }
        }
        private IList<CallToActionField> _callToActionFields;
        public IList<CallToActionField> CallToActionFields
        {
            get { return _callToActionFields; }
            set { _callToActionFields = value; }
        }
        private IList<CallToActionMessageSequence> _callToActionMessageSequences;
        public IList<CallToActionMessageSequence> CallToActionMessageSequences
        {
            get { return _callToActionMessageSequences; }
            set { _callToActionMessageSequences = value; }
        }
        private IList<CallToAction> _callsToAction;
        public IList<CallToAction> CallsToAction { get { return _callsToAction; } }
    }
}
